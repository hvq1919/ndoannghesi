package net.tk.doannghesi.tools.log;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by VanQuan on 21/08/2014.
 */
public class Toaster {

    /**
     *
     * @param context
     * @param mess The message to show
     */
    public static void showToast(Context context, String mess) {
        Toast.makeText(context, mess, Toast.LENGTH_SHORT).show();
    }

}
