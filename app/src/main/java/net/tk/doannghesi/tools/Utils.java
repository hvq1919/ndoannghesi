package net.tk.doannghesi.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;

import net.tk.doannghesi.Define;
import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.retrofit.objects.Ads;
import net.tk.doannghesi.tools.encryptfile.CryptoUtils;
import net.tk.doannghesi.tools.log.Logger;

import java.io.IOException;
import java.io.InputStream;

public class Utils {

	public static String convertCoinToString(long coin) {
		if (coin < 1000L)
			return coin + "K";
		else if (1000L <= coin && coin < 1000000L) // "M"
			return calculate(coin, 100L) + "M";
		else if (1000000L <= coin && coin < 1000000000L) // "G"
			return calculate(coin, 100000L) + "G";
		else if (1000000000L <= coin && coin < 1000000000000L) // "T"
			return calculate(coin, 100000000L) + "T";
		else if (1000000000000L <= coin && coin < 1000000000000000L) // "P"
			return calculate(coin, 100000000000L) + "P";
		else if (1000000000000000L <= coin && coin < 1000000000000000000L) // "E"
			return calculate(coin, 100000000000000L) + "E";

		return "...";
	}

	public static String convertExpToString(long coin) {
		return convertExp(coin) + " Exp";
	}

	private static String convertExp(long coin) {
		if (coin < 1000L)
			return coin + "";
		else if (1000L <= coin && coin < 1000000L) // "M"
			return calculate(coin, 100L) + "K";
		else if (1000000L <= coin && coin < 1000000000L) // "G"
			return calculate(coin, 100000L) + "M";
		else if (1000000000L <= coin && coin < 1000000000000L) // "T"
			return calculate(coin, 100000000L) + "G";
		else if (1000000000000L <= coin && coin < 1000000000000000L) // "P"
			return calculate(coin, 100000000000L) + "T";
		else if (1000000000000000L <= coin && coin < 1000000000000000000L) // "E"
			return calculate(coin, 100000000000000L) + "P";

		return "...";
	}

	private static String calculate(long coin, long range) {
		int n = (int) (coin / range);
		int left = n / 10;
		int right = n % 10;
		if (right == 0)
			return left + "";
		else
			return left + "." + right;
	}

	/**
	 * Lv 1->2 : 0exp -> 100exp
	 * 
	 * Lv 2->3 : 100exp -> 400exp
	 * 
	 * Lv n -> n+1 : (n-1)* (n-1) * 100 -> n * n *100
	 */
	public static int calculateCurrentLevel(long exp) {
		int level = (int) Math.sqrt((exp / 100)) + 1;
		return level;
	}

	public static int getCurrentPecent(long exp) {
		int level = calculateCurrentLevel(exp);
		int exp_extra = (int) (exp - (level - 1) * (level - 1) * 100);
		int exp_expert = (2 * level - 1) * 100;

		Logger.d("level:" + level);
		Logger.d("exp_extra:" + exp_extra);
		Logger.d("exp_expert:" + exp_expert);
		Logger.d("percent:" + exp_extra * 360 / exp_expert);

		return exp_extra * 360 / exp_expert;

	}

	public static boolean checkInternet(final Context context, String title, String mess) {
		if (!isNetworkAvailable(context)) {
			buildDialog(context, "Setting", "No", title, mess, new OnClickDialog() {

				@Override
				public void PositiveClick() {
					context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
				}

				@Override
				public void NegativeClick() {
				}
			});
			return false;
		}
		return true;

	}

	private static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public static interface OnClickDialog {
		public void PositiveClick();

		public void NegativeClick();
	}

	public static void buildExitDialog(final Activity context){
		final Ads adsObject = SavedStore.getAdsObject();
		if (adsObject == null || !adsObject.getShowAds()) {
			Utils.buildDialog(context, "Yes", "No", "Confirm exit!", "Do you want to exit app?",
					new OnClickDialog() {

						@Override
						public void PositiveClick() {
							context.finish();
						}

						@Override
						public void NegativeClick() {
						}
					});
		} else {
			Utils.buildDialog(context, "Thoát", "Chơi", adsObject.getTitle(), adsObject.getMessage(),
					new OnClickDialog() {

						@Override
						public void PositiveClick() {
							context.finish();
						}

						@Override
						public void NegativeClick() {
							try {
								context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
										.parse(adsObject.getUrlLink())));
							} catch (Exception e) {
							}
						}
					});
		}
	}

	public static void buildDialog(final Context context, String textPositiveButton, String textNegativeButton, String title, String message,
			final OnClickDialog onClickDialog) {
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Light_NoTitleBar);
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
		builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.setMessage(message);
		if (!TextUtils.isEmpty(title))
			builder.setTitle(title);
		builder.setPositiveButton(textPositiveButton, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				onClickDialog.PositiveClick();
			}
		});
		builder.setNegativeButton(textNegativeButton, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				onClickDialog.NegativeClick();
			}
		});

		builder.show();
	}
	
	public static Bitmap getBitmapFromAsset(Context context, String filePath) {
		AssetManager assetManager = context.getAssets();

		InputStream istr;
		Bitmap bitmap = null;
		try {
			istr = assetManager.open(filePath);

			bitmap = CryptoUtils.decryptBitmap(Define.app_id/*key*/, istr);
			// bitmap = BitmapFactory.decodeStream(istr);
		} catch (IOException e) {
			// handle exception
		}

		return bitmap;
	}

	public static Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
		int width = bm.getWidth();
		int height = bm.getHeight();
		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
		// CREATE A MATRIX FOR THE MANIPULATION
		Matrix matrix = new Matrix();
		// RESIZE THE BIT MAP
		matrix.postScale(scaleWidth, scaleHeight);

		// "RECREATE" THE NEW BITMAP
		Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
		bm.recycle();
		return resizedBitmap;
	}

}
