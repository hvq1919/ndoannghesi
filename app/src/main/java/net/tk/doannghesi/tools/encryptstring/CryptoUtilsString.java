package net.tk.doannghesi.tools.encryptstring;

import org.apache.commons.codec.binary.Base64;

public class CryptoUtilsString {

	public static String encryptBase64String(String s) {
		byte[] bytesEncoded = Base64.encodeBase64(s.getBytes());
		return new String(bytesEncoded);
	}

	public static String decryptBase64String(String s) {
		// Decode data on other side, by processing encoded data
		byte[] valueDecoded = Base64.decodeBase64(s.getBytes());
		return new String(valueDecoded);
	}
}
