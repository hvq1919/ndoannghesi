package net.tk.doannghesi.tools.utils;

/**
 * Created by quanhv on 2/18/2016.
 */

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by quanhv on 2/1/2016.
 */
public class DateUtils {
    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static Date convertStringToDate(String dateString, String formatString) {
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        try {
            Date date = format.parse(dateString);
            return date;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 2018-12-28 02:20:34 will be return 28
     * @param dateString
     * @param formatString
     * @return
     */
    public static int convertStringToDays(String dateString, String formatString) {
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        try {
            Date date = format.parse(dateString);
            String day   = convertDateToString(date,"dd");
            return Integer.parseInt(day);
        } catch (Exception e) {
            return 0;
        }
    }

    public static String convertDateToString(Date date, String formatString) {
        SimpleDateFormat format = new SimpleDateFormat(formatString);
        try {
            String datetime = format.format(date);
            return datetime;
        } catch (Exception e) {
            return "";
        }
    }

}
