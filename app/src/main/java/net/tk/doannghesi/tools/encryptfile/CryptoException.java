package net.tk.doannghesi.tools.encryptfile;

public class CryptoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8792000499735485066L;

	public CryptoException() {
	}

	public CryptoException(String message, Throwable throwable) {
		super(message, throwable);
	}
}