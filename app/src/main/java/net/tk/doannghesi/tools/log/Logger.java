package net.tk.doannghesi.tools.log;

import android.util.Log;

public class Logger {
	private static final String TAG = "TAG";
	private static boolean shouldLog = true;

	public static void d(String mess) {
		if (shouldLog)
			Log.d(TAG, mess);
	}

	public static void i(String mess) {
		if (shouldLog)
			Log.i(TAG, mess);
	}

	public static void w(String mess) {
		if (shouldLog)
			Log.w(TAG, mess);
	}

	public static void v(String mess) {
		if (shouldLog)
			Log.v(TAG, mess);
	}

	public static void e(String mess) {
		if (shouldLog)
			Log.e(TAG, mess);
	}

}
