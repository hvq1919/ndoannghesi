package net.tk.doannghesi.tools;

import java.util.Collection;

/**
 * Utility class for array handling
 */
public class ArrayUtils {

    /**
     * Check if 2 arrays contain the same elements
     * @param lhs Array to check
     * @param rhs Array to check
     * @param <T> Type
     * @return true if the are the same
     */
    public static <T> boolean equals(Collection<T> lhs, Collection<T> rhs) {
        return lhs.size( ) == rhs.size( ) && lhs.containsAll(rhs)  && rhs.containsAll(lhs);
    }
}
