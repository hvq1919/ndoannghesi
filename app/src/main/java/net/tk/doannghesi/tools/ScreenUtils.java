package net.tk.doannghesi.tools;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

/**
 * Utility class for screen related tasks
 */
public class ScreenUtils {

    /**
     * Get the screen pixel density
     * @param context Context to extract info from
     * @return
     *      0.75 - for ldpi
     *      1.0 - for mdpi
     *      1.5 - for hdpi
     *      2.0 - for xhdpi
     */
    public static float getDensity(Context context){
        return context.getResources().getDisplayMetrics().density;
    }

    /**
     * Get the screen width
     * @param context Context to extract info from
     * @return Screen width in pixels
     */
    public static int getWidth(Context context){
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    /**
     * Get the screen height
     * @param context Context to extract info from
     * @return Screen height in pixels
     */
    public static int getHeight(Context context){
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    /**
     * Get the device resolution representation
     * @param context Context to extract info from
     * @return Screen resolution
     */
    public static String getResolution(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x + "x" + size.y;
    }

    /**
     * To get dimension in code from xml
     * @param context
     * @param res
     * @return
     */
    public static int getDimension(Context context, int res){
        return (int) context.getResources().getDimension(res);
    }

    /**
     * To get dimension in code from xml translate to metric
     * @param context
     * @param res
     * @return
     */
    public static int getDimensionInMetric(Context context, int res){
        return (int) (context.getResources().getDimension(res) / context.getResources().getDisplayMetrics().density);
    }
}
