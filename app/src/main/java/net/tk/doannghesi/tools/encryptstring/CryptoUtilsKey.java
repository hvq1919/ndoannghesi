package net.tk.doannghesi.tools.encryptstring;

import android.util.Log;

import net.tk.doannghesi.Define;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class CryptoUtilsKey {

	// Encrypt cach nay thi no ra / trong name nen khong luu duoc
	public static String encryptString(String s) {
		try {
			DESKeySpec keySpec = new DESKeySpec(Define.app_id.getBytes());
			SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
			SecretKey key = factory.generateSecret(keySpec);

			DesEncrypter encrypter = new DesEncrypter(key);
			String encrypted = encrypter.encrypt(s);

			Log.d("TAG", "encryption: " + encrypted);

			return encrypted;
		} catch (Exception e) {
			return "";
		}

	}

	public static String decryptString(String s) {
		try {
			DESKeySpec keySpec = new DESKeySpec(Define.app_id.getBytes());
			SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
			SecretKey key = factory.generateSecret(keySpec);

			DesEncrypter encrypter = new DesEncrypter(key);
			String decrypted = encrypter.decrypt(s);
			Log.d("TAG", "decrypted: " + decrypted);
			return decrypted;
		} catch (Exception e) {
			return "";
		}

	}
}
