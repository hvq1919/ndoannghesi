package net.tk.doannghesi.tools.utils;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteCantOpenDatabaseException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class SQLite {
	private static final String DATABASE_NAME = "";// App.APP_NAME+".db";
	private static final int DATABASE_VERSION = 4;

	public static SQLite ins = null;

	private int retainCount = 0;

	private Context context;
	// private SQLiteDatabase db;
	private OpenHelper openHelper;

	public SQLite(Context c) {
		context = c;
		retainCount = 0;
		openHelper = new OpenHelper(context);
	}

	/* Hàm mở kết nối tới database */
	public SQLiteDatabase open() throws SQLException {
		retainCount++;
		return openHelper.getWritableDatabase();
	}

	public static void init(Context c) {
		ins = new SQLite(c);
	}

	/* Hàm đóng kết nối với database */
	public void close() {
		retainCount--;
		if (retainCount <= 0)
			openHelper.close();
	}

	// ------------------------------------------------------------------------

	/** Select data (1) db.query */
	// public void getData() {
	// String[] columns = new String[]
	// {COLUMN_ID,COLUMN_ACC,COLUMN_PASSWORD,COLUMN_NAME};
	// Cursor c = db.query(TABLE_ACCOUNT, columns, null, null, null, null,
	// null);
	// /*if(c==null)
	// Log.v("Cursor", "C is NULL");*/
	// int iMK = c.getColumnIndex(COLUMN_PASSWORD);
	//
	// for(c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
	// c.getString(iMK);
	// }
	// c.close();
	// }

	/** select data (2) db.rawQuery */
	// public void getAllContacts() {
	// // Select All Query
	// String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;
	//
	// Cursor cursor = db.rawQuery(selectQuery, null);
	//
	// // looping through all rows and adding to list
	// if (cursor.moveToFirst()) {
	// do {
	// Integer.parseInt(cursor.getString(0));
	// } while (cursor.moveToNext());
	// }
	// }

	/** Updating single contact */
	// public int updateContact(Contact contact) {
	// ContentValues values = new ContentValues();
	// values.put(KEY_NAME, contact.getName());
	//
	// // updating row
	// return db.update(TABLE, values, FIELD_KEY + " = ?",
	// new String[] { String.valueOf(contact.getID()) });
	// }

	/** Deleting single contact */
	// public void delete(Contact contact) {
	// db.delete(TABLE, KEY_ID + " = ?",
	// new String[] { String.valueOf(contact.getID()) });
	// }

	// ---------------- class OpenHelper ------------------
	// private static class OpenHelper extends SQLiteOpenHelper {
	//
	// public OpenHelper(Context context) {
	// super(context, DATABASE_NAME, null, DATABASE_VERSION);
	//
	// //SQLiteDatabase.openDatabase("", null, SQLiteDatabase.OPEN_READONLY);
	// }
	//
	// @Override
	// public void onCreate(SQLiteDatabase arg0) {
	// arg0.execSQL("CREATE TABLE " + TABLE_ACCOUNT + " ("
	// + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
	// + COLUMN_ACC + " TEXT NOT NULL, "
	// + COLUMN_PASSWORD + " TEXT NOT NULL, "
	// + COLUMN_NAME + " TEXT NOT NULL);");
	// }
	//
	// @Override
	// public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
	// arg0.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
	// onCreate(arg0);
	// }
	// }

	public class OpenHelper extends SQLiteOpenHelper {

		// The Android's default system path of your application database.
		private String DB_PATH;// "/data/data/PAKAGE/databases/";

		// private SQLiteDatabase myDataBase;

		private final Context myContext;

		/**
		 * Constructor Takes and keeps a reference of the passed context in
		 * order to access to the application assets and resources.
		 * 
		 * @param context
		 */
		public OpenHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			DB_PATH = "/data/data/"
					+ context.getApplicationContext().getPackageName()
					+ "/databases/";
			this.myContext = context;
			try {
				createDataBase();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/**
		 * Creates a empty database on the system and rewrites it with your own
		 * database.
		 * */
		public void createDataBase() throws IOException {

			boolean dbExist = checkDataBase();

			if (dbExist) {
				// do nothing - database already exist
			} else {

				// By calling this method and empty database will be created
				// into the default system path
				// of your application so we are gonna be able to overwrite that
				// database with our database.
				this.getReadableDatabase();

				try {

					copyDataBase();

				} catch (IOException e) {

					throw new Error("Error copying database");

				}
			}

		}

		/**
		 * Check if the database already exist to avoid re-copying the file each
		 * time you open the application.
		 * 
		 * @return true if it exists, false if it doesn't
		 */
		private boolean checkDataBase() throws SQLiteCantOpenDatabaseException {

			SQLiteDatabase checkDB = null;

			try {
				String myPath = DB_PATH + DATABASE_NAME;
				checkDB = SQLiteDatabase.openDatabase(myPath, null,
						SQLiteDatabase.OPEN_READONLY);

			} catch (SQLiteException e) {
				// database does't exist yet.
			}

			if (checkDB != null) {

				checkDB.close();

			}

			return checkDB != null ? true : false;
		}

		/**
		 * Copies your database from your local assets-folder to the just
		 * created empty database in the system folder, from where it can be
		 * accessed and handled. This is done by transfering bytestream.
		 * */
		private void copyDataBase() throws IOException {

			// Open your local db as the input stream
			InputStream myInput = myContext.getAssets().open(DATABASE_NAME);

			// Path to the just created empty db
			String outFileName = DB_PATH + DATABASE_NAME;

			// Open the empty db as the output stream
			OutputStream myOutput = new FileOutputStream(outFileName);

			// transfer bytes from the inputfile to the outputfile
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0) {
				myOutput.write(buffer, 0, length);
			}

			// Close the streams
			myOutput.flush();
			myOutput.close();
			myInput.close();

		}

		// public void openDataBase() throws SQLException{
		//
		// //Open the database
		// String myPath = DB_PATH + DATABASE_NAME;
		// myDataBase = SQLiteDatabase.openDatabase(myPath, null,
		// SQLiteDatabase.OPEN_READONLY);
		//
		// }

		// @Override
		// public synchronized void close() {
		//
		// if(myDataBase != null)
		// myDataBase.close();
		//
		// super.close();
		//
		// }

		@Override
		public void onCreate(SQLiteDatabase db) {

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			context.deleteDatabase(DATABASE_NAME);
			try {
				createDataBase();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// Add your public helper methods to access and get content from the
		// database.
		// You could return cursors by doing "return myDataBase.query(....)" so
		// it'd be easy
		// to you to create adapters for your views.

	}
}