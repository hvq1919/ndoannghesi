package net.tk.doannghesi.tools.encryptstring;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

public class DesEncrypter {
	Cipher ecipher;

	Cipher dcipher;

	public DesEncrypter(SecretKey key) throws Exception {
		ecipher = Cipher.getInstance("DES");
		dcipher = Cipher.getInstance("DES");
		ecipher.init(Cipher.ENCRYPT_MODE, key);
		dcipher.init(Cipher.DECRYPT_MODE, key);
	}

	public String encrypt(String str) throws Exception {
		// Encode the string into bytes using utf-8
		byte[] utf8 = str.getBytes("UTF8");

		// Encrypt
		byte[] enc = ecipher.doFinal(utf8);

		//return android.util.Base64.encodeToString(enc, android.util.Base64.DEFAULT);
		
		byte[] bytesEncoded = Base64.encodeBase64(enc);
		return new String(bytesEncoded);
		
		// Encode bytes to base64 to get a string
		//return new sun.misc.BASE64Encoder().encode(enc);
	}

	public String decrypt(String str) throws Exception {
		// Decode base64 to get bytes
		//byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

		//byte[] dec = android.util.Base64.decode(str.getBytes(), android.util.Base64.DEFAULT);
		
		byte[] dec = Base64.decodeBase64(str.getBytes());
		
		
		byte[] utf8 = dcipher.doFinal(dec);

		// Decode using utf-8
		return new String(utf8, "UTF8");
	}
}
