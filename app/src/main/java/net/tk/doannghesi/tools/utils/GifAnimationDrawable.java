/**
 * Copyright (C) 2013 Orthogonal Labs, Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.tk.doannghesi.tools.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <p>
 * Creates an AnimationDrawable from a GIF image.
 * </p>
 * 
 * @author Femi Omojola <femi@hipmob.com>
 */
public class GifAnimationDrawable extends AnimationDrawable {
	private Context context;
	private boolean decoded;

	private GifDecoder mGifDecoder;

	private Bitmap mTmpBitmap;

	private int height, width;

	public GifAnimationDrawable(Context context, File f) throws IOException {
		this(context, new BufferedInputStream(new FileInputStream(f), 32768));
		this.context = context;
	}

	public GifAnimationDrawable(Context context, InputStream is) throws IOException {
		super();
		this.context = context;
		InputStream bis = is;
		if (!BufferedInputStream.class.isInstance(bis))
			bis = new BufferedInputStream(is, 32768);
		decoded = false;
		mGifDecoder = new GifDecoder();
		mGifDecoder.read(bis);
		mTmpBitmap = mGifDecoder.getFrame(0);
		height = mTmpBitmap.getHeight();
		width = mTmpBitmap.getWidth();
		setOneShot(mGifDecoder.getLoopCount() != 0);
		setVisible(true, true);
	}

	public boolean isDecoded() {
		return decoded;
	}

	private Runnable loader = new Runnable() {
		public void run() {
			mGifDecoder.complete();
			int i, n = mGifDecoder.getFrameCount();
			for (i = 0; i < n; i++) {
				mTmpBitmap = mGifDecoder.getFrame(i);
				addFrame(new BitmapDrawable(context.getResources(), mTmpBitmap), mGifDecoder.getDelay(i));
			}
			start();
			decoded = true;
		}
	};

	public int getMinimumHeight() {
		return height;
	}

	public int getMinimumWidth() {
		return width;
	}

	public int getIntrinsicHeight() {
		return height;
	}

	public int getIntrinsicWidth() {
		return width;
	}

	public void startGIF(boolean inline) {
		if (inline) {
			loader.run();
		} else {
			new Thread(loader).start();
		}
	}

	public void destroyGIF() {
		mGifDecoder = null;
	}
}
