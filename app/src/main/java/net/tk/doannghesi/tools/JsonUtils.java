package net.tk.doannghesi.tools;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by VanQuan on 28/08/2014.
 */
public class JsonUtils {

    /**
     * To get String ArrayList with key from Json Array
     * @param jsonArray
     * @param key
     * @return String ArrayList
     */
    public static ArrayList<String> getStringList(JSONArray jsonArray,String key){
        ArrayList<String> strings = new ArrayList<String>();
        for(int i = 0; i< jsonArray.length();i++){
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String s = jsonObject.getString(key);
                strings.add(s);
            }catch (Exception e){
            }
        }
        return strings;
    }
}
