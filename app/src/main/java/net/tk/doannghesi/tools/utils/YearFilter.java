package net.tk.doannghesi.tools.utils;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.Calendar;

public class YearFilter implements InputFilter {
	private int curYear;

	public YearFilter() {
		Calendar calendar = Calendar.getInstance();
		curYear = calendar.get(Calendar.YEAR) + 1;
	}

	@Override
	public CharSequence filter(CharSequence source, int start, int end,
			Spanned dest, int dstart, int dend) {

		try {
			int input = Integer.parseInt(dest.toString() + source.toString());
			if (curYear > input)
				return null;
		} catch (NumberFormatException nfe) {
		}
		return "";
	}

}
