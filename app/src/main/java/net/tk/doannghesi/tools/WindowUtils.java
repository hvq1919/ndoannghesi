package net.tk.doannghesi.tools;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.util.Calendar;

/**
 * Utility class for window creation
 */
public class WindowUtils {

   

    public static Dialog buildDateSelectionDialog(Context context, Calendar calendar,
                                                  DatePickerDialog.OnDateSetListener onDateSetListener){
        return new DatePickerDialog(context, onDateSetListener,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    public static Dialog buildTimeSelectionDialog(Context context, Calendar calendar,
                                                  TimePickerDialog.OnTimeSetListener onTimeSetListener){
        return new TimePickerDialog(context, onTimeSetListener, calendar.get(Calendar.HOUR_OF_DAY), 0, true);
    }

    public static Dialog buildGenericDialog(Context context, String title, String message,
                                            String negativeButtonText, DialogInterface.OnClickListener negativeButtonOnClickListener,
                                            String positiveButtonText, DialogInterface.OnClickListener positiveButtonOnClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, AlertDialog.THEME_HOLO_LIGHT);
        builder.setTitle(title)
                .setMessage(message);

        if (negativeButtonText != null) {
            builder.setNegativeButton(negativeButtonText, negativeButtonOnClickListener);
        }
        if (positiveButtonText != null) {
            builder.setPositiveButton(positiveButtonText, positiveButtonOnClickListener);
        }

        return builder.create();
    }
}
