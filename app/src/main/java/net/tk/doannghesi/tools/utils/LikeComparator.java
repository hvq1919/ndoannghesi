package net.tk.doannghesi.tools.utils;

import net.tk.doannghesi.retrofit.objects.Artist;

import java.util.Comparator;

/**
 * Created by admin on 7/9/2017.
 */

public class LikeComparator implements Comparator<Artist> {
    @Override
    public int compare(Artist lhs, Artist rhs) {
        return rhs.getTotalLike().compareTo(lhs.getTotalLike());
    }
}