package net.tk.doannghesi.tools.utils;

import android.os.Handler;
import android.os.Message;

public class CTimer extends Handler {

	public boolean enable = true;
	
	public interface OnHandlerListener {
		void onHandler();
	}
	
	private OnHandlerListener hd;
	private long delay = 1;
	private boolean isRepeat;
	
	public void setTimer(OnHandlerListener l, int delay, boolean isRepeat) {
		enable = true;
		hd = l;
		this.delay = delay;
		this.isRepeat = isRepeat;
		setDelay(delay);
	}
	
    @Override
    public void handleMessage(Message msg) {
    	hd.onHandler();
    	if(isRepeat)
    		setDelay(delay);
    }

    private void setDelay(long delayMillis) {
    	if(!enable) return;
    	this.removeMessages(0);
        sendMessageDelayed(obtainMessage(0), delayMillis);
    }
    public void start(){
    	enable = true;
    }
    public void stop(){
    	enable = false;
    }
    
    public boolean isStarting(){
    	return enable;
    }
};
