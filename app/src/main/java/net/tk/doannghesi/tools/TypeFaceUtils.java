package net.tk.doannghesi.tools;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

/**
 * Typeface utility class
 */
public class TypeFaceUtils {

	private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

	/**
	 * Get a typeface
	 * 
	 * @param context
	 *            Context to get the typeface from
	 * @param assetPath
	 *            Assets path
	 * @return Typeface
	 */
	public static Typeface get(Context context, String assetPath) {
		synchronized (cache) {
			if (!cache.containsKey(assetPath)) {
				try {
					Typeface t = Typeface.createFromAsset(context.getAssets(),
							assetPath);
					cache.put(assetPath, t);
				} catch (Exception e) {
					return null;
				}
			}
			return cache.get(assetPath);
		}
	}
}
