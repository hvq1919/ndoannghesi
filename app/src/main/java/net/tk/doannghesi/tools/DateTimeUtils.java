package net.tk.doannghesi.tools;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Date and time utility methods
 */
@SuppressLint("SimpleDateFormat")
public class DateTimeUtils {
	public static String normalDate(Calendar calendar) {
		return new SimpleDateFormat("d MMMM yyyy").format(calendar.getTime());
	}

	public static String amPmTime(Calendar calendar) {
		return new SimpleDateFormat("hh.mma").format(calendar.getTime());
	}

	public static Date dateFrom(String dateInput) {
		try {
			return new SimpleDateFormat("MM/dd/yyyy").parse(dateInput);
		} catch (ParseException e) {
			return null;
		}
	}

	public static String getFormattedTime(int secs) {
		// int secs = (int) Math.round((double) milliseconds / 1000); // for
		// millisecs arg instead of secs
		if (secs < 60)
			return secs + "s";
		else {
			int mins = (int) secs / 60;
			int remainderSecs = secs - (mins * 60);
			if (mins < 60) {
				return (mins < 10 ? "0" : "") + mins + ":" + (remainderSecs < 10 ? "0" : "") + remainderSecs + "s";
			} else {
				int hours = (int) mins / 60;
				int remainderMins = mins - (hours * 60);
				return (hours < 10 ? "0" : "") + hours + ":" + (remainderMins < 10 ? "0" : "") + remainderMins + ":"
						+ (remainderSecs < 10 ? "0" : "") + remainderSecs + "s";
			}
		}
	}

}