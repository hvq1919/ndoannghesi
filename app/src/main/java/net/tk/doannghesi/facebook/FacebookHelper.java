package net.tk.doannghesi.facebook;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.GraphJSONArrayCallback;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.Sharer.Result;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareLinkContent.Builder;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import net.tk.doannghesi.api.SavedStore;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.fragment.app.Fragment;

public class FacebookHelper {
	private static final String TAG = "TAG";
	// private static final List<String> PERMISSIONS_PUBLIC =
	// Arrays.asList("publish_actions");
	private static final List<String> PERMISSIONS_READING = Arrays.asList("public_profile", "email");

//	private static final List<String> PERMISSIONS_READING = Arrays.asList("public_profile", "user_birthday", "email",
//			"user_friends");

	// private final String PENDING_ACTION_BUNDLE_KEY = "PendingAction";
	//
	// private PendingAction pendingAction = PendingAction.NONE;
	//
	// private enum PendingAction {
	// NONE, POST_PHOTO, POST_STATUS_UPDATE
	// }

	private Activity mContext;

	private CallbackManager callbackManager;
	private OnLoginSuccess mOnLoginSuccess;
	private OnGetFriendListSuccess mOnGetFriendListSuccess;

	private AccessToken mAccessToken;
	private ShareDialog shareDialog;
	private FacebookCallback<Result> shareCallBack;
	private LoginButton mbtnLoginButton;
	private FacebookCallback<LoginResult> facebookCallBack = new FacebookCallback<LoginResult>() {
		@Override
		public void onSuccess(LoginResult result) {

			mAccessToken = result.getAccessToken();
			makeFacebookUserRequest(mAccessToken);
			// makeUserFriendListsResquest(mAccessToken);
		}

		@Override
		public void onError(FacebookException error) {
			if (mOnLoginSuccess != null)
				mOnLoginSuccess.onLoginSuccess(null);
		}

		@Override
		public void onCancel() {
			if (mOnLoginSuccess != null)
				mOnLoginSuccess.onLoginSuccess(null);
		}
	};

	// ------------------ Interfaces ------------------- //
	public interface OnGetFriendListSuccess {
		public void onGetFriendList(JSONObject jsonObject);
	}

	public interface OnLoginSuccess {
		public void onLoginSuccess(FacebookUser facebookUser);
	}

	public FacebookHelper(Activity mContext,String fb_app_id) {
		super();
		this.mContext = mContext;
		init(fb_app_id);
	}

	public FacebookHelper(Activity mContext, FacebookCallback<Result> callback,String fb_app_id) {
		super();
		this.mContext = mContext;
		shareCallBack = callback;
		init(fb_app_id);
	}

	private void init(String fb_app_id) {
		FacebookSdk.sdkInitialize(mContext.getApplicationContext());
		FacebookSdk.setApplicationId(fb_app_id);

		callbackManager = CallbackManager.Factory.create();

		mbtnLoginButton = new LoginButton(mContext);
		mbtnLoginButton.setReadPermissions(PERMISSIONS_READING);
		// mbtnLoginButton.setPublishPermissions(PERMISSIONS_PUBLIC);

		mbtnLoginButton.registerCallback(callbackManager, facebookCallBack);

		/* Login init */
		// LoginManager.getInstance().registerCallback(callbackManager,
		// facebookCallBack);

		/* Share init */
		shareDialog = new ShareDialog(mContext);
		// this part is optional
		if (shareCallBack != null)
			shareDialog.registerCallback(callbackManager, shareCallBack);

	}

	// If using in a fragment
	public void setFragment(Fragment frg) {
		mbtnLoginButton.setFragment(frg);
	}

	public void makeUserFriendListsResquest(AccessToken accessToken) {
		GraphRequest request = GraphRequest.newMyFriendsRequest(accessToken, new GraphJSONArrayCallback() {

			@Override
			public void onCompleted(JSONArray objects, GraphResponse response) {

				if (mOnGetFriendListSuccess != null)
					mOnGetFriendListSuccess.onGetFriendList(response.getJSONObject());
				SavedStore.editListFbFriends(objects.toString());
//				Log.d(TAG, "FriendListsArray:" + objects.toString());
//
//				[
//				{
//					"picture":{"data":{"url":"https:\/\/fbcdn-profile-a.akamaihd.net\/hprofile-ak-xlp1\/v\/t1.0-1\/p50x50\/12961575_954574354661233_8735585051773884726_n.jpg?oh=f6a0130def920ed4cdcdd1d3c43f0d24&oe=58352E1A&__gda__=1474876209_5aef83be8fdc5dc047cbdd986513bbae","is_silhouette":false}},"id":"847232502062086","link":"https:\/\/www.facebook.com\/app_scoped_user_id\/847232502062086\/","name":"Trần Thị Hồng Tươi"},{"picture":{"data":{"url":"https:\/\/fbcdn-profile-a.akamaihd.net\/hprofile-ak-xfa1\/v\/t1.0-1\/c69.156.487.487\/s50x50\/397778_104985799621554_113505523_n.jpg?oh=601881bcdd87ed9948469a633613d887&oe=57E9DEB4&__gda__=1476110468_ce2be52a5ee7b9a4aa88f248b44fdaf7","is_silhouette":false}},
//					"id":"825932474193546",
//					"link":"https:\/\/www.facebook.com\/app_scoped_user_id\/825932474193546\/",
//					"name":"Tình Kiếp"
//				}
//				,
//				{
//					"picture":{"data":{"url":"https:\/\/fbcdn-profile-a.akamaihd.net\/hprofile-ak-prn2\/v\/t1.0-1\/c33.33.414.414\/s50x50\/946484_1383694171843078_1545027502_n.jpg?oh=e9932e01d59f86ba9db8534ffa30d32c&oe=5828202C&__gda__=1479985721_a3d91940129e0ccd54af7fe50d1eb27b","is_silhouette":false}},
//					"id":"1664733050405854",
//					"link":"https:\/\/www.facebook.com\/app_scoped_user_id\/1664733050405854\/",
//					"name":"Huỳnh Phương Nhật Thảo"
//				}
//				]
			}
		});

		Bundle parameters = new Bundle();
		parameters.putString("fields", "id,name,link,picture");
		request.setParameters(parameters);
		request.executeAsync();
	}

	private void makeFacebookUserRequest(AccessToken accessToken) {
		GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
			@Override
			public void onCompleted(JSONObject object, GraphResponse response) {
				// Application code
				//Logger.showLogD(TAG, "User object:" + response.getJSONObject().toString());

				if (mOnLoginSuccess != null)
					mOnLoginSuccess.onLoginSuccess(parseFacebookUser(response.getJSONObject()));

			}
		});
		Bundle parameters = new Bundle();
		//parameters.putString("fields", "id,name,first_name,last_name,email,gender, birthday");
		parameters.putString("fields", "id,name,first_name,last_name,email");
		request.setParameters(parameters);
		request.executeAsync();
	}

	/**
	 * TO parse info of facebook user
	 *
	 */
	private FacebookUser parseFacebookUser(JSONObject jsonObject) {
		if (jsonObject != null) {
			return new FacebookUser(jsonObject.optString("id"), jsonObject.optString("email"),
					jsonObject.optString("name"), jsonObject.optString("last_name"),
					jsonObject.optString("first_name"), jsonObject.optString("birthday"),
					jsonObject.optString("gender"), "http://graph.facebook.com/" + jsonObject.optString("id")
							+ "/picture?type=large");
		}
		return null;
	}

	// ----------------- Public Methods ------------------------ //
	@SuppressLint("NewApi")
	private void doLogin() {
		// LoginManager.getInstance().logInWithPublishPermissions(mContext,
		// PERMISSIONS);

		// LoginManager.getInstance().logInWithReadPermissions(mContext,
		// PERMISSIONS);

		mbtnLoginButton.callOnClick();
	}

	public void doLogin(OnLoginSuccess onLoginSuccess) {
		//Logger.showLogD(TAG, "doLogin");
		mOnLoginSuccess = onLoginSuccess;
		doLogin();
	}

	public void doLogout() {
		LoginManager.getInstance().logOut();
	}

	/**
	 * To post a message to wall
	 */
	public void doSharePost() {
		if (ShareDialog.canShow(ShareLinkContent.class)) {
			Builder builder = new ShareLinkContent.Builder();
			shareDialog.show(builder.build());
		} else
			doLogin();
	}

	/**
	 * To share post with link
	 * 
	 * @param url
	 * @param contentTitle
	 * @param contentDescription
	 */
	public void doShareLink(String url, String contentTitle, String contentDescription) {
		if (ShareDialog.canShow(ShareLinkContent.class)) {
			Builder builder = new ShareLinkContent.Builder();
			if (!TextUtils.isEmpty(contentTitle))
				builder.setContentTitle(contentTitle);
			if (!TextUtils.isEmpty(contentDescription))
				builder.setContentDescription(contentDescription);
			if (!TextUtils.isEmpty(url))
				builder.setContentUrl(Uri.parse(url));

			shareDialog.show(builder.build());
		} else
			doLogin();
	}

	
	

	
	/**
	 * To share post with image
	 * 
	 * @param bm
	 */
	public void doShareImage(Bitmap bm) {
		if (ShareDialog.canShow(SharePhotoContent.class)) {
			SharePhoto sharePhoto = new SharePhoto.Builder().setBitmap(bm).build();
			ArrayList<SharePhoto> photos = new ArrayList<>();
			photos.add(sharePhoto);

			SharePhotoContent sharePhotoContent = new SharePhotoContent.Builder().setPhotos(photos).build();
			shareDialog.show(sharePhotoContent);
		} else
			doLogin();
	}

	public void setShareCallBack(FacebookCallback<Result> callback) {
		shareCallBack = callback;
	}

	public CallbackManager getCallBackManager() {
		return callbackManager;
	}

	public void setOnGetFriendListSuccess(OnGetFriendListSuccess friendListSuccess) {
		mOnGetFriendListSuccess = friendListSuccess;
	}

	public AccessToken getAccessToken() {
		if (mAccessToken != null)
			return mAccessToken;
		else
			return AccessToken.getCurrentAccessToken();
	}
}
