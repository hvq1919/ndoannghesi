package net.tk.doannghesi.retrofit.api;


import net.tk.doannghesi.retrofit.objects.Ads;
import net.tk.doannghesi.retrofit.objects.Artist;
import net.tk.doannghesi.retrofit.objects.ArtistUser;
import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.retrofit.objects.Config;
import net.tk.doannghesi.retrofit.objects.Qcs;
import net.tk.doannghesi.retrofit.objects.User;
import net.tk.doannghesi.ui.models.Questions;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by admin on 7/3/2017.
 */

public interface APIService {

    // Artist
    @POST("/api/put_artist/index.php")
    Call<BaseObject<Artist>> createArtist(@Body Artist artist);

    @POST("/api/put_list_artist/index.php")
    Call<BaseObject<Artist>> createListArtist(@Body ArrayList<Artist> artistPosts);

    @GET("/api/get_list_artist.php")
    Call<List<Artist>> getListArtist();
    @GET("/api/get_list_artist_week.php")
    Call<List<Artist>> getListArtistWeek();

    // Get ads , config
    @GET("/api/ads/get_config.php")
    Call<BaseObject<Config>> getConfig();

    @GET("/api/ads/get_ads.php")
    Call<BaseObject<Ads>> getAds();

    @GET("/api/ads/get_qcs.php")
    Call<BaseObject<List<Qcs>>> getListQcs();


    // User
    @POST("/api/insert_user.php")
    Call<BaseObject<User>> createUser(@Body User user);

    @POST("/api/update_user_vip.php")
    Call<BaseObject<User>> updateUserVip(@Body User user);

    @POST("/api/update_user_level.php")
    Call<BaseObject<User>> updateUserLevel(@Body User user);

    @POST("/api/update_user_fb.php")
    Call<BaseObject<User>> updateUserFb(@Body User user);

    @GET("/api/get_list_user.php")
    Call<BaseObject<List<User>>> getListUsers();
    @GET("/api/get_list_user_week.php")
    Call<BaseObject<List<User>>> getListUsersWeek();

    // Friends list

    /**
     * list fb id split by ,
     *
     * @param list
     * @return
     */
    @FormUrlEncoded
    @POST("/api/list_friend.php")
    Call<BaseObject<List<User>>> getListFriends(@Field("list_fb_id") String list);


    @FormUrlEncoded
    @POST("/api/like_artist.php")
    Call<BaseObject<User>> likeArtist(@Field("device_id") String device_id, @Field("artist_id") Integer artist_id);

    @FormUrlEncoded
    @POST("/api/list_user_artist.php")
    Call<BaseObject<List<ArtistUser>>> listUserArtist(@Field("artist_id") Integer artist_id);

    @FormUrlEncoded
    @POST("/api/get_question_id.php")
    Call<BaseObject<Questions>> getQuestionById(@Field("id") Integer id);

    @FormUrlEncoded
    @POST("/api/suggestion.php")
    Call<BaseObject<String>> suggestion(@Field("content") String content);

    @FormUrlEncoded
    @POST("/api/suggestion_artist.php")
    Call<BaseObject<String>> suggestionArtist(@Field("name") String name, @Field("link_fb") String link_fb, @Field("main_info") String main_info);

    @FormUrlEncoded
    @POST("/api/update_user_week.php")
    Call<BaseObject<String>> updateUserWeek(@Field("device_id") String device_id);

}