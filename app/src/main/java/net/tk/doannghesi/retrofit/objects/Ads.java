package net.tk.doannghesi.retrofit.objects;

/**
 * Created by admin on 7/8/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Ads implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("show_ads")
    @Expose
    private Boolean showAds;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("url_icon")
    @Expose
    private String urlIcon;
    @SerializedName("url_link")
    @Expose
    private String urlLink;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getShowAds() {
        return showAds;
    }

    public void setShowAds(Boolean showAds) {
        this.showAds = showAds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrlIcon() {
        return urlIcon;
    }

    public void setUrlIcon(String urlIcon) {
        this.urlIcon = urlIcon;
    }

    public String getUrlLink() {
        return urlLink;
    }

    public void setUrlLink(String urlLink) {
        this.urlLink = urlLink;
    }

}