package net.tk.doannghesi.retrofit.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 7/8/2017.
 */

public class Config {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("qcs_ver")
    @Expose
    private Integer qcsVer;
    @SerializedName("list_hacker")
    @Expose
    private String listHacker;
    @SerializedName("hienthi")
    @Expose
    private Integer hienthi;
    @SerializedName("version_code")
    @Expose
    private Integer versionCode;
    @SerializedName("show_admob")
    @Expose
    private Boolean showAdmob;
    @SerializedName("noads")
    @Expose
    private Boolean noads;

    @SerializedName("total_question")
    @Expose
    private Integer totalQuestion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQcsVer() {
        return qcsVer;
    }

    public void setQcsVer(Integer qcsVer) {
        this.qcsVer = qcsVer;
    }

    public String getListHacker() {
        return listHacker;
    }

    public void setListHacker(String listHacker) {
        this.listHacker = listHacker;
    }

    public Integer getHienthi() {
        return hienthi;
    }

    public void setHienthi(Integer hienthi) {
        this.hienthi = hienthi;
    }

    public Integer getTotalQuestion() {
        return totalQuestion;
    }

    public void setTotalQuestion(Integer totalQuestion) {
        this.totalQuestion = totalQuestion;
    }

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public Boolean getShowAdmob() {
        return showAdmob;
    }

    public void setShowAdmob(Boolean showAdmob) {
        this.showAdmob = showAdmob;
    }

    public Boolean getNoads() {
        return noads;
    }

    public void setNoads(Boolean noads) {
        this.noads = noads;
    }
}
