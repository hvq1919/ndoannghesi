package net.tk.doannghesi.retrofit.objects;

/**
 * Created by admin on 7/8/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.tk.doannghesi.retrofit.api.ApiUtils;
import net.tk.doannghesi.tools.log.Logger;

import java.io.Serializable;

public class Qcs implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("coins")
    @Expose
    private Integer coins;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("package_name")
    @Expose
    private String packageName;
    @SerializedName("url_icon")
    @Expose
    private String urlIcon;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCoins() {
        return coins;
    }

    public void setCoins(Integer coins) {
        this.coins = coins;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getUrlIcon() {
        Logger.d("Qcs Url Icon : " + ApiUtils.BASE_URL + urlIcon);
        return ApiUtils.BASE_URL + urlIcon;
    }

    public void setUrlIcon(String urlIcon) {
        this.urlIcon = urlIcon;
    }

}