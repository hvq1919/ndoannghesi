package net.tk.doannghesi.retrofit.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by admin on 7/10/2017.
 */

public class ArtistUser extends User {
    @SerializedName("num_like")
    @Expose
    private Integer numLike;

    public Integer getNumLike() {
        return numLike;
    }

    public void setNumLike(Integer numLike) {
        this.numLike = numLike;
    }
}
