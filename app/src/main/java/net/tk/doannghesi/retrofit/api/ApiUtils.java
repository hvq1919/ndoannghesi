package net.tk.doannghesi.retrofit.api;

/**
 * Created by admin on 7/3/2017.
 */

public class ApiUtils {

    private ApiUtils() {}


    //public static final String BASE_URL = "http://192.168.1.149:8888/";
    //public static final String BASE_URL = "http://10.0.2.2:8888/";
    public static final String BASE_URL = "http://doannghesi.tk/";

    public static APIService getAPIService() {

        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
