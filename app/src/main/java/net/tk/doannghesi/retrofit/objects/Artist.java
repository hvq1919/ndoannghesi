package net.tk.doannghesi.retrofit.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import net.tk.doannghesi.retrofit.api.ApiUtils;

/**
 * Created by admin on 7/4/2017.
 */

public class Artist {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("total_like")
    @Expose
    private Integer totalLike;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return ApiUtils.BASE_URL + imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getTotalLike() {
        return totalLike;
    }

    public void setTotalLike(Integer totalLike) {
        this.totalLike = totalLike;
    }
}
