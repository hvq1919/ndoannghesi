package net.tk.doannghesi.ui.fragments.thuthach.quickmoving.objects;

import android.graphics.Canvas;

public interface IParent {
	/**
	 * To draw object
	 * 
	 * @param canvas
	 * @param w
	 * @param h
	 */
	public void draw(Canvas canvas, int w, int h);

}
