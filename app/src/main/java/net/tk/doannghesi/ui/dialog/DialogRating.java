package net.tk.doannghesi.ui.dialog;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import net.tk.doannghesi.R;

public class DialogRating extends BaseFullScreenDialog {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.dialog_fragment_rating, container, false);
		ImageView imgRating = (ImageView) rootView.findViewById(R.id.imgKhickLe);
		imgRating.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogRating.this.dismiss();

				try {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=net.tk.doannghesi")));
				} catch (android.content.ActivityNotFoundException anfe) {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri
							.parse("https://play.google.com/store/apps/details?id=net.tk.doannghesi")));
				}
			}
		});

		return rootView;
	}
}
