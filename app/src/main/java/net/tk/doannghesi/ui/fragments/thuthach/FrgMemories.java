package net.tk.doannghesi.ui.fragments.thuthach;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.ui.controls.FlowLayout;
import net.tk.doannghesi.ui.dialog.DialogHintAThuThach;
import net.tk.doannghesi.ui.models.Item_THGN;
import net.tk.doannghesi.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class FrgMemories extends FrgBaseThuThach implements OnClickListener {
	private float RATE_CELL = 1 / 10f;
	private int TIME_HINT = 3000;

	private Handler mHandler = new Handler();

	private int item_found = 0;
	private int mCol = 4, mRow = 4, mNum = 4;
	private FlowLayout flowLayout;

	private int item_size;

	private ArrayList<Item_THGN> mItemsTHGN = new ArrayList<Item_THGN>();

	private Button btnTiepTuc;

	private TextView textLevel;

	private TextView textTime;

	private TextView textPass;

	private DialogHintAThuThach dialogHintAThuThach;

	private int currentLevel;

	private int TIME_RATE;

	private int mTime;

	private int mPassed;
	private boolean canPlay;

	private Runnable runnable3sHint = new Runnable() {

		@Override
		public void run() {
			for (Item_THGN item_THGN : mItemsTHGN) {
				item_THGN.getMark().setVisibility(View.VISIBLE);
			}
			canPlay = true;
		}
	};
	private Runnable runnable = new Runnable() {

		@Override
		public void run() {
			mTime -= 1;
			textTime.setText("" + mTime);
			if (mTime < 3)
				textTime.setTextColor(Color.RED);

			if (mTime <= 0) {
				flowLayout.removeAllViews();
				mHandler.removeCallbacks(this);

				// TODO show dialog Game OVer
				// Toaster.showToast(mActivity, "GAMEOVER");
				getDialogGameOver().show(mActivity.getFragmentManager(), "TAG");

				// showDialogWin(mActivity);
			} else
				mHandler.postDelayed(this, 1000);
		}
	};

	public FrgMemories() {
		// TODO Auto-generated constructor stub

		// Dua vao level hien tai de tao col vs row lun
		// K0 dung 2 constructor ben duoi
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.frg_memories, container, false);

		flowLayout = (FlowLayout) mRootView.findViewById(R.id.flowLayout);
		btnTiepTuc = (Button) mRootView.findViewById(R.id.btnTiepTuc);
		textLevel = (TextView) mRootView.findViewById(R.id.textLevel);
		textTime = (TextView) mRootView.findViewById(R.id.textTime);
		textPass = (TextView) mRootView.findViewById(R.id.textVuotQua);

		btnTiepTuc.setOnClickListener(this);

		// TODO test, se tao 1 dialog o lop base thu thach , bat dau game se goi
		// ham reset game nay
		// resetGame();
		dialogHintAThuThach = DialogHintAThuThach.newInstance(mResources.getString(R.string.ten_thuthach_memories),
				mResources.getString(R.string.note_memories), new OnClickListener() {

					@Override
					public void onClick(View v) {
						resetGame();
						SavedStore.editFirstHint(true);
						dialogHintAThuThach.dismiss();
					}
				});
		dialogHintAThuThach.show(mActivity.getFragmentManager(), "TAG");

		return mRootView;
	}

	private void calculateLayouts(int col, int row) {
		flowLayout.getLayoutParams().width = deviceWidth;

		item_size = 2 + (int) (deviceWidth / (col + RATE_CELL * (col + 1)));

	}

	private void createGrid(int col, int row) {
		recycleListItem();

		int start = new Random().nextInt(200);
		int n = col * row;

		for (int i = 0; i < n; i++) {
			Bitmap bm = getBitmapFromAsset(mActivity, mActivity.getListQuestion().get(start + i).getImg_name());
			if (i < mNum)
				mItemsTHGN.add(new Item_THGN(1, bm));
			else
				mItemsTHGN.add(new Item_THGN(0, null));
		}
		Collections.shuffle(mItemsTHGN);

		for (int i = 0; i < mItemsTHGN.size(); i++) {
			final Item_THGN item_THGN = mItemsTHGN.get(i);
			final int j = i;

			View child = mActivity.getLayoutInflater().inflate(R.layout.item_thgn, null);
			// TextView tvContent = (TextView)
			// child.findViewById(R.id.tvContent);
			// tvContent.setText(objectAnswers.get(i).getText());

			final View mark = child.findViewById(R.id.img_default);
			final ImageView imgMain = (ImageView) child.findViewById(R.id.imgMain);
			if (item_THGN.getBitmap() != null) {
				imgMain.setImageBitmap(item_THGN.getBitmap());
				mark.setVisibility(View.GONE);
			}
			item_THGN.setMark(mark);
			item_THGN.setImgMain(imgMain);

			child.findViewById(R.id.item_thgn_container).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (!canPlay)
						return;
					if (item_THGN.getBitmap() != null) {
						if (!item_THGN.isFind()) {
							playSound(soundBlue);
							mark.setVisibility(View.GONE);
							item_THGN.setFind(true);
							item_found += 1;
							if (item_found == mNum) {
								doPassAScreen();
							}
						}
					} else {
						getDialogGameOver().show(mActivity.getFragmentManager(), "TAG");
						mHandler.removeCallbacksAndMessages(null);
					}
				}
			});

			FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(item_size, item_size);
			if ((i + 1) % mCol != 0)
				params.rightMargin = (int) (item_size * RATE_CELL);
			params.bottomMargin = (int) (item_size * RATE_CELL);
			if (i / mCol == 0)
				params.topMargin = (int) (item_size * RATE_CELL);

			child.setLayoutParams(params);

			flowLayout.addView(child);

		}

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		recycleListItem();

		mHandler.removeCallbacks(null);

	}

	@Override
	protected void resetGame() {
		mPassed = 1;
		item_found = 0;
		initColRow();
		createGame(mCol, mRow);

		textLevel.setText(mPassed + "/3");
		textTime.setText("" + mTime);
		textTime.setTextColor(Color.parseColor("#93D907"));

		canPlay = false;
		mHandler.postDelayed(runnable3sHint, TIME_HINT);

		mHandler.postDelayed(runnable, TIME_HINT);
		// TODO post delay timer
	}

	private void createGame(int col, int row) {
		flowLayout.removeAllViews();
		calculateLayouts(col, row);
		createGrid(col, row);

	}

	private void recycleListItem() {
		if (mItemsTHGN == null || mItemsTHGN.size() == 0)
			return;

		for (Item_THGN item_THGN : mItemsTHGN) {
			if (item_THGN.getBitmap() != null)
				item_THGN.getBitmap().recycle();
			item_THGN = null;
		}
		mItemsTHGN.clear();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnTiepTuc:
			btnTiepTuc.setVisibility(View.GONE);
			textPass.setVisibility(View.GONE);

			doNextScreen();
			break;

		default:
			break;
		}

	}

	private void initColRow() {
		// TODO Auto-generated constructor stub

		// Get current level qua savestored rui tu tinh ra col vs row lun,
		// K0 dung constructor ben duoi

			currentLevel = SavedStore.getCurrentNumberQuestion();


		if (currentLevel / 10 < 10) {
			mCol = 3; // 4 // 4
			mRow = 4; // 4 // 5
			mNum = 4; // 5 // 6
			TIME_RATE = 5;// 5 // 5
		} else if (currentLevel / 10 < 20) {
			mCol = 4; // 4 // 4
			mRow = 4; // 5 // 6
			mNum = 5; // 6 // 7
			TIME_RATE = 10; // 10 // 10
		} else if (currentLevel / 10 < 30) {
			mCol = 5; // 6 // 6
			mRow = 6; // 6 // 7
			TIME_RATE = 15; // 15 // 15
		} else if (currentLevel / 10 < 40) {
			mCol = 6; // 6 // 6
			mRow = 6; // 7 // 8
			TIME_RATE = 20; // 20 // 20
		} else {
			mCol = 7; // 8 // 8
			mRow = 8; // 8 // 9
			TIME_RATE = 25; // 25 // 25
		}

		mTime = TIME_RATE;
	}

	private void doNextScreen() {
		Log.d("TAG", "ROW:" + mRow + " _ COL:" + mCol);

		mTime = TIME_RATE;
		mNum += 1;
		if (mPassed == 2) {
			if (mCol % 2 == 1)
				mCol += 1;
			else
				mRow += 1;
		} else {
			mRow += 1;
		}

		Log.d("TAG", "ROW1:" + mRow + " _ COL1:" + mCol);

		item_found = 0;
		createGame(mCol, mRow);
		textLevel.setText(mPassed + "/3");
		textTime.setText("" + mTime);
		textTime.setTextColor(Color.parseColor("#93D907"));

		canPlay = false;
		mHandler.postDelayed(runnable3sHint, TIME_HINT);

		mHandler.postDelayed(runnable, TIME_HINT);
	}

	private void doPassAScreen() {
		flowLayout.removeAllViews();
		mHandler.removeCallbacksAndMessages(null);
		mPassed += 1;
		if (mPassed > 3) { // TODO show dialog win thu thach
			// Toaster.showToast(mActivity, "VUOT qua THU THACH");

			// TODO tinh coins se cong
			showDialogWin(mActivity);
		} else {
			btnTiepTuc.setVisibility(View.VISIBLE);
			textPass.setVisibility(View.VISIBLE);
		}
	}

}
