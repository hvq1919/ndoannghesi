package net.tk.doannghesi.ui.fragments.thuthach.quickmoving.controls;
//package com.example.canvasthreadbasic.controls;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.util.AttributeSet;
//import android.util.Log;
//import android.view.SurfaceHolder;
//import android.view.SurfaceHolder.Callback;
//import android.view.SurfaceView;
//
//import com.example.canvasthreadbasic.R;
//import com.example.canvasthreadbasic.thread.MyThread;
//
//public class MySurfaceViewOld extends SurfaceView implements Callback {
//	private static final String TAG = "TAG";
//	private SurfaceHolder surfaceHolder;
//	private Bitmap bmpIcon;
//	private MyThread myThread;
//
//	int xPos = 0;
//	int yPos = 0;
//	int deltaX = 5;
//	int deltaY = 5;
//	int iconWidth;
//	int iconHeight;
//
//	public MySurfaceViewOld(Context context) {
//		super(context);
//		init();
//	}
//
//	public MySurfaceViewOld(Context context, AttributeSet attrs) {
//		super(context, attrs);
//		init();
//	}
//
//	public MySurfaceViewOld(Context context, AttributeSet attrs, int defStyle) {
//		super(context, attrs, defStyle);
//		init();
//	}
//
//	private void init() {
//		Log.d(TAG, "MySurfaceView init");
//
//		// setFocusable(true); // make sure we get key events
//
//		surfaceHolder = getHolder();
//		bmpIcon = BitmapFactory.decodeResource(getResources(),
//				R.drawable.ic_launcher);
//
//		iconWidth = bmpIcon.getWidth();
//		iconHeight = bmpIcon.getHeight();
//
//		surfaceHolder.addCallback(this);
//
//	}
//
//	public int getDuration() {
//		return myThread.getSleep();
//	}
//
//	public void setDuration(int time) {
//		myThread.setSleep(time);
//	}
//
//	public void drawSomething(Canvas canvas) {
//		// canvas.drawColor(Color.BLACK);
//		// canvas.drawBitmap(bmpIcon, getWidth() / 2, getHeight() / 2, null);
//
//		xPos += deltaX;
//		if (deltaX > 0) {
//			if (xPos >= getWidth() - iconWidth) {
//				deltaX *= -1;
//			}
//		} else {
//			if (xPos <= 0) {
//				deltaX *= -1;
//			}
//		}
//
//		yPos += deltaY;
//		if (deltaY > 0) {
//			if (yPos >= getHeight() - iconHeight) {
//				deltaY *= -1;
//			}
//		} else {
//			if (yPos <= 0) {
//				deltaY *= -1;
//			}
//		}
//
//		canvas.drawColor(Color.BLACK);
//		canvas.drawBitmap(bmpIcon, xPos, yPos, null);
//
//	}
//
//	@Override
//	public void surfaceChanged(SurfaceHolder holder, int format, int width,
//			int height) {
//		Log.d(TAG, "MySurfaceView surfaceChanged");
//
//	}
//
//	@Override
//	public void surfaceCreated(SurfaceHolder holder) {
//		Log.d(TAG, "MySurfaceView surfaceCreated");
//
//		myThread = new MyThread(this);
//		if (myThread != null)
//			myThread.surfaceCreated();
//
//	}
//
//	@Override
//	public void surfaceDestroyed(SurfaceHolder holder) {
//		Log.d(TAG, "MySurfaceView surfaceDestroyed");
//
//		if (myThread != null)
//			myThread.surfaceDestroyed();
//
//	}
//
//	public void onResume() {
//		if (myThread != null)
//			myThread.onResume();
//	}
//
//	public void onPause() {
//		if (myThread != null)
//			myThread.onPause();
//	}
//
//}
