package net.tk.doannghesi.ui.fragments.thuthach.boomdot.objects;

import android.graphics.Canvas;

public interface IParent {
	/**
	 * To draw object
	 * 
	 * @param canvas
	 * @param w
	 * @param h
	 * @param pauseGame
	 */
	public void draw(Canvas canvas, int w, int h);

}
