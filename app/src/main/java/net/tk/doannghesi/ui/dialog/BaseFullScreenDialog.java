package net.tk.doannghesi.ui.dialog;

import android.app.DialogFragment;
import android.os.Bundle;

public abstract class BaseFullScreenDialog extends DialogFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NO_FRAME, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
		setCancelable(false);
	}

}
