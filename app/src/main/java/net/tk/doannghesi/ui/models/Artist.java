package net.tk.doannghesi.ui.models;

/**
 * Created by quanhv on 2/14/2016.
 */
public class Artist {
    private String img_name;
    private String name;
    private int like;

    public Artist() {
    }


    public String getImg_name() {
        return img_name;
    }

    public String getName() {
        return name;
    }

    public int getLike() {
        return like;
    }

    public void setImg_name(String img_name) {
        this.img_name = img_name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLike(int like) {
        this.like = like;
    }
}
