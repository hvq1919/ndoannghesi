package net.tk.doannghesi.ui.fragments.thuthach.trollertap.thread;

import android.graphics.Canvas;

import net.tk.doannghesi.ui.fragments.thuthach.trollertap.controls.MySurfaceView;

public class MyThread extends Thread {

	MySurfaceView myView;
	private boolean running = false;
	private boolean isPause;

	private int sleep = 50;

	public MyThread(MySurfaceView view) {
		myView = view;
	}

	public void setRunning(boolean run) {
		running = run;
	}

	public void setSleep(int time) {
		this.sleep = time;
	}

	public int getSleep() {
		return sleep;
	}

	public void surfaceCreated() {
		setRunning(true);
		start();
	}

	public void surfaceDestroyed() {
		boolean retry = true;
		setRunning(false);
		while (retry) {
			try {
				join();
				retry = false;
			} catch (InterruptedException e) {
			}
		}
	}

	public void onResume() {
		synchronized (this) {
			isPause = false;
		}
	}

	public void onPause() {
		synchronized (this) {
			isPause = true;
		}
	}

	@Override
	public void run() {
		while (running) {
			if (!isPause) {
				Canvas canvas = myView.getHolder().lockCanvas();

				if (canvas != null) {
					synchronized (myView.getHolder()) {
						myView.drawSomething(canvas);
					}
					myView.getHolder().unlockCanvasAndPost(canvas);
				}
			}
//			try {
//				sleep(sleep);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}

		}
	}
}
