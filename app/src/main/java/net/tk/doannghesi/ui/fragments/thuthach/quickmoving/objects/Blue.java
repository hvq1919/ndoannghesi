package net.tk.doannghesi.ui.fragments.thuthach.quickmoving.objects;

import android.graphics.Canvas;
import android.graphics.Color;

public class Blue extends Parent {
	private int color = Color.parseColor("#2196F3");// Blue

	public Blue() {
		super();
		init();
	}

	public Blue(int xPos, int yPos) {
		super(xPos, yPos);
		init();
	}

	public Blue(int xPos) {
		super(xPos);
		init();
	}

	private void init() {
		setColor(color);
	}

	@Override
	public void draw(Canvas canvas, int w, int h) {
		super.draw(canvas, w, h);
	}

	// -----------------------------------------------------------//
	// ---------------------- Setter && Getter ------------------ //
	// -----------------------------------------------------------//

}
