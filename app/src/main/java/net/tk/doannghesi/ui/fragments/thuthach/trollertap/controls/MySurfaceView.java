package net.tk.doannghesi.ui.fragments.thuthach.trollertap.controls;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.Define;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.FrgTrollerTap;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.objects.CurrentMainDot;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.objects.SquareParent;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.thread.MyThread;
import net.tk.doannghesi.R;

import java.util.ArrayList;
import java.util.Random;

public class MySurfaceView extends SurfaceView implements Callback {
	private static final String TAG = "TAG";

	// paint_level_size = device's width / RATE_PAINT_LEVEL_SIZE
	private final int RATE_PAINT_LEVEL_SIZE = 6;

	// /////////////////////////////////////
	// ----- Gift --------------------
	private Paint mGiftPaint;
	private int mGiftColor = Color.parseColor("#2196F3");// Color.BLUE;
	// private int current_num_gift;
	// private float cX_Gift, cY_Gift, cy_Gift_Text;
	// private float r_gift;

	// Draw animation when va cham vs gift
	// private final int COUNT_GIFT_ANIMATION = 50;
	// int count_gift_animation = COUNT_GIFT_ANIMATION;
	// float cX_Gift_animation;
	// float cY_Gift_animation;

	// /////////////////////////////////////
	// ----- Vat can --------------------
	private int RATE_SPEED = Define.RATE_SPEED_DEFAULT;// 400;//450;// 500// 550
	private int RATE_WIDTH = Define.RATE_WIDTH_DEFAULT;// 8
	private int RATE_HEIGHT = Define.RATE_HEIGHT_DEFAULT;// 10

	private float vatcan_speed; // = SurfaceView's height / RATE_SPEED
	private float vatcan_width; // = SurfaceView's width / RATE_WIDTH
	private float vatcan_height; // = SurfaceView's height / RATE_HEIGHT

	// /////////////////////////////////////
	// ----- Main dot --------------------

	private int RATE_RADIUS = 18;

	// Get from SharedPreferences
	private int RATE_G;
	private int RATE_V0;
	private float TIME_TANG;

	// Chuyen dong Main dot
	private float G; // = SurfaceView's width / RATE_G
	private float v0;// = SurfaceView's width / RATE_V0
	private float radius; // = SurfaceView's width / RATE_RADIUS

	private int alpha = 55;
	private float t = 0;
	private float time_tang;

	private SurfaceHolder surfaceHolder;
	private MyThread myThread;

	// Setting for Main dot
	private CurrentMainDot mCurrentMainDot;
	private Paint paint;
	private int color;// = Color.parseColor("#FFEE58");// Yellow
	// private int shadow_color;// = Color.parseColor("#84FFFF");
	// private float rate_width;
	private float mainDotX;
	private float mainDotY;

	private float mX, mY;

	// ////////////////////////////////////////
	// --------Main Menu ------------
	private int main_menu_color = Color.parseColor(Define.MAIN_MENU_COLOR);
	private Paint mMainMenuPaint;

	// private int bg_color = Color.parseColor("#607D8B");
	private int bg_vatcan = Color.parseColor(Define.BG_VAT_CAN);// Color.parseColor("#C62828");

	private Paint mVatcanPaint;
	private ArrayList<SquareParent> mSquareParents;

	private boolean isToLeft;

	private int currentLevel = -1;
	// private int gift;

	private Typeface tf_dot_tappy;
	// private Typeface tf_best_score;

	private FrgTrollerTap mainFragment;

	public int view_width;
	public int view_height;

	public int mCotinueGift = 0;

	private Bitmap mTrollerBitmap;
	private Bitmap mBitmapLevel_1_10, mBitmapLevel_11_20, mBitmapLevel_21_40, mBitmapLevel_41_99, mBitmapLevel_100;
	private Paint paintImageLevel;

	private Random mRandom;

	// ////////////////////////////////////////
	// --------Game Status ------------
	public enum GAME_STATUS {
		GAME_NEW, GAME_RUNNIG, /* GAME_COFIRM, */GAME_OVER
	};

	public interface IGameStatus {
		public void onGameRunning();

		public void onGameOver();

	}

	private IGameStatus mGameStatus;

	public void setIGameStatus(IGameStatus gameStatus) {
		mGameStatus = gameStatus;
	}

	private GAME_STATUS currentStatus = GAME_STATUS.GAME_NEW;

	public MySurfaceView(Context context) {
		super(context);
		tf_dot_tappy = Typeface.createFromAsset(context.getAssets(), "dottappy.otf");
		// tf_best_score = Typeface.createFromAsset(context.getAssets(),
		// "best_score.otf");

		init();
	}

	public MySurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// mActivity = (MainActivity) context;
		tf_dot_tappy = Typeface.createFromAsset(context.getAssets(), "dottappy.otf");
		// tf_best_score = Typeface.createFromAsset(context.getAssets(),
		// "best_score.otf");

		init();
	}

	public MySurfaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// mActivity = (MainActivity) context;
		tf_dot_tappy = Typeface.createFromAsset(context.getAssets(), "dottappy.otf");
		// tf_best_score = Typeface.createFromAsset(context.getAssets(),
		// "best_score.otf");

		init();
	}

	private void init() {
		Log.d(TAG, "MySurfaceView init");

		setFocusable(true); // make sure we get key events
		surfaceHolder = getHolder();

		background_color = Color.parseColor(Define.BACKGROUND[0]);

		surfaceHolder.addCallback(this);

		// try {
		// gift = Integer.parseInt(SavedStore.getGiftNum());
		// } catch (Exception e) {
		// gift = -1;
		// }

		mCurrentMainDot = (CurrentMainDot) SavedStore.getMainDot();
		if (mCurrentMainDot == null) {
			color = Define.MAIN_COLOR_DEFAULT;
			RATE_G = Define.RATE_G_DEFAULT;
			RATE_V0 = Define.RATE_V0_DEFAULT;
			TIME_TANG = Define.TIME_TANG_DEFAULT;

			mCurrentMainDot = new CurrentMainDot(TIME_TANG, RATE_G, RATE_V0, color);
			SavedStore.saveMainDot(mCurrentMainDot);
		} else {
			color = mCurrentMainDot.getMain_color();
			RATE_G = mCurrentMainDot.getRATE_G();
			RATE_V0 = mCurrentMainDot.getRATE_V0();
			TIME_TANG = mCurrentMainDot.getTime_tang();
		}
		time_tang = TIME_TANG;

		initPaint();

		mSquareParents = new ArrayList<SquareParent>();
		mRandom = new Random();
	}

	private Bitmap getImage(int id, int width, int height) {
		Bitmap bmp = BitmapFactory.decodeResource(getResources(), id);
		Bitmap img = Bitmap.createScaledBitmap(bmp, width, height, true);
		bmp.recycle();
		return img;
	}

	private void initPaint() {
		paint = new Paint();
		paint.setColor(color);
		paint.setFlags(Paint.ANTI_ALIAS_FLAG);
		// paint.setFilterBitmap(true);// For bitmap
		// paint.setDither(true); // for bitmap

		mVatcanPaint = new Paint();
		mVatcanPaint.setColor(bg_vatcan);
		mVatcanPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

		// paint_level = new Paint();
		// paint_level.setFlags(Paint.ANTI_ALIAS_FLAG);

		paint_level_troke = new Paint();
		paint_level_troke.setFlags(Paint.ANTI_ALIAS_FLAG);
		paint_level_troke.setStyle(Style.STROKE);
		paint_level_troke.setStrokeWidth(5);
		paint_level_troke.setColor(Color.parseColor("#878787"));

		mGiftPaint = new Paint();
		mGiftPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

		mMainMenuPaint = new Paint();
		mMainMenuPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		mMainMenuPaint.setColor(main_menu_color);
		// mMainMenuPaint.setTypeface(tf_dot_tappy);
		mMainMenuPaint.setTextAlign(Paint.Align.CENTER);

		paintImageLevel = new Paint();
		paintImageLevel.setFlags(Paint.ANTI_ALIAS_FLAG);
		paintImageLevel.setAlpha(30);
	}

	private void initDrawObjects(int w, int h) {
		initRate(w, h);
		initVatCan(w, h);
		// createGift(w, h);
		// createVatCan(w, h, false);

		view_height = h;
		view_width = w;

		// shadow_color = SavedStore.getShadowColor();
		// rate_width = SavedStore.getShadowWidth();
		// paint.setShadowLayer(rate_width * radius, 0F, 0F, shadow_color);

		mTrollerBitmap = getImage(R.drawable.troll2, (int) radius * 2, (int) radius * 2);
		mBitmapLevel_1_10 = getImage(Define.level1_10[mRandom.nextInt(Define.level1_10.length)],
				(int) paint_level_size * 2, (int) paint_level_size * 2);
		mBitmapLevel_11_20 = getImage(Define.level11_20[mRandom.nextInt(Define.level11_20.length)],
				(int) paint_level_size * 2, (int) paint_level_size * 2);
		mBitmapLevel_21_40 = getImage(Define.level21_40[mRandom.nextInt(Define.level21_40.length)],
				(int) paint_level_size * 2, (int) paint_level_size * 2);
		mBitmapLevel_41_99 = getImage(Define.level41_99[mRandom.nextInt(Define.level41_99.length)],
				(int) paint_level_size * 2, (int) paint_level_size * 2);
		mBitmapLevel_100 = getImage(Define.level100[mRandom.nextInt(Define.level100.length)],
				(int) paint_level_size * 2, (int) paint_level_size * 2);

	}

	private void initRate(int w, int h) {
		paint_level_size = w / RATE_PAINT_LEVEL_SIZE;

		radius = w / RATE_RADIUS;
		G = (1f * w) / RATE_G;
		v0 = (1f * w) / RATE_V0;

		// Log.d("TAG", "color:" + color + " RATE_G:" + RATE_G + " RATE_V0:" +
		// RATE_V0 + " TIME_TANG:" + TIME_TANG);
		// paint.setShadowLayer(radius / 2, 0F, 0F, shadow_color);
	}

	private void initVatCan(int w, int h) {
		// vatcan_speed = SurfaceView's height / RATE_SPEED
		vatcan_speed = h * 1f / RATE_SPEED;
		vatcan_width = w / RATE_WIDTH; // = SurfaceView's width / RATE_WIDTH
		vatcan_height = h / RATE_HEIGHT; // = SurfaceView's height / RATE_HEIGHT
	}

	public void restartGameSaveMe(int width, int height) {
		mSquareParents.clear();
		time_tang = TIME_TANG;
		currentStatus = GAME_STATUS.GAME_NEW;

		isBitmapGet = false;
		// isReachBestScore = false;

		// reset vatcan
		resetVatCan();

		isToLeft = false;

		resetMainDot(width, height);

		background_color = Color.parseColor(Define.BACKGROUND[0]);
		// createVatCan(width, height, false);

		// createGift(width, height);

		// if (onNewLevel != null)
		// onNewLevel.onNewLevel(currentLevel);
	}
	
	public void restartGame(int width, int height) {
		mSquareParents.clear();
		time_tang = TIME_TANG;
		// count_gift_animation = COUNT_GIFT_ANIMATION;
		currentLevel = -1;
		currentStatus = GAME_STATUS.GAME_NEW;

		isBitmapGet = false;
		// isReachBestScore = false;

		// reset vatcan
		resetVatCan();

		isToLeft = false;

		resetMainDot(width, height);

		background_color = Color.parseColor(Define.BACKGROUND[0]);
		// createVatCan(width, height, false);

		// createGift(width, height);

		// if (onNewLevel != null)
		// onNewLevel.onNewLevel(currentLevel);
	}

	private void resetVatCan() {
		bg_vatcan = Color.parseColor(Define.BG_VAT_CAN);
		RATE_HEIGHT = Define.RATE_HEIGHT_DEFAULT;
		RATE_WIDTH = Define.RATE_WIDTH_DEFAULT;
		RATE_SPEED = Define.RATE_SPEED_DEFAULT;

		mVatcanPaint.setColor(bg_vatcan);
		if (view_height > 0 && view_width > 0)
			initVatCan(view_width, view_height);
	}

	private void resetMainDot(int width, int height) {
		t = 0;
		mainDotX = width / 2;
		mainDotY = height / 2;
	}

	private int bg_count = 0;
	private int background_color;

	private boolean isReachBestScore;

	private void startNewLevel(int w, int h, boolean isLeft) {
		// time_tang += 0.01f;
		// if (time_tang > 0.5f)
		// time_tang = 0.5f;

		// Check 10 level => change background
		bg_count = (currentLevel + 1) / 5;
		bg_count = bg_count % Define.BACKGROUND.length;
		background_color = Color.parseColor(Define.BACKGROUND[bg_count]);

		currentLevel += 1;
		// if (currentLevel > best_score) {
		// best_score = currentLevel;
		// isReachBestScore = true;
		// }
		// if (onNewLevel != null)
		// onNewLevel.onNewLevel(currentLevel);

		createVatCan(w, h, isLeft);

		mainFragment.playSound(mainFragment.soundNewLevel);
	}

	private void createVatCan(int w, int h, boolean isLeft) {
		Log.d("TAG", "createVatCan:" + RATE_HEIGHT + " width:" + RATE_WIDTH);
		mSquareParents.clear();
		int num_vatcan = 2;
		if (currentLevel < 5 && currentLevel > 2)
			num_vatcan = mRandom.nextInt(2) + 2;// = [2,3]
		else if (currentLevel >= 5)
			num_vatcan = mRandom.nextInt(3) + 2;// = [2,4]
		for (int i = 0; i < num_vatcan; i++) {
			float topY = h / num_vatcan * i;
			float botY = h / num_vatcan * (i + 1);
			SquareParent squareParent = new SquareParent(mVatcanPaint, vatcan_width, vatcan_height, topY, botY, w, h,
					isLeft);
			if (currentLevel < 4)
				squareParent.setSpeed(vatcan_speed / 2);
			else if (currentLevel < 10)
				squareParent.setSpeed(vatcan_speed / 1.8f);
			else if (currentLevel < 20)
				squareParent.setSpeed(vatcan_speed / 1.6f);
			else
				squareParent.setSpeed(vatcan_speed);
			mSquareParents.add(squareParent);
		}
	}

	/**
	 * This method set event listener for drawing.
	 * 
	 * @param event
	 *            the instance of MotionEvent
	 * @return
	 */
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touch_start(ev.getX(), ev.getY());
			break;
		case MotionEvent.ACTION_MOVE:
			// touch_move(ev.getX(), ev.getY());
			break;
		case MotionEvent.ACTION_UP:
			// this.onActionUp(event);
			break;
		default:
			break;
		}
		return true;
	}

	private void touch_start(float x, float y) {
		// if (currentStatus == GAME_STATUS.GAME_NEW)
		// createVatCan(view_width, view_height, false);

		currentStatus = GAME_STATUS.GAME_RUNNIG;
		t = 0;
		mX = mainDotX;
		mY = mainDotY;
		if (isToLeft)
			left = -1.0f;
		else
			left = 1.0f;

		if (currentStatus != GAME_STATUS.GAME_NEW && mGameStatus != null)
			mGameStatus.onGameRunning();

		mainFragment.playSound(mainFragment.soundTap);
	}

	public int getDuration() {
		return myThread.getSleep();
	}

	public void setDuration(int time) {
		myThread.setSleep(time);
	}

	public void drawSomething(Canvas canvas) {
		canvas.drawColor(background_color);

		drawObject(canvas, getWidth(), getHeight());
	}

	float left = 1.0f;
	float mNewX;

	private void drawObject(Canvas canvas, int width, int height) {
		drawVatCanTopBot(canvas, width, height);

		switch (currentStatus) {
		case GAME_NEW:
			drawCircleLevel(canvas, width, height);
			// canvas.drawCircle(mainDotX, mainDotY, radius, paint);
			if (mTrollerBitmap != null)
				canvas.drawBitmap(mTrollerBitmap, mainDotX - radius, mainDotY - radius, paint);

			drawTapToJump(canvas, width, height);
			drawDotTappy(canvas, width, height);
			drawBestScore(canvas, width, height);
			// drawNumGift(canvas, width, height);
			break;
		case GAME_RUNNIG:
			// Draw level
			drawLevel(canvas, width, height);

			// Draw Gift
			// drawGifts(canvas, width, height);

			if (t > 2) {
				t += time_tang;
			} else {
				t += time_tang + 0.3;
			}

			mNewX = mX + (float) (v0 * Math.cos(Math.toRadians(alpha)) * t) * left;
			mainDotX = mNewX;

			if (!isToLeft && mainDotX > width - radius) {
				isToLeft = true;
				startNewLevel(width, height, true);
			}
			if (isToLeft && mainDotX < radius) {
				isToLeft = false;
				startNewLevel(width, height, false);
			}

			if (mNewX > width - radius) {
				mainDotX = 2 * (width - radius) - mNewX;
			}
			if (mNewX < radius) {
				mainDotX = 2 * radius - mNewX;
			}

			mainDotY = mY - (float) (v0 * Math.sin(Math.toRadians(alpha)) * t - G * t * t / 2); // (v0sinα)t-gt2/2

			// canvas.drawCircle(mainDotX, mainDotY, radius, paint);
			canvas.drawBitmap(mTrollerBitmap, mainDotX - radius, mainDotY - radius, paint);

			// Log.d("TAG", "Running");
			for (SquareParent squareParent : mSquareParents)
				squareParent.draw(canvas, width, height);

			// Draw animation when va cham vs gift
			// count_gift_animation += 1;
			// if (count_gift_animation < COUNT_GIFT_ANIMATION) {
			// mGiftPaint.setColor(mGiftColor);
			// mGiftPaint.setTextAlign(Paint.Align.CENTER);
			// mGiftPaint.setTextSize(2 * r_gift);
			// canvas.drawText("+" + getNumGiftTang(), cX_Gift_animation,
			// cY_Gift_animation - count_gift_animation,
			// mGiftPaint);
			// }
			//
			// if (checkVaChamMainVsGifts()) {
			// // Reset position of gift animation
			// count_gift_animation = 0;
			// cX_Gift_animation = cX_Gift;
			// cY_Gift_animation = cY_Gift;
			//
			// int rate = getNumGiftTang();
			// gift += rate;
			// createGift(width, height);
			// }

			if (mainDotY < (radius + vatcan_width / 2) || mainDotY > (height - radius - vatcan_width / 2)) {
				vachamed();
			}
			for (SquareParent squareParent : mSquareParents)
				if (checkVaChamCircleVsRect(squareParent.getRect())) {
					vachamed();
					break;
				}
			break;
		case GAME_OVER:
			// Draw level
			drawLevel(canvas, width, height);

			drawDotTappy(canvas, width, height);
			drawBestScore(canvas, width, height);
			// drawNumGift(canvas, width, height);

			resetMainDot(width, height);
			// canvas.drawCircle(mainDotX, mainDotY, radius, paint);
			// canvas.drawBitmap(mTrollerBitmap, mainDotX - radius, mainDotY -
			// radius, paint);

			for (SquareParent squareParent : mSquareParents) {
				squareParent.setAnimation(false);
				squareParent.draw(canvas, width, height);
			}

			// Draw bitmap to share facebook
			if (!isBitmapGet) {
				if (myBitmap != null) {
					myBitmap.recycle();
					myBitmap = null;
				}

				Bitmap bmpWithBorder = Bitmap.createBitmap(width, height, Config.RGB_565);
				Canvas c = new Canvas(bmpWithBorder);
				c.drawColor(background_color);

				drawVatCanTopBot(c, width, height);
				// Draw level
				drawLevel(c, width, height);

				drawDotTappy(c, width, height);
				drawBestScore(c, width, height);
				// drawNumGift(c, width, height);

				// resetMainDot(width, height);
				// c.drawCircle(mainDotX, mainDotY, radius, paint);

				for (SquareParent squareParent : mSquareParents) {
					squareParent.setAnimation(false);
					squareParent.draw(c, width, height);
				}

				int border = 50;
				myBitmap = Bitmap.createBitmap(width + border * 2, height + border * 2, Config.RGB_565);
				Canvas c2 = new Canvas(myBitmap);
				c2.drawColor(bg_vatcan);
				c2.drawBitmap(bmpWithBorder, border, border, null);

				bmpWithBorder.recycle();
				bmpWithBorder = null;

				isBitmapGet = true;
			}

			break;
		// case GAME_COFIRM:
		// // Draw level
		// drawLevel(canvas, width, height);
		//
		// // Draw Gift
		// drawGifts(canvas, width, height);
		//
		// resetMainDot(width, height);
		// canvas.drawCircle(mainDotX, mainDotY, radius, paint);
		//
		// for (SquareParent squareParent : mSquareParents) {
		// squareParent.setAnimation(false);
		// squareParent.draw(canvas, width, height);
		// }
		// break;
		default:
			break;
		}

	}

	private void vachamed() {
		mainFragment.playSound(mainFragment.soundGameOver);
		// if (currentLevel > 4) {
		// currentStatus = GAME_STATUS.Ga;
		// mCotinueGift = (int) (10 * Math.pow(mCotinue_constant, mCotinue));
		// mCotinue += 1;
		// } else
		if (mGameStatus != null)
			mGameStatus.onGameOver();
	}

	// private void drawNumGift(Canvas canvas, int width, int height) {
	// mGiftPaint.setColor(mGiftColor);
	// mGiftPaint.setTextSize((float) (1.4 * radius));
	// mGiftPaint.setTextAlign(Paint.Align.LEFT);
	// // mGiftPaint.setTypeface(tf_best_score);
	// float y = getYCenter(height * 9.7f / 12, mGiftPaint);
	// canvas.drawText("" + (gift + 1), width / 2, y, mGiftPaint);
	// canvas.drawCircle(width / 2 - r_gift * 2, height * 9.7f / 12, r_gift,
	// mGiftPaint);
	// }

	private void drawBestScore(Canvas canvas, int width, int height) {
		mVatcanPaint.clearShadowLayer();
		mVatcanPaint.setTextSize((float) (1.4 * radius));
		// mVatcanPaint.setTypeface(tf_best_score);
		// canvas.drawText("BEST SCORE : " + (best_score + 1), width / 2, height
		// * 10.7f / 12, mVatcanPaint);
		// canvas.drawText("GAMES PLAYED : " + (mainFragment.gamePlayed + 1),
		// width / 2, height * 22.5f / 24, mVatcanPaint);
	}

	private void drawDotTappy(Canvas canvas, int width, int height) {
		mVatcanPaint.setTextSize(3 * radius);
		mVatcanPaint.setTextAlign(Paint.Align.CENTER);
		mVatcanPaint.setTypeface(tf_dot_tappy);
		mVatcanPaint.setShadowLayer(radius / 10, radius / 12, radius / 12, Color.GRAY);
		canvas.drawText("TAP TROLL", width / 2, height / 6, mVatcanPaint);
	}

	int rate_jump_pos = 0;

	private void drawTapToJump(Canvas canvas, int width, int height) {
		mMainMenuPaint.setTextSize(radius);
		if (rate_jump_pos > radius / 3)
			isToTop = false;
		if (rate_jump_pos < (-1 * radius / 3))
			isToTop = true;

		if (isToTop)
			rate_jump_pos += 1;
		else
			rate_jump_pos -= 1;
		canvas.drawText("TAP TO JUMP", width / 2, height / 2 - paint_level_size * 3 / 8 + rate_jump_pos, mMainMenuPaint);

	}

	private void drawVatCanTopBot(Canvas canvas, int width, int height) {
		// mVatcanPaint;
		// radius/2;
		// vatcan_width/2;

		mVatcanPaint.clearShadowLayer();
		int num_vat_total = (int) (width / 2 / radius);
		if (num_vat_total % 2 == 0)
			num_vat_total += 1;
		float left = (width - (num_vat_total - 2) * 2 * radius) / 2;
		int num_vat_draw = (num_vat_total - 1) / 2;

		for (int i = 0; i < num_vat_draw; i++) {
			canvas.drawRect(left + 2 * radius * 2 * i, 0, left + 2 * radius * (2 * i + 1), vatcan_width / 2,
					mVatcanPaint);

			canvas.drawRect(left + 2 * radius * 2 * i, height - vatcan_width / 2, left + 2 * radius * (2 * i + 1),
					height, mVatcanPaint);
		}
	}

	// Level Paint
	private Paint /* paint_level, */paint_level_troke;
	private int paint_level_size;

	private void drawCircleLevel(Canvas canvas, int width, int height) {
		// paint_level.setColor(Color.LTGRAY);
		// canvas.drawCircle(width / 2, height / 2, paint_level_size,
		// paint_level);

		if (currentStatus == GAME_STATUS.GAME_OVER)
			return;

		if (currentLevel < 10 && mBitmapLevel_1_10 != null)
			canvas.drawBitmap(mBitmapLevel_1_10, width / 2 - paint_level_size, height / 2 - paint_level_size,
					paintImageLevel);
		else if (10 <= currentLevel && currentLevel < 20 && mBitmapLevel_11_20 != null)
			canvas.drawBitmap(mBitmapLevel_11_20, width / 2 - paint_level_size, height / 2 - paint_level_size,
					paintImageLevel);
		else if (20 <= currentLevel && currentLevel < 40 && mBitmapLevel_21_40 != null)
			canvas.drawBitmap(mBitmapLevel_21_40, width / 2 - paint_level_size, height / 2 - paint_level_size,
					paintImageLevel);
		else if (40 <= currentLevel && currentLevel < 100 && mBitmapLevel_41_99 != null)
			canvas.drawBitmap(mBitmapLevel_41_99, width / 2 - paint_level_size, height / 2 - paint_level_size,
					paintImageLevel);
		else if (100 <= currentLevel && mBitmapLevel_100 != null)
			canvas.drawBitmap(mBitmapLevel_100, width / 2 - paint_level_size, height / 2 - paint_level_size,
					paintImageLevel);

	}

	private void drawLevel(Canvas canvas, int width, int height) {
		drawCircleLevel(canvas, width, height);
		// paint_level.setColor(background_color);
		// paint_level.setTextAlign(Paint.Align.CENTER);
		// paint_level.setTextSize(paint_level_size);

		paint_level_troke.setTextAlign(Paint.Align.CENTER);
		paint_level_troke.setTextSize(paint_level_size);
		paint_level_troke.setStrokeWidth(paint_level_size / 15);

		int xPos = (width / 2);
		float yPos = getYCenter(height / 2, paint_level_troke);
		// int yPos = (int) ((height / 2) - ((paint_level.descent() +
		// paint_level.ascent()) / 2));

		if (currentLevel < 9) {
			// canvas.drawText("0" + (currentLevel + 1), xPos, yPos/3,
			// paint_level);
			if (currentStatus == GAME_STATUS.GAME_OVER)
				canvas.drawText("0" + (currentLevel + 1), xPos, yPos / 2, paint_level_troke);
			else
				canvas.drawText("0" + (currentLevel + 1), xPos, yPos / 3, paint_level_troke);
		} else {
			// canvas.drawText("" + (currentLevel + 1), xPos, yPos/3,
			// paint_level);
			if (currentStatus == GAME_STATUS.GAME_OVER)
				canvas.drawText("" + (currentLevel + 1), xPos, yPos / 2, paint_level_troke);
			else
				canvas.drawText("" + (currentLevel + 1), xPos, yPos / 3, paint_level_troke);

		}
	}

	int gDeltaY = 0;
	boolean isToTop;

	// private void drawGifts(Canvas canvas, int width, int height) {
	// if (gDeltaY > r_gift / 2)
	// isToTop = false;
	// if (gDeltaY < (-1 * r_gift / 2))
	// isToTop = true;
	//
	// if (isToTop)
	// gDeltaY += 1;
	// else
	// gDeltaY -= 1;
	//
	// mGiftPaint.setColor(mGiftColor);
	// canvas.drawCircle(cX_Gift, cY_Gift + gDeltaY, r_gift, mGiftPaint);
	//
	// mGiftPaint.setColor(Color.WHITE);
	// mGiftPaint.setTextAlign(Paint.Align.CENTER);
	// mGiftPaint.setTextSize(r_gift);
	//
	// int rate = getNumGiftTang();
	// if (rate > 1)
	// canvas.drawText("x" + rate, cX_Gift, cy_Gift_Text + gDeltaY, mGiftPaint);
	//
	// }
	//
	// private void createGift(int width, int height) {
	// r_gift = radius * 2 / 3;
	//
	// cX_Gift = vatcan_width + r_gift + new Random().nextInt((int) (width - 2 *
	// vatcan_width - 2 * r_gift));
	// cY_Gift = vatcan_width + r_gift + new Random().nextInt((int) (height - 2
	// * vatcan_width - 2 * r_gift));
	// cy_Gift_Text = getYCenter(cY_Gift, mGiftPaint);
	// // cY_Gift - ((mGiftPaint.descent() + mGiftPaint.ascent()) / 2);
	//
	// }
	//
	// private int getNumGiftTang() {
	// int tang = (currentLevel + 1) / 10 + 1;
	// if (tang > 5)
	// return 5;
	// else
	// return tang;
	// }

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		Log.d(TAG, "MySurfaceView surfaceChanged w:" + width + "h:" + height);
		mainDotX = width / 2;
		mainDotY = height / 2;

		initDrawObjects(width, height);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "MySurfaceView surfaceCreated");

		myThread = new MyThread(this);
		if (myThread != null)
			myThread.surfaceCreated();

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "MySurfaceView surfaceDestroyed");

		if (myThread != null)
			myThread.surfaceDestroyed();

	}

	public void onResume() {
		if (myThread != null)
			myThread.onResume();
	}

	public void onPause() {
		if (myThread != null)
			myThread.onPause();
	}

	private boolean checkVaChamCircleVsRect(Rect rect) {
		float px = mainDotX;
		float py = mainDotY;

		if (px < rect.left)
			px = rect.left;
		else if (px > rect.right)
			px = rect.right;

		if (py < rect.top)
			py = rect.top;
		else if (py > rect.bottom)
			py = rect.bottom;

		float dx = mainDotX - px;
		float dy = mainDotY - py;

		return (dx * dx + dy * dy) <= this.radius * this.radius;
	}

	// private boolean checkVaChamMainVsGifts() {
	// float total = (mainDotX - cX_Gift) * (mainDotX - cX_Gift) + (mainDotY -
	// cY_Gift) * (mainDotY - cY_Gift);
	// double d = Math.sqrt(total);
	// if (d <= (radius + r_gift))
	// return true;
	// return false;
	// }

	private float getYCenter(float height, Paint paint) {
		float yPos = (height - ((paint.descent() + paint.ascent()) / 2));
		return yPos;
	}

	// /////////////////////////////////////////////
	// ------------ Setter & Getter --------------
	// ////////////////////////////////////////////

	// public int getCurrent_num_gift() {
	// return current_num_gift;
	// }
	//
	// public void setCurrent_num_gift(int current_num_gift) {
	// this.current_num_gift = current_num_gift;
	// }

	public void setCurrentStatus(GAME_STATUS currentStatus) {
		this.currentStatus = currentStatus;
	}

	public CurrentMainDot getmCurrentMainDot() {
		return mCurrentMainDot;
	}

	public void setmCurrentMainDot(CurrentMainDot mCurrentMainDot) {
		this.mCurrentMainDot = mCurrentMainDot;
	}

	public void setColor(int color) {
		this.color = color;
		paint.setColor(color);
	}

	public int getColor() {
		return color;
	}

	// public int getGift() {
	// return gift;
	// }
	//
	// public void setGift(int gift) {
	// this.gift = gift;
	// }

	public int getCurrentLevel() {
		return currentLevel;
	}

	public boolean isReachBestScore() {
		return isReachBestScore;
	}

	public void setReachBestScore(boolean isReachBestScore) {
		this.isReachBestScore = isReachBestScore;
	}

	public void setMainFragment(FrgTrollerTap mainFragment) {
		this.mainFragment = mainFragment;
	}

	private void updateVatCan() {
		if (view_height > 0 && view_width > 0)
			initVatCan(view_width, view_height);

		// for (SquareParent squareParent : mSquareParents) {
		// squareParent.setSpeed(vatcan_speed);
		// squareParent.setVatcan_height(vatcan_height);
		// squareParent.setVatcan_width(vatcan_width);
		// }
	}

	public void setRATE_SPEED(int rATE_SPEED) {
		RATE_SPEED = rATE_SPEED;
		updateVatCan();
	}

	public void setRATE_WIDTH(int rATE_WIDTH) {
		RATE_WIDTH = rATE_WIDTH;
		updateVatCan();
	}

	public void setRATE_HEIGHT(int rATE_HEIGHT) {
		RATE_HEIGHT = rATE_HEIGHT;
		updateVatCan();
	}

	public void setBg_vatcan(int bg_vatcan) {
		this.bg_vatcan = bg_vatcan;
		mVatcanPaint.setColor(bg_vatcan);
	}

	private Bitmap myBitmap;
	private boolean isBitmapGet;

	/**
	 * Get the bitmap to share facebook
	 * 
	 * @return
	 */
	public Bitmap getBitmap() {
		return myBitmap;

	}
}
