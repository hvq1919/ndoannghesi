package net.tk.doannghesi.ui.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.retrofit.objects.Artist;
import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.retrofit.objects.User;
import net.tk.doannghesi.tools.Utils;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.tools.utils.LikeComparator;
import net.tk.doannghesi.ui.activities.MainActivity;
import net.tk.doannghesi.ui.controls.CircleImageView;
import net.tk.doannghesi.ui.fragments.FrgTopPlayerNgheSi;
import net.tk.doannghesi.R;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NgheSiAdapter extends BaseCusAdapter<Artist/*ArtistKey*/> {
    private Context mContext;
    private boolean isShowDialog;
    String deviceId;

//    public NgheSiAdapter(Context context, ArrayList<ArtistKey> lists) {
//        super(context, lists);
//        mContext = context;
//    }

    public NgheSiAdapter(Context context, ArrayList<Artist> lists) {
        super(context, lists);
        mContext = context;
        deviceId = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_nghe_si, parent, false);
            viewHolder.textCoun = (TextView) convertView.findViewById(R.id.textCoun);
            viewHolder.textName = (TextView) convertView.findViewById(R.id.textName);
            viewHolder.textLevel = (TextView) convertView.findViewById(R.id.textLevel);
            viewHolder.rootViewItem = convertView.findViewById(R.id.rootViewItem);
            viewHolder.ivAvatar = (ImageView) convertView.findViewById(R.id.ivAvatar);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        //final ArtistKey artistKey = getItem(position);
        final Artist artist = getItem(position);


        //Logger.d("KEY:" + artistKey.getKey()+ " Like:" + artistKey.getArtist().getLike());


        // Populate the data into the template view using the data object
        viewHolder.textCoun.setText((position + 1) + "");
//        viewHolder.textName.setText(artistKey.getArtist().getName());
//        viewHolder.textLevel.setText(artistKey.getArtist().getLike() + "");
        viewHolder.textName.setText(artist.getName());
        viewHolder.textLevel.setText("" + artist.getTotalLike());

        // Check position
        if (position == 0) {
            viewHolder.textName.setTextColor(Color.parseColor("#990000"));
            ((CircleImageView) viewHolder.ivAvatar).setBorderWidth(2);
            ((CircleImageView) viewHolder.ivAvatar).setBorderColor(Color.parseColor("#990000"));
        } else if (position == 1) {
            viewHolder.textName.setTextColor(Color.parseColor("#84007A"));
            ((CircleImageView) viewHolder.ivAvatar).setBorderWidth(2);
            ((CircleImageView) viewHolder.ivAvatar).setBorderColor(Color.parseColor("#84007A"));
        } else if (position == 2) {
            viewHolder.textName.setTextColor(Color.parseColor("#182265"));
            ((CircleImageView) viewHolder.ivAvatar).setBorderWidth(2);
            ((CircleImageView) viewHolder.ivAvatar).setBorderColor(Color.parseColor("#182265"));
        } else {
            viewHolder.textName.setTextColor(Color.parseColor("#333333"));
            ((CircleImageView) viewHolder.ivAvatar).setBorderWidth(0);
            ((CircleImageView) viewHolder.ivAvatar).setBorderColor(Color.parseColor("#333333"));

        }

        ImageLoader.getInstance().displayImage(artist.getImageUrl(), viewHolder.ivAvatar);

//        viewHolder.ivAvatar.setImageBitmap(Utils.getResizedBitmap(
//                Utils.getBitmapFromAsset(mContext, artistKey.getArtist().getImg_name()), 200, 138));
        viewHolder.ivAvatar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toaster.showToast(mContext, "" + ngheSi.getName());
                if (artist.getTotalLike() > 0)
                    ((MainActivity) mContext).pushFragment(FrgTopPlayerNgheSi.newInstance(artist), true);
                else ((MainActivity) mContext).showToast(mContext.getResources().getString(R.string.frg_khongconguoilike), true);
            }
        });

        viewHolder.rootViewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isShowDialog)
                    buildDialogConfirm(String.format(mContext.getResources().getString(R.string.nghesi_adapter_vote_artist), artist.getName()), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestApiLike(artist);
                        }

                    });
                else
                    requestApiLike(artist);

                isShowDialog = true;
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }

    /**
     * To update like of artist and update player_artist table
     */
    private void requestApiLike(Artist artist) {
        int currentCoins = SavedStore.getCoin();
        if (currentCoins < 100) {
            ((MainActivity) mContext).showToast(mContext.getResources().getString(R.string.frg_vongxoay_khongducoins), true);
            return;
        }

        if (!((MainActivity) mContext).isOnline()) {
            ((MainActivity) mContext).showToast(mContext.getResources().getString(R.string.khong_co_ket_noi_internet), true);
            return;
        }

        // TODO handle 1 ngày sau moi cho binh chon
        long time_first = SavedStore.getTimeFirst();
        long cur_time = System.currentTimeMillis();

        long remain_time = 24 * 60 * 60 * 1000 - cur_time + time_first;
        Logger.d("remain_ time: " + remain_time);
        if (remain_time > 0) {
            ((MainActivity) mContext).showToast("Bạn mới cài game, sau 1 ngày mới được bình chọn", true);
            return;
        }

        ((MainActivity) mContext).mAPIService.likeArtist(deviceId, artist.getId()).enqueue(new Callback<BaseObject<User>>() {
            @Override
            public void onResponse(Call<BaseObject<User>> call, Response<BaseObject<User>> response) {
            }

            @Override
            public void onFailure(Call<BaseObject<User>> call, Throwable t) {
            }
        });

        artist.setTotalLike(artist.getTotalLike() + 1);
        Collections.sort(getDatas(), new LikeComparator());
        notifyDataSetChanged();
        ((MainActivity) mContext).showToast("Thành công", false);
        int coins = SavedStore.getCoin() - 100;
        SavedStore.editCoin(coins);
    }


    // View lookup cache
    private static class ViewHolder {
        TextView textCoun, textName, textLevel;
        ImageView ivAvatar;
        View rootViewItem;
    }

    public void buildDialogConfirm(String text, DialogInterface.OnClickListener clickListener) {
        new AlertDialog.Builder(mContext).setMessage(text).setPositiveButton("Yes", clickListener)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
    }


    class ImageDownloaderTask extends AsyncTask<String, Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;

        public ImageDownloaderTask(ImageView imageView) {
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return (Utils.getResizedBitmap(
                    Utils.getBitmapFromAsset(mContext, params[0]), 200, 138));
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            if (imageViewReference != null) {
                ImageView imageView = imageViewReference.get();
                if (imageView != null && bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        }
    }
}
