package net.tk.doannghesi.ui.fragments.thuthach.boomdot;

import android.graphics.Color;

public class Define {
	public static int RADIUS = 20;
	public static int BLUE_COLOR = Color.parseColor("#2196F3");// Blue
	public static int RED_COLOR = Color.parseColor("#F44336");// Red
	
	public static int BLUE_COLOR_BLUR = Color.parseColor("#332196F3");// Blue blur
	public static int RED_COLOR_BLUR = Color.parseColor("#33F44336");// Red blur
	
	// public static int RED_COLOR = Color.parseColor("#F44336");// Red
	// #CFD8DC

	public static int[][] BackgroundColors = new int[][] { { 202, 227, 244 }, { 244, 235, 202 }, { 202, 208, 202 },
			{ 219, 202, 244 }, { 202, 244, 226 }, { 197, 182, 230 }, { 182, 202, 230 }, { 230, 216, 182 },
			{ 182, 230, 225 }, { 230, 182, 182 }, { 199, 230, 182 } };

	public static enum TYPE_COLOR {
		BULE, RED
	}
	
	public static int RC_SIGN_IN = 9001;
}
