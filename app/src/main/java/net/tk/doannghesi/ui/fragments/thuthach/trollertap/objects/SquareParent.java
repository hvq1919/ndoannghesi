package net.tk.doannghesi.ui.fragments.thuthach.trollertap.objects;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import java.util.Random;

public class SquareParent {
	private float vatcan_width;
	private float vatcan_height;

	private float speed = 1f;

	private float topY;
	private float botY;

	private Paint mPaint;
	private Rect r = new Rect();

	private boolean isLeft;
	private boolean isAnimation = true;
	private boolean isAnimationToTop;

	public SquareParent(Paint mPaint, float vatcan_width, float vatcan_height, float topY, float botY, int w, int h,
			boolean left) {
		super();
		this.topY = topY;
		this.botY = botY;
		this.mPaint = mPaint;
		this.isLeft = left;
		this.vatcan_width = vatcan_width;
		this.vatcan_height = vatcan_height;
		if (isLeft)
			randomLeft();
		else
			randomRight(w);

	}

	public void draw(Canvas c, int width, int height) {
		if (isAnimation) {
			if (isAnimationToTop) {
				r.top -= speed;
				r.bottom -= speed;
			} else {
				r.top += speed;
				r.bottom += speed;
			}
			if (r.top < topY)
				isAnimationToTop = false;
			if (r.bottom > botY)
				isAnimationToTop = true;
		}
		mPaint.clearShadowLayer();
		c.drawRect(r, mPaint);
	}

	public void randomLeft() {
		randomRect(0);
	}

	public void randomRight(int width) {
		randomRect(width);
	}

	private void randomRect(int width) {
		r.left = (int) (width - vatcan_width / 2);
		r.right = (int) (width + vatcan_width / 2);

		int height = (int) (vatcan_height * 10);
		int hi = (int) (height / 2 + new Random().nextInt(height / 2)) / 10;

		// Log.d("TAG", "TopY:" + topY + " BotY:" + botY + " hi:" + hi +
		// " height:" + height) ;
		// Log.d("TAG", "n:" + (botY - topY - hi));
		int tamRect = (int) (new Random().nextInt((int) (botY - topY - hi)) + topY + hi / 2);

		r.top = (int) (tamRect - hi / 2);
		r.bottom = (int) (tamRect + hi / 2);
	}

	public boolean isLeft() {
		return isLeft;
	}

	public void setLeft(boolean isLeft) {
		this.isLeft = isLeft;
	}

	public boolean isAnimation() {
		return isAnimation;
	}

	public void setAnimation(boolean isAnimation) {
		this.isAnimation = isAnimation;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float sp) {
		float r = sp * 10;
		// int i = new Random().nextInt((int) (3 * r / 2));
		// this.speed = (r / 2 + i) / 10f;

		this.speed = new Random().nextInt((int) (2 * r)) / 10f;
	}

	public Rect getRect() {
		return r;
	}

	public void setRect(Rect r) {
		this.r = r;
	}

	public float getVatcan_width() {
		return vatcan_width;
	}

	public void setVatcan_width(float vatcan_width) {
		this.vatcan_width = vatcan_width;
	}

	public float getVatcan_height() {
		return vatcan_height;
	}

	public void setVatcan_height(float vatcan_height) {
		this.vatcan_height = vatcan_height;
	}
	
	
}
