package net.tk.doannghesi.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gc.materialdesign.views.ButtonRectangle;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import net.tk.doannghesi.Define;
import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.tools.Utils;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.ui.activities.MainActivity.IOnGetConfig;
import net.tk.doannghesi.ui.fragments.FrgVongXoay.IOnUpdateCoin;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.ui.fragments.thuthach.FrgFindSamePic;
import net.tk.doannghesi.ui.fragments.thuthach.FrgMemories;
import net.tk.doannghesi.ui.fragments.thuthach.boomdot.FrgBoomDot;
import net.tk.doannghesi.ui.fragments.thuthach.diffrencecolor.FrgDiffrenceColor;
import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.FrgQuickMoving;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.FrgTrollerTap;
import net.tk.doannghesi.BuildConfig;
import net.tk.doannghesi.R;

public class FrgHome extends BaseFragment implements OnClickListener {

    private View rootView;

    // private TextView tvCoin;

    // private ShimmerTextView ShimmerTextView;
    // private Shimmer mShimmer;

    private View wheel;
    private ImageView iv_vongxoay, imgNew;

    private ButtonRectangle btnChoiNgay;
    private ButtonRectangle btnTopScore;
    private ButtonRectangle btnNgheSi;
    private ButtonRectangle btnExit;

    private ImageView imgMusic;
    private ImageView imgSound;
    private boolean isMusic;
    private boolean isSound;

    // private int mCurrentCoin;

    @Override
    public void onResume() {
        super.onResume();
        mActivity.mBackGroundSound.startSound();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.d("FrgHome - onCreateView");
        mRootView = inflater.inflate(R.layout.frg_home, container, false);
        initObjects();

        if (SavedStore.getNewQcs()) imgNew.setVisibility(View.VISIBLE);
        else imgNew.setVisibility(View.GONE);

        imgMusic.setOnClickListener(this);
        imgSound.setOnClickListener(this);

        btnChoiNgay.setOnClickListener(this);
        btnNgheSi.setOnClickListener(this);
        btnExit.setOnClickListener(this);
        btnTopScore.setOnClickListener(this);
        mRootView.findViewById(R.id.img_tip).setOnClickListener(this);
        mRootView.findViewById(R.id.iv_vongxoay).setOnClickListener(this);
        mRootView.findViewById(R.id.btnTopScore).setOnClickListener(this);
        mRootView.findViewById(R.id.btnMuaCoins).setOnClickListener(this);

        checkPassGoogle();
        loadAdMob();

        signInFireBase();

        // Inflate the layout for this fragment
        return mRootView;

    }

    private AdView mAdView;

    private void loadAdMob() {
        mAdView = (AdView) mRootView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity.setIOnGetConfig(new IOnGetConfig() {

            @Override
            public void OnGetConfig() {
                checkPassGoogle();
            }
        });


        int versionCode = BuildConfig.VERSION_CODE;
        if (SavedStore.getVersionCode() != 0 && SavedStore.getVersionCode() != versionCode) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(mActivity);
            builder1.setMessage(mResources.getString(R.string.dialog_update_app));
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mActivity.getPackageName())));
                            } catch (Exception e) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                        .parse("https://play.google.com/store/apps/details?id=" + mActivity.getPackageName())));
                            }
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

    private void checkPassGoogle() {
        if (SavedStore.getIsBinhChon()) {
            btnNgheSi.setVisibility(View.VISIBLE);
        }

        int hienthi = SavedStore.getHienThi();
        if (hienthi == 300 || (hienthi == 301 && SavedStore.getCurrentNumberQuestion() > 15)) {
            btnNgheSi.setVisibility(View.VISIBLE);
        } else {
            btnNgheSi.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Logger.d("FrgHome - Onstart");
        // googleApiClientConnect();

        // SavedStore.editSpin(20);
        // SavedStore.editCurrentNumberQuestion("753");
    }

    private void initObjects() {
        btnTopScore = (ButtonRectangle) mRootView.findViewById(R.id.btnTopScore);
        btnChoiNgay = (ButtonRectangle) mRootView.findViewById(R.id.btnChoiNgay);
        btnNgheSi = (ButtonRectangle) mRootView.findViewById(R.id.btnNgheSi);
        btnExit = (ButtonRectangle) mRootView.findViewById(R.id.btnExit);

        // tvCoin = (TextView) mRootView.findViewById(R.id.coin);
        iv_vongxoay = (ImageView) mRootView.findViewById(R.id.iv_vongxoay);
        imgNew = (ImageView) mRootView.findViewById(R.id.imgNew);

		/* Sound setting */
        imgMusic = (ImageView) mRootView.findViewById(R.id.imgMusic);
        imgSound = (ImageView) mRootView.findViewById(R.id.imgSound);
        isMusic = SavedStore.getMusic();
        isSound = SavedStore.getSound();

        if (isMusic)
            imgMusic.setImageResource(R.drawable.music_on);
        else
            imgMusic.setImageResource(R.drawable.music_off);
        if (isSound)
            imgSound.setImageResource(R.drawable.speaker_on);
        else
            imgSound.setImageResource(R.drawable.speaker_off);

        rootView = mRootView.findViewById(R.id.rootView);



        if (SavedStore.getSpinNow())
            iv_vongxoay.setImageResource(R.drawable.icon_vongxoay_spin);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgMusic:
                // TODO test
                mActivity.getKey();
                // #End test


                if (isMusic) {
                    isMusic = false;
                    SavedStore.editMusic(false);
                    imgMusic.setImageResource(R.drawable.music_off);
                    mActivity.mBackGroundSound.pauseSound();
                } else {
                    isMusic = true;
                    SavedStore.editMusic(true);
                    imgMusic.setImageResource(R.drawable.music_on);
                    mActivity.mBackGroundSound.startSound();
                }
                break;
            case R.id.imgSound:
                // TODO test
                mActivity.getListHacker();
                // #End test

                if (isSound) {
                    isSound = false;
                    SavedStore.editSound(false);
                    imgSound.setImageResource(R.drawable.speaker_off);
                } else {
                    isSound = true;
                    SavedStore.editSound(true);
                    imgSound.setImageResource(R.drawable.speaker_on);
                }
                break;

            case R.id.btnChoiNgay:

                int thuthach = SavedStore.getThuThach();

                // TODO test
                //	thuthach = Define.THUTHACH_MEMORIES;

                if (thuthach == Define.THUTHACH_BOOMDOT)
                    mActivity.pushFragment(new FrgBoomDot(), true);
                else if (thuthach == Define.THUTHACH_DIFFRENCECOLOR)
                    mActivity.pushFragment(new FrgDiffrenceColor(), true);
                else if (thuthach == Define.THUTHACH_MEMORIES)
                    mActivity.pushFragment(new FrgMemories(), true);
                else if (thuthach == Define.THUTHACH_QUICKMOVING)
                    mActivity.pushFragment(new FrgQuickMoving(), true);
                else if (thuthach == Define.THUTHACH_SAMEPIC)
                    mActivity.pushFragment(new FrgFindSamePic(), true);
                else if (thuthach == Define.THUTHACH_TROLLERTAP)
                    mActivity.pushFragment(new FrgTrollerTap(), true);
                else
                    mActivity.pushFragment(new FrgMain(), true);

                break;
            case R.id.btnMuaCoins:
                if (mActivity.isOnline())
                    mActivity.pushFragment(new PurchaseFragment(), true);
                else
                    mActivity.showToast("Kiểm tra kết nối mạng", true);

                break;
            case R.id.btnNgheSi:
                // NGhe Si
                if (mActivity.isOnline())
                    mActivity.pushFragment(new FrgNgheSi(), true);
                else
                    mActivity.showToast("Kiểm tra kết nối mạng", true);

                // mActivity.showToast("Comming soon.", false);

                // ParseUser parseUser = ParseUser.getCurrentUser();
                // parseUser.put("Level", 3);
                // parseUser.saveInBackground(new SaveCallback() {
                //
                // @Override
                // public void done(ParseException arg0) {
                // Logger.d("Saved Data");
                // }
                // });

                break;
            case R.id.btnTopScore:
                if (mActivity.isOnline())
                    mActivity.pushFragment(new FrgTopPlayer(), true);
                else
                    mActivity.showToast("Kiểm tra kết nối mạng", true);
                break;
            case R.id.btnExit:
//                Utils.buildDialog(mActivity, "Yes", "No", "Confirm exit!", "Do you want to exit app?", new OnClickDialog() {
//
//                    @Override
//                    public void PositiveClick() {
//                        mActivity.finish();
//                    }
//
//                    @Override
//                    public void NegativeClick() {
//                    }
//                });

                Utils.buildExitDialog(mActivity);
                break;
            case R.id.img_tip:
                if (mActivity.isOnline()) {
                    mActivity.pushFragment(new FrgSuggestion(), true);
                } else {
                    mActivity.showToast(mResources.getString(R.string.khong_co_ket_noi_internet), true);
                }
                break;
            case R.id.iv_vongxoay:
                if (mActivity.isOnline()) {
                    iv_vongxoay.setImageResource(R.drawable.icon_vongxoay);
                    SavedStore.editSpinNow(false);

                    mActivity.pushFragment(FrgVongXoay.newInstance(new IOnUpdateCoin() {
                        @Override
                        public void onUpdateCoin(int coin) {
                            // mCurrentCoin += coin;
                            // tvCoin.setText("" + mCurrentCoin);
                        }
                    }), true);

                } else {

                    mActivity.showToast(mResources.getString(R.string.khong_co_ket_noi_internet), true);

                    // Toaster.showToast(mActivity, "No internet");
                }
                break;

            default:
                break;
        }

    }

}
