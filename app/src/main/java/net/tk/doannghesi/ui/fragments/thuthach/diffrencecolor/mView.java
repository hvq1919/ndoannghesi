package net.tk.doannghesi.ui.fragments.thuthach.diffrencecolor;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.tk.doannghesi.ui.fragments.thuthach.diffrencecolor.FrgDiffrenceColor.IOnStartCorrectImage;
import net.tk.doannghesi.R;

import java.util.Random;

public class mView {
	FrgDiffrenceColor findcolor;
	private int n, index;
	private int color1, color2, color3;
	private static ImageView[] array_icon;
	private TextView score;
	public SoundPool sp = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
	public int soundIds[] = new int[3];

	private mView mView;

	private boolean canClick = true;

	public mView(FrgDiffrenceColor findcolor) {
		this.findcolor = findcolor;

		soundIds[0] = sp.load(findcolor.getMainActivity(), R.raw.correct, 1);
		soundIds[1] = sp.load(findcolor.getMainActivity(), R.raw.clickanswer, 1);
		soundIds[2] = sp.load(findcolor.getMainActivity(), R.raw.wronganswer, 1);

		findcolor.setIOnStartCorrectImage(new IOnStartCorrectImage() {

			@Override
			public void onStartCorrectImage() {
				startCorrectImage();
			}
		});

	}

	public void Show() {
		init();
		createGamePlay();

	}

	private void init() {
		findcolor.checkPause = false;
		n = random(5, 4);
		index = random(n * n, 0);
		Log.d("TAG", "n:" + n + " Index:" + index);
		array_icon = new ImageView[n * n];
		color1 = random(255, 0);
		color2 = random(255, 0);
		color3 = random(255, 0);

		score = findcolor.getTvScore();
		String s = String.format("%02d", findcolor.mScore);
		score.setText("" + s);

	}

	@SuppressWarnings("deprecation")
	private void DrawBoard(LinearLayout l) {
		int size = 400;
		Bitmap view = Bitmap.createBitmap(400, 400, Config.ARGB_8888);
		Canvas c = new Canvas(view);
		Paint p = new Paint();
		p.setAntiAlias(true);

		p.setColor(Color.parseColor("#dddddd"));

		RectF r1 = new RectF(0, 0, size, size);
		c.drawRoundRect(r1, 7, 7, p);
		l.setBackgroundDrawable(new BitmapDrawable(findcolor.getResources(), view));
	}

	private void DrawIcon(ImageView image) {
		int size = 400 / n - 10;
		Bitmap bitmap = Bitmap.createBitmap(size, size, Config.ARGB_8888);
		Canvas c = new Canvas(bitmap);
		Paint p = new Paint();
		p.setAntiAlias(true);

		p.setColor(Color.rgb(color1, color2, color3));
		RectF r = new RectF(0, 0, size, size);
		c.drawRoundRect(r, 5, 5, p);
		image.setImageBitmap(bitmap);
	}

	private void Draw_Icon(ImageView image) {
		int size = 400 / n;
		Bitmap view = Bitmap.createBitmap(size, size, Config.ARGB_8888);
		Canvas c = new Canvas(view);
		Paint p = new Paint();
		p.setAntiAlias(true);

		int h = new Random().nextInt(30) + 5;

		int r = color1 + h;
		if (r > 255)
			r = 255;

		int g = color2 + h;
		if (g > 255)
			g = 255;

		int b = color3 + h;
		if (b > 255)
			b = 255;

		p.setColor(Color.rgb(r, g, b));
		// p.setColor(Color.rgb(color1 + hard, color2 + hard/*random(10, 10)*/,
		// color3 + hard));

		RectF r1 = new RectF(0, 0, size, size);
		c.drawRoundRect(r1, 5, 5, p);
		image.setImageBitmap(view);
	}

	private void createGamePlay() {

		final LinearLayout show_view = findcolor.getShow_view();
		final LinearLayout show_board = findcolor.getShow_board();
		DrawBoard(show_board);
		LinearLayout[] layouts = new LinearLayout[n];
		LinearLayout.LayoutParams layout_param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
				LinearLayout.LayoutParams.MATCH_PARENT, 1);
		layout_param.setMargins(2, 2, 2, 2);
		for (int i = 0; i < n; i++) {
			layouts[i] = new LinearLayout(findcolor.getMainActivity());
			layouts[i].setGravity(Gravity.CENTER_HORIZONTAL);
			layouts[i].setOrientation(LinearLayout.HORIZONTAL);
			layouts[i].setLayoutParams(layout_param);
			show_view.addView(layouts[i]);
		}

		OnClickListener ocl = new OnClickListener() {

			@Override
			public void onClick(View v) {
				int myId = v.getId();
				if (!canClick)
					return;
				if (myId == index && !findcolor.checkPause) {
					sp.play(soundIds[0], 1.0f, 1.0f, 1, 0, 1.0f);

					for (int i = 0; i < n * n; i++) {
						if (i == index)
							startAnimationthis(array_icon[i]);
						else
							startAnimation1(array_icon[i]);
					}

					findcolor.timer.cancel();
					findcolor.CreateTimer(findcolor.TIME);


					canClick = false;
					findcolor.mScore += 1;
					String s = String.format("%02d", findcolor.mScore);
					score.setText("" + s);

					// SaveHighScore();
					// TODO check Heighscore

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							show_view.removeAllViews();
							Show();
							canClick = true;
						}
					}, 500);

				} else {
					startAnimationthis(array_icon[myId]);
					sp.play(soundIds[1], 1.0f, 1.0f, 1, 0, 1.0f);
				}
			}
		};

		for (int j = 0; j < n * n; j++) {
			int y = j / n;
			array_icon[j] = new ImageView(findcolor.getMainActivity());
			array_icon[j].setLayoutParams(layout_param);
			array_icon[j].setId(j);
			array_icon[j].setOnClickListener(ocl);
			if (j == index) {
				Draw_Icon(array_icon[j]);
			} else {
				DrawIcon(array_icon[j]);
			}
			layouts[y].addView(array_icon[j]);

		}
	}

	private void startAnimation1(ImageView iv) {
		Animation anime;
		int u = random(6, 1);
		if (u == 1) {
			anime = AnimationUtils.loadAnimation(findcolor.getMainActivity(), R.anim.slideup);
		} else if (u == 2) {
			anime = AnimationUtils.loadAnimation(findcolor.getMainActivity(), R.anim.slidedown);
		} else if (u == 3) {
			anime = AnimationUtils.loadAnimation(findcolor.getMainActivity(), R.anim.slideleft);
		} else if (u == 4) {
			anime = AnimationUtils.loadAnimation(findcolor.getMainActivity(), R.anim.slideright);
		} else if (u == 5) {
			anime = AnimationUtils.loadAnimation(findcolor.getMainActivity(), R.anim.fadeout);
		} else {
			anime = AnimationUtils.loadAnimation(findcolor.getMainActivity(), R.anim.rotate);
		}
		iv.startAnimation(anime);
	}

	private void startAnimationthis(ImageView iv) {
		Animation anime = AnimationUtils.loadAnimation(findcolor.getMainActivity(), R.anim.scale);
		iv.startAnimation(anime);
	}

	private void startCorrectImage() {
		Log.d("TAG", "startCorrectImage _ n:" + n + " Index:" + index);
		// startAnimationthis(array_icon[index]);

		Animation anime = AnimationUtils.loadAnimation(findcolor.getMainActivity(), R.anim.scale_big);
		array_icon[index].startAnimation(anime);
	}

	private int random(int u, int v) {
		Random r = new Random();
		return (int) (Math.abs(r.nextInt()) % u + v);
	}

}
