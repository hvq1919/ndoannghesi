package net.tk.doannghesi.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 7/27/2017.
 */

public class FrgSuggestion extends BaseFragment implements View.OnClickListener {

    EditText edtGopy;
    Button btnGopY;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frg_suggestion, container, false);

        edtGopy = (EditText) mRootView.findViewById(R.id.edt_gopy);

        mRootView.findViewById(R.id.back).setOnClickListener(this);
        mRootView.findViewById(R.id.btn_gopy).setOnClickListener(this);
        return mRootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                mActivity.popFragment();
                break;

            case R.id.btn_gopy:
                if(!TextUtils.isEmpty(edtGopy.getText().toString())){
                    mActivity.showLoadingDialog();
                    mActivity.mAPIService.suggestion(edtGopy.getText().toString()).enqueue(new Callback<BaseObject<String>>() {
                        @Override
                        public void onResponse(Call<BaseObject<String>> call, Response<BaseObject<String>> response) {
                            mActivity.dismissLoadingDialog();
                            edtGopy.setText("");
                            mActivity.showToast(mResources.getString(R.string.gopy_thanhcong), false);
                            try  {
                                InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), 0);
                            } catch (Exception e) {

                            }
                            mActivity.popFragment();
                        }

                        @Override
                        public void onFailure(Call<BaseObject<String>> call, Throwable t) {
                            mActivity.dismissLoadingDialog();
                        }
                    });
                }else{
                    mActivity.showToast(mResources.getString(R.string.nhap_gopy), true);
                }
                break;
            default:
                break;
        }
    }
}
