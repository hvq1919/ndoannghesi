package net.tk.doannghesi.ui.fragments.thuthach.boomdot.objects;

import android.graphics.Canvas;
import android.graphics.Paint;

import net.tk.doannghesi.ui.fragments.thuthach.boomdot.Define;
import net.tk.doannghesi.ui.fragments.thuthach.boomdot.Define.TYPE_COLOR;

public class RunningDot {
	// speed = device's height / RATE_SPEED
	private final int RATE_SPEED = 100;
	
	// radius = device's width / RATE_RADIUS
	private final int RATE_RADIUS = 25;
	
	private int width;
	private int height;

	private float speed;
	private TYPE_COLOR type_color;

	private float radius;
	private float xPos;
	private float yPos;
	private Paint paint;

	// Dot will display from screen's top or bottom
	private boolean fromTop;

	public RunningDot( int width, int height) {
		super();
		this.width = width;
		this.height = height;

		initPaint();
		initDot();
	}

	private void initPaint() {
		paint = new Paint();
	}

	private boolean randomBoolean() {
		return Math.random() < 0.5;
	}

	public void initDot() {
		xPos = width / 2;
		
		radius = width/RATE_RADIUS;

		// Random display position
		if (randomBoolean()) {
			fromTop = true;
			yPos = radius;
		} else {
			fromTop = false;
			yPos = height - radius;
		}

		// Random color of dot
		if (randomBoolean()){
			paint.setColor(Define.BLUE_COLOR);
			type_color = TYPE_COLOR.BULE;
		}else{
			paint.setColor(Define.RED_COLOR);
			type_color = TYPE_COLOR.RED;
		}

		paint.setFlags(Paint.ANTI_ALIAS_FLAG);
	}

	public void draw(Canvas c, int width, int height) {
		if (fromTop)
			yPos += height/RATE_SPEED;
		else
			yPos -= height/RATE_SPEED;

		c.drawCircle(xPos, yPos, radius, paint);
	}

	// //////////////////////////////////////////////////////////
	// ----- Setter && Getter -------------
	// //////////////////////////////////////////////////////////
	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public TYPE_COLOR getType_color() {
		return type_color;
	}

	public void setType_color(TYPE_COLOR type_color) {
		this.type_color = type_color;
	}

	public float getRadius() {
		return radius;
	}

	public float getxPos() {
		return xPos;
	}

	public float getyPos() {
		return yPos;
	}

}
