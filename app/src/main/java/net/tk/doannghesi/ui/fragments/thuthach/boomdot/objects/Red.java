package net.tk.doannghesi.ui.fragments.thuthach.boomdot.objects;

import android.graphics.Canvas;
import android.graphics.Color;

public class Red extends Parent {

	private int color = Color.parseColor("#F44336");// Red
	private int radius = 60;

	public Red() {
		super();
		init();
	}

	public Red(int r) {
		super();
		init(r);
	}

	public Red(int xPos, int yPos) {
		super(xPos, yPos);
		init();
	}

	private void init() {
		setRadius(radius);
		setColor(color);
	}

	private void init(int r) {
		setRadius(r);
		setColor(color);
	}

	@Override
	public void draw(Canvas canvas, int w, int h) {
		super.draw(canvas, w, h);
	}
}
