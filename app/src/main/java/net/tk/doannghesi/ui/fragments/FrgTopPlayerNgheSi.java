package net.tk.doannghesi.ui.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import net.tk.doannghesi.retrofit.objects.Artist;
import net.tk.doannghesi.retrofit.objects.ArtistUser;
import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.ui.adapter.TopPlayerNgheSiAdapter;
import net.tk.doannghesi.ui.controls.CircleImageView;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgTopPlayerNgheSi extends BaseFragment implements OnClickListener {

    private final int NUM_OF_USER = 200;
    private ListView mListView;
    private TextView textName, textLevel;
    private RelativeLayout rootView;
    private CircleImageView ivAvatar;

    private TopPlayerNgheSiAdapter mPlayerAdapter;

    private Artist mArtist;


    // New version
    public static FrgTopPlayerNgheSi newInstance(Artist artist) {
        FrgTopPlayerNgheSi frgTopPlayerNgheSi = new FrgTopPlayerNgheSi();
        frgTopPlayerNgheSi.setArtist(artist);

        return frgTopPlayerNgheSi;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frg_top_player_nghesi, container, false);
        mListView = (ListView) mRootView.findViewById(R.id.listView);
        rootView = (RelativeLayout) mRootView.findViewById(R.id.rootView);

        textLevel = (TextView) mRootView.findViewById(R.id.textLevel);
        textName = (TextView) mRootView.findViewById(R.id.textName);
        ivAvatar = (CircleImageView) mRootView.findViewById(R.id.ivAvatar);

        updateUi();

        mPlayerAdapter = new TopPlayerNgheSiAdapter(mActivity, new ArrayList<ArtistUser>());
        mListView.setAdapter(mPlayerAdapter);


        mRootView.findViewById(R.id.back).setOnClickListener(this);
        mRootView.findViewById(R.id.fb).setOnClickListener(this);

        // TODO
//        cheatData();

        getListUsersOfArtists();

        return mRootView;
    }

//    private void cheatData(){
//        FirebaseDatabase.getInstance().getReference().child("artists_users").child(mArtistKey.getKey()).child("6c108782234ba4e4").removeValue();
//
//
//        ArtistPost artist = mArtistKey.getArtist();
//        if(mArtistKey.getArtist().getName().equals("Sơn Tùng MTP")){
//            artist.setLike(115);
//        }else if(mArtistKey.getArtist().getName().equals("Khởi My")){
//            artist.setLike(121);
//        }else if(mArtistKey.getArtist().getName().equals("Noo Phước Thịnh")){
//            artist.setLike(81);
//        }
//        else if(mArtistKey.getArtist().getName().equals("Hari Won")){
//            artist.setLike(80);
//        }
//        else if(mArtistKey.getArtist().getName().equals("Girl Lê")){
//            artist.setLike(67);
//        }
//        else if(mArtistKey.getArtist().getName().equals("Hồ Quang Hiếu")){
//            artist.setLike(76);
//        }
//        else if(mArtistKey.getArtist().getName().equals("Trấn Thành")){
//            artist.setLike(74);
//        }
//        else if(mArtistKey.getArtist().getName().equals("Kelvin Khánh")){
//            artist.setLike(52);
//        }
//        else if(mArtistKey.getArtist().getName().equals("Hoài Lâm")){
//            artist.setLike(62);
//        }
//
//        FirebaseDatabase.getInstance().getReference().child("NgheSi").child(mArtistKey.getKey()).setValue(artist);
//
//    }

    private void updateUi() {
        textLevel.setText(mArtist.getTotalLike() + "");
        textName.setText(mArtist.getName());

        ImageLoader.getInstance().displayImage(mArtist.getImageUrl(),ivAvatar);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    private void getListUsersOfArtists() {
        mActivity.showLoadingDialog();
        mActivity.mAPIService.listUserArtist(mArtist.getId()).enqueue(new Callback<BaseObject<List<ArtistUser>>>() {
            @Override
            public void onResponse(Call<BaseObject<List<ArtistUser>>> call, Response<BaseObject<List<ArtistUser>>> response) {
                mActivity.dismissLoadingDialog();
                if(response.body()!= null &&  response.body().isSuccess() && response.body().getData()!= null){
                    ArrayList<ArtistUser> artistUsers = new ArrayList<ArtistUser>(response.body().getData());
                    mPlayerAdapter.clear();
                    mPlayerAdapter.addAll(artistUsers);
                }else{
                    Logger.d("Get list artist user error.");
                }
            }

            @Override
            public void onFailure(Call<BaseObject<List<ArtistUser>>> call, Throwable t) {
                mActivity.dismissLoadingDialog();
                Logger.d("Invalid response's format");
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                mActivity.popFragment();
                break;
            case R.id.fb:
                rootView.setDrawingCacheEnabled(true);
                rootView.buildDrawingCache();
                Bitmap bm = rootView.getDrawingCache();

                mActivity.getFacebookHelper().doShareImage(bm);
                break;

            default:
                break;
        }
    }

    public void setArtist(Artist artist) {
        this.mArtist = artist;
    }
}