package net.tk.doannghesi.ui.fragments.thuthach.diffrencecolor;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.ui.activities.MainActivity;
import net.tk.doannghesi.ui.dialog.DialogHintAThuThach;
import net.tk.doannghesi.ui.fragments.thuthach.FrgBaseThuThach;
import net.tk.doannghesi.R;

import java.util.Random;

public class FrgDiffrenceColor extends FrgBaseThuThach {
	public final int TIME = 6000;

	public CountDownTimer timer;
	private long millis_pause;

	public boolean checkPause;
	public int highScore;
	public int mScore;

	private Handler mHandler;
	private mView mView;

	private TextView tvScore;
	private LinearLayout show_view, show_board;

	public interface IOnStartCorrectImage {
		public void onStartCorrectImage();
	}

	private IOnStartCorrectImage mIOnStartCorrectImage;

	private DialogHintAThuThach dialogHintAThuThach;

	public void setIOnStartCorrectImage(IOnStartCorrectImage iOnStartCorrectImage) {
		mIOnStartCorrectImage = iOnStartCorrectImage;
	}

	public MainActivity getMainActivity() {
		return mActivity;
	}

	@Override
	public void onDestroyView() {
		timer.cancel();
		mScore = 0;
		super.onDestroyView();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.frg_diffrencecolor, container, false);

		tvScore = (TextView) mRootView.findViewById(R.id.tv_score);
		show_board = (LinearLayout) mRootView.findViewById(R.id.show_board);
		show_view = (LinearLayout) mRootView.findViewById(R.id.show_view);

		mHandler = new Handler();
		mView = new mView(this);
		setFont(tvScore);
		tvScore.setText("00");

		String note = String.format(mResources.getString(R.string.note_diffrencecolor), numThuThach);
		dialogHintAThuThach = DialogHintAThuThach.newInstance(mResources.getString(R.string.ten_thuthach_diffrence_color),
				note, new OnClickListener() {

					@Override
					public void onClick(View v) {
						resetGame();
						SavedStore.editFirstHint(true);
						dialogHintAThuThach.dismiss();
					}
				});
		dialogHintAThuThach.show(mActivity.getFragmentManager(), "TAG");

		// mView.Show();
		// setFont(tvScore);
		// tvScore.setText("00");
		// CreateMenu();
		// CreateTimer(TIME);

		return mRootView;
	}

	int time_ = 5;

	public void CreateTimer(long index) {
		time_ = 5;
		final TextView tv_time = (TextView) mRootView.findViewById(R.id.tv_time);
		timer = new CountDownTimer(index, 1000) {

			public void onTick(long millisUntilFinished) {
				String scr = String.format("%02d", millisUntilFinished / 1000);
				tv_time.setText(scr + "");
				millis_pause = millisUntilFinished;

				time_ -= 1;
				// Log.d("TAG", "millisUntilFinished:" + millisUntilFinished +
				// " -Time:" + time_);
				if (time_ == 0) {
					//
					// if (mIOnStartCorrectImage != null)
					// mIOnStartCorrectImage.onStartCorrectImage();

					// mHandler.postDelayed(new Runnable() {
					//
					// @Override
					// public void run() {
					//
					// }
					// }, 1000);
				}
			}

			public void onFinish() {
				if (mIOnStartCorrectImage != null)
					mIOnStartCorrectImage.onStartCorrectImage();

				mView.sp.play(mView.soundIds[2], 1.0f, 1.0f, 1, 0, 1.0f);

				if (mScore > numThuThach) // TODO win
					showDialogWin(mActivity);
				else {
					// TODO test
					// showDialogSaveMe(mActivity);

					if (mScore > 5) {
						showDialogSaveMe(mActivity);
					} else {
						onDimissConfirmDialogClick();
					}
				}

				// final Dialog d = new Dialog(mActivity, R.style.Transparent);
				// d.setContentView(R.layout.game_over);
				// d.getWindow().getAttributes().windowAnimations =
				// R.style.Dialogdown;
				//
				// TextView score = (TextView) d.findViewById(R.id.score);
				// setFont(score);
				// String s = String.format("%02d", mScore);
				// score.setText("Your Score: " + s);
				//
				// tv_time.setText("00");
				//
				// time_up = (TextView) d.findViewById(R.id.timeup);
				// setFont(time_up);
				// Animation anim = AnimationUtils.loadAnimation(mActivity,
				// R.anim.timeup);
				// time_up.setText("Time up!");
				// time_up.setTextColor(Color.rgb(random(254, 1), random(254,
				// 1), random(148, 1)));
				// time_up.startAnimation(anim);
				// checkPause = true;
				//
				// Button newgame = (Button) d.findViewById(R.id.bt_newgame);
				// newgame.setOnClickListener(new OnClickListener() {
				// @Override
				// public void onClick(View v) {
				// resetGame();
				// }
				// });
				//
				// d.show();
			}
		}.start();
	}

	private int random(int u, int v) {
		Random r = new Random();
		return (int) (Math.abs(r.nextInt()) % u + v);
	}

	public void setFont(TextView id) {
		String fontPath;
		fontPath = "fonts/dottappy.otf";
		Typeface tf = Typeface.createFromAsset(mActivity.getAssets(), fontPath);
		id.setTypeface(tf);
	}

	public TextView getTvScore() {
		return tvScore;
	}

	public LinearLayout getShow_view() {
		return show_view;
	}

	public LinearLayout getShow_board() {
		return show_board;
	}

	@Override
	protected void onSaveMeSuccess() {
		// TODO Auto-generated method stub
		super.onSaveMeSuccess();
		CreateTimer(TIME);
		//mScore = 0;
		show_view.removeAllViews();
		new mView(FrgDiffrenceColor.this).Show();
	}
	@Override
	protected void resetGame() {
		// mView.Show();
		// CreateMenu();
		// CreateTimer(TIME);

		CreateTimer(TIME);
		mScore = 0;
		show_view.removeAllViews();
		new mView(FrgDiffrenceColor.this).Show();
	}

	@Override
	protected String getLinkUrl() {
		return "https://play.google.com/store/apps/details?id=net.tk.differentcolor";
	}
}
