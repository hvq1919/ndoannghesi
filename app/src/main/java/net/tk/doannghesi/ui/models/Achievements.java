package net.tk.doannghesi.ui.models;

import java.io.Serializable;

public class Achievements implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5960722402097730171L;

	private boolean achievement_1_questions;
	private boolean achievement_10_questions;
	private boolean achievement_50_questions;
	private boolean achievement_100_questions;
	private boolean achievement_500_questions;
	private boolean leaderboard_top_questions;

	public Achievements() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Achievements(boolean achievement_1_questions, boolean achievement_10_questions,
			boolean achievement_50_questions, boolean achievement_100_questions, boolean achievement_500_questions,
			boolean leaderboard_top_questions) {
		super();
		this.achievement_1_questions = achievement_1_questions;
		this.achievement_10_questions = achievement_10_questions;
		this.achievement_50_questions = achievement_50_questions;
		this.achievement_100_questions = achievement_100_questions;
		this.achievement_500_questions = achievement_500_questions;
		this.leaderboard_top_questions = leaderboard_top_questions;
	}

	public boolean isAchievement_1_questions() {
		return achievement_1_questions;
	}

	public void setAchievement_1_questions(boolean achievement_1_questions) {
		this.achievement_1_questions = achievement_1_questions;
	}

	public boolean isAchievement_10_questions() {
		return achievement_10_questions;
	}

	public void setAchievement_10_questions(boolean achievement_10_questions) {
		this.achievement_10_questions = achievement_10_questions;
	}

	public boolean isAchievement_50_questions() {
		return achievement_50_questions;
	}

	public void setAchievement_50_questions(boolean achievement_50_questions) {
		this.achievement_50_questions = achievement_50_questions;
	}

	public boolean isAchievement_100_questions() {
		return achievement_100_questions;
	}

	public void setAchievement_100_questions(boolean achievement_100_questions) {
		this.achievement_100_questions = achievement_100_questions;
	}

	public boolean isAchievement_500_questions() {
		return achievement_500_questions;
	}

	public void setAchievement_500_questions(boolean achievement_500_questions) {
		this.achievement_500_questions = achievement_500_questions;
	}

	public boolean isLeaderboard_top_questions() {
		return leaderboard_top_questions;
	}

	public void setLeaderboard_top_questions(boolean leaderboard_top_questions) {
		this.leaderboard_top_questions = leaderboard_top_questions;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
