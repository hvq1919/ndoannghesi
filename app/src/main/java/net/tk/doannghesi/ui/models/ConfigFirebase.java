package net.tk.doannghesi.ui.models;

import java.util.List;

public class ConfigFirebase {
    private boolean noads;
    private boolean onlyFacebookUser;
    private boolean show_admob;
    private int hienthi;
    private int qcs_ver;
    private int version_code;
    private String list_hacker;
    private String getKeyAdBuddiz;
    private AdsObject ads;
    private List<Qcs> qcs;

    public boolean isNoads() {
        return noads;
    }

    public void setNoads(boolean noads) {
        this.noads = noads;
    }

    public boolean isOnlyFacebookUser() {
        return onlyFacebookUser;
    }

    public void setOnlyFacebookUser(boolean onlyFacebookUser) {
        this.onlyFacebookUser = onlyFacebookUser;
    }

    public boolean isShow_admob() {
        return show_admob;
    }

    public void setShow_admob(boolean show_admob) {
        this.show_admob = show_admob;
    }

    public int getHienthi() {
        return hienthi;
    }

    public void setHienthi(int hienthi) {
        this.hienthi = hienthi;
    }

    public int getQcs_ver() {
        return qcs_ver;
    }

    public void setQcs_ver(int qcs_ver) {
        this.qcs_ver = qcs_ver;
    }

    public int getVersion_code() {
        return version_code;
    }

    public void setVersion_code(int version_code) {
        this.version_code = version_code;
    }

    public String getList_hacker() {
        return list_hacker;
    }

    public void setList_hacker(String list_hacker) {
        this.list_hacker = list_hacker;
    }

    public String getGetKeyAdBuddiz() {
        return getKeyAdBuddiz;
    }

    public void setGetKeyAdBuddiz(String getKeyAdBuddiz) {
        this.getKeyAdBuddiz = getKeyAdBuddiz;
    }

    public AdsObject getAds() {
        return ads;
    }

    public void setAds(AdsObject ads) {
        this.ads = ads;
    }

    public List<Qcs> getQcs() {
        return qcs;
    }

    public void setQcs(List<Qcs> qcs) {
        this.qcs = qcs;
    }

}


