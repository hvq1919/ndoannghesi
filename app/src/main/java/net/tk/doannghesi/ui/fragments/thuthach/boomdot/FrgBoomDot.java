package net.tk.doannghesi.ui.fragments.thuthach.boomdot;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.ui.dialog.DialogHintAThuThach;
import net.tk.doannghesi.ui.fragments.thuthach.FrgBaseThuThach;
import net.tk.doannghesi.ui.fragments.thuthach.boomdot.controls.MySurfaceView;
import net.tk.doannghesi.ui.fragments.thuthach.boomdot.controls.MySurfaceView.GAME_STATUS;
import net.tk.doannghesi.R;

public class FrgBoomDot extends FrgBaseThuThach {
	// private RelativeLayout gameOverLayout;
	//private Button btnChoiLai, btnChoiThu, btnNext;
	private MySurfaceView svImg;
	private DialogHintAThuThach dialogHintAThuThach;

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.frg_boomdot, container, false);

		// gameOverLayout = (RelativeLayout)
		// mRootView.findViewById(R.id.gameOverLayout);
		// gameOverLayout.setVisibility(View.GONE);

		// btnChoiLai = (Button) mRootView.findViewById(R.id.btnChoiLai);
		// btnChoiThu = (Button) mRootView.findViewById(R.id.btnLuyenTap);
		// btnNext = (Button) mRootView.findViewById(R.id.btnNext);
		svImg = (MySurfaceView) mRootView.findViewById(R.id.svImg);

		svImg.setmFrgBoomDot(this);

		// btnChoiLai.setOnClickListener(this);
		// btnChoiThu.setOnClickListener(this);
		// btnNext.setOnClickListener(this);

		String note = String.format(mResources.getString(R.string.note_boomdot), numThuThach);
		dialogHintAThuThach = DialogHintAThuThach.newInstance(mResources.getString(R.string.ten_thuthach_boomdot), note,
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						resetGame();
						SavedStore.editFirstHint(true);
						dialogHintAThuThach.dismiss();
					}
				});
		dialogHintAThuThach.show(mActivity.getFragmentManager(), "TAG");

		return mRootView;
	}

	public void gameOver() {
		mActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				showGameOver();
			}
		});
	}

	private void showGameOver() {
		// gameOverLayout.setVisibility(View.VISIBLE);

		int level = svImg.getLevel();
		if (level > numThuThach) // TODO win
			showDialogWin(mActivity);
		else {
			// TODO test
			// showDialogSaveMe(mActivity);

			if (level > 10) {
				showDialogSaveMe(mActivity);
			} else {
				onDimissConfirmDialogClick();
			}
			
		}

		

	}

	@Override
	protected void onDimissConfirmDialogClick() {
		super.onDimissConfirmDialogClick();

		// DialogGameOver dialogGameOver = getDialogGameOver();
		// dialogGameOver.setStringTextTitle(mResources.getString(R.string.thuthach_find_same_pic_gameover)
		// + "\n "
		// +
		// String.format(mResources.getString(R.string.thuthach_find_same_pic_need),
		// numThuThach));
		// dialogGameOver.show(mActivity.getFragmentManager(), "TAG");
		
		
	}

	@Override
	protected void onSaveMeSuccess() {
		super.onSaveMeSuccess();
		// Toaster.showToast(mActivity, "onSaveMeSuccess");

		svImg.setCurrentStatus(GAME_STATUS.GAME_NEW);
		// gameOverLayout.setVisibility(View.GONE);

		svImg.resetLevelSaveMe();
	}

	@Override
	public void onPause() {
		svImg.onPause();
		super.onPause();
	}

	@Override
	public void onResume() {
		svImg.onResume();
		super.onResume();
	}

	@Override
	protected void resetGame() {
		// playSound(soundGameOver);
		svImg.setCurrentStatus(GAME_STATUS.GAME_NEW);
		// gameOverLayout.setVisibility(View.GONE);

		svImg.resetLevel();

	}

	@Override
	protected String getLinkUrl() {
		// TODO Auto-generated method stub
		return "https://play.google.com/store/apps/details?id=net.tk.boomdot01";
	}
}
