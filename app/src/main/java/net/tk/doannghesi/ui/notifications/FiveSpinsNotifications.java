package net.tk.doannghesi.ui.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.NotificationCompat;

import net.tk.doannghesi.ui.activities.MainActivity;
import net.tk.doannghesi.R;

public class FiveSpinsNotifications {

	public static void showDialog(Context ctx) {
		Intent intent = new Intent(ctx, MainActivity.class);
		PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder b = new NotificationCompat.Builder(ctx);
		Resources r = ctx.getResources();

		b.setAutoCancel(true).setDefaults(Notification.DEFAULT_ALL).setWhen(System.currentTimeMillis())
				.setSmallIcon(R.drawable.ic_launcher).setTicker(r.getString(R.string.vua_moi_co))
				.setContentTitle(r.getString(R.string.app_name)).setContentText(r.getString(R.string.vua_moi_co))
				.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND).setContentIntent(contentIntent)
		// .setContentInfo("Info")
		;

		NotificationManager notificationManager = (NotificationManager) ctx
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(1, b.build());
	}

}
