package net.tk.doannghesi.ui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.facebook.FacebookHelper.OnLoginSuccess;
import net.tk.doannghesi.facebook.FacebookUser;
import net.tk.doannghesi.tools.log.Toaster;
import net.tk.doannghesi.ui.activities.MainActivity;
import net.tk.doannghesi.R;

public class DialogShareFafebook extends BaseFullScreenDialog {

    private MainActivity mActivity;

//	public DialogShareFafebook(MainActivity context) {
//		super();
//		this.mActivity = context;
//	}

    public static DialogShareFafebook newInstance(MainActivity context) {
        DialogShareFafebook dialogShareFafebook = new DialogShareFafebook();

        dialogShareFafebook.setmActivity(context);
        return dialogShareFafebook;
    }

    public void setmActivity(MainActivity mActivity) {
        this.mActivity = mActivity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.dialog_fragment_share, container, false);
        ImageView imgRating = (ImageView) rootView.findViewById(R.id.imgKhickLe);
        imgRating.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogShareFafebook.this.dismiss();

                doShareFacebook();

            }
        });

        return rootView;
    }

    private void doShareFacebook() {
        FacebookUser facebookUser = (FacebookUser) SavedStore.getFbUserObject();
        if (facebookUser != null)
            shareFacebook();
        else
            mActivity.getFacebookHelper().doLogin(new OnLoginSuccess() {
                @Override
                public void onLoginSuccess(FacebookUser facebookUser) {
                    if (facebookUser != null) {
                        SavedStore.saveFbUserObject(facebookUser);

                        //signInParse(facebookUser);
                        // signInParseTest(facebookUser);

                        // Logger.d("Facebook Object : " +
                        // facebookUser.toString());
                        Toaster.showToast(mActivity, "Success Logged Facebook");
                        shareFacebook();
                    } else
                        mActivity.showToast("Cann't login Facebook", true);
                }

            });

    }

    private void shareFacebook() {
        String url = "https://play.google.com/store/apps/details?id=net.tk.doannghesi";
        String contentTitle = "";
        String contentDescription = "";
        mActivity.getFacebookHelper().doShareLink(url, contentTitle, contentDescription);
    }

//    private void signInParse(final FacebookUser facebookUser) {
//        // mActivity.showLoadingDialog();
//
//        ParseUser parseUser = ParseUser.getCurrentUser();
//        if (parseUser == null)
//            return;
//
//        parseUser.put("AvatarUrl", facebookUser.getAvatarURL());
//        parseUser.put("FbId", facebookUser.getId());
//        parseUser.put("full_name", facebookUser.getName());
//
//        parseUser.saveInBackground();
//    }

}
