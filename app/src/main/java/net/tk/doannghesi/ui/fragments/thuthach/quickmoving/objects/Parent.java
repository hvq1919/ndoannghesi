package net.tk.doannghesi.ui.fragments.thuthach.quickmoving.objects;

import android.graphics.Canvas;
import android.graphics.Paint;

import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.Define;

import java.util.Random;

public abstract class Parent implements IParent {
	// protected float density;
	private int xPos = 0;
	private int yPos = 0;
	private int deltaX = 5;
	private int deltaY = 5;

	private float radius;
	private int color;

	private int width;
	private int height;
	private Paint paint;

	public Parent() {
		super();
		initPaint();
	}

	public Parent(int xPos, int yPos) {
		super();
		this.xPos = xPos;
		this.yPos = yPos;
		initPaint();
	}

	public Parent(int xPos) {
		super();
		this.xPos = xPos;
		initPaint();
	}

	private void initPaint() {
		paint = new Paint();
		paint.setFlags(Paint.ANTI_ALIAS_FLAG);
	}

	public void initParent(int w, int h) {
		createPosXY(w, h);
		width = w;
		height = h;

		initObject();
	}

	private void initObject() {
		radius = height / Define.RATE_HEIGHT_DOT;

	}

	public void initSpeed(int level) {
		int speed = 4;
		if (level > 5)
			speed = Math.round(2 + width / Define.RATE_SPEED + level);
		deltaX = (new Random().nextInt(speed) + 1);
		deltaY = (new Random().nextInt(speed) + 1);
	}

	public void createPosXY(int w, int h) {
		if (xPos == 0 && yPos == 0) {
			xPos = new Random().nextInt(w);
			yPos = new Random().nextInt(h);
		}
	}

	@Override
	public void draw(Canvas canvas, int w, int h) {
		xPos += deltaX;
		if (deltaX > 0) {
			if (xPos >= getWidth() - radius) {
				deltaX *= -1;
			}
		} else {
			if (xPos <= radius) {
				deltaX *= -1;
			}
		}

		yPos += deltaY;
		if (deltaY > 0) {
			if (yPos >= getHeight() - radius) {
				deltaY *= -1;
			}
		} else {
			if (yPos <= radius) {
				deltaY *= -1;
			}
		}

		canvas.drawCircle(xPos, yPos, radius, paint);
	}

	// -----------------------------------------------------------//
	// ---------------------- Setter && Getter ------------------ //
	// -----------------------------------------------------------//

	public int getxPos() {
		return xPos;
	}

	public void setxPos(int xPos) {
		this.xPos = xPos;
	}

	public int getyPos() {
		return yPos;
	}

	public void setyPos(int yPos) {
		this.yPos = yPos;
	}

	public int getDeltaX() {
		return deltaX;
	}

	public void setDeltaX(int deltaX) {
		this.deltaX = deltaX;
	}

	public int getDeltaY() {
		return deltaY;
	}

	public void setDeltaY(int deltaY) {
		this.deltaY = deltaY;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float f) {
		this.radius = f;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
		paint.setColor(color);
	}

}
