package net.tk.doannghesi.ui.dialog;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import net.tk.doannghesi.R;

public class DialogGameOver extends BaseFullScreenDialog {
    private OnClickListener choilaiOnclick, luyentapOnclick, nextOnclick;
    private TextView textTitle;

    private String stringTextTitle;

//	public DialogGameOver(OnClickListener choilaiOnclick, OnClickListener luyentapOnclick, OnClickListener nextOnclick) {
//		super();
//		this.choilaiOnclick = choilaiOnclick;
//		this.luyentapOnclick = luyentapOnclick;
//		this.nextOnclick = nextOnclick;
//	}

    public static DialogGameOver newInstance(OnClickListener choilaiOnclick, OnClickListener luyentapOnclick, OnClickListener nextOnclick) {
        DialogGameOver dialogGameOver = new DialogGameOver();
        dialogGameOver.setChoilaiOnclick(choilaiOnclick);
        dialogGameOver.setLuyentapOnclick(luyentapOnclick);
        dialogGameOver.setNextOnclick(nextOnclick);
        return dialogGameOver;
    }

    public void setChoilaiOnclick(OnClickListener choilaiOnclick) {
        this.choilaiOnclick = choilaiOnclick;
    }

    public void setLuyentapOnclick(OnClickListener luyentapOnclick) {
        this.luyentapOnclick = luyentapOnclick;
    }

    public void setNextOnclick(OnClickListener nextOnclick) {
        this.nextOnclick = nextOnclick;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.dialog_fragment_gameover, container, false);

        rootView.findViewById(R.id.btnChoiLai).setOnClickListener(choilaiOnclick);
        rootView.findViewById(R.id.btnLuyenTap).setOnClickListener(luyentapOnclick);
        rootView.findViewById(R.id.btnNext).setOnClickListener(nextOnclick);
        textTitle = (TextView) rootView.findViewById(R.id.textTitle);

        if (!TextUtils.isEmpty(stringTextTitle))
            textTitle.setText(stringTextTitle);

        return rootView;
    }

    public String getStringTextTitle() {
        return stringTextTitle;
    }

    public void setStringTextTitle(String stringTextTitle) {
        this.stringTextTitle = stringTextTitle;
    }

}
