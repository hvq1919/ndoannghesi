package net.tk.doannghesi.ui.activities;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import net.tk.doannghesi.ui.dialog.FullscreenLoadingDialog;
import net.tk.doannghesi.R;

public class BaseUtilsActivity extends BaseActivity {

	// Toast params
	private Toast mToast;
	private LinearLayout toast_layout_root;
	private ImageView image;
	private TextView text;

	// Loading Dialog
	private FullscreenLoadingDialog mLoadingDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		initToast();
		initLoadingDialog();
	}

	private void initLoadingDialog() {
		mLoadingDialog = new FullscreenLoadingDialog();
	}

	private void initToast() {
		mToast = new Toast(this);
		LayoutInflater inflater = getLayoutInflater();
		View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup) findViewById(R.id.toast_layout_root));

		toast_layout_root = (LinearLayout) layout.findViewById(R.id.toast_layout_root);
		image = (ImageView) layout.findViewById(R.id.image);
		text = (TextView) layout.findViewById(R.id.text);

		mToast.setDuration(Toast.LENGTH_SHORT);
		mToast.setView(layout);
	}

	public void showToast(String mess, boolean isError) {
		if (isError) {
			toast_layout_root.setBackgroundResource(R.drawable.shape_toast_error);
			image.setImageResource(R.drawable.toast_error);
		} else {
			toast_layout_root.setBackgroundResource(R.drawable.shape_toast_sucess);
			image.setImageResource(R.drawable.toast_success);
		}

		text.setText(mess);
		mToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
		mToast.show();
	}

	public void showLoadingDialog() {
		if (!mLoadingDialog.isAdded())
			mLoadingDialog.show(mFragmentManager, "TAG");
	}

	public void dismissLoadingDialog() {
		if (mLoadingDialog != null && mLoadingDialog.isAdded())
			mLoadingDialog.dismiss();
	}

}
