package net.tk.doannghesi.ui.controls.dialog;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.easyandroidanimations.library.Animation;
import com.easyandroidanimations.library.AnimationListener;
import com.easyandroidanimations.library.RotationAnimation;
import com.gc.materialdesign.views.ButtonFloatSmall;
import com.gc.materialdesign.views.ButtonRectangle;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.tools.log.Toaster;
import net.tk.doannghesi.tools.sound.SoundPoolHelper;
import net.tk.doannghesi.R;

import java.util.Random;

public class WheelDialog extends Dialog implements View.OnClickListener {
	private final int Degrees_MAX = 7200;
	private final int Degrees_MIN = 6840;
	private final int TIME_ROTATE = 5000;

	int[] coins = { 10, 5, 30, 10, 5 };
	int[] degrees_rage = { 30, 90, 150, 210, 270, 330 };

	private Context mContext;
	private Resources mResources;

	private Handler mHandler;

	private ButtonFloatSmall btnClose;
	private ButtonRectangle btnSpin;
	private ImageView imgWheel;
	private TextView text_tra_loi_dung_or_sai;
	private TextView textTimeSpin;
	private TextView textTotalSpin;

	private RotationAnimation rotationAnimation;

	/* The degree that wheel rotated , it's in range from 0 to 360 */
	private int degree_rotated = 0;
	private int degree_random;

	public interface OnCoinsCallBack {
		public void onCoins(int coin);
	}

	private OnCoinsCallBack mOnCoinsCallBack;

	private int soundBgsound;
	private SoundPoolHelper mSoundPoolHelper;

	private BroadcastReceiver receiver;

	public WheelDialog(Context context, OnCoinsCallBack onCoinsCallBack) {
		super(context);
		this.mContext = context;
		mResources = mContext.getResources();
		mHandler = new Handler();
		mOnCoinsCallBack = onCoinsCallBack;

		mSoundPoolHelper = new SoundPoolHelper(1, mContext);
		soundBgsound = mSoundPoolHelper.load(mContext, R.raw.song2, 1);

	}

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.wheel_dialog);

		this.setCancelable(false);

		initObjects();
		updateUi();

		btnClose.setOnClickListener(this);
		btnSpin.setOnClickListener(this);

		
	}

	private void initObjects() {
		btnClose = (ButtonFloatSmall) findViewById(R.id.btnClose);
		btnSpin = (ButtonRectangle) findViewById(R.id.btnSpin);
		imgWheel = (ImageView) findViewById(R.id.imgWheel);
		text_tra_loi_dung_or_sai = (TextView) findViewById(R.id.text_tra_loi_dung_or_sai);
		textTimeSpin = (TextView) findViewById(R.id.textTimeSpin);
		textTotalSpin = (TextView) findViewById(R.id.textTotalSpin);

		rotationAnimation = new RotationAnimation(imgWheel).setDuration(TIME_ROTATE)
				.setPivot(RotationAnimation.PIVOT_CENTER).setInterpolator(new AccelerateDecelerateInterpolator())
				.setListener(new AnimationListener() {
					@Override
					public void onAnimationEnd(Animation arg0) {
						mSoundPoolHelper.pause(soundBgsound);

						btnSpin.setEnabled(true);
						btnClose.setEnabled(true);

						degree_rotated = (degree_random + degree_rotated - 360) % 360;

						if (degree_rotated <= degrees_rage[0] || degree_rotated > degrees_rage[degrees_rage.length - 1]) {
							showCoin(0);
							mOnCoinsCallBack.onCoins(0);
						} else
							for (int i = 0; i < degrees_rage.length; i++)
								if (degrees_rage[i] < degree_rotated && degree_rotated <= degrees_rage[i + 1]) {
									mOnCoinsCallBack.onCoins(coins[i]);
									showCoin(coins[i]);
									break;
								}

					}

				});

	}

	private void updateUi() {
		updateSpin();
	}

	private void updateSpin() {
		textTotalSpin.setText("" + SavedStore.getSpin() + "/5");
	}

	@Override
	protected void onStop() {

		LocalBroadcastManager.getInstance(mContext).unregisterReceiver(receiver);

		super.onStop();
		mSoundPoolHelper.release();

	}

	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnClose:
			dismiss();
			break;

		case R.id.btnSpin:
			int spin = SavedStore.getSpin();
			if (spin - 1 < 0) {
				Toaster.showToast(mContext, mResources.getString(R.string.het_luot_quay));
				break;
			}
			SavedStore.editSpin(spin - 1);
			updateSpin();

			text_tra_loi_dung_or_sai.setVisibility(View.INVISIBLE);
			btnSpin.setEnabled(false);
			btnClose.setEnabled(false);

			/* play sound when spin */
			playSound(soundBgsound);

			degree_random = getDegreesRandom();
			rotationAnimation.setDegrees(degree_random);
			rotationAnimation.animate();
			break;
		default:
			break;
		}
	}

	private void showCoin(int i) {
		text_tra_loi_dung_or_sai.setVisibility(View.VISIBLE);
		text_tra_loi_dung_or_sai.setText(mResources.getString(R.string.ban_quay_duoc).replace("#", "" + i));
		text_tra_loi_dung_or_sai.setTextColor(mContext.getResources().getColor(R.color.main_color));

	}

	/**
	 * play a sound
	 * 
	 * @param soundId
	 */
	private void playSound(int soundId) {
		mSoundPoolHelper.play(soundId);
	}

	/**
	 * Get Degrees random
	 * 
	 * @return
	 */
	private int getDegreesRandom() {
		Random r = new Random();
		int i1 = r.nextInt(Degrees_MAX - Degrees_MIN) + Degrees_MIN;
		return i1;
	}
}
