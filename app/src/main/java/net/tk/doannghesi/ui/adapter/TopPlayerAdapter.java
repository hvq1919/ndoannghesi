package net.tk.doannghesi.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import net.tk.doannghesi.retrofit.objects.User;
import net.tk.doannghesi.ui.controls.CircleImageView;
import net.tk.doannghesi.R;

import java.util.ArrayList;

public class TopPlayerAdapter extends BaseCusAdapter<User> {
	private Context mContext;

	public TopPlayerAdapter(Context context, ArrayList<User> lists) {
		super(context, lists);
		mContext = context;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final User player = getItem(position);

		ViewHolder viewHolder; // view lookup cache stored in tag
		if (convertView == null) {
			viewHolder = new ViewHolder();
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.item_top_player, parent, false);
			viewHolder.textCoun = (TextView) convertView.findViewById(R.id.textCoun);
			viewHolder.textName = (TextView) convertView.findViewById(R.id.textName);
			viewHolder.textLevel = (TextView) convertView.findViewById(R.id.textLevel);

			viewHolder.ivAvatar = (ImageView) convertView.findViewById(R.id.ivAvatar);
			viewHolder.thienton = (ImageView) convertView.findViewById(R.id.thienton);
			viewHolder.vip = (ImageView) convertView.findViewById(R.id.vip);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		// Populate the data into the template view using the data object
		viewHolder.textCoun.setText((position + 1) + "");
		viewHolder.textName.setText(player.getFullname()/*getUsername()*/);
		viewHolder.textLevel.setText(player.getLevel() + "");

		// Check vip
		if (player.getVip()) {
			viewHolder.vip.setVisibility(View.VISIBLE);
		} else {
			viewHolder.vip.setVisibility(View.GONE);
		}

		// Check position
		if (position == 0) {
			viewHolder.thienton.setVisibility(View.VISIBLE);
			viewHolder.thienton.setImageResource(R.drawable.thienton);
			viewHolder.textName.setTextColor(Color.parseColor("#990000"));
			((CircleImageView) viewHolder.ivAvatar).setBorderWidth(2);
			((CircleImageView) viewHolder.ivAvatar).setBorderColor(Color.parseColor("#990000"));
		} else if (position == 1) {
			viewHolder.thienton.setVisibility(View.VISIBLE);
			viewHolder.thienton.setImageResource(R.drawable.nhikiem);
			viewHolder.textName.setTextColor(Color.parseColor("#84007A"));
			((CircleImageView) viewHolder.ivAvatar).setBorderWidth(2);
			((CircleImageView) viewHolder.ivAvatar).setBorderColor(Color.parseColor("#84007A"));
		} else if (position == 2) {
			viewHolder.thienton.setVisibility(View.VISIBLE);
			viewHolder.thienton.setImageResource(R.drawable.tamtinh);
			viewHolder.textName.setTextColor(Color.parseColor("#182265"));
			((CircleImageView) viewHolder.ivAvatar).setBorderWidth(2);
			((CircleImageView) viewHolder.ivAvatar).setBorderColor(Color.parseColor("#182265"));
		} else {
			viewHolder.thienton.setVisibility(View.GONE);
			viewHolder.textName.setTextColor(Color.parseColor("#333333"));
			((CircleImageView) viewHolder.ivAvatar).setBorderWidth(0);
			((CircleImageView) viewHolder.ivAvatar).setBorderColor(Color.parseColor("#333333"));

		}

		ImageLoader.getInstance().displayImage(player.getAvatarurl(), viewHolder.ivAvatar);

		// Return the completed view to render on screen
		return convertView;
	}

	// View lookup cache
	private static class ViewHolder {
		TextView textCoun, textName, textLevel;
		ImageView ivAvatar, thienton, vip;
	}

}
