package net.tk.doannghesi.ui.databases;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import net.tk.doannghesi.tools.encryptstring.CryptoUtilsString;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.tools.sqlite.SQLiteAssetHelper;
import net.tk.doannghesi.ui.models.Questions;

import java.util.ArrayList;

public class MyDatabase extends SQLiteAssetHelper {

	private static final String DATABASE_NAME = "database.db";
	private static final int DATABASE_VERSION = 1;

	public MyDatabase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

		// you can use an alternate constructor to specify a database location
		// (such as a folder on the sd card)
		// you must ensure that this folder is available and you have permission
		// to write to it
		// super(context, DATABASE_NAME,
		// context.getExternalFilesDir(null).getAbsolutePath(), null,
		// DATABASE_VERSION);

	}

	/**
	 * TODO test
	 * 
	 * @return
	 */
	public ArrayList<Questions> getQuestion() {
		Logger.d("getQuestion");
		SQLiteDatabase db = getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

		String[] sqlSelect = { "img_name", "answer", "wrong_answer", "note", "name" };
		String sqlTables = "questions";

		qb.setTables(sqlTables);
		Cursor c = qb.query(db, sqlSelect, null, null, null, null, null);

		ArrayList<Questions> questions = new ArrayList<Questions>();
		if (c.moveToFirst()) {
			do {
				Questions question = new Questions();
				question.setImg_name(c.getString(c.getColumnIndex(sqlSelect[0])));
				question.setAnswer(CryptoUtilsString.decryptBase64String(c.getString(c.getColumnIndex(sqlSelect[1]))));
				question.setWrong_answer(CryptoUtilsString.decryptBase64String(c.getString(c
						.getColumnIndex(sqlSelect[2]))));
				question.setNote(CryptoUtilsString.decryptBase64String(c.getString(c.getColumnIndex(sqlSelect[3]))));
				question.setName(CryptoUtilsString.decryptBase64String(c.getString(c.getColumnIndex(sqlSelect[4]))));

				// adding to todo list
				questions.add(question);
			} while (c.moveToNext());
		}

		return questions;

	}
}
