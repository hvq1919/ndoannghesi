package net.tk.doannghesi.ui.models;

import java.io.Serializable;

/**
 * Created by admin on 7/19/2016.
 */
public class AdsObject implements Serializable{
    private String message;
    private boolean show;
    private String title;
    private String url_icon;
    private String url_link;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl_icon() {
        return url_icon;
    }

    public void setUrl_icon(String url_icon) {
        this.url_icon = url_icon;
    }

    public String getUrl_link() {
        return url_link;
    }

    public void setUrl_link(String url_link) {
        this.url_link = url_link;
    }
}
