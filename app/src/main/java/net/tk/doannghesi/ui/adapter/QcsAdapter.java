package net.tk.doannghesi.ui.adapter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.retrofit.objects.Qcs;
import net.tk.doannghesi.ui.controls.OldRoundedBitmapDisplayer;
import net.tk.doannghesi.R;

import java.util.ArrayList;

/**
 * Created by admin on 8/17/2016.
 */
public abstract  class QcsAdapter extends BaseCusAdapter<Qcs> {
    private DisplayImageOptions options;

    private ArrayList<String> listInstall;

    private Context mContext;
    public QcsAdapter(Context context, ArrayList<Qcs> lists) {
        super(context, lists);
        mContext = context;

        listInstall = SavedStore.getQcsInstall();

        options = new DisplayImageOptions.Builder()
                .displayer(new OldRoundedBitmapDisplayer(10)) //rounded corner bitmap
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .build();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Qcs qcs = getItem(position);

        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_qcs, parent, false);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            viewHolder.tvContent = (TextView) convertView.findViewById(R.id.tvContent);
            viewHolder.tvSetting = (TextView) convertView.findViewById(R.id.tvSetting);

            viewHolder.imgIcon = (ImageView) convertView.findViewById(R.id.imgIcon);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data into the template view using the data object
        viewHolder.tvTitle.setText(qcs.getName());
        //viewHolder.tvContent.setText(qcs.getContent());

        int color = Color.BLUE;
        if (qcs.getCoins() >= 200) color = Color.RED;

        String text1 = qcs.getContent() + " để nhận ";
        String text2 = " coins";

        Spannable wordtoSpan = new SpannableString(text1 + qcs.getCoins() + text2);
        wordtoSpan.setSpan(new ForegroundColorSpan(color), text1.length(), text1.length() + ("" + qcs.getCoins()).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new RelativeSizeSpan(1.15f), text1.length(), text1.length() + ("" + qcs.getCoins()).length(), 0);
        viewHolder.tvContent.setText(wordtoSpan);

        if (listInstall.contains(qcs.getPackageName()) || isAppInstalled(qcs.getPackageName())) {
            viewHolder.tvSetting.setActivated(true);
            viewHolder.tvSetting.setText("Đã nhận");
            viewHolder.tvSetting.setOnClickListener(null);
            listInstall.add(qcs.getPackageName());
            SavedStore.editQcsInstall(listInstall);
        } else {
            viewHolder.tvSetting.setText("Cài đặt");
            viewHolder.tvSetting.setActivated(false);
            viewHolder.tvSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoGooglePlay(qcs);
                }
            });
        }

        ImageLoader.getInstance().displayImage(qcs.getUrlIcon(), viewHolder.imgIcon, options);


        // Return the completed view to render on screen
        return convertView;
    }

    private boolean isAppInstalled(String packageName) {
        PackageManager pm = mContext.getPackageManager();
        boolean installed = false;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            installed = false;
        }
        return installed;
    }

    static class ViewHolder {
        TextView tvTitle, tvContent, tvSetting;
        ImageView imgIcon;
    }

    public abstract void gotoGooglePlay(Qcs qcs);
}
