package net.tk.doannghesi.ui.fragments.thuthach.boomdot.objects;

import android.graphics.Canvas;
import android.graphics.Color;

public class Blue extends Parent {
	private int color = Color.parseColor("#2196F3");// Blue
	private int radius = 20;

	public Blue() {
		super();
		init();
	}

	public Blue(int xPos, int yPos) {
		super(xPos, yPos);
		init();
	}



	public Blue(int r) {
		super();
		init(r);
	}
	
	private void init() {
		setRadius(radius);
		setColor(color);
	}

	private void init(int r) {
		setRadius(r);
		setColor(color);
	}
	
	@Override
	public void draw(Canvas canvas, int w, int h) {
		super.draw(canvas, w, h);
	}

	// -----------------------------------------------------------//
	// ---------------------- Setter && Getter ------------------ //
	// -----------------------------------------------------------//

}
