package net.tk.doannghesi.ui.fragments.thuthach.trollertap;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.tools.sound.SoundPoolHelper;
import net.tk.doannghesi.ui.dialog.DialogHintAThuThach;
import net.tk.doannghesi.ui.fragments.thuthach.FrgBaseThuThach;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.controls.MySurfaceView;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.controls.MySurfaceView.GAME_STATUS;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.controls.MySurfaceView.IGameStatus;
import net.tk.doannghesi.R;

import java.util.Random;

public class FrgTrollerTap extends FrgBaseThuThach {

	private MySurfaceView svImg;

	private LinearLayout rootView;

	// private RelativeLayout llGameOver;
	// private TextView tvTroller;
	// private ImageView imgReplay, imgTroller/* , imgRateme, imgShare */;

	private Random mRandom = new Random();
	/* Sound */
	private SoundPoolHelper mSoundPoolHelper;
	public int soundClick;
	public int soundTap;
	public int soundNewLevel;
	public int soundGameOver;

	private DialogHintAThuThach dialogHintAThuThach;

	private void loadSound() {
		mSoundPoolHelper = new SoundPoolHelper(2, mActivity);
		soundClick = mSoundPoolHelper.load(mActivity, R.raw.click, 3);
		soundTap = mSoundPoolHelper.load(mActivity, R.raw.jump2, 2);
		soundNewLevel = mSoundPoolHelper.load(mActivity, R.raw.newlevel, 1);
		soundGameOver = mSoundPoolHelper.load(mActivity, R.raw.gameover, 4);

	}

	/**
	 * play a sound
	 * 
	 * @param soundId
	 */
	public void playSound(int soundId) {
		if (!SavedStore.getSound())
			mSoundPoolHelper.play(soundId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mSoundPoolHelper.release();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.frg_troller_tap, container, false);

		rootView = (LinearLayout) mRootView.findViewById(R.id.rootView);
		svImg = (MySurfaceView) mRootView.findViewById(R.id.svImg);

		// llGameOver = (RelativeLayout)
		// mRootView.findViewById(R.id.rlGameOver);
		// imgReplay = (ImageView) mRootView.findViewById(R.id.imgReplay);
		// imgTroller = (ImageView) mRootView.findViewById(R.id.imgTroller);
		// tvTroller = (TextView) mRootView.findViewById(R.id.tvTroller);

		// imgReplay.setOnClickListener(this);

		loadSound();
		setBgRootView(Color.parseColor(Define.BG_VAT_CAN));

		setListener();

		svImg.setMainFragment(this);

		String note = String.format(mResources.getString(R.string.note_taptroller), numThuThach);
		dialogHintAThuThach =  DialogHintAThuThach.newInstance(mResources.getString(R.string.ten_thuthach_tap_troller), note,
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						resetGame();
						SavedStore.editFirstHint(true);
						dialogHintAThuThach.dismiss();
					}
				});
		try {
			dialogHintAThuThach.show(mActivity.getFragmentManager(), "TAG");
		}catch (Exception e){}
		return mRootView;
	}

	private void setListener() {
		svImg.setIGameStatus(new IGameStatus() {

			@Override
			public void onGameRunning() {
				gameRunning();
			}

			@Override
			public void onGameOver() {
				gameOver();

			}

		});
	}

	public void gameRunning() {
		// llGameOver.setVisibility(View.GONE);
	}

	public void gameOver() {
		mActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				showGameOver();
			}
		});
	}

	private void setBgRootView(int color) {
		rootView.setBackgroundColor(color);
	}

	private void showGameOver() {
		svImg.setCurrentStatus(GAME_STATUS.GAME_OVER);

		int level = svImg.getCurrentLevel();
		// Toaster.showToast(mActivity, "Level:" + level);
		if ((level + 1) > numThuThach) // TODO win
			showDialogWin(mActivity);
		else {
			// TODO test
			// showDialogSaveMe(mActivity);
			//
			if (level > 10) {
				showDialogSaveMe(mActivity);
			} else {
				onDimissConfirmDialogClick();
			}
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		svImg.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		svImg.onPause();
	}

	@Override
	public void onStart() {
		super.onStart();

		// googleApiClientConnect();
	}

	public MySurfaceView getSvImg() {
		return svImg;
	}

	public void setSvImg(MySurfaceView svImg) {
		this.svImg = svImg;
	}

	@Override
	protected void onSaveMeSuccess() {
		super.onSaveMeSuccess();
		svImg.restartGameSaveMe(svImg.view_width, svImg.view_height);
		setBgRootView(Color.parseColor(Define.BG_VAT_CAN));
	}

	@Override
	protected void resetGame() {
		// llGameOver.setVisibility(View.GONE);

		svImg.restartGame(svImg.view_width, svImg.view_height);
		setBgRootView(Color.parseColor(Define.BG_VAT_CAN));

	}

	@Override
	protected String getLinkUrl() {
		return "https://play.google.com/store/apps/details?id=net.tk.dottapy";
	}
}
