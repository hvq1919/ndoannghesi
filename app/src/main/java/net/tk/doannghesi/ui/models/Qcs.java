package net.tk.doannghesi.ui.models;

import java.io.Serializable;

/**
 * Created by admin on 8/17/2016.
 */
public class Qcs implements Serializable {
    private int coins;
    private String content;
    private String name;
    private String package_name;
    private String url_icon;

    public int getCoins() {
        return coins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public String getUrl_icon() {
        return url_icon;
    }

    public void setUrl_icon(String url_icon) {
        this.url_icon = url_icon;
    }
}
