package net.tk.doannghesi.ui.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.retrofit.objects.Artist;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.ui.adapter.NgheSiAdapter;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgNgheSi extends BaseFragment implements OnClickListener {

    private ListView mListView;

    private NgheSiAdapter mNgheSiAdapter;

    private Button btnTopTuan, btnAll;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frg_nghesi, container, false);
        mListView = (ListView) mRootView.findViewById(R.id.listView);

        //mNgheSiAdapter = new NgheSiAdapter(mActivity, new ArrayList<ArtistKey>());
        mNgheSiAdapter = new NgheSiAdapter(mActivity, new ArrayList<Artist>());
        mListView.setAdapter(mNgheSiAdapter);

        btnAll = (Button) mRootView.findViewById(R.id.btn_all);
        btnTopTuan = (Button) mRootView.findViewById(R.id.btn_top_tuan);

        mRootView.findViewById(R.id.back).setOnClickListener(this);
        mRootView.findViewById(R.id.img_gopy_nghesi).setOnClickListener(this);
        mRootView.findViewById(R.id.btn_all).setOnClickListener(this);
        mRootView.findViewById(R.id.btn_top_tuan).setOnClickListener(this);


        // TODO Just added
        getListNgheSiFireBase();

        btnTopTuan.setActivated(true);
        getTopWeek();

        return mRootView;
    }


    private void getListNgheSiFireBase(){
        Logger.e("=== GetListUserFireBase");
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("NgheSi");
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Logger.e("=== There are " + dataSnapshot.getChildrenCount() + " players");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getTopWeek() {
        mActivity.showLoadingDialog();

        mActivity.mAPIService.getListArtistWeek().enqueue(new Callback<List<Artist>>() {
            @Override
            public void onResponse(Call<List<Artist>> call, Response<List<Artist>> response) {

                ArrayList<Artist> artists = new ArrayList<Artist>(response.body());
                Log.e("TAG", "Number of Artist:" + artists.size());
                mActivity.dismissLoadingDialog();

                mNgheSiAdapter.addAllNewList(artists);

                if (!SavedStore.getFirstNgheSi()) {
                    SavedStore.editFirstNgheSi(true);
                    mActivity.showToast("Click avatar xem người like", false);
                }
            }

            @Override
            public void onFailure(Call<List<Artist>> call, Throwable t) {
                Log.e("TAG", "Unable to submit post to API.");
                mActivity.dismissLoadingDialog();
            }
        });
    }

    private void getAll() {
        mActivity.showLoadingDialog();

        mActivity.mAPIService.getListArtist().enqueue(new Callback<List<Artist>>() {
            @Override
            public void onResponse(Call<List<Artist>> call, Response<List<Artist>> response) {

                ArrayList<Artist> artists = new ArrayList<Artist>(response.body());
                Log.e("TAG", "Number of Artist:" + artists.size());
                mActivity.dismissLoadingDialog();

                mNgheSiAdapter.addAllNewList(artists);
            }

            @Override
            public void onFailure(Call<List<Artist>> call, Throwable t) {
                Log.e("TAG", "Unable to submit post to API.");
                mActivity.dismissLoadingDialog();
            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                mActivity.popFragment();
                break;
            case R.id.img_gopy_nghesi:
                mActivity.pushFragment(new FrgSuggestionArtist(), true);
                break;
            case R.id.btn_all:
                if (!btnAll.isActivated()) {
                    btnAll.setActivated(true);
                    btnTopTuan.setActivated(false);
                    getAll();
                }
                break;
            case R.id.btn_top_tuan:
                if (!btnTopTuan.isActivated()) {
                    btnAll.setActivated(false);
                    btnTopTuan.setActivated(true);
                    getTopWeek();
                }
                break;
            default:
                break;
        }
    }

}
