package net.tk.doannghesi.ui.models;

/**
 * Created by quanhv on 2/17/2016.
 */
public class Player_Artist {
    private int like;
    private String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }
}
