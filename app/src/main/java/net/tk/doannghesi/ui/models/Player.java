package net.tk.doannghesi.ui.models;

import java.io.Serializable;

public class Player implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String username;
	private String fullname;
	private String fbid;
	private String avatarurl;
	private int level;
	private boolean vip;

	public Player(String username, String fullname, String fbid, String avatarurl, int level, boolean vip) {
		this.username = username;
		this.fullname = fullname;
		this.fbid = fbid;
		this.avatarurl = avatarurl;
		this.level = level;
		this.vip = vip;
	}

	public Player() {
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	public String getUsername() {
		return username;
	}


	public String getFullname() {
		return fullname;
	}

	public String getFbid() {
		return fbid;
	}

	public String getAvatarurl() {
		return avatarurl;
	}

	public int getLevel() {
		return level;
	}

	public boolean getVip() {
		return vip;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public void setFbid(String fbid) {
		this.fbid = fbid;
	}

	public void setAvatarurl(String avatarurl) {
		this.avatarurl = avatarurl;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public void setVip(boolean vip) {
		this.vip = vip;
	}
}


