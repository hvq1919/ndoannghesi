package net.tk.doannghesi.ui.dialog;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.tk.doannghesi.R;

public class DialogWinThuThach extends BaseFullScreenDialog {
    private OnClickListener choilaiOnclick, luyentapOnclick, nextOnclick;

    private ImageView imgWin;
    private TextView textTitle, textCoins;

    public interface IOnLayout {
        public void onLayoutBuild();
    }

    private IOnLayout mOnLayout;

//	public DialogWinThuThach(OnClickListener choilaiOnclick, OnClickListener luyentapOnclick,
//			OnClickListener nextOnclick) {
//		super();
//		this.choilaiOnclick = choilaiOnclick;
//		this.luyentapOnclick = luyentapOnclick;
//		this.nextOnclick = nextOnclick;
//	}


    public static DialogWinThuThach newInstance(OnClickListener choilaiOnclick, OnClickListener luyentapOnclick,
                                                OnClickListener nextOnclick) {
        DialogWinThuThach dialogWinThuThach = new DialogWinThuThach();
        dialogWinThuThach.setChoilaiOnclick(choilaiOnclick);
        dialogWinThuThach.setLuyentapOnclick(luyentapOnclick);
        dialogWinThuThach.setNextOnclick(nextOnclick);

        return dialogWinThuThach;
    }


    public void setChoilaiOnclick(OnClickListener choilaiOnclick) {
        this.choilaiOnclick = choilaiOnclick;
    }

    public void setLuyentapOnclick(OnClickListener luyentapOnclick) {
        this.luyentapOnclick = luyentapOnclick;
    }

    public void setNextOnclick(OnClickListener nextOnclick) {
        this.nextOnclick = nextOnclick;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.dialog_fragment_win, container, false);

        Log.d("TAG", "Dialog Fragment onCreateView");

        rootView.findViewById(R.id.btnChoiLai).setOnClickListener(choilaiOnclick);
        rootView.findViewById(R.id.btnLuyenTap).setOnClickListener(luyentapOnclick);
        rootView.findViewById(R.id.btnNext).setOnClickListener(nextOnclick);

        imgWin = (ImageView) rootView.findViewById(R.id.imgWin);
        textTitle = (TextView) rootView.findViewById(R.id.textTitle);
        textCoins = (TextView) rootView.findViewById(R.id.textCoins);

        if (mOnLayout != null)
            mOnLayout.onLayoutBuild();
        return rootView;
    }


    public void setOnLayout(IOnLayout mOnLayout) {
        this.mOnLayout = mOnLayout;
    }

    public void setTextTitle(String title) {
        textTitle.setText(title);
    }

    public void setTextCoins(String text) {
        Log.d("TAG", "Dialog Fragment setTextCoins");
        textCoins.setText(text);
    }

    public void setImage(int resId) {
        imgWin.setImageResource(resId);
    }

}
