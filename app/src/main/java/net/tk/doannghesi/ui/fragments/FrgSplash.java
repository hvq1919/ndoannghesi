package net.tk.doannghesi.ui.fragments;

import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.retrofit.objects.User;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgSplash extends BaseFragment  {
	private final int TIME = 2000; 

	private ImageView img;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mRootView = inflater.inflate(R.layout.frg_splash, container, false);

		img = (ImageView) mRootView.findViewById(R.id.img);

		// TODO comment this line to get only user from facebook
		// if(!SavedStore.getOnlyFbUser()) signInFireBase();
		createUser();

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(TIME);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(TIME);
		fadeOut.setDuration(TIME);

		AnimationSet animation = new AnimationSet(false);
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);
		animation.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				mActivity.pushFragment(new FrgHome(), false);
				//mActivity.pushFragment(new FrgMemories(), false);

			}
		});
		img.setAnimation(animation);

		// }

		// TODO test

		// mActivity.pushFragment(new FrgFindSamePic(), true);

		//SavedStore.editCoin(90000);
		// SavedStore.editKC(15);
		// SavedStore.editSpin(15);
		// SavedStore.editIsVip(false);

	 	//SavedStore.editCurrentNumberQuestion("" + 705);
		//SavedStore.editCurrentOnlineQuestion(327);
		//SavedStore.editIsOnlineQuestion(true);

		return mRootView;

	}

	/**
	 * To auto create new user via device id.
	 *
	 */
	private void createUser() {
		String deviceId = Settings.Secure.getString(mActivity.getContentResolver(),
				Settings.Secure.ANDROID_ID);

		User user = new User();
		user.setDeviceId(deviceId);
		user.setFullname(deviceId);

		mActivity.mAPIService.createUser(user).enqueue(new Callback<BaseObject<User>>() {
			@Override
			public void onResponse(Call<BaseObject<User>> call, Response<BaseObject<User>> response) {

				if(response.body() != null && response.isSuccessful() && response.body().getData()!= null){
					Log.e("TAG", "Success:" + response.body().isSuccess());
					Log.e("TAG","Mess:" + response.body().getMessage());
					Log.e("TAG","Device Id:" + response.body().getData().getDeviceId());
				}
			}

			@Override
			public void onFailure(Call<BaseObject<User>> call, Throwable t) {
				Log.e("TAG", "Response from server is invalid format.");
			}
		});
	}


}
