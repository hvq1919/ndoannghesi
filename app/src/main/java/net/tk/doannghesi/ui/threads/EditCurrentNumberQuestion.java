package net.tk.doannghesi.ui.threads;

import android.os.AsyncTask;

import net.tk.doannghesi.api.SavedStore;

public class EditCurrentNumberQuestion extends AsyncTask<Void, Void, Void> {
	private int currentNumberQuestion;
	private OnDone onDone;

	public EditCurrentNumberQuestion(int currentNumberQuestion, OnDone done) {
		super();
		this.currentNumberQuestion = currentNumberQuestion;
		this.onDone = done;
	}

	@Override
	protected Void doInBackground(Void... params) {
		SavedStore.editCurrentNumberQuestion("" + (currentNumberQuestion + 1));
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		if (onDone != null)
			onDone.doOnDone();
	}

	public interface OnDone {
		public void doOnDone();
	}
}
