package net.tk.doannghesi.ui.models;

import android.view.View;

public class ObjectHint {
	private int id;
	private String text;
	private View child;
	
	public ObjectHint(int id, String text, View child) {
		super();
		this.id = id;
		this.text = text;
		this.child = child;
	}
	public ObjectHint() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ObjectHint(int id, String text) {
		super();
		this.id = id;
		this.text = text;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public View getChild() {
		return child;
	}
	public void setChild(View child) {
		this.child = child;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
}
