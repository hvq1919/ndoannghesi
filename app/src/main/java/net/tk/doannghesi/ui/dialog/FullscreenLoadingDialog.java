package net.tk.doannghesi.ui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import net.tk.doannghesi.R;

public class FullscreenLoadingDialog extends BaseFullScreenDialog {

	public static final String TAG = FullscreenLoadingDialog.class.getSimpleName();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.dialog_fragment_loading, container, false);
		final ImageView ivLoading = (ImageView) rootView.findViewById(R.id.iv_loading);
		ivLoading.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_loading));
		return rootView;
	}

}
