package net.tk.doannghesi.ui.fragments.thuthach.trollertap;

import android.graphics.Color;

import net.tk.doannghesi.R;

public class Define {
	public final static String[] BACKGROUND = { "#B0BEC5", "#BCAAA4", "#B39DDB", "#64B5F6", "#81C784", "#AFB42B",
			"#FFA726", "#FFAB91", "#EF9A9A", "#B71C1C", "#FF6D00", "#1B5E20", "#01579B", "#311B92", "#263238",
			"#3E2723" };

	public final static int MAIN_COLOR_DEFAULT = Color.parseColor("#FFEE58");

	public final static String BG_VAT_CAN = "#607D8B";
	public final static String BG_VAT_CAN_SPEED = "#6A1B9A";
	public final static String BG_VAT_CAN_WIDTH_HEIGHT = "#6D4C41";

	public final static String MAIN_MENU_COLOR = "#E64A19";

	public final static int RATE_WIDTH_DEFAULT = 8;
	public final static int RATE_HEIGHT_DEFAULT = 10;
	public final static int RATE_SPEED_DEFAULT =  450;// 400

	// public final static int RATE_WIDTH_GIAM = 12;
	// public final static int RATE_HEIGHT_GIAM = 12;
	// public final static int RATE_SPEED_GIAM1 = 450;
	// public final static int RATE_SPEED_GIAM2 = 500;

	public final static int RATE_G_DEFAULT = 120;
	public final static int RATE_V0_DEFAULT = 16;
	public final static float TIME_TANG_DEFAULT = 0.25f;

	public final static float[] RATE_SHADOW = { 0.5f, 0.8f, 1f, 1.2f };
	public final static int[] RATE_SHADOW_COS = { 500, 700, 1000 };

	public final static String shadow_width_color1 = "#97AAB3";
	public final static String shadow_width_color2 = "#7E95A0";
	public final static String shadow_width_color3 = "#5B727D";

	public final static int shadow_width_colorint1 = Color.parseColor(shadow_width_color1);
	public final static int shadow_width_colorint2 = Color.parseColor(shadow_width_color2);
	public final static int shadow_width_colorint3 = Color.parseColor(shadow_width_color3);

	public final static int[] shadow_width_colorint = { shadow_width_colorint1, shadow_width_colorint2,
			shadow_width_colorint3 };

	public final static String shadow_color1 = "#65CD07";
	public final static String shadow_color2 = "#359905";
	public final static String shadow_color3 = "#036564";
	public final static String shadow_color4 = "#06329D";
	public final static String shadow_color5 = "#040263";
	public final static String shadow_color6 = "#620361";
	public final static String shadow_color7 = "#CE0203";
	public final static String shadow_color8 = "#FD3305";
	public final static String shadow_color9 = "#FE640A";
	public final static String shadow_color10 = "#FCFD0C";
	public final static String shadow_color11 = "#2A7FFF";
	public final static String shadow_color12 = "#7F55FF";

	public final static int shadow_colorint1 = Color.parseColor(shadow_color1);
	public final static int shadow_colorint2 = Color.parseColor(shadow_color2);
	public final static int shadow_colorint3 = Color.parseColor(shadow_color3);
	public final static int shadow_colorint4 = Color.parseColor(shadow_color4);
	public final static int shadow_colorint5 = Color.parseColor(shadow_color5);
	public final static int shadow_colorint6 = Color.parseColor(shadow_color6);
	public final static int shadow_colorint7 = Color.parseColor(shadow_color7);
	public final static int shadow_colorint8 = Color.parseColor(shadow_color8);
	public final static int shadow_colorint9 = Color.parseColor(shadow_color9);
	public final static int shadow_colorint10 = Color.parseColor(shadow_color10);
	public final static int shadow_colorint11 = Color.parseColor(shadow_color11);
	public final static int shadow_colorint12 = Color.parseColor(shadow_color12);

	public final static int[] shadow_colorint = { shadow_colorint1, shadow_colorint2, shadow_colorint3,
			shadow_colorint4, shadow_colorint5, shadow_colorint6, shadow_colorint7, shadow_colorint8, shadow_colorint9,
			shadow_colorint10, shadow_colorint11, shadow_colorint12 };

	public final static String main_color1 = "#26C6DA";
	public final static String main_color2 = "#29B6F6";
	public final static String main_color3 = "#26A69A";
	public final static String main_color4 = "#5C6BC0";
	public final static String main_color5 = "#7E57C2";
	public final static String main_color6 = "#AB47BC";
	public final static String main_color7 = "#FFA726";
	public final static String main_color8 = "#FF7043";
	public final static String main_color9 = "#EC407A";
	public final static String main_color10 = "#D4E157";
	public final static String main_color11 = "#66BB6A";
	public final static String main_color12 = "#8D6E63";

	public final static int main_colorint1 = Color.parseColor(main_color1);
	public final static int main_colorint2 = Color.parseColor(main_color2);
	public final static int main_colorint3 = Color.parseColor(main_color3);
	public final static int main_colorint4 = Color.parseColor(main_color4);
	public final static int main_colorint5 = Color.parseColor(main_color5);
	public final static int main_colorint6 = Color.parseColor(main_color6);
	public final static int main_colorint7 = Color.parseColor(main_color7);
	public final static int main_colorint8 = Color.parseColor(main_color8);
	public final static int main_colorint9 = Color.parseColor(main_color9);
	public final static int main_colorint10 = Color.parseColor(main_color10);
	public final static int main_colorint11 = Color.parseColor(main_color11);
	public final static int main_colorint12 = Color.parseColor(main_color12);

	public final static int[] main_colorint = { main_colorint1, main_colorint2, main_colorint3, main_colorint4,
			main_colorint5, main_colorint6, main_colorint7, main_colorint8, main_colorint9, main_colorint10,
			main_colorint11, main_colorint12 };

	public static final int RC_SIGN_IN = 9001;
	public static final int RC_REQUEST = 10001;

	// Drawable
	public final static int[] level1_10 = { R.drawable.t_1_10_1, R.drawable.t_1_10_2, R.drawable.t_1_10_3,
			R.drawable.t_1_10_4, R.drawable.t_1_10_5 };
	public final static int[] level11_20 = { R.drawable.t_11_20_1, R.drawable.t_11_20_2, R.drawable.t_11_20_3,
			R.drawable.t_11_20_4, R.drawable.t_11_20_5 };
	public final static int[] level21_40 = { R.drawable.t_21_31_1, R.drawable.t_21_31_2, R.drawable.t_21_31_3,
			R.drawable.t_21_31_4, R.drawable.t_21_31_5 };
	public final static int[] level41_99 = { R.drawable.t_41_99_1, R.drawable.t_41_99_2, R.drawable.t_41_99_3,
			R.drawable.t_41_99_4, R.drawable.t_41_99_5, R.drawable.t_41_99_6 };
	public final static int[] level100 = { R.drawable.t_100_1, R.drawable.t_100_2 };

	// String
	public final static int[] level1_10_string = { R.string.level1_10_1, R.string.level1_10_2, R.string.level1_10_3,
			R.string.level1_10_4 };
	public final static int[] level11_20_string = { R.string.level11_20_1, R.string.level11_20_2,
			R.string.level11_20_3, R.string.level11_20_4 };
	public final static int[] level21_40_string = { R.string.level21_40_1, R.string.level21_40_2,
			R.string.level21_40_3, R.string.level21_40_4 };
	public final static int[] level41_99_string = { R.string.level41_99_1, R.string.level41_99_2,
			R.string.level41_99_3, R.string.level41_99_4, R.string.level41_99_5 };
	public final static int[] level100_string = { R.string.level100 };
}
