package net.tk.doannghesi.ui.threads;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.AsyncTask;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.R;

import java.util.Random;

public class BackGroundSound extends AsyncTask<Void, Void, Void> {
    private Context mContext;

    private MediaPlayer player;

    public BackGroundSound(Context mContext) {
        super();
        this.mContext = mContext;
    }

    @Override
    protected Void doInBackground(Void... params) {
        int bg = R.raw.bg1;
        int i = new Random().nextInt(4);
        if (i == 0) bg = R.raw.bg1;
        else if (i == 1) bg = R.raw.bg2;
        else if (i == 2) bg = R.raw.bg3;
        else bg = R.raw.bg4;

        player = MediaPlayer.create(mContext, bg);
        player.setLooping(true); // Set looping
        player.setVolume(50, 50);
        // player.start();

        return null;
    }

    public void startSound() {
        if (SavedStore.getMusic() && player != null)
            player.start();
    }

    public void pauseSound() {
        if (player != null) player.pause();
    }

    public void releaseSound() {
        if (player != null) player.release();
    }

}
