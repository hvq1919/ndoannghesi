package net.tk.doannghesi.ui.fragments.thuthach.quickmoving;

public class Define {
	
	public static final int RC_SIGN_IN = 9001;
	public static final int RC_REQUEST = 10001;
	
	
	public final static String[] BACKGROUND = { "#240502", "#422F05", "#1D4205", "#063833", "#04092E", "#1B0326",
			"#101010" };

	public final static float TANG_RATE_SPEED1 = 1.3f;
	public final static float TANG_RATE_RADIUS1 = 1.3f;

	public final static float TANG_RATE_SPEED2 = 0.7f;
	public final static float TANG_RATE_RADIUS2 = 0.7f;

	public final static float RATE_HEIGHT_MAIN = 50;
	public final static float RATE_HEIGHT_DOT = 60;
	public final static float RATE_SPEED = 360;
}
