package net.tk.doannghesi.ui.fragments.thuthach.quickmoving.objects;

import android.graphics.Canvas;
import android.graphics.Color;

public class Red extends Parent {

	private int color = Color.parseColor("#F44336");// Red

	public Red() {
		super();
		init();
	}

	public Red(int xPos, int yPos) {
		super(xPos, yPos);
		init();
	}

	public Red(int xPos) {
		super(xPos);
		init();
	}

	private void init() {
		setColor(color);
	}

	@Override
	public void draw(Canvas canvas, int w, int h) {
		super.draw(canvas, w, h);
	}
}
