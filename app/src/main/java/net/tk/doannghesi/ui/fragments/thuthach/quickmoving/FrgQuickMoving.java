package net.tk.doannghesi.ui.fragments.thuthach.quickmoving;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.ui.dialog.DialogHintAThuThach;
import net.tk.doannghesi.ui.fragments.thuthach.FrgBaseThuThach;
import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.controls.MySurfaceView;
import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.controls.MySurfaceView.GAME_STATUS;
import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.controls.MySurfaceView.IGameStatus;
import net.tk.doannghesi.R;

public class FrgQuickMoving extends FrgBaseThuThach implements OnClickListener {

	private MySurfaceView svImg;
	//private RelativeLayout relGameOver;

	boolean isPause;

	private DialogHintAThuThach dialogHintAThuThach;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.frg_quickmoving, container, false);

		//mRootView.findViewById(R.id.playGame).setOnClickListener(this);

		initObjects();
		setListener();

		
		String note = String.format(mResources.getString(R.string.note_quickmoving), numThuThach);
		dialogHintAThuThach = DialogHintAThuThach.newInstance(mResources.getString(R.string.ten_thuthach_quickmoving), note,
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						resetGame();
						SavedStore.editFirstHint(true);
						dialogHintAThuThach.dismiss();
					}
				});
		try{
			dialogHintAThuThach.show(mActivity.getFragmentManager(), "TAG");
		}catch (Exception e){}
		
		return mRootView;
	}

	private void initObjects() {
		svImg = (MySurfaceView) mRootView.findViewById(R.id.svImg);
		//relGameOver = (RelativeLayout) mRootView.findViewById(R.id.gameOver);

		svImg.setMainFragment(this);
	}

	@Override
	public void onClick(View v) {
//		switch (v.getId()) {
//		case R.id.playGame:
//			relGameOver.setVisibility(View.GONE);
//			svImg.setVisibility(View.VISIBLE);
//			svImg.setCurrentStatus(GAME_STATUS.GAME_RUNNIG);
//			svImg.restartGame();
//			break;
//
//		default:
//			break;
//		}

	}

	private void setListener() {
		svImg.setIGameStatus(new IGameStatus() {

			@Override
			public void onGameOver() {
				gameOver();

			}

		});
	}

	int ad = 0;

	private void gameOver() {
		int level = svImg.getLevel();
		if (level > numThuThach) // TODO win
			showDialogWin(mActivity);
		else {
			// TODO test
			// showDialogSaveMe(mActivity);

			if (level > 5) {
				showDialogSaveMe(mActivity);
			} else {
				onDimissConfirmDialogClick();
			}
		}
		
		
//		mActivity.runOnUiThread(new Runnable() {
//			@Override
//			public void run() {
//				// svImg.setVisibility(View.GONE);
//				relGameOver.setVisibility(View.VISIBLE);
//
//				// TODO Play sound gameover
//				// playSound(soundGameOver);
//
//			}
//		});

	}

	@Override
	public void onResume() {
		svImg.onResume();

		// TODO start sound
		// mActivity.mBackGroundSound.startSound();
		super.onResume();
	}

	@Override
	public void onPause() {
		svImg.onPause();
		super.onPause();
	}
	
	@Override
	protected void onSaveMeSuccess() {
		super.onSaveMeSuccess();
		svImg.setVisibility(View.VISIBLE);
		svImg.setCurrentStatus(GAME_STATUS.GAME_RUNNIG);
		svImg.restartGameSaveMe();
	}

	@Override
	protected void resetGame() {
		//relGameOver.setVisibility(View.GONE);
		svImg.setVisibility(View.VISIBLE);
		svImg.setCurrentStatus(GAME_STATUS.GAME_RUNNIG);
		svImg.restartGame();
		
	}

	@Override
	protected String getLinkUrl() {
		// TODO Auto-generated method stub
		return "https://play.google.com/store/apps/details?id=net.tk.quickmoving";
	}
}
