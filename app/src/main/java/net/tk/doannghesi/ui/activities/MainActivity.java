package net.tk.doannghesi.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.share.Sharer;
import com.facebook.share.Sharer.Result;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import net.tk.doannghesi.Define;
import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.facebook.FacebookHelper;
import net.tk.doannghesi.retrofit.api.APIService;
import net.tk.doannghesi.retrofit.api.ApiUtils;
import net.tk.doannghesi.retrofit.objects.Ads;
import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.retrofit.objects.Config;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.ui.fragments.FrgSplash;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.ui.threads.BackGroundSound;
import net.tk.doannghesi.BuildConfig;
import net.tk.doannghesi.R;

import androidx.annotation.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseUtilsActivity {
    // Firebase Remote Config
    private FirebaseRemoteConfig firebaseRemoteConfig;

    public boolean isBinhChon;
    public BackGroundSound mBackGroundSound;
    private InterstitialAd mInterstitialAd;

    private int time_loadAds = 60 * 2 * 1000;

    public boolean showShareDialog;

    private FacebookHelper mFacebookHelper;

    private FacebookCallback<Result> callback = new FacebookCallback<Sharer.Result>() {

        @Override
        public void onSuccess(Result result) {
            // TODO check and add blue dot
            showToast("Đã post lên tường.", false);
        }

        @Override
        public void onError(FacebookException error) {
            // Logger.showLogD("TAG", "Share onError");
            showToast("Có sự cố xảy ra.", true);
        }

        @Override
        public void onCancel() {
            // Logger.showLogD("TAG", "Share onCancel");
            // showToast("Cancel");
        }
    };

    private void initFacebook() {
        mFacebookHelper = new FacebookHelper(this, callback, "288183178568111");
    }

    public interface IOnGetConfig {
        public void OnGetConfig();
    }

    private IOnGetConfig mIOnGetConfig;
    public boolean notShowAds;

    public void setIOnGetConfig(IOnGetConfig iOnGetConfig) {
        mIOnGetConfig = iOnGetConfig;
    }

    private void getAdsObject() {
        mAPIService.getAds().enqueue(new Callback<BaseObject<Ads>>() {
            @Override
            public void onResponse(Call<BaseObject<Ads>> call, Response<BaseObject<Ads>> response) {
                if (response.body()!= null && response.body().isSuccess() && response.body().getData() != null) {
                    Logger.d("Get Ads successfully.");
                    SavedStore.editAdsObject(response.body().getData());
                } else {
                    Logger.d("Get Ads error.");
                }
            }

            @Override
            public void onFailure(Call<BaseObject<Ads>> call, Throwable t) {
                Logger.d("Response from server is invalid.");
            }
        });
    }



    public APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Init Admob here and in Manifest
        MobileAds.initialize(this, "ca-app-pub-4460351454781130~8754975209");


        mAPIService = ApiUtils.getAPIService();
        getConfig();

        getKey();
        getListHacker();
        getAdsObject();

        loadAds();
        initFacebook();

        mHandler.postDelayed(new Runnable() {

            @Override
            public void run() {
                showAds();

                mHandler.postDelayed(this, time_loadAds);
            }
        }, time_loadAds);

        mTimeOnlineHandler = new Handler();
        current_time = min * time[0];

		/* init background sound */
        mBackGroundSound = new BackGroundSound(this);
        mBackGroundSound.execute();

        Logger.d("MainActivity- On create");

        pushFragment(new FrgSplash(), false);

        configFirebaseRemote();

    }

    /** Get Remote config from firebase  **/
    private void configFirebaseRemote(){
        // Get Firebase Remote Config instance.
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        // Create a Remote Config Setting to enable developer mode,
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        firebaseRemoteConfig.setConfigSettings(configSettings);
        firebaseRemoteConfig.setDefaults(R.xml.remote_config_default);

        firebaseRemoteConfig.fetch(BuildConfig.DEBUG ? 60 : 60 * 60 * 12) // 1 Min or 12h
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.e("TAG","=== Fetch config success");
                            firebaseRemoteConfig.activateFetched();
                        } else {
                            Log.e("TAG","=== Fetch config fail");
                        }
                        // TODO get value and save to SaveStore
                        String testValue  = firebaseRemoteConfig.getString("test_value");
                        Log.e("TAG","=== TestValue:" + testValue);
                    }
                });
    }

    public void getKey() {
        Logger.e("==== getKey: .... ");
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("getKeyAdBuddiz");
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getValue(String.class);

                Logger.e("==== KEYBuz:" + key);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Logger.e("==== onCancelled:" + databaseError.getMessage());
            }

        });

        Logger.e("==== set .... ");
        //DatabaseReference db = FirebaseDatabase.getInstance().getReference("ttt");
        //db.setValue("abc");

        DatabaseReference  a = FirebaseDatabase.getInstance().getReference();
        a.child("xxx").setValue("Hello, World");
    }

    // ===== FireBase ====== //
    public void getListHacker() {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("list_hacker");
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String key = dataSnapshot.getValue(String.class);
                String[] list_hacker = key.split(",");

                Logger.e("==== HACKER:" + key);
//String key = dataSnapshot.getValue(String.class);
//                String[] list_hacker = key.split(",");
//
//                Logger.e("==== HACKER:" + key);
//                String android_id = Settings.Secure.getString(MainActivity.this.getContentResolver(),
//                        Settings.Secure.ANDROID_ID);
//                for (String hacker : list_hacker) {
//                    if (hacker.equals(android_id)) {
//                        SavedStore.editCoin(49);
//                        SavedStore.editCurrentNumberQuestion(0 + "");
//                    }
//                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void getConfig() {
        mAPIService.getConfig().enqueue(new Callback<BaseObject<Config>>() {
            @Override
            public void onResponse(Call<BaseObject<Config>> call, Response<BaseObject<Config>> response) {

                if (response.body()!= null && response.body().isSuccess() && response.body().getData() != null) {
                    Logger.d("Get Config successfully.");
                    Config config = response.body().getData();
                    SavedStore.editShowAdMob(config.getShowAdmob());

                    if (SavedStore.getQcs_Ver() != config.getQcsVer()) SavedStore.editNewQcs(true);
                    else SavedStore.editNewQcs(false);
                    SavedStore.editQcs_Ver(config.getQcsVer());

                    SavedStore.editNoadsFirebase(config.getNoads());

                    String[] list_hacker = config.getListHacker().split(",");
                    String android_id = Settings.Secure.getString(MainActivity.this.getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    for (String hacker : list_hacker) {
                        if (hacker.equals(android_id)) {
                            SavedStore.editCoin(49);
                            SavedStore.editCurrentNumberQuestion(0 + "");
                        }
                    }

                    SavedStore.editHienThi(config.getHienthi());

                    SavedStore.editVersionCode(config.getVersionCode());

                    Logger.e("Total Question Online:" + config.getTotalQuestion());
                    SavedStore.editTotalOnlineQuestion(config.getTotalQuestion());
                    int currentOnline = SavedStore.getCurrentOnlineQuestion();
                    if (currentOnline >= config.getTotalQuestion())
                        SavedStore.editIsOnlineQuestion(false);
                } else {
                    Logger.d("Get Config Error .");
                }
            }

            @Override
            public void onFailure(Call<BaseObject<Config>> call, Throwable t) {
                Logger.d("Oh shit. Server response is invalid format.");
            }
        });
    }


    private boolean getRandomBoolean() {
        return Math.random() < 0.5;
        // I tried another approaches here, still the same result
    }



    private void loadAds() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.bat_chu_material_full));

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                Log.d("TAG", "Close ads");

                requestNewInterstitial();
                // beginPlayingGame();
            }
        });

        requestNewInterstitial();

    }

    public void showAds() {
//        boolean noadsFirebase = SavedStore.getNoadsFirebase();
//        if (noadsFirebase) return;
//
//        boolean noadsSku = SavedStore.getNoadsSKU();
//        if (noadsSku) return;

//        if (!notShowAds && !SavedStore.getShowAdMob()) {
//
//            showStartApp();
//            // check and show another ad network
//
//        } else if (!notShowAds) {
//            int i = new Random().nextInt(100);
//            if (i % 5 == 0) showStartApp();
//            else
                showInterstitial();
//        }
    }

    public void showAdsThuThach() {
//        boolean noadsFirebase = SavedStore.getNoadsFirebase();
//        if (noadsFirebase) return;
//
//        boolean noadsSku = SavedStore.getNoadsSKU();
//        if (noadsSku) return;
//
//        if (!SavedStore.getShowAdMob()) {
//            showStartApp();
//            //check and show another ad network
//
//        } else {
//            int i = new Random().nextInt(100);
//            if (i % 5 == 0) showStartApp();
//            else
            showInterstitial();
//        }
    }

    private void showInterstitial() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    doShowInterstitial();
                }
            });
        } else {
            doShowInterstitial();
        }
    }

    private void doShowInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            //Log.d(TAG, "Interstitial ad is not loaded yet");
            // showStartApp();
            requestNewInterstitial();
        }
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder().build();

        mInterstitialAd.loadAd(adRequest);
    }

    // @Override
    // protected void OnDoneGetDatabase() {
    // super.OnDoneGetDatabase();
    //
    // pushFragment(new FrgHome());
    //
    // }


    @Override
    protected void onResume() {
        super.onResume();
        //startAppAd.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        //startAppAd.onPause();
        mBackGroundSound.pauseSound();
        // mBackGroundSound.releaseSound();
        // mBackGroundSound.cancel(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /* start service */
        // startService(new Intent(this, MyService.class));

		/* release background sound */
        mBackGroundSound.releaseSound();
        mBackGroundSound.cancel(true);
    }

    @Override
    protected void onStop() {
        Logger.d("MainActivity - Onstop");
        super.onStop();

        mTimeOnlineHandler.removeCallbacks(mTimeOnlineRunnable);
    }

    @Override
    protected void onStart() {
        Logger.d("MainActivity - onStart");
        super.onStart();

        mTimeOnlineHandler.post(mTimeOnlineRunnable);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Toast.makeText(this, "onActivityResult + resultCode" + resultCode,
        // Toast.LENGTH_SHORT).show();
//        Log.d("TAG", "MainActivity onActivityResult  " + requestCode);
//        if (requestCode == IabConfiguration.RC_REQUEST) {
//            PurchaseFragment fragment = (PurchaseFragment)
//                    getmFragmentManager().findFragmentById(R.id.fragment);
//            fragment.onActivityResult(requestCode, resultCode, data);
//        } else
            if (requestCode == CallbackManagerImpl.RequestCodeOffset.Share.toRequestCode()
                || requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()) {
            Log.d("TAG", "MainActivity onActivityResult  FACEBOOK" + requestCode);
            mFacebookHelper.getCallBackManager().onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == Define.RC_SIGN_IN) {
            BaseFragment fragment = (BaseFragment) getFragmentManager().findFragmentById(R.id.fragment);
            fragment.onActivityResult(requestCode, resultCode, data);

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public int[] time = {2, 10, 30, 60, 120};
    public int[] coins = {5, 30, 100, 300, 800};
    public int current_index;
    public int current_time;
    public int min = 60;

    public boolean isGetGift;

    public interface IOnGetGift {
        public void onGetGift();

        public void onTime(int secs);
    }

    public IOnGetGift mOnGetGift;

    public void setOnGetGift(IOnGetGift onGetGift) {
        mOnGetGift = onGetGift;
    }

    public Handler mTimeOnlineHandler;

    public Runnable mTimeOnlineRunnable = new Runnable() {

        @Override
        public void run() {
            // Logger.d("Runnable - currenttime:" + current_time);

            if (isOnline()) {
                // Logger.d("Runnable - is Online");

                current_time -= 1;
                if (mOnGetGift != null)
                    mOnGetGift.onTime(current_time);

                if (current_time <= 0) {
                    if (mOnGetGift != null)
                        mOnGetGift.onGetGift();

                    current_index += 1;
                    if (current_index >= time.length)
                        current_index = 0;
                    current_time = min * time[current_index];
                } else
                    mTimeOnlineHandler.postDelayed(this, 1000);
            }

        }
    };

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public FacebookHelper getFacebookHelper() {
        return mFacebookHelper;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
