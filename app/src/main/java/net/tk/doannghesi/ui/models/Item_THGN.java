package net.tk.doannghesi.ui.models;

import android.graphics.Bitmap;
import android.view.View;

public class Item_THGN {
	private int id;
	private Bitmap bitmap;
	private boolean isFind;
	private View mark;
	private View imgMain;

	public Item_THGN(int id, Bitmap bitmap) {
		super();
		this.id = id;
		this.bitmap = bitmap;
	}

	public View getImgMain() {
		return imgMain;
	}

	public void setImgMain(View imgMain) {
		this.imgMain = imgMain;
	}

	public View getMark() {
		return mark;
	}

	public void setMark(View mark) {
		this.mark = mark;
	}

	public boolean isFind() {
		return isFind;
	}

	public void setFind(boolean isFind) {
		this.isFind = isFind;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

}
