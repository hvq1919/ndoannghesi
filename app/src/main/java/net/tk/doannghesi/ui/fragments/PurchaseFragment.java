package net.tk.doannghesi.ui.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.retrofit.objects.Qcs;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.tools.log.Toaster;
import net.tk.doannghesi.ui.adapter.QcsAdapter;
import net.tk.doannghesi.ui.controls.NonScrollListView;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchaseFragment extends BaseFragment implements OnClickListener {

    private static final String TAG = "TAG";

    private QcsAdapter qcsAdapter;
    private NonScrollListView lv_nonscroll_list;

    public interface IOnUpdateGiftPurchase {
        public void onUpdateGift(int gift);
    }

    private IOnUpdateGiftPurchase mIOnUpdateGift;

//    public static PurchaseFragment newInstance(IOnUpdateGiftPurchase mIOnUpdateGift) {
//        PurchaseFragment frgVongXoay = new PurchaseFragment();
//        frgVongXoay.setmIOnUpdateGift(mIOnUpdateGift);
//        return frgVongXoay;
//    }

    public void setmIOnUpdateGift(IOnUpdateGiftPurchase mIOnUpdateGift) {
        this.mIOnUpdateGift = mIOnUpdateGift;
    }

    //private TextView dialogNumGift;
    //private int currentGift;


    private TextView textCoin;
    private int mCurrentCoin;



    private boolean isAppInstalled(String packageName) {
        PackageManager pm = mActivity.getPackageManager();
        boolean installed = false;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            installed = false;
        }
        return installed;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frg_purchase, container, false);

        textCoin = (TextView) mRootView.findViewById(R.id.coin);
        lv_nonscroll_list = (NonScrollListView) mRootView.findViewById(R.id.lv_nonscroll_list);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((ScrollView) mRootView.findViewById(R.id.scrollView)).fullScroll(ScrollView.FOCUS_UP);
            }
        }, 1500);

        updateUi();

        // Create the helper, passing it our context and the public key to
        // verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        //mHelper = new IabHelper(mActivity, IabConfiguration.BASE64_ENCODED_PUBLIC_KEY);

        // enable debug logging (for a production application, you should set
        // this to false).
        //mHelper.enableDebugLogging(false);

        Log.d(TAG, "Starting setup.");
        //mHelper.startSetup(this);

        mRootView.findViewById(R.id.imgbuy1).setOnClickListener(this);
        mRootView.findViewById(R.id.imgbuy2).setOnClickListener(this);
        mRootView.findViewById(R.id.imgbuy3).setOnClickListener(this);
        //mRootView.findViewById(R.id.imgbuy4).setOnClickListener(this);
        mRootView.findViewById(R.id.imgNoAds).setOnClickListener(this);
        mRootView.findViewById(R.id.back).setOnClickListener(this);

        return mRootView;
    }

    private void updateUi() {
        mCurrentCoin = SavedStore.getCoin();

        SavedStore.editNewQcs(false);

        Qcs qcsClick = SavedStore.getQcsClick();
        if(qcsClick != null && isAppInstalled(qcsClick.getPackageName())){
            mCurrentCoin = mCurrentCoin + qcsClick.getCoins();
            SavedStore.editCoin(mCurrentCoin);
            SavedStore.clearQcsClick();
            ArrayList<String> listInstall = SavedStore.getQcsInstall();
            listInstall.add(qcsClick.getPackageName());
            SavedStore.editQcsInstall(listInstall);
        }

        int i = new Integer(mCurrentCoin + 1);
        textCoin.setText("" + i);
        //textCoin.setText("" + mCurrentCoin);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity.showLoadingDialog();

        qcsAdapter = new QcsAdapter(mActivity, new ArrayList<Qcs>()) {
            @Override
            public void gotoGooglePlay(Qcs qcs) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + qcs.getPackageName())));
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + qcs.getPackageName())));
                }
                if (!SavedStore.editQcsClick(qcs))
                    Toaster.showToast(mActivity, "Có lỗi xảy ra");
            }
        };
        lv_nonscroll_list.setAdapter(qcsAdapter);

        getQcs();
    }

    private void getQcs() {
        mActivity.mAPIService.getListQcs().enqueue(new Callback<BaseObject<List<net.tk.doannghesi.retrofit.objects.Qcs>>>() {
            @Override
            public void onResponse(Call<BaseObject<List<net.tk.doannghesi.retrofit.objects.Qcs>>> call, Response<BaseObject<List<net.tk.doannghesi.retrofit.objects.Qcs>>> response) {
                mActivity.dismissLoadingDialog();
                if(response.body() != null &&  response.body().isSuccess() && response.body().getData()!= null){
                    Logger.d("Get list qcs successfully.");
                    qcsAdapter.clear();
                    qcsAdapter.addAll(new ArrayList<Qcs>(response.body().getData()));
                }else{

                    Logger.d("Get list qcs error.");
                }
            }

            @Override
            public void onFailure(Call<BaseObject<List<net.tk.doannghesi.retrofit.objects.Qcs>>> call, Throwable t) {
                mActivity.dismissLoadingDialog();
                Logger.d("Response from server is invalid format.");
            }
        });
    }


    @Override
    public void onClick(View v) {
        String payload = "";
        switch (v.getId()) {
//            case R.id.imgbuy1:
//                mHelper.launchPurchaseFlow(mActivity, IabConfiguration.SKU_BLUE_DOTS_1, IabConfiguration.RC_REQUEST, this, payload);
//                break;
//            case R.id.imgbuy2:
//                mHelper.launchPurchaseFlow(mActivity, IabConfiguration.SKU_BLUE_DOTS_2, IabConfiguration.RC_REQUEST, this, payload);
//                break;
//
//            case R.id.imgbuy3:
//                mHelper.launchPurchaseFlow(mActivity, IabConfiguration.SKU_BLUE_DOTS_3, IabConfiguration.RC_REQUEST, this, payload);
//                break;
//
////            case R.id.imgbuy4:
////                mHelper.launchPurchaseFlow(mActivity, IabConfiguration.SKU_BLUE_DOTS_4, IabConfiguration.RC_REQUEST, this, payload);
////                break;
//            case R.id.imgNoAds:
//                mHelper.launchPurchaseFlow(mActivity, IabConfiguration.SKU_NO_ADS, IabConfiguration.RC_REQUEST, this, payload);
//                break;

            case R.id.back:
                mActivity.popFragment();
                break;
            default:
                break;
        }

    }
//
//    @Override
//    public void onIabSetupFinished(IabResult result) {
//        Log.d(TAG, "onIabSetupFinished - Setup finished.");
//
//        if (!result.isSuccess()) {
//            // Oh noes, there was a problem.
//            Log.d(TAG, "Problem setting up in-app billing: " + result);
//            return;
//        }
//
//        // Have we been disposed of in the meantime? If so, quit.
//        if (mHelper == null)
//            return;
//
//        // IAB is fully set up. Now, let's get an inventory of stuff we own.
//        Log.d(TAG, "onIabSetupFinished - Setup successful. Querying inventory.");
//        mHelper.queryInventoryAsync(this);
//
//    }
//
//    @Override
//    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
//        Log.d(TAG, "Query inventory finished.");
//
//        // Have we been disposed of in the meantime? If so, quit.
//        if (mHelper == null)
//            return;
//
//        // Is it a failure?
//        if (result.isFailure()) {
//            Log.d(TAG, "Failed to query inventory: " + result);
//            return;
//        }
//
//        Log.d(TAG, "Query inventory was successful.");
//
//		/*
//         * Check for items we own. Notice that for each purchase, we check the
//		 * developer payload to see if it's correct! See
//		 * verifyDeveloperPayload().
//		 */
//        Purchase BLUE_DOTS_1 = inventory.getPurchase(IabConfiguration.SKU_BLUE_DOTS_1);
//        if (BLUE_DOTS_1 != null && verifyDeveloperPayload(BLUE_DOTS_1)) {
//            Log.d(TAG, "onQueryInventoryFinished - We have gas. Consuming it. SKU_BLUE_DOTS_1");
//            mHelper.consumeAsync(inventory.getPurchase(IabConfiguration.SKU_BLUE_DOTS_1), this);
//            return;
//        }
//
//        // Check for gas delivery -- if we own gas, we should fill up the tank
//        // immediately
//        Purchase BLUE_DOTS_2 = inventory.getPurchase(IabConfiguration.SKU_BLUE_DOTS_2);
//        if (BLUE_DOTS_2 != null && verifyDeveloperPayload(BLUE_DOTS_2)) {
//            Log.d(TAG, "onQueryInventoryFinished - We have gas. Consuming it. SKU_BLUE_DOTS_2");
//            mHelper.consumeAsync(inventory.getPurchase(IabConfiguration.SKU_BLUE_DOTS_2), this);
//            return;
//        }
//
//        // Check for gas delivery -- if we own gas, we should fill up the tank
//        // immediately
//        Purchase BLUE_DOTS_3 = inventory.getPurchase(IabConfiguration.SKU_BLUE_DOTS_3);
//        if (BLUE_DOTS_3 != null && verifyDeveloperPayload(BLUE_DOTS_3)) {
//            Log.d(TAG, "onQueryInventoryFinished - We have gas. Consuming it. SKU_BLUE_DOTS_3");
//            mHelper.consumeAsync(inventory.getPurchase(IabConfiguration.SKU_BLUE_DOTS_3), this);
//            return;
//        }
//
//        // Check for gas delivery -- if we own gas, we should fill up the tank
//        // immediately
////        Purchase BLUE_DOTS_4 = inventory.getPurchase(IabConfiguration.SKU_BLUE_DOTS_4);
////        if (BLUE_DOTS_4 != null && verifyDeveloperPayload(BLUE_DOTS_4)) {
////            Log.d(TAG, "onQueryInventoryFinished - We have gas. Consuming it. SKU_BLUE_DOTS_4");
////            mHelper.consumeAsync(inventory.getPurchase(IabConfiguration.SKU_BLUE_DOTS_4), this);
////            return;
////        }
//
//        // Check for gas delivery -- if we own gas, we should fill up the tank
//        // immediately
//        Purchase NO_ADS = inventory.getPurchase(IabConfiguration.SKU_NO_ADS);
//        if (NO_ADS != null && verifyDeveloperPayload(NO_ADS)) {
//            Log.d(TAG, "onQueryInventoryFinished - We have gas. Consuming it. SKU_NO_ADS");
//            mHelper.consumeAsync(inventory.getPurchase(IabConfiguration.SKU_NO_ADS), this);
//            return;
//        }
//
//        Log.d(TAG, "onQueryInventoryFinished - Initial inventory query finished; enabling main UI.");
//
//    }
//
//    @Override
//    public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
//        Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
//
//        // if we were disposed of in the meantime, quit.
//        if (mHelper == null)
//            return;
//
//        if (result.isFailure()) {
//            Log.d(TAG, "onIabPurchaseFinished - isFailure");
//            return;
//        }
//        if (!verifyDeveloperPayload(purchase)) {
//            Log.d(TAG, "onIabPurchaseFinished - Error purchasing. Authenticity verification failed.");
//            return;
//        }
//
//        Log.d(TAG, "Purchase successful.");
//        mHelper.consumeAsync(purchase, this);
//
//    }
//
//    @Override
//    public void onConsumeFinished(Purchase purchase, IabResult result) {
//        Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);
//
//        // if we were disposed of in the meantime, quit.
//        if (mHelper == null)
//            return;
//
//        // We know this is the "gas" sku because it's the only one we
//        // consume,
//        // so we don't check which sku was consumed. If you have more than
//        // one
//        // sku, you probably should check...
//        if (result.isSuccess()) {
//            // successfully consumed, so we apply the effects of the item in
//            // our
//            // game world's logic, which in our case means filling the gas
//            // tank a bit
//            Log.d(TAG, "onConsumeFinished - Consumption successful. Provisioning...... SAVE DATA");
//
//            if (purchase.getSku().equals(IabConfiguration.SKU_BLUE_DOTS_1)) {
//                mCurrentCoin += 400;
//            } else if (purchase.getSku().equals(IabConfiguration.SKU_BLUE_DOTS_2)) {
//                mCurrentCoin += 1000;
//            } else if (purchase.getSku().equals(IabConfiguration.SKU_BLUE_DOTS_3)) {
//                mCurrentCoin += 3000;
//            }
////            else if (purchase.getSku().equals(IabConfiguration.SKU_BLUE_DOTS_4)) {
////                mCurrentCoin += 5000;
////            }
//            else if (purchase.getSku().equals(IabConfiguration.SKU_NO_ADS)) {
//                SavedStore.editNoadsSKU(true);
//            }
//
//            SavedStore.editCoin(mCurrentCoin);
//            updateUi();
//
//            if (mIOnUpdateGift != null)
//                mIOnUpdateGift.onUpdateGift(mCurrentCoin);
//        } else {
//            Log.d(TAG, "onConsumeFinished -  Error while consuming: " + result);
//        }
//
//        Log.d(TAG, "onConsumeFinished -   End consumption flow.");
//
//    }
//
//    /**
//     * Verifies the developer payload of a purchase.
//     */
//    boolean verifyDeveloperPayload(Purchase p) {
//        String payload = p.getDeveloperPayload();
//
//		/*
//         * TODO: verify that the developer payload of the purchase is correct.
//		 * It will be the same one that you sent when initiating the purchase.
//		 *
//		 * WARNING: Locally generating a random string when starting a purchase
//		 * and verifying it here might seem like a good approach, but this will
//		 * fail in the case where the user purchases an item on one device and
//		 * then uses your app on a different device, because on the other device
//		 * you will not have access to the random string you originally
//		 * generated.
//		 *
//		 * So a good developer payload has these characteristics:
//		 *
//		 * 1. If two different users purchase an item, the payload is different
//		 * between them, so that one user's purchase can't be replayed to
//		 * another user.
//		 *
//		 * 2. The payload must be such that you can verify it even when the app
//		 * wasn't the one who initiated the purchase flow (so that items
//		 * purchased by the user on one device work on other devices owned by
//		 * the user).
//		 *
//		 * Using your own server to store and verify developer payloads across
//		 * app installations is recommended.
//		 */
//
//        return true;
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Log.d(TAG, "Purchase fragment onActivityResult(" + requestCode + "," + resultCode + "," + data);
//        if (mHelper == null)
//            return;
//
//        // Pass on the activity result to the helper for handling
//        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
//            // not handled, so handle it ourselves (here's where you'd
//            // perform any handling of activity results not related to in-app
//            // billing...
//            super.onActivityResult(requestCode, resultCode, data);
//        } else {
//            Log.d(TAG, "onActivityResult handled by IABUtil.");
//        }
//    }
}
