package net.tk.doannghesi.ui.fragments.thuthach;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.ui.controls.FlowLayout;
import net.tk.doannghesi.ui.dialog.DialogHintAThuThach;
import net.tk.doannghesi.ui.models.Item_THGN;
import net.tk.doannghesi.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class FrgFindSamePic extends FrgBaseThuThach implements OnClickListener {
    private float RATE_CELL = 1 / 10f;

    private int TIME_RATE;

    private int item_size;

    private FlowLayout flowLayout;

    private Button btnTiepTuc;
    private TextView textTime, textLevel, textPass;

    private ArrayList<Item_THGN> mItemsTHGN = new ArrayList<Item_THGN>();

    private int mCol, mRow;
    private int mTime; // Thoi gian cho 1 lan pass, tang dan cho lan sau
    private int mPassed = 1; // pass 3 lan se hoan thanh thu thach

    private int lastItem = -1, lastItem2 = -1;
    private boolean isClick, isPostDelay;

    private Handler mHandler = new Handler();

    private int currentLevel;

    private Runnable runnable = new Runnable() {

        @Override
        public void run() {
            mTime -= 1;
            textTime.setText("" + mTime);
            if (mTime < 10)
                textTime.setTextColor(Color.RED);

            if (mTime <= 0) {
                flowLayout.removeAllViews();
                mHandler.removeCallbacks(this);

                // TODO show dialog Game OVer
                // Toaster.showToast(mActivity, "GAMEOVER");
                try {
                    getDialogGameOver().show(mActivity.getFragmentManager(), "TAG");
                } catch (Exception e) {
                }

                // showDialogWin(mActivity);
            } else
                mHandler.postDelayed(this, 1000);
        }
    };

    private DialogHintAThuThach dialogHintAThuThach;

    public FrgFindSamePic() {
    }

    // public FrgFindSamePic(int col, int row) {
    // mCol = col;
    // mRow = row;
    // }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frg_find_same_pic, container, false);

        flowLayout = (FlowLayout) mRootView.findViewById(R.id.flowLayout);

        btnTiepTuc = (Button) mRootView.findViewById(R.id.btnTiepTuc);
        textLevel = (TextView) mRootView.findViewById(R.id.textLevel);
        textTime = (TextView) mRootView.findViewById(R.id.textTime);
        textPass = (TextView) mRootView.findViewById(R.id.textVuotQua);

        btnTiepTuc.setOnClickListener(this);

        // TODO test, se tao 1 dialog o lop base thu thach , bat dau game se goi
        // ham reset game nay
        // resetGame();
        dialogHintAThuThach = DialogHintAThuThach.newInstance(mResources.getString(R.string.ten_thuthach_thgn),
                mResources.getString(R.string.note_thgn), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        resetGame();
                        SavedStore.editFirstHint(true);
                        dialogHintAThuThach.dismiss();
                    }
                });
        dialogHintAThuThach.show(mActivity.getFragmentManager(), "TAG");

        return mRootView;
    }

    @Override
    protected void resetGame() {
        mPassed = 1;
        initColRow();
        createGame(mCol, mRow);

        textLevel.setText(mPassed + "/3");
        textTime.setText("" + mTime);
        textTime.setTextColor(Color.parseColor("#93D907"));

        mHandler.post(runnable);

    }

    private void doPassAScreen() {
        flowLayout.removeAllViews();
        mHandler.removeCallbacks(runnable);
        mPassed += 1;
        if (mPassed > 3) { // TODO show dialog win thu thach
            // Toaster.showToast(mActivity, "VUOT qua THU THACH");

            // TODO tinh coins se cong
            showDialogWin(mActivity);
        } else {
            btnTiepTuc.setVisibility(View.VISIBLE);
            textPass.setVisibility(View.VISIBLE);
        }
    }

    private void createGame(int col, int row) {
        flowLayout.removeAllViews();
        calculateLayouts(col, row);
        createGrid(col, row);
    }

    private void calculateLayouts(int col, int row) {
        flowLayout.getLayoutParams().width = deviceWidth;

        item_size = 2 + (int) (deviceWidth / (col + RATE_CELL * (col + 1)));

    }

    private void createGrid(int col, int row) {
        recycleListItem();

        int start = new Random().nextInt(200);
        int n = col * row / 2;

        for (int i = 0; i < n; i++) {
            Bitmap bm = getBitmapFromAsset(mActivity, mActivity.getListQuestion().get(start + i).getImg_name());
            mItemsTHGN.add(new Item_THGN(i, bm));
            mItemsTHGN.add(new Item_THGN(i, bm));
        }
        Collections.shuffle(mItemsTHGN);

        for (int i = 0; i < mItemsTHGN.size(); i++) {
            final Item_THGN item_THGN = mItemsTHGN.get(i);
            final int j = i;

            View child = mActivity.getLayoutInflater().inflate(R.layout.item_thgn, null);
            // TextView tvContent = (TextView)
            // child.findViewById(R.id.tvContent);
            // tvContent.setText(objectAnswers.get(i).getText());

            final View mark = child.findViewById(R.id.img_default);
            final ImageView imgMain = (ImageView) child.findViewById(R.id.imgMain);
            imgMain.setImageBitmap(mItemsTHGN.get(i).getBitmap());
            item_THGN.setMark(mark);
            item_THGN.setImgMain(imgMain);

            child.findViewById(R.id.item_thgn_container).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO test
                    // mActivity.popFragment();
                    // mActivity.pushFragment(new FrgMain(), true);
                    // return;

                    if (!item_THGN.isFind()) {
                        // if (isClick)
                        // return;

                        // if (Build.VERSION.SDK_INT >= 16)
                        // startAnimationHide((ImageView) mark);//
                        // else
                        mark.setVisibility(View.GONE);
                        item_THGN.setFind(true);
                        if (lastItem != -1) {
                            // isClick = true;

                            if (isPostDelay) {
                                // mHandler.removeCallbacks(null);

                                mItemsTHGN.get(lastItem2).getMark().setVisibility(View.VISIBLE);
                                mItemsTHGN.get(lastItem).getMark().setVisibility(View.VISIBLE);
                                mItemsTHGN.get(lastItem).setFind(false);
                                mItemsTHGN.get(lastItem2).setFind(false);

                                lastItem = j;
                                lastItem2 = -1;
                                // isClick = false;
                                isPostDelay = false;
                                return;
                            }

                            if (mItemsTHGN.get(lastItem).getId() == item_THGN.getId()) {
                                playSound(soundWin);
                                // item_THGN.getMark().setVisibility(View.VISIBLE);
                                // item_THGN.getMark().setAlpha(0.4f);
                                // mItemsTHGN.get(lastItem).getMark().setVisibility(View.VISIBLE);
                                // mItemsTHGN.get(lastItem).getMark().setAlpha(0.4f);

                                item_THGN.getImgMain().setVisibility(View.GONE);
                                mItemsTHGN.get(lastItem).getImgMain().setVisibility(View.GONE);

                                lastItem = -1;
                                lastItem2 = -1;
                                // isClick = false;

                                // Correct
                                // TODO check win
                                for (Item_THGN item_THGN1 : mItemsTHGN) {
                                    if (!item_THGN1.isFind())
                                        return;
                                }
                                // Toaster.showToast(mActivity, "WIn");
                                doPassAScreen();

                            } else {
                                //playSound(soundClickHint);
                                isPostDelay = true;
                                lastItem2 = j;

                                // mHandler.postDelayed(new Runnable() {
                                //
                                // @Override
                                // public void run() {
                                // //Toaster.showToast(mActivity, "PostDelay");
                                // if (!isPostDelay)
                                // return;
                                // mark.setVisibility(View.VISIBLE);
                                // mItemsTHGN.get(lastItem).getMark().setVisibility(View.VISIBLE);
                                // item_THGN.setFind(false);
                                // mItemsTHGN.get(lastItem).setFind(false);
                                //
                                // lastItem = -1;
                                // lastItem2 = -1;
                                // // isClick = false;
                                // isPostDelay = false;
                                // }
                                // }, 2000);
                            }
                        } else
                            lastItem = j;
                    }
                }
            });

            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(item_size, item_size);
            if ((i + 1) % mCol != 0)
                params.rightMargin = (int) (item_size * RATE_CELL);
            params.bottomMargin = (int) (item_size * RATE_CELL);
            if (i / mCol == 0)
                params.topMargin = (int) (item_size * RATE_CELL);

            child.setLayoutParams(params);

            flowLayout.addView(child);
        }

    }

    private void recycleListItem() {
        if (mItemsTHGN == null || mItemsTHGN.size() == 0)
            return;

        for (Item_THGN item_THGN : mItemsTHGN) {
            item_THGN.getBitmap().recycle();
            item_THGN = null;
        }
        mItemsTHGN.clear();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void startAnimationHide(final ImageView iv) {
        Animation anime;
        int u = new Random().nextInt(7) + 1;
        if (u == 1) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.hide_slideup);
        } else if (u == 2) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_out_bottom);
        } else if (u == 3) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.hide_slideleft);
        } else if (u == 4) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_out_left);
        } else if (u == 5) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_out_right);
        } else if (u == 6) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.fadeout);
        } else {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.rotate);
        }
        anime.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                iv.setHasTransientState(false);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                iv.setVisibility(View.GONE);
                iv.setHasTransientState(true);

            }
        });
        iv.startAnimation(anime);
    }

    private void startAnimationShow(final ImageView iv) {
        iv.setVisibility(View.VISIBLE);
        Animation anime;
        int u = new Random().nextInt(7) + 1;
        if (u == 1) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.show_slidedown);
        } else if (u == 2) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.show_slideright);
        } else if (u == 3) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_in_left);
        } else if (u == 4) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_in_bottom);
        } else if (u == 5) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_in_right);
        } else if (u == 6) {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.fadeout);
        } else {
            anime = AnimationUtils.loadAnimation(mActivity, R.anim.rotate);
        }
        anime.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }
        });
        iv.startAnimation(anime);
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        recycleListItem();

        mHandler.removeCallbacks(runnable);
    }


    private void initColRow() {
        // TODO Auto-generated constructor stub

        // Get current level qua savestored rui tu tinh ra col vs row lun,
        // K0 dung constructor ben duoi


        currentLevel = SavedStore.getCurrentNumberQuestion();

        if (currentLevel / 10 < 10) {
            mCol = 3; // 4 // 4
            mRow = 4; // 4 // 5
            TIME_RATE = 30;// 40 // 50
        } else if (currentLevel / 10 < 20) {
            mCol = 4; // 4 // 4
            mRow = 4; // 5 // 6
            TIME_RATE = 60; // 70 // 80
        } else if (currentLevel / 10 < 30) {
            mCol = 5; // 6 // 6
            mRow = 6; // 6 // 7
            TIME_RATE = 90; // 100 // 110
        } else if (currentLevel / 10 < 40) {
            mCol = 6; // 6 // 6
            mRow = 6; // 7 // 8
            TIME_RATE = 120; // 130 // 140
        } else {
            mCol = 7; // 8 // 8
            mRow = 8; // 8 // 9
            TIME_RATE = 150; // 160 // 170
        }

        mTime = TIME_RATE;

        // 7,8 180s
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnTiepTuc:
                btnTiepTuc.setVisibility(View.GONE);
                textPass.setVisibility(View.GONE);

                doNextScreen();
                break;

            default:
                break;
        }
    }

    private void doNextScreen() {
        Log.d("TAG", "ROW:" + mRow + " _ COL:" + mCol);

        mTime = TIME_RATE + (mPassed - 1) * 10;
        if (mPassed == 2) {
            if (mCol % 2 == 1)
                mCol += 1;
            else
                mRow += 1;
        } else {
            mRow += 1;
        }

        Log.d("TAG", "ROW1:" + mRow + " _ COL1:" + mCol);

        createGame(mCol, mRow);
        mHandler.post(runnable);
        textLevel.setText(mPassed + "/3");
        textTime.setText("" + mTime);
        textTime.setTextColor(Color.parseColor("#93D907"));
    }

}
//