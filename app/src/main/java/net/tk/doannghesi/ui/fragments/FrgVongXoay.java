package net.tk.doannghesi.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyandroidanimations.library.Animation;
import com.easyandroidanimations.library.AnimationListener;
import com.easyandroidanimations.library.RotationAnimation;
import com.gc.materialdesign.views.ButtonRectangle;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.retrofit.objects.User;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.tools.sound.SoundPoolHelper;
import net.tk.doannghesi.tools.utils.DateUtils;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.R;

import org.json.JSONObject;

import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgVongXoay extends BaseFragment implements OnClickListener {

    private final int Degrees_MAX = 7200;
    private final int Degrees_MIN = 6840;
    private final int TIME_ROTATE = 5000;

    // int[] coins = { 10, 5, 30, 10, 5 };
    // int[] degrees_rage = { 30, 90, 150, 210, 270, 330 };

    int[] coins = {10, 100, 5, 20, 30, 5}; // 100 se thay bang kimcuong
    int[] degrees_rage = {27, 99, 117, 189, 243, 279, 333};

    private Handler mHandler;

    private ButtonRectangle btnSpin;
    private ImageView imgWheel;
    private TextView text_tra_loi_dung_or_sai;
    private TextView textTimeSpin;
    private TextView textTotalSpin;
    private TextView tvCoin;
    private TextView tvKc;
    private LinearLayout llTiLeCao;

    private int mCurrentCoin;
    private int mCurrentKc;
    private boolean isVip;
    private int spin_num = 9;

    private RotationAnimation rotationAnimation;

    /* The degree that wheel rotated , it's in range from 0 to 360 */
    private int degree_rotated = 0;
    private int degree_random;

    private int soundBgsound;
    private SoundPoolHelper mSoundPoolHelper;

    public interface IOnUpdateCoin {
        public void onUpdateCoin(int coin);
    }

    private IOnUpdateCoin mIonUpdateCoin;


    public static FrgVongXoay newInstance(IOnUpdateCoin mIonUpdateCoin) {
        FrgVongXoay frgVongXoay = new FrgVongXoay();
        frgVongXoay.setIonUpdateCoin(mIonUpdateCoin);
        return frgVongXoay;
    }

    public void setIonUpdateCoin(IOnUpdateCoin onUpdateCoin) {
        this.mIonUpdateCoin = onUpdateCoin;
        mHandler = new Handler();
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.mBackGroundSound.startSound();
    }

    private void getTimeOnline() {
        String dateInfo = FirebaseRemoteConfig.getInstance().getString("current_date");
        try{
            JSONObject dInfo = new JSONObject(dateInfo);
            String url = dInfo.getString("date_url");
            final String param = dInfo.getString("date_param");

            Ion.with(mActivity)
                    .load(url)
                    .asString().setCallback(new FutureCallback<String>() {
                @Override
                public void onCompleted(Exception e, String response) {
                    mActivity.dismissLoadingDialog();

                    Logger.e("=== TIME: " + response);

                    if (!TextUtils.isEmpty(response)) {
                        try{
                            JSONObject currentTimeServer = new JSONObject(response);
                            String dateString = currentTimeServer.getString(param);
                            Logger.e("=== dateString: " + dateString);
                            int days = DateUtils.convertStringToDays(dateString.split("T")[0], "yyyy-MM-dd");

                            Logger.e("DAY: " + days + " - SavedStore.getDaySpin() : " + SavedStore.getDaySpin());

                            if (SavedStore.getDaySpin() != -1 && days != SavedStore.getDaySpin()) {
                                SavedStore.editSpin(spin_num);
                                updateSpin();
                            }
                            SavedStore.editDaySpin(days);
                        }catch (Exception ex){}
                    }
//                    else {
//                        // getTimeOnline();
//                        mActivity.showToast(mActivity.getResources().getString(R.string.post_loi), true);
//                        mActivity.dismissLoadingDialog();
//                    }
                }
            });

        }catch (Exception e){}
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity.showLoadingDialog();
        getTimeOnline();
    }

    public void buildDialogConfirm(String text, DialogInterface.OnClickListener clickListener) {
        new AlertDialog.Builder(mActivity).setMessage(text).setCancelable(false)
                .setPositiveButton("Yes", clickListener).show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frg_vongxoay, container, false);

        mSoundPoolHelper = new SoundPoolHelper(1, mActivity);
        soundBgsound = mSoundPoolHelper.load(mActivity, R.raw.song2, 1);

        initObjects();
        updateUi();
        getListHacker();

        btnSpin.setOnClickListener(this);
        mRootView.findViewById(R.id.back).setOnClickListener(this);

        textTimeSpin.setText("Mỗi ngày đều có 10 spins");

        // Logger.e("=== TEST VALUE VONGXOAY:" + FirebaseRemoteConfig.getInstance().getString("test_value"));

        return mRootView;
    }

    private void getListHacker() {
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("list_hacker");
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                String key = dataSnapshot.getValue(String.class);
//                String[] list_hacker = key.split(",");
//
//                Logger.e("==== HACKER:" + key);
//
//                String android_id = Settings.Secure.getString(MainActivity.this.getContentResolver(),
//                        Settings.Secure.ANDROID_ID);
//                for (String hacker : list_hacker) {
//                    if (hacker.equals(android_id)) {
//                        SavedStore.editCoin(49);
//                        SavedStore.editCurrentNumberQuestion(0 + "");
//                    }
//                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void initObjects() {
        tvKc = (TextView) mRootView.findViewById(R.id.kc);
        tvCoin = (TextView) mRootView.findViewById(R.id.coin);
        btnSpin = (ButtonRectangle) mRootView.findViewById(R.id.btnSpin);
        imgWheel = (ImageView) mRootView.findViewById(R.id.imgWheel);
        text_tra_loi_dung_or_sai = (TextView) mRootView.findViewById(R.id.text_tra_loi_dung_or_sai);
        textTimeSpin = (TextView) mRootView.findViewById(R.id.textTimeSpin);
        textTotalSpin = (TextView) mRootView.findViewById(R.id.textTotalSpin);
        llTiLeCao = (LinearLayout) mRootView.findViewById(R.id.llDoiCao);

        int tileCao = SavedStore.getTiLeCao();
        if (tileCao >= 50)
            llTiLeCao.setVisibility(View.GONE);

        isVip = SavedStore.getIsVip();
        if (isVip) {
            spin_num = 15;
            mRootView.findViewById(R.id.llDoiVip).setVisibility(View.GONE);
            mRootView.findViewById(R.id.llMuaVip).setVisibility(View.GONE);
        }

        mRootView.findViewById(R.id.btnKcDoiCoin).setOnClickListener(this);
        mRootView.findViewById(R.id.btnKcDoiCao).setOnClickListener(this);
        mRootView.findViewById(R.id.btnKcDoiVip).setOnClickListener(this);
        mRootView.findViewById(R.id.btnMuaVip).setOnClickListener(this);

        rotationAnimation = new RotationAnimation(imgWheel).setDuration(TIME_ROTATE)
                .setPivot(RotationAnimation.PIVOT_CENTER).setInterpolator(new AccelerateDecelerateInterpolator())
                .setListener(new AnimationListener() {
                    @Override
                    public void onAnimationEnd(Animation arg0) {
                        mSoundPoolHelper.pause(soundBgsound);

                        btnSpin.setEnabled(true);

                        degree_rotated = (degree_random + degree_rotated - 360) % 360;

                        if (degree_rotated <= degrees_rage[0] || degree_rotated > degrees_rage[degrees_rage.length - 1]) {
                            showCoin(0);
                            mIonUpdateCoin.onUpdateCoin(0);

                        } else
                            for (int i = 0; i < degrees_rage.length; i++)
                                if (degrees_rage[i] < degree_rotated && degree_rotated <= degrees_rage[i + 1]) {
                                    if (i != 1) {
                                        mIonUpdateCoin.onUpdateCoin(coins[i]);
                                        showCoin(coins[i]);

                                        mCurrentCoin += coins[i];
                                        SavedStore.editCoin(mCurrentCoin);
                                        int k = new Integer(mCurrentCoin + 1);
                                        tvCoin.setText("" + k);

                                    } else {// Kimcuong
                                        text_tra_loi_dung_or_sai.setVisibility(View.VISIBLE);
                                        text_tra_loi_dung_or_sai.setText("Bạn vừa quay được 1 viên Kim cương.");
                                        text_tra_loi_dung_or_sai.setTextColor(mActivity.getResources().getColor(
                                                R.color.main_color));

                                        mCurrentKc += 1;
                                        SavedStore.editKC(mCurrentKc);
                                        int c = new Integer(mCurrentKc + 1);
                                        tvKc.setText("" + c);
                                        // tvKc.setText("" + mCurrentKc);
                                        mActivity.showToast("Oh, Bạn thật may mắn", false);
                                    }
                                    break;
                                }
                    }

                });

    }

    private void updateUi() {
        mCurrentKc = SavedStore.getKC();
        int c = new Integer(mCurrentKc + 1);
        tvKc.setText("" + c);
        // tvKc.setText(mCurrentKc + "");

        mCurrentCoin = SavedStore.getCoin();
        int i = new Integer(mCurrentCoin + 1);
        tvCoin.setText("" + i);

        updateSpin();
    }

    private void updateSpin() {
        int s = new Integer(SavedStore.getSpin() + 1);
        textTotalSpin.setText("" + s + "/" + (spin_num + 1));

    }

    @Override
    public void onStop() {
        super.onStop();
        mSoundPoolHelper.release();
    }

    private void showCoin(int i) {
        text_tra_loi_dung_or_sai.setVisibility(View.VISIBLE);
        text_tra_loi_dung_or_sai.setText(mResources.getString(R.string.ban_quay_duoc).replace("#", "" + i));
        text_tra_loi_dung_or_sai.setTextColor(mActivity.getResources().getColor(R.color.main_color));

    }

    /**
     * play a sound
     *
     * @param soundId
     */
    private void playSound(int soundId) {
        if (SavedStore.getSound())
            mSoundPoolHelper.play(soundId);
    }

    /**
     * Get Degrees random
     *
     * @return
     */
    private int getDegreesRandom() {
        Random r = new Random();
        int i1 = r.nextInt(Degrees_MAX - Degrees_MIN) + Degrees_MIN;
        return i1;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                mActivity.popFragment();
                break;
            case R.id.btnSpin:
                int spin = SavedStore.getSpin();
                if (spin - 1 < -1 /*0*/) {
                    mActivity.showToast(mResources.getString(R.string.het_luot_quay), true);

                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            int hienthi = SavedStore.getHienThi();
                            if (!SavedStore.getFirstRating() && (hienthi == 300 || (hienthi == 301 && SavedStore.getCurrentNumberQuestion() > 15))) {
                                buildDialogConfirm(mResources.getString(R.string.frg_vongxoay_text_rating), new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            SavedStore.editFirstRating(true);
                                            int spin = SavedStore.getSpin();
                                            SavedStore.editSpin(spin + 5);
                                            updateSpin();

                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + mActivity.getPackageName())));
                                        } catch (Exception e) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri
                                                    .parse("https://play.google.com/store/apps/details?id=" + mActivity.getPackageName())));
                                        }
                                        Logger.d("Package name: https://play.google.com/store/apps/details?id=" + mActivity.getPackageName());
                                    }
                                });
                            }
                        }
                    }, 2000);


                    break;
                }
                SavedStore.editSpin(spin - 1);
                updateSpin();

                text_tra_loi_dung_or_sai.setVisibility(View.INVISIBLE);
                btnSpin.setEnabled(false);

                /* play sound when spin */
                playSound(soundBgsound);

                degree_random = getDegreesRandom();
                rotationAnimation.setDegrees(degree_random);
                rotationAnimation.animate();
                break;

            case R.id.btnKcDoiCoin:
                if (mCurrentKc > -1 /*0*/) {
                    mCurrentCoin += 100;
                    mCurrentKc -= 1;

                    int i = new Integer(mCurrentCoin + 1);
                    tvCoin.setText("" + i);

                    int c = new Integer(mCurrentKc + 1);
                    tvKc.setText("" + c);
                    // tvKc.setText("" + mCurrentKc);

                    SavedStore.editCoin(mCurrentCoin);
                    SavedStore.editKC(mCurrentKc);

                    mActivity.showToast("Đổi thành công", false);
                } else
                    mActivity.showToast("Bạn đã hết kim cương", true);

                break;
            case R.id.btnKcDoiCao:
                if (mCurrentKc >= 2 /*3*/) {
                    mCurrentKc -= 3;
                    int tileCao = SavedStore.getTiLeCao();

                    int c = new Integer(mCurrentKc + 1);
                    tvKc.setText("" + c);
                    // tvKc.setText(mCurrentKc + "");

                    SavedStore.editKC(mCurrentKc);
                    SavedStore.editTiLeCao((tileCao + 5));

                    mActivity.showToast("Đổi thành công", false);
                } else
                    mActivity.showToast("Bạn không đủ kim cương", true);

                break;
            case R.id.btnKcDoiVip:
                if (mCurrentKc >= 9 /*10*/) {
                    mCurrentKc -= 10;

                    int tileCao = SavedStore.getTiLeCao();

                    SavedStore.editIsVip(true);
                    SavedStore.editKC(mCurrentKc);
                    SavedStore.editTiLeCao(tileCao + 10);

                    int c = new Integer(mCurrentKc + 1);
                    tvKc.setText("" + c);
                    //tvKc.setText(mCurrentKc + "");
                    mActivity.showToast("Đổi thành công", false);
                    updateVipToServer();
                } else
                    mActivity.showToast("Bạn không đủ kim cương", true);
                break;
            case R.id.btnMuaVip:
                if (mCurrentCoin >= 1999/*2000*/) {
                    mCurrentCoin -= 2000;
                    SavedStore.editIsVip(true);
                    SavedStore.editCoin(mCurrentCoin);
                    int i = new Integer(mCurrentCoin + 1);
                    tvCoin.setText("" + i);

                    int tileCao = SavedStore.getTiLeCao();
                    SavedStore.editTiLeCao(tileCao + 10);
                    mActivity.showToast("Đổi thành công", false);

//                    ParseUser currentUser = ParseUser.getCurrentUser();
//                    currentUser.put("vip", true);
//                    currentUser.saveInBackground();

                    updateVipToServer();
                } else
                    mActivity.showToast("Bạn không đủ coins", true);
                break;

            default:
                break;
        }

    }

    private void updateVipToServer() {
        String android_id = Settings.Secure.getString(mActivity.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        User user = new User();
        user.setDeviceId(android_id);
        user.setFullname(android_id);
        user.setVip(true);
        mActivity.mAPIService.updateUserVip(user).enqueue(new Callback<BaseObject<User>>() {
            @Override
            public void onResponse(Call<BaseObject<User>> call, Response<BaseObject<User>> response) {
                if (response.body() != null)
                    Logger.d("Update user vip success :" + response.body().isSuccess());
            }

            @Override
            public void onFailure(Call<BaseObject<User>> call, Throwable t) {
                Logger.d("Invalid response's format.");
            }
        });

    }
}
