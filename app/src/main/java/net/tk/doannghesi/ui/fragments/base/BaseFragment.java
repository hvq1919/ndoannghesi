package net.tk.doannghesi.ui.fragments.base;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import net.tk.doannghesi.Define;
import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.facebook.FacebookUser;
import net.tk.doannghesi.tools.ScreenUtils;
import net.tk.doannghesi.tools.encryptfile.CryptoUtils;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.ui.activities.MainActivity;
import net.tk.doannghesi.ui.models.Player;

import java.io.IOException;
import java.io.InputStream;

public abstract class BaseFragment extends Fragment {
    protected MainActivity mActivity;
    protected Resources mResources;
    protected View mRootView;

    public int deviceWidth;
    public int deviceHeight;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        mResources = mActivity.getResources();

        Logger.d("BaseFragment - onCreate");

        deviceHeight = ScreenUtils.getHeight(mActivity);
        deviceWidth = ScreenUtils.getWidth(mActivity);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.d("BaseFragment - onCreateView");
        return super.onCreateView(inflater, container, savedInstanceState);

    }

    protected void signInFireBase() {
        String android_id = Settings.Secure.getString(mActivity.getContentResolver(),
                Settings.Secure.ANDROID_ID);


        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users");



        FacebookUser facebookUser = (FacebookUser) SavedStore.getFbUserObject();
        if (facebookUser == null) {
            //if (!SavedStore.getOnlyFbUser()) {
                Player player = new Player(android_id, android_id, "", "",
                        SavedStore.getCurrentNumberQuestion() + 1,
                        SavedStore.getIsVip());

                myRef.child(android_id).setValue(player);
            //}
        } else {
            Player player = new Player(android_id, facebookUser.getName(), facebookUser.getId(), facebookUser.getAvatarURL(),
                    SavedStore.getCurrentNumberQuestion() + 1
                    , SavedStore.getIsVip());

            myRef.child(android_id).setValue(player);
        }
    }


    // /////////////////////////////////////////
    // / Method public ////////////////////////

    public void buildDialogConfirm(String text, DialogInterface.OnClickListener clickListener) {
        new AlertDialog.Builder(mActivity).setMessage(text).setPositiveButton("Yes", clickListener)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
    }

    public void buildDialogConfirmHuongDan(String text) {
        new AlertDialog.Builder(mActivity).setMessage(text)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    // do something when the button is clicked
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                }).show();
    }

    protected Bitmap getBitmapFromAsset(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();

        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);

            bitmap = CryptoUtils.decryptBitmap(Define.app_id/*key*/, istr);
            // bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            // handle exception
        }

        return bitmap;
    }
}
