package net.tk.doannghesi.ui.fragments.thuthach.trollertap.objects;

import java.io.Serializable;

public class CurrentMainDot implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2765855209625938600L;
	private float time_tang;
	private int RATE_G;
	private int RATE_V0;
	private int main_color;

	public CurrentMainDot(float time_tang, int rATE_G, int rATE_V0) {
		super();
		this.time_tang = time_tang;
		RATE_G = rATE_G;
		RATE_V0 = rATE_V0;
	}

	public CurrentMainDot(float time_tang, int rATE_G, int rATE_V0, int main_color) {
		super();
		this.time_tang = time_tang;
		RATE_G = rATE_G;
		RATE_V0 = rATE_V0;
		this.main_color = main_color;
	}

	public CurrentMainDot() {
		super();
		// TODO Auto-generated constructor stub
	}

	public float getTime_tang() {
		return time_tang;
	}

	public void setTime_tang(float time_tang) {
		this.time_tang = time_tang;
	}

	public int getRATE_G() {
		return RATE_G;
	}

	public void setRATE_G(int rATE_G) {
		RATE_G = rATE_G;
	}

	public int getRATE_V0() {
		return RATE_V0;
	}

	public void setRATE_V0(int rATE_V0) {
		RATE_V0 = rATE_V0;
	}

	public int getMain_color() {
		return main_color;
	}

	public void setMain_color(int main_color) {
		this.main_color = main_color;
	}

}
