package net.tk.doannghesi.ui.fragments;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.easyandroidanimations.library.BlinkAnimation;
import com.easyandroidanimations.library.BounceAnimation;
import com.gc.materialdesign.views.ButtonRectangle;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.tk.doannghesi.Define;
import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.retrofit.api.ApiUtils;
import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.retrofit.objects.User;
import net.tk.doannghesi.tools.DateTimeUtils;
import net.tk.doannghesi.tools.Utils;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.tools.log.Toaster;
import net.tk.doannghesi.tools.sound.SoundPoolHelper;
import net.tk.doannghesi.ui.activities.MainActivity.IOnGetGift;
import net.tk.doannghesi.ui.controls.FlowLayout;
import net.tk.doannghesi.ui.controls.MyView;
import net.tk.doannghesi.ui.controls.MyView.OnGetPercent;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.ui.fragments.thuthach.FrgFindSamePic;
import net.tk.doannghesi.ui.fragments.thuthach.FrgMemories;
import net.tk.doannghesi.ui.fragments.thuthach.boomdot.FrgBoomDot;
import net.tk.doannghesi.ui.fragments.thuthach.diffrencecolor.FrgDiffrenceColor;
import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.FrgQuickMoving;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.FrgTrollerTap;
import net.tk.doannghesi.ui.models.Achievements;
import net.tk.doannghesi.ui.models.ObjectAnswer;
import net.tk.doannghesi.ui.models.ObjectHint;
import net.tk.doannghesi.ui.models.PathInfo;
import net.tk.doannghesi.ui.models.Questions;
import net.tk.doannghesi.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgMain extends BaseFragment implements OnClickListener {
    private final int TIME_POST_DELAY = 2500;

    /* Width of textHint will equal 7/8 of device's width */
    private final float RATE_TEXT_HINT = 1f * 7 / 8;

    /* Width of textAnswer will equal 3/4 of device's width */
    private final float RATE_TEXT_ANSWER = 1f * 3 / 4;

    /* Padding will equal 1/10 item's width */
    private final float RATE_PADDING = 1f * 1 / 10;

    /* textAnswer and textHint will have 7 item each line */
    private final int NUM_ITEM = 8;

    private static final int[] bg_main = {R.drawable.bg_main_level1_10, R.drawable.bg_main_level10_20,
            R.drawable.bg_main_level20_30, R.drawable.bg_main_level30_40, R.drawable.bg_main_level40_100,
            R.drawable.bg_main_level100_200, R.drawable.bg_main_level200_500};

    /* Scratch view */
    private MyView mMyView;
    private TextView textPercent;

    private LinearLayout llMain;

    private LinearLayout llWarning;

    /* Actionbar View */
    private RelativeLayout actionbar;
    private TextView textCoin;
    private TextView textCurrentQuestion;
    private ImageView fb;
    private ImageView gift_online;
    private TextView textGiftOnline;

    private RelativeLayout wrapper_img;
    private ImageView img;
    private FlowLayout textAnswer;
    private FlowLayout textHint;
    private View rootView;

    // chua view Cao lai, mo mot o chu, huong dan, next cau hoi
    private View llHintQuestion;
    private boolean isMoMotO;
    private boolean isCaoLai;
    private boolean isHuongDan;
    private boolean isNextQuestion;

    private View actionbarDivider;
    private View imgDivider;
    private View textHintDivider;

    private int widthAnswerItem;
    private int widthHintItem;

    private TextView text_tra_loi_dung_or_sai;
    private TextView text_tenNgheSi;
    private TextView text_cong_5_coin;
    private ButtonRectangle btnNext;

    private float percent = 5f;

    private ArrayList<ObjectHint> objectHints;
    private ArrayList<ObjectAnswer> objectAnswers;

    /* List view use for animation */
    private ArrayList<View> listViewsAnimation;

    private Bitmap oldBitmap;

    //private  int startQuestion;
    private int numberCurrentQuestion;
    private Questions mCurrentQuestion;

    private boolean isFull;

    /*---------- BackGround RUnnable -------------*/
    // private int timeRunActionBar;
    private int timeBackground;

    // private ObjectAnimator animatorActionbar;
    private ObjectAnimator animatorBackground;
    // private int colorActionbar1, colorActionbar2;
    private int colorBackground1, colorBackground2;

    private Handler mHandler;

    private int mCurrentCoin;

    private boolean isNotEnoughCoin;

    /* Sound */
    private SoundPoolHelper mSoundPoolHelper;
    private int soundClickHint;
    private int soundClickAnswer;
    private int soundWrongAnswer;
    private int soundCorrect;
    private int soundBgsound;

    private Achievements achievements;

    // private BackGroundSound mBackgroundSound;


    private boolean isClick;
    private boolean isPositiveClick;


    private boolean isOnline;

    @Override
    public void onResume() {
        super.onResume();
        mActivity.mBackGroundSound.startSound();

        Logger.d("MainFrg OnResume");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        calculatePadding();
    }

    private void calculatePadding() {

        widthAnswerItem = (int) ((deviceWidth * RATE_TEXT_ANSWER) / (NUM_ITEM * (1 + 1 / RATE_PADDING)) / RATE_PADDING);

        widthHintItem = (int) ((deviceWidth * RATE_TEXT_HINT) / (NUM_ITEM * (1 + 1 / RATE_PADDING)) / RATE_PADDING);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frg_main, container, false);

        isOnline = SavedStore.getIsOnlineQuestion();

        initObject();
        calculateSizeOfViews();
        runnable();
        loadSound();
        checkOnorOff();
        // setBackGroundMain();
        loadAdMob();

        llWarning.setOnClickListener(this);
        fb.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        gift_online.setOnClickListener(this);
        mRootView.findViewById(R.id.imgCaolai).setOnClickListener(this);
        mRootView.findViewById(R.id.imgMoMotO).setOnClickListener(this);
        mRootView.findViewById(R.id.imgHuongDan).setOnClickListener(this);
        mRootView.findViewById(R.id.imgNextQuestion).setOnClickListener(this);
        mRootView.findViewById(R.id.gift_online).setOnClickListener(this);

        mRootView.findViewById(R.id.back).setOnClickListener(this);

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new drawCanvas().execute();
            }
        }, 50);

        // Inflate the layout for this fragment
        return mRootView;

    }

    private AdView mAdView;

    private void loadAdMob() {
        mAdView = (AdView) mRootView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    /**
     * Set background for actionbar and background
     */
    private void runnable() {
        mHandler = new Handler();

        // colorActionbar1 = mActivity.getRandomActionBarColor();
        colorBackground1 = mActivity.getRandomBgColor();

        // animatorActionbar = ObjectAnimator.ofInt(actionbar,
        // "backgroundColor", colorActionbar1);
        // animatorActionbar.setEvaluator(new ArgbEvaluator());
        // animatorActionbar.setDuration(timeRunActionBar);

        animatorBackground = ObjectAnimator.ofInt(rootView, "backgroundColor", colorBackground1);
        animatorBackground.setEvaluator(new ArgbEvaluator());
        animatorBackground.setDuration(timeBackground);

        // mHandler.post(runnableActionbar);
        mHandler.post(runnableBackground);

    }

    private void initObject() {
        //percent = SavedStore.getTiLeCao() * 1.0f;

        int num = SavedStore.getCurrentNumberQuestion();
        if(num == 0 || num == 1 ) percent = 30;
        else if(num == 2) percent = 15;
        else percent = SavedStore.getTiLeCao() * 1.0f;

        mMyView = (MyView) mRootView.findViewById(R.id.myView);
        textPercent = (TextView) mRootView.findViewById(R.id.textPercent);
        mMyView.setOnGetPercent(new OnGetPercent() {
            @Override
            public void onGetPercent(float p) {
                if (p >= percent) {
                    textPercent.setText(String.format("%.2f", percent) + "%");
                    mMyView.setCanScratch(false);
                } else
                    textPercent.setText(String.format("%.2f", p) + "%");

            }
        });

        llWarning = (LinearLayout) mRootView.findViewById(R.id.ll_waring);
        actionbar = (RelativeLayout) mRootView.findViewById(R.id.actionbar);
        textCoin = (TextView) mRootView.findViewById(R.id.coin);
        textCurrentQuestion = (TextView) mRootView.findViewById(R.id.textCurrentQuestion);
        fb = (ImageView) mRootView.findViewById(R.id.fb);
        gift_online = (ImageView) mRootView.findViewById(R.id.gift_online);
        textGiftOnline = (TextView) mRootView.findViewById(R.id.textTimeGiftOnline);
        if (mActivity.isGetGift)
            gift_online.setImageResource(R.drawable.gift_normal2);
        else
            gift_online.setImageResource(R.drawable.gift_normal0);

        mActivity.setOnGetGift(new IOnGetGift() {

            @Override
            public void onTime(int secs) {
                textGiftOnline.setText(DateTimeUtils.getFormattedTime(secs));
            }

            @Override
            public void onGetGift() {
                mActivity.isGetGift = true;
                gift_online.setImageResource(R.drawable.gift_normal2);

                new BounceAnimation(gift_online).animate();

            }
        });

        llMain = (LinearLayout) mRootView.findViewById(R.id.llMain);

        wrapper_img = (RelativeLayout) mRootView.findViewById(R.id.wrapper_img);
        rootView = mRootView.findViewById(R.id.rootView);
        img = (ImageView) mRootView.findViewById(R.id.img);
        textAnswer = (FlowLayout) mRootView.findViewById(R.id.textAnswer);
        textHint = (FlowLayout) mRootView.findViewById(R.id.textHint);
        actionbarDivider = mRootView.findViewById(R.id.actionbarDivider);
        imgDivider = mRootView.findViewById(R.id.imgDivider);
        textHintDivider = mRootView.findViewById(R.id.textHintDivider);

        llHintQuestion = mRootView.findViewById(R.id.llHintQuestion);

        text_tenNgheSi = (TextView) mRootView.findViewById(R.id.text_tenNgheSi);
        text_tra_loi_dung_or_sai = (TextView) mRootView.findViewById(R.id.text_tra_loi_dung_or_sai);
        text_cong_5_coin = (TextView) mRootView.findViewById(R.id.text_cong_5_coin);
        btnNext = (ButtonRectangle) mRootView.findViewById(R.id.btnNext);

		/* Set background */

        objectHints = new ArrayList<ObjectHint>();
        objectAnswers = new ArrayList<ObjectAnswer>();

        listViewsAnimation = new ArrayList<View>();

        // timeRunActionBar = SavedStore.getTimeRunnalbeActionbar();
        timeBackground = SavedStore.getTimeRunnalbeBackground();

        mCurrentCoin = SavedStore.getCoin();
        int i = new Integer(mCurrentCoin + 1);
        textCoin.setText("" + i);

        // startQuestion = SavedStore.getStart_Question();

        achievements = (Achievements) SavedStore.getAchievements();
        if (achievements == null)
            achievements = new Achievements();

		/* Dialog */
    }

    private void loadSound() {

        mSoundPoolHelper = new SoundPoolHelper(1, mActivity);
        soundClickHint = mSoundPoolHelper.load(mActivity, R.raw.clickhint, 1);
        soundClickAnswer = mSoundPoolHelper.load(mActivity, R.raw.clickanswer, 1);
        soundWrongAnswer = mSoundPoolHelper.load(mActivity, R.raw.wronganswer, 1);
        soundCorrect = mSoundPoolHelper.load(mActivity, R.raw.correct, 1);
        soundBgsound = mSoundPoolHelper.load(mActivity, R.raw.bgsound1, 1);

    }

    private void calculateSizeOfViews() {
        /* Divider */
        actionbarDivider.getLayoutParams().height = widthAnswerItem / 2;
        imgDivider.getLayoutParams().height = widthAnswerItem / 2;
        textHintDivider.getLayoutParams().height = widthAnswerItem / 2;

		/* Action bar */
        LayoutParams actionbarParams = actionbar.getLayoutParams();
        actionbarParams.height = deviceHeight / 10;

		/* Image size */
        LayoutParams imgParams = wrapper_img.getLayoutParams();
        imgParams.width = (int) (deviceWidth * RATE_TEXT_HINT);
        imgParams.height = (int) ((deviceWidth * RATE_TEXT_HINT) * 11 / 16);

		/* Text Answer */
        textAnswer.getLayoutParams().width = (int) (deviceWidth * RATE_TEXT_ANSWER);

		/* Text Hint */
        textHint.getLayoutParams().width = (int) (deviceWidth * RATE_TEXT_HINT);

    }

    private void setBackGroundMain() {
        if (numberCurrentQuestion <= 10)
            llMain.setBackgroundResource(bg_main[0]);
        else if (numberCurrentQuestion <= 20)
            llMain.setBackgroundResource(bg_main[1]);
        else if (numberCurrentQuestion <= 30)
            llMain.setBackgroundResource(bg_main[2]);
        else if (numberCurrentQuestion <= 40)
            llMain.setBackgroundResource(bg_main[3]);
        else if (numberCurrentQuestion <= 100)
            llMain.setBackgroundResource(bg_main[4]);
        else if (numberCurrentQuestion <= 200)
            llMain.setBackgroundResource(bg_main[5]);
        else
            llMain.setBackgroundResource(bg_main[6]);
    }

    private void updateUi() {
        Logger.e("Current Online:"+ SavedStore.getCurrentOnlineQuestion());

        int n = new Integer(numberCurrentQuestion + 1);
        textCurrentQuestion.setText("" + n /*(numberCurrentQuestion *//*- Define.START_QUESTION*//* + 1)*/);

        setBackGroundMain();

        makeListText(mCurrentQuestion);
        setImage(numberCurrentQuestion);
        setBackground();

        textAnswer.removeAllViews();
        textHint.removeAllViews();
        for (int i = 0; i < objectAnswers.size(); i++) {
            View child = mActivity.getLayoutInflater().inflate(R.layout.item, null);
            // TextView tvContent = (TextView)
            // child.findViewById(R.id.tvContent);
            // tvContent.setText(objectAnswers.get(i).getText());

            child.findViewById(R.id.relItem).setOnClickListener(new ClickListenerAnswer(objectAnswers.get(i), child));

            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(widthAnswerItem, widthAnswerItem);
            params.rightMargin = (int) (widthAnswerItem * RATE_PADDING);
            params.bottomMargin = (int) (widthAnswerItem * RATE_PADDING);
            child.setLayoutParams(params);
            textAnswer.addView(child);
            objectAnswers.get(i).setChild(child);
            // textHint.addView(child);
            /* Create list views animation */
            listViewsAnimation.add(child);
        }

        for (int i = 0; i < objectHints.size(); i++) {
            View child = mActivity.getLayoutInflater().inflate(R.layout.item_hint, null);
            TextView tvContent = (TextView) child.findViewById(R.id.tvContent);
            tvContent.setText(objectHints.get(i).getText());
            tvContent.setTypeface(tvContent.getTypeface(), Typeface.BOLD);

            child.findViewById(R.id.relItem).setOnClickListener(new ClickListenerHint(objectHints.get(i), child));

            FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(widthHintItem, widthHintItem);
            params.rightMargin = (int) (widthHintItem * RATE_PADDING);
            params.bottomMargin = (int) (widthHintItem * RATE_PADDING);
            child.setLayoutParams(params);
            textHint.addView(child);
            objectHints.get(i).setChild(child);
        }

        if(numberCurrentQuestion == 0 || numberCurrentQuestion == 3 || numberCurrentQuestion == 9){
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    llWarning.setVisibility(View.VISIBLE);
                }
            },5000);
        }
    }

    private void checkOnorOff() {
        numberCurrentQuestion = SavedStore.getCurrentNumberQuestion();
        if (isOnline) {
            mActivity.showLoadingDialog();
            mActivity.mAPIService.getQuestionById(SavedStore.getCurrentOnlineQuestion()).enqueue(new Callback<BaseObject<Questions>>() {
                @Override
                public void onResponse(Call<BaseObject<Questions>> call, Response<BaseObject<Questions>> response) {
                    mActivity.dismissLoadingDialog();
                    if (response.body() != null && response.body().isSuccess() && response.body().getData() != null) {
                        mCurrentQuestion = response.body().getData();
                    } else {

                        mCurrentQuestion = mActivity.getCurrentQuestion(numberCurrentQuestion - SavedStore.getStart_Question());
                    }
                    updateUi();
                }

                @Override
                public void onFailure(Call<BaseObject<Questions>> call, Throwable t) {
                    mActivity.dismissLoadingDialog();
                    mCurrentQuestion = mActivity.getCurrentQuestion(numberCurrentQuestion - SavedStore.getStart_Question());
                    updateUi();
                }
            });
        } else {
            mCurrentQuestion = mActivity.getCurrentQuestion(numberCurrentQuestion - SavedStore.getStart_Question());
            updateUi();
        }


    }

    /**
     * play a sound
     *
     * @param soundId
     */
    private void playSound(int soundId) {
        if (SavedStore.getSound())
            mSoundPoolHelper.play(soundId);
    }

    /**
     * To set background for view
     */
    private void setBackground() {
        // actionbar.setBackgroundColor(mActivity.getRandomActionBarColor());
        rootView.setBackgroundColor(mActivity.getRandomBgColor());
    }

    /**
     * To set image
     */
    private void setImage(int numberQuestion) {
        if (isOnline) {
            Logger.e("URL:" + mCurrentQuestion.getImg_name());
            ImageLoader.getInstance().displayImage(ApiUtils.BASE_URL + mCurrentQuestion.getImg_name(), img);
        } else {
            Bitmap bm = getBitmapFromAsset(mActivity, mCurrentQuestion.getImg_name());
            if (bm != null)
                img.setImageBitmap(bm);
            // Recycle bitmap
            if (oldBitmap != null)
                oldBitmap.recycle();
            oldBitmap = bm;
        }
    }

    /**
     * To create object text from question
     *
     * @param currentQuestion
     */
    private void makeListText(Questions currentQuestion) {
        objectAnswers.clear();
        objectHints.clear();
        listViewsAnimation.clear();

        String hint = shuffle(currentQuestion.getWrong_answer());
        String answer = currentQuestion.getAnswer();

        for (int i = 0; i < hint.length(); i++) {
            ObjectHint objectHint = new ObjectHint();
            objectHint.setId(i);
            objectHint.setText(hint.charAt(i) + "");
            objectHints.add(objectHint);
        }

        for (int i = 0; i < answer.length(); i++) {
            ObjectAnswer objectAnswer = new ObjectAnswer();
            objectAnswer.setId(i);
            objectAnswer.setText(answer.charAt(i) + "");
            objectAnswers.add(objectAnswer);
        }

    }

    /**
     * To random characters's order of string
     *
     * @param input
     * @return
     */
    private String shuffle(String input) {
        List<Character> characters = new ArrayList<Character>();
        for (char c : input.toCharArray()) {
            characters.add(c);
        }
        StringBuilder output = new StringBuilder(input.length());
        while (characters.size() != 0) {
            int randPicker = (int) (Math.random() * characters.size());
            output.append(characters.remove(randPicker));
        }
        return output.toString();
    }

    // /**
    // * To get bitmap from asset folder
    // *
    // * @param context
    // * @param filePath
    // * @return
    // */
    // private Bitmap getBitmapFromAsset(Context context, String filePath) {
    // AssetManager assetManager = context.getAssets();
    //
    // InputStream istr;
    // Bitmap bitmap = null;
    // try {
    // istr = assetManager.open(filePath);
    // bitmap = BitmapFactory.decodeStream(istr);
    // } catch (IOException e) {
    // // handle exception
    // }
    //
    // return bitmap;
    // }

    @Override
    public void onDestroyView() {
        SavedStore.editPaths(mMyView.getPaths());
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (oldBitmap != null)
            oldBitmap.recycle();
        // mHandler.removeCallbacks(runnableActionbar);
        mHandler.removeCallbacks(runnableBackground);

        mSoundPoolHelper.release();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_waring:
                llWarning.setVisibility(View.GONE);
                break;
            case R.id.fb:

                rootView.setDrawingCacheEnabled(true);
                rootView.buildDrawingCache();
                Bitmap bm = rootView.getDrawingCache();

                mActivity.getFacebookHelper().doShareImage(bm);

                break;
            case R.id.gift_online:
                if (!mActivity.isOnline())
                    mActivity.showToast(mResources.getString(R.string.khong_co_ket_noi_internet), true);
                else {
                    mActivity.mTimeOnlineHandler.removeCallbacks(mActivity.mTimeOnlineRunnable);
                    mActivity.mTimeOnlineHandler.post(mActivity.mTimeOnlineRunnable);

                    if (mActivity.isGetGift) {
                        gift_online.setImageResource(R.drawable.gift_normal0);
                        mActivity.isGetGift = false;

                        mActivity.mTimeOnlineHandler.removeCallbacks(mActivity.mTimeOnlineRunnable);
                        mActivity.mTimeOnlineHandler.post(mActivity.mTimeOnlineRunnable);

                        if (mActivity.current_index == 0) {
                            mCurrentCoin += mActivity.coins[mActivity.coins.length - 1];
                            int i = new Integer(mCurrentCoin + 1);
                            textCoin.setText("" + i);
                            Toaster.showToast(mActivity, "Bạn nhận được:" + mActivity.coins[mActivity.coins.length - 1]
                                    + " coins");

                        } else {
                            mCurrentCoin += mActivity.coins[mActivity.current_index - 1];
                            int i = new Integer(mCurrentCoin + 1);
                            textCoin.setText("" + i);
                            mActivity.showToast("Bạn nhận được:" + mActivity.coins[mActivity.current_index - 1] + " coins",
                                    false);

                        }

                        // TODO add coins animation

                    } else {
                        textGiftOnline.setVisibility(View.VISIBLE);
                        mHandler.postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                textGiftOnline.setVisibility(View.GONE);
                            }
                        }, 5000);
                    }
                }

                // DateTimeUtils.getFormattedTime(secs)

                break;
            case R.id.btnNext:
                int totalOnline = SavedStore.getTotalOnlineQuestion();
                int currentOnline = SavedStore.getCurrentOnlineQuestion();

                if (!isOnline && (numberCurrentQuestion - SavedStore.getStart_Question()) == (mActivity.getListQuestion().size() - 1)) {
                    SavedStore.editStart_Question(numberCurrentQuestion + 1);

                    if (totalOnline > 0 && totalOnline > currentOnline) {
                        isOnline = true;
                        SavedStore.editIsOnlineQuestion(true);
                    }

                    new makeListQuestion(new IOnDoneMakeList() {
                        @Override
                        public void onDone() {
                            doBtnNextClicked();
                        }
                    }).execute();
                } else {
                    if (isOnline && currentOnline > totalOnline) {
                        isOnline = false;
                        SavedStore.editIsOnlineQuestion(false);
                    }

                    doBtnNextClicked();
                }
                break;
            case R.id.back:
                mActivity.popFragment();
                break;

            case R.id.imgCaolai:
                if (!isCaoLai)
                    buildDialogConfirm(String.format(mResources.getString(R.string.text_cao_lai), Define.COIN_CAOLAI), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            isCaoLai = true;
                            doOnCaoLai();
                        }
                    });
                else
                    doOnCaoLai();
                break;
            case R.id.imgMoMotO:
                if (objectAnswers.size() == 0)
                    break;
                if (!isMoMotO/* SavedStore.getNoted() */)
                    buildDialogConfirm(String.format(mResources.getString(R.string.text_note_clicked), Define.COIN_NOTE),
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // SavedStore.editNoted(true);
                                    isMoMotO = true;
                                    doOnNoteClicked();
                                }
                            });
                else
                    doOnNoteClicked();

                break;
            case R.id.imgHuongDan:
                if (!isHuongDan)
                    buildDialogConfirm(String.format(mResources.getString(R.string.text_huong_dan), Define.COIN_HUONGDAN),
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    isHuongDan = true;
                                    doOnHuongDan();
                                }
                            });
                else
                    doOnHuongDan();
                break;
            case R.id.imgNextQuestion:
                if (!isNextQuestion)
                    buildDialogConfirm(String.format(mResources.getString(R.string.text_next_question), Define.COIN_NEXT_QUESTION),
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    isNextQuestion = true;
                                    doOnNextQuestion();
                                }
                            });
                else
                    doOnNextQuestion();
                break;
            default:
                break;
        }
    }

    private void doOnCaoLai() {

		/* Update coin */
        if ((mCurrentCoin - Define.COIN_CAOLAI) < -1 /*0*/) {
            if (!isNotEnoughCoin)
                Toaster.showToast(mActivity, mResources.getString(R.string.not_enough_coin));
            return;
        }

        mCurrentCoin -= Define.COIN_CAOLAI;
        SavedStore.editCoin(mCurrentCoin);
        int i = new Integer(mCurrentCoin + 1);
        textCoin.setText("" + i);

        mMyView.caoLai();
        mMyView.setCanScratch(true);

        textPercent.setText(String.format("%.2f", 0.00) + "%");
    }

    private void doOnHuongDan() {
        /* Update coin */
        if ((mCurrentCoin - Define.COIN_HUONGDAN) < -1 /*0*/) {
            if (!isNotEnoughCoin)
                Toaster.showToast(mActivity, mResources.getString(R.string.not_enough_coin));
            return;
        }

        buildDialogConfirmHuongDan(mCurrentQuestion.getNote());

        mCurrentCoin -= Define.COIN_HUONGDAN;
        SavedStore.editCoin(mCurrentCoin);
        int i = new Integer(mCurrentCoin + 1);
        textCoin.setText("" + i);

    }

    private void doBtnNextClicked() {
        mMyView.resetMyView();
        mMyView.setCanScratch(true);
        isFull = false;

        // TODO
        if (numberCurrentQuestion > 0 && (numberCurrentQuestion + 10) % 15 == 0) {
            mActivity.popFragment();

            // int thuthach = new Random().nextInt(6) + 1;
            int thuthach = (numberCurrentQuestion / 15) % 7 + 1;

            // TODO test
            // thuthach = Define.THUTHACH_TROLLERTAP;

            SavedStore.editThuThach(thuthach);

            if (thuthach == Define.THUTHACH_BOOMDOT)
                mActivity.pushFragment(new FrgBoomDot(), true);
            else if (thuthach == Define.THUTHACH_DIFFRENCECOLOR)
                mActivity.pushFragment(new FrgDiffrenceColor(), true);
            else if (thuthach == Define.THUTHACH_MEMORIES)
                mActivity.pushFragment(new FrgMemories(), true);
            else if (thuthach == Define.THUTHACH_QUICKMOVING)
                mActivity.pushFragment(new FrgQuickMoving(), true);
            else if (thuthach == Define.THUTHACH_SAMEPIC)
                mActivity.pushFragment(new FrgFindSamePic(), true);
            else
                mActivity.pushFragment(new FrgTrollerTap(), true);

            return;
        }

        textHint.setVisibility(View.VISIBLE);
        llHintQuestion.setVisibility(View.VISIBLE);
        text_tenNgheSi.setVisibility(View.GONE);
        text_tra_loi_dung_or_sai.setVisibility(View.GONE);
        text_cong_5_coin.setVisibility(View.GONE);
        btnNext.setVisibility(View.GONE);
        mAdView.setVisibility(View.GONE);

        // mHandler.postDelayed(new Runnable() {
        // @Override
        // public void run() {
        // new EditCurrentNumberQuestion(numberCurrentQuestion, new OnDone()
        // {
        // @Override
        // public void doOnDone() {
        checkOnorOff();
        // }
        // }).execute();
        // }
        // }, 50);
    }

    private void doOnNextQuestion() {
        /* Update coin */
        if ((mCurrentCoin - Define.COIN_NEXT_QUESTION) < -1 /*0*/) {
            if (!isNotEnoughCoin)
                Toaster.showToast(mActivity, mResources.getString(R.string.not_enough_coin));
            return;
        }

        SavedStore.editCurrentNumberQuestion("" + (numberCurrentQuestion + 1));
        if (isOnline) {
            SavedStore.editCurrentOnlineQuestion(SavedStore.getCurrentOnlineQuestion() + 1);
            SavedStore.editStart_Question(SavedStore.getStart_Question() + 1);
        }

        int num = SavedStore.getCurrentNumberQuestion();
        if(num == 0 || num == 1 ) percent = 30;
        else if(num == 2) percent = 15;
        else percent = SavedStore.getTiLeCao() * 1.0f;

        mMyView.clearAllBitmap();
        textPercent.setText(String.format("%.2f", 0.00) + "%");

        btnNext.setVisibility(View.VISIBLE);
        textHint.setVisibility(View.GONE);
        llHintQuestion.setVisibility(View.GONE);
        // text_cong_5_coin.setVisibility(View.VISIBLE);
        mAdView.setVisibility(View.VISIBLE);
        // text_cong_5_coin.setTextColor(mActivity.getRandomActionBarColor());

        text_tenNgheSi.setVisibility(View.VISIBLE);
        text_tenNgheSi.setText(mCurrentQuestion.getName());

        if (numberCurrentQuestion > 38 && numberCurrentQuestion % 20 == 0) {

            // TODO check tang coins

            Utils.buildDialog(mActivity, "Yes", "No", mResources.getString(R.string.dialog_click_ad_title), mResources.getString(R.string.dialog_click_ad_des),
                    new Utils.OnClickDialog() {

                        @Override
                        public void PositiveClick() {
                            mActivity.showAdsThuThach();
                            isPositiveClick = true;
                        }

                        @Override
                        public void NegativeClick() {
                        }
                    });

        } else if (numberCurrentQuestion % 3 == 2)
            mActivity.showAds();

        mCurrentCoin -= Define.COIN_NEXT_QUESTION;
        SavedStore.editCoin(mCurrentCoin);
        int i = new Integer(mCurrentCoin + 1);
        textCoin.setText("" + i);
    }

    /**
     * To handle event when click note image
     */
    private void doOnNoteClicked() {
        if (isFull)
            return;

		/* Update coin */
        if (mCurrentCoin - Define.COIN_NOTE < -1 /*0*/) {
            if (!isNotEnoughCoin)
                Toaster.showToast(mActivity, mResources.getString(R.string.not_enough_coin));
            return;
        }

        mCurrentCoin -= Define.COIN_NOTE;
        SavedStore.editCoin(mCurrentCoin);
        int i = new Integer(mCurrentCoin + 1);
        textCoin.setText("" + i);

        // TODO
        /* Open a word in text answer */
        for (ObjectAnswer ob : objectAnswers)
            if (ob.getObjectHint() == null) {
                TextView tvContent = (TextView) ob.getChild().findViewById(R.id.tvContent);
                tvContent.setText(ob.getText());
                tvContent.setTextColor(Color.CYAN);

                for (ObjectHint oh : objectHints)
                    if (oh.getText().equals(ob.getText())) {
                        oh.getChild().setVisibility(View.INVISIBLE);
                        break;
                    }

                objectAnswers.remove(ob);

                if (isFull())
                    checkAnswer();

                break;
            }

    }

    private boolean isFull() {
        for (int i = 0; i < objectAnswers.size(); i++)
            if (objectAnswers.get(i).getObjectHint() == null) {
                isFull = false;
                return false;
            }
        isFull = true;
        return true;
    }

    private void checkAnswer() {
        if (isCorrectAnswer()) {
            doCorrect();
        } else {
            playSound(soundWrongAnswer);

            text_tra_loi_dung_or_sai.setVisibility(View.VISIBLE);
            text_tra_loi_dung_or_sai.setText(mResources.getString(R.string.tra_loi_sai));
            text_tra_loi_dung_or_sai.setTextColor(mResources.getColor(R.color.pink));

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    text_tra_loi_dung_or_sai.setVisibility(View.GONE);
                }
            }, TIME_POST_DELAY);

            for (View v : listViewsAnimation)
                new BounceAnimation(v).animate();

            new BounceAnimation(text_tra_loi_dung_or_sai).animate();
        }
    }

    private void doCorrect() {
        playSound(soundCorrect);

        mCurrentCoin += 5;
        SavedStore.editCoin(mCurrentCoin);
        int i = new Integer(mCurrentCoin + 1);
        textCoin.setText("" + i);

        SavedStore.editCurrentNumberQuestion("" + (numberCurrentQuestion + 1));
        if (isOnline) {
            SavedStore.editCurrentOnlineQuestion(SavedStore.getCurrentOnlineQuestion() + 1);
            SavedStore.editStart_Question(SavedStore.getStart_Question() + 1);
        }

        int num = SavedStore.getCurrentNumberQuestion();
        if(num == 0 || num == 1 ) percent = 30;
        else if(num == 2) percent = 15;
        else percent = SavedStore.getTiLeCao() * 1.0f;

        mMyView.clearAllBitmap();
        textPercent.setText(String.format("%.2f", 0.00) + "%");

        btnNext.setVisibility(View.VISIBLE);
        textHint.setVisibility(View.GONE);
        llHintQuestion.setVisibility(View.GONE);
        text_cong_5_coin.setVisibility(View.VISIBLE);
        mAdView.setVisibility(View.VISIBLE);
        text_cong_5_coin.setTextColor(mActivity.getRandomActionBarColor());

        text_tenNgheSi.setVisibility(View.VISIBLE);
        text_tenNgheSi.setText(mCurrentQuestion.getName());

        text_tra_loi_dung_or_sai.setVisibility(View.VISIBLE);
        text_tra_loi_dung_or_sai.setText(mResources.getString(R.string.tra_loi_dung));
        text_tra_loi_dung_or_sai.setTextColor(mResources.getColor(R.color.main_color));

        for (View v : listViewsAnimation)
            new BlinkAnimation(v).animate();
        new BlinkAnimation(text_tra_loi_dung_or_sai).animate();

        if (numberCurrentQuestion > 38 && numberCurrentQuestion % 20 == 0) {

            // TODO check tang coins

            Utils.buildDialog(mActivity, "Yes", "No", mResources.getString(R.string.dialog_click_ad_title), mResources.getString(R.string.dialog_click_ad_des),
                    new Utils.OnClickDialog() {

                        @Override
                        public void PositiveClick() {
                            mActivity.showAdsThuThach();
                            isPositiveClick = true;
                        }

                        @Override
                        public void NegativeClick() {
                        }
                    });

        } else if (numberCurrentQuestion % 3 == 2)
            mActivity.showAds();


        Logger.e("Correcct----");

        // Update user level week
        String deviceId = Settings.Secure.getString(mActivity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        mActivity.mAPIService.updateUserWeek(deviceId).enqueue(new Callback<BaseObject<String>>() {
            @Override
            public void onResponse(Call<BaseObject<String>> call, Response<BaseObject<String>> response) {
                Logger.e("UpdateUserWeek Sucess:" + response.message());
            }

            @Override
            public void onFailure(Call<BaseObject<String>> call, Throwable t) {
                Logger.e("UpdateUserWeek Error:" + t.getMessage());
            }
        });
    }

    private boolean isCorrectAnswer() {
        for (ObjectAnswer ob : objectAnswers)
            if (ob.getObjectHint() == null || !ob.getText().equals(ob.getObjectHint().getText()))
                return false;

        return true;
    }

    // ///////////////////////////////////////////////////
    // /////////// ClickListener Class //////////////////
    // //////////////////////////////////////////////////

    // ---------------- Listener on hint ----------------------
    private class ClickListenerHint implements OnClickListener {
        private ObjectHint objectHint;
        private View child;

        public ClickListenerHint(ObjectHint hint, View v) {
            super();
            this.objectHint = hint;
            this.child = v;
        }

        @Override
        public void onClick(View v) {
            if (isFull) {
                // TODO play sound when click hint with full answer
                return;
            }
            // TODO play sound when click hint
            playSound(soundClickHint);

            child.setVisibility(View.INVISIBLE);
            for (int i = 0; i < objectAnswers.size(); i++) {
                ObjectAnswer ob = objectAnswers.get(i);
                if (ob.getObjectHint() == null) {
                    ob.setObjectHint(objectHint);
                    ((TextView) ob.getChild().findViewById(R.id.tvContent)).setText(ob.getObjectHint().getText());
                    if (isFull())
                        checkAnswer();
                    break;
                }
            }

        }

    }

    // ----------------- Listener on answer ----------------------
    private class ClickListenerAnswer implements OnClickListener {
        private ObjectAnswer objectAnswer;
        private View child;

        public ClickListenerAnswer(ObjectAnswer answer, View v) {
            super();
            this.objectAnswer = answer;
            this.child = v;
        }

        @Override
        public void onClick(View v) {
            if (objectAnswer.getObjectHint() != null) {
                // TODO play sound when click answer item
                playSound(soundClickAnswer);

                isFull = false;
                objectAnswer.getObjectHint().getChild().setVisibility(View.VISIBLE);
                objectAnswer.setObjectHint(null);
                ((TextView) child.findViewById(R.id.tvContent)).setText("");
            } else
                ; // TODO play sound when click empty answer item
        }

    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        // googleApiClientConnect();

        Logger.d("MainFrg- Onstart");

        if (isClick) {
            isClick = false;
            mCurrentCoin += 200;
            SavedStore.editCoin(mCurrentCoin);
            int i = new Integer(mCurrentCoin + 1);
            textCoin.setText("" + i);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.d("MainFrg- onStop");
        if (isPositiveClick) {
            isClick = true;
            isPositiveClick = false;
        }

        SavedStore.editCoin(mCurrentCoin);
        SavedStore.saveAchievements(achievements);

        // TODO just comnet
        signInFireBase();

        String android_id = Settings.Secure.getString(mActivity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        User user = new User();
        user.setDeviceId(android_id);
        user.setFullname(android_id);
        user.setLevel(SavedStore.getCurrentNumberQuestion() + 1);
        mActivity.mAPIService.updateUserLevel(user).enqueue(new Callback<BaseObject<User>>() {
            @Override
            public void onResponse(Call<BaseObject<User>> call, Response<BaseObject<User>> response) {
                if(response.body() != null ) Logger.d("Update user level: " + response.body().isSuccess());
            }

            @Override
            public void onFailure(Call<BaseObject<User>> call, Throwable t) {
                Logger.d("Invalid response's format.");
            }
        });
    }

    // ---------------- Runnable ------------------ //
    // private Runnable runnableActionbar = new Runnable() {
    //
    // @Override
    // public void run() {
    // colorActionbar2 = mActivity.getRandomActionBarColor();
    //
    // // Update the color values
    // animatorActionbar.setIntValues(colorActionbar1, colorActionbar2);
    // animatorActionbar.start();
    //
    // // Order the colors
    // colorActionbar1 = colorActionbar2;
    //
    // mHandler.postDelayed(this, timeRunActionBar);
    // }
    // };

    private Runnable runnableBackground = new Runnable() {

        @Override
        public void run() {
            colorBackground2 = mActivity.getRandomBgColor();

            // Update the color values
            animatorBackground.setIntValues(colorBackground1, colorBackground2);
            animatorBackground.start();

            // Order the colors
            colorBackground1 = colorBackground2;

            mHandler.postDelayed(this, timeBackground);

        }
    };

    private ArrayList<PathInfo> getPathExist() {
        ArrayList<PathInfo> pathInfos = SavedStore.getPaths();
        return pathInfos;
    }

    private class drawCanvas extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // ArrayList<PathInfo> pathInfos = getPathExist();
            // if (pathInfos != null && pathInfos.size() > 0)
            // mActivity.showLoadingDialog();

        }

        @SuppressLint("WrongThread")
        @Override
        protected Void doInBackground(Void... params) {
//            try {
//                Thread.sleep(2000);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

            final ArrayList<PathInfo> pathInfos = getPathExist();
            if (pathInfos != null && pathInfos.size() > 0)
                mMyView.drawPaths(pathInfos);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mMyView.validate();

            // mActivity.dismissLoadingDialog();
        }

    }

    public interface IOnDoneMakeList {
        public void onDone();
    }

    private class makeListQuestion extends AsyncTask<Void, Void, Void> {
        private IOnDoneMakeList iOnDoneMakeList;

        public makeListQuestion(IOnDoneMakeList iOnDoneMakeList) {
            super();
            this.iOnDoneMakeList = iOnDoneMakeList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mActivity.showLoadingDialog();
        }

        @Override
        protected Void doInBackground(Void... params) {
            Collections.shuffle(mActivity.getListQuestion());
            SavedStore.editListQuestions(mActivity.getListQuestion());
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mActivity.dismissLoadingDialog();

            if (iOnDoneMakeList != null)
                iOnDoneMakeList.onDone();
        }

    }

}
