package net.tk.doannghesi.ui.fragments.thuthach;
//package net.tk.doannghesi.ui.fragments;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Random;
//import java.util.Stack;
//
//import net.tk.doannghesi.ui.controls.FlowLayout;
//import net.tk.doannghesi.ui.fragments.base.BaseFragment;
//import net.tk.doannghesi.ui.models.Item_THGN;
//import net.tk.doannghesi.R;
//import android.graphics.Bitmap;
//import android.os.Bundle;
//import android.os.Handler;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.view.animation.Animation.AnimationListener;
//import android.view.animation.AnimationUtils;
//import android.widget.ImageView;
//
//public class FrgFindSamePic extends BaseFragment {
//	private float RATE_CELL = 1 / 10f;
//
//	private int item_size;
//
//	private FlowLayout flowLayout;
//
//	private ArrayList<Item_THGN> mItemsTHGN = new ArrayList<Item_THGN>();
//
//	private int mCol, mRow;
//
//	private Handler mHandler = new Handler();
//	private Stack<Item_THGN> mThgns = new Stack<Item_THGN>();
//
//	public FrgFindSamePic() {
//		// TODO Auto-generated constructor stub
//	}
//
//	public FrgFindSamePic(int col, int row) {
//		mCol = col;
//		mRow = row;
//	}
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		mRootView = inflater.inflate(R.layout.frg_find_same_pic, container, false);
//
//		flowLayout = (FlowLayout) mRootView.findViewById(R.id.flowLayout);
//
//		calculateLayouts(mCol, mRow);
//		createGrid(mCol, mRow);
//
//		return mRootView;
//	}
//
//	private void calculateLayouts(int col, int row) {
//		flowLayout.getLayoutParams().width = deviceWidth;
//
//		item_size = 2 + (int) (deviceWidth / (col + RATE_CELL * (col + 1)));
//
//	}
//
//	private void createGrid(int col, int row) {
//
//		// mHandler.postDelayed(new Runnable() {
//		//
//		// @Override
//		// public void run() {
//		// if(mThgns.size()>0){
//		// // TODO animation hide
//		//
//		// Item_THGN firstThgn = mThgns.firstElement();
//		// firstThgn.getChild().findViewById(R.id.img_default).setVisibility(View.VISIBLE);
//		// firstThgn.setFind(false);
//		// mThgns.removeElementAt(0);
//		//
//		// mHandler.postDelayed(this, 2000);
//		// }
//		// else mHandler.removeCallbacks(this);
//		// }
//		// }, 2000);
//
//		int start = new Random().nextInt(200);
//		int n = col * row / 2;
//
//		for (int i = 0; i < n; i++) {
//			Bitmap bm = getBitmapFromAsset(mActivity, mActivity.getListQuestion().get(start + i).getImg_name());
//			mItemsTHGN.add(new Item_THGN(i, bm));
//			mItemsTHGN.add(new Item_THGN(i, bm));
//		}
//		Collections.shuffle(mItemsTHGN);
//
//		for (int i = 0; i < mItemsTHGN.size(); i++) {
//			final Item_THGN item_THGN = mItemsTHGN.get(i);
//			final int j = i;
//
//			View child = mActivity.getLayoutInflater().inflate(R.layout.item_thgn, null);
//
//			final ImageView imgMain = (ImageView) child.findViewById(R.id.imgMain);
//			imgMain.setImageBitmap(mItemsTHGN.get(i).getBitmap());
//			item_THGN.setChild(child);
//
//			child.findViewById(R.id.item_thgn_container).setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//					if (!item_THGN.isFind()) {
//						// item_THGN.getChild().findViewById(R.id.img_default).setVisibility(View.GONE);
//						item_THGN.setFind(true);
//						startAnimationHide((ImageView) item_THGN.getChild().findViewById(R.id.img_default));
//
//						if (mThgns.size() > 0) {
//							Item_THGN lastThgn = mThgns.lastElement();
//
//							if (lastThgn.getId() == item_THGN.getId()) {
//								// TODO check win
//								mThgns.pop();
//							} else {
//								if (mThgns.size() >= 2) {
//									for (int i = 0; i < mThgns.size(); i++) {
//										Item_THGN thgn = mThgns.elementAt(i);
//										thgn.setFind(false);
//										startAnimationShow((ImageView) item_THGN.getChild().findViewById(
//												R.id.img_default));
//									}
//									mThgns.clear();
//								}
//								mThgns.add(item_THGN);
//							}
//						} else
//							mThgns.add(item_THGN);
//					}
//				}
//			});
//
//			FlowLayout.LayoutParams params = new FlowLayout.LayoutParams(item_size, item_size);
//			if ((i + 1) % mCol != 0)
//				params.rightMargin = (int) (item_size * RATE_CELL);
//			params.bottomMargin = (int) (item_size * RATE_CELL);
//			if (i / mCol == 0)
//				params.topMargin = (int) (item_size * RATE_CELL);
//
//			child.setLayoutParams(params);
//
//			flowLayout.addView(child);
//		}
//
//	}
//
//	private void startAnimationHide(final ImageView iv) {
//		Animation anime;
//		int u = new Random().nextInt(7) + 1;
//		if (u == 1) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.hide_slideup);
//		} else if (u == 2) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_out_bottom);
//		} else if (u == 3) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.hide_slideleft);
//		} else if (u == 4) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_out_left);
//		} else if (u == 5) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_out_right);
//		} else if (u == 6) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.fadeout);
//		} else {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.rotate);
//		}
//		anime.setAnimationListener(new AnimationListener() {
//
//			@Override
//			public void onAnimationStart(Animation animation) {
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation animation) {
//			}
//
//			@Override
//			public void onAnimationEnd(Animation animation) {
//				iv.setVisibility(View.GONE);
//			}
//		});
//		iv.startAnimation(anime);
//	}
//
//	private void startAnimationShow(final ImageView iv) {
//		iv.setVisibility(View.VISIBLE);
//		Animation anime;
//		int u = new Random().nextInt(7) + 1;
//		if (u == 1) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.show_slidedown);
//		} else if (u == 2) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.show_slideright);
//		} else if (u == 3) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_in_left);
//		} else if (u == 4) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_in_bottom);
//		} else if (u == 5) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.slide_in_right);
//		} else if (u == 6) {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.fadeout);
//		} else {
//			anime = AnimationUtils.loadAnimation(mActivity, R.anim.rotate);
//		}
//		anime.setAnimationListener(new AnimationListener() {
//
//			@Override
//			public void onAnimationStart(Animation animation) {
//			}
//
//			@Override
//			public void onAnimationRepeat(Animation animation) {
//			}
//
//			@Override
//			public void onAnimationEnd(Animation animation) {
//			}
//		});
//		iv.startAnimation(anime);
//	}
//
//	@Override
//	public void onDestroyView() {
//		super.onDestroyView();
//		recycleAllViews();
//
//	}
//
//	private void recycleAllViews() {
//		for (Item_THGN item_THGN : mItemsTHGN) {
//			item_THGN.getBitmap().recycle();
//			item_THGN = null;
//		}
//		mItemsTHGN = null;
//	}
//}
