package net.tk.doannghesi.ui.fragments.thuthach.trollertap.objects;

import android.graphics.Canvas;
import android.graphics.Color;

public class Red extends Parent {

	private int color = Color.parseColor("#F44336");// Red
	private int radius = 10;

	public Red() {
		super();
		init();
	}

	public Red(float de) {
		super(de);
		init();
	}

	public Red(int xPos, int yPos) {
		super(xPos, yPos);
		init();
	}

	public Red(int xPos) {
		super(xPos);
		init();
	}

	private void init() {
		setRadius(radius * density);
		setColor(color);
	}

	@Override
	public void draw(Canvas canvas, int w, int h, boolean pauseGame) {
		super.draw(canvas, w, h, pauseGame);
	}
}
