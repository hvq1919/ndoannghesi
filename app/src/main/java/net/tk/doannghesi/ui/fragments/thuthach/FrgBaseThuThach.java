package net.tk.doannghesi.ui.fragments.thuthach;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.tools.Utils;
import net.tk.doannghesi.tools.Utils.OnClickDialog;
import net.tk.doannghesi.tools.sound.SoundPoolHelper;
import net.tk.doannghesi.ui.activities.MainActivity;
import net.tk.doannghesi.ui.dialog.DialogConfirmSaveMe;
import net.tk.doannghesi.ui.dialog.DialogGameOver;
import net.tk.doannghesi.ui.dialog.DialogRating;
import net.tk.doannghesi.ui.dialog.DialogShareFafebook;
import net.tk.doannghesi.ui.dialog.DialogWinThuThach;
import net.tk.doannghesi.ui.dialog.DialogWinThuThach.IOnLayout;
import net.tk.doannghesi.ui.fragments.FrgMain;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.R;

import java.util.Random;

public abstract class FrgBaseThuThach extends BaseFragment {

    private DialogGameOver dialogGameOver;
    private DialogWinThuThach dialogWinThuThach;
    private DialogConfirmSaveMe dialogConfirmSaveMe;

    int coins = 100;
    protected int numSaveMe = 0;
    private int coinsSaveMe = 10;

    protected int currentLevel, numThuThach;

    private boolean isWin;

    public FrgBaseThuThach() {

        currentLevel = SavedStore.getCurrentNumberQuestion();
        numThuThach = (currentLevel / 100 + 1) * 10;
    }

    private int[] khichle = {R.string.khichle_1, R.string.khichle_2, R.string.khichle_3, R.string.khichle_4,
            R.string.khichle_5, R.string.khichle_6, R.string.khichle_7, R.string.khichle_8, R.string.khichle_9,
            R.string.khichle_10,};
    private int[] img = {R.drawable.t_1_10_1, R.drawable.t_1_10_2, R.drawable.t_1_10_3, R.drawable.t_1_10_4,
            R.drawable.t_1_10_5, R.drawable.t_11_20_1, R.drawable.t_11_20_2, R.drawable.t_11_20_3,
            R.drawable.t_11_20_4, R.drawable.t_11_20_5, R.drawable.t_21_31_1, R.drawable.t_21_31_2,
            R.drawable.t_21_31_3, R.drawable.t_21_31_4, R.drawable.t_21_31_5};

    /* Sound */
    private SoundPoolHelper mSoundPoolHelper;
    public int soundRed;
    public int soundBlue;
    public int soundGameOver;
    public int soundWin;
    public int soundClickHint;
    public int soundTouchBlueDots;
    public int soundLevelUp;
    public int soundSpin;

    private void loadSound() {
        mSoundPoolHelper = new SoundPoolHelper(2, mActivity);
        soundBlue = mSoundPoolHelper.load(mActivity, R.raw.rain, 2);
        soundRed = mSoundPoolHelper.load(mActivity, R.raw.rain2, 1);
        soundGameOver = mSoundPoolHelper.load(mActivity, R.raw.gameover, 3);
        soundWin = mSoundPoolHelper.load(mActivity, R.raw.correct, 3);
        soundClickHint = mSoundPoolHelper.load(mActivity, R.raw.clickhint, 1);
        soundTouchBlueDots = mSoundPoolHelper.load(mActivity, R.raw.clickanswer, 1);
        soundLevelUp = mSoundPoolHelper.load(mActivity, R.raw.correct, 1);
        soundSpin = mSoundPoolHelper.load(mActivity, R.raw.song2, 1);
    }

    @Override
    public void onDestroyView() {
        // TODO Auto-generated method stub
        super.onDestroyView();
        mSoundPoolHelper.release();
        mActivity.notShowAds = false;

    }

    /**
     * play a sound
     *
     * @param soundId
     */
    public void playSound(int soundId) {
        // if (!SavedStore.getSound())
        mSoundPoolHelper.play(soundId);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity.notShowAds = true;

        loadSound();

        dialogGameOver = DialogGameOver.newInstance(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                resetGame();
                dialogGameOver.dismiss();
            }
        }, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onClickLuyenTap();
            }

        }, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                OnClickNext();
            }

        });

        dialogWinThuThach = DialogWinThuThach.newInstance(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                resetGame();
                dialogWinThuThach.dismiss();
            }
        }, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                onClickLuyenTap();

            }
        }, new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                doOnNextThuThach();
                dialogWinThuThach.dismiss();
            }
        });

        dialogConfirmSaveMe = DialogConfirmSaveMe.newInstance(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Neu du tien thi moi savemeclick, neu k0 du tien thi gameover
                // dialogConfirmSaveMe.saveMeClicked();

                onSaveMeConfirmDialogClick();
            }
        }, new DialogConfirmSaveMe.IOnDimissClick() {

            @Override
            public void onDimissClick() {
                onDimissConfirmDialogClick();
            }
        });

    }

    public DialogGameOver getDialogGameOver() {
        // / TODO play sound gameover
        playSound(soundGameOver);
        return dialogGameOver;
    }

    public DialogWinThuThach getDialogWinThuThach() {
        return dialogWinThuThach;
    }

    public DialogConfirmSaveMe getDialogConfirmSaveMe() {
        return dialogConfirmSaveMe;
    }

    public void showDialogSaveMe(final MainActivity mainActivity) {
        coinsSaveMe = (int) (10 * Math.pow(2, numSaveMe));
        numSaveMe += 1;

        String textContinue = String.format(mainActivity.getResources().getString(R.string.confirm_dialog_tieptuc),
                coinsSaveMe);

        dialogConfirmSaveMe.setTextContinue(textContinue);

        dialogConfirmSaveMe.show(mainActivity.getFragmentManager(), "TAG");

    }

    public void showDialogWin(final MainActivity mainActivity) {
        // / TODO play sound gameover
        playSound(soundWin);

        int currentLevel = 0;
        currentLevel = SavedStore.getCurrentNumberQuestion();

        coins = 20;
        if (currentLevel > 100)
            coins = 50;
        else if (currentLevel > 500)
            coins = 100;
        else if (currentLevel > 1000)
            coins = 150;

        if (isWin) coins = 5;
        isWin = true;

        int curCoins = SavedStore.getCoin();
        SavedStore.editCoin(curCoins + coins);

        dialogWinThuThach.setOnLayout(new IOnLayout() {

            @Override
            public void onLayoutBuild() {
                int n_img = new Random().nextInt(img.length);
                int n_string = new Random().nextInt(khichle.length);

                String nhancoins = String.format(
                        mainActivity.getResources().getString(R.string.thuthach_nhanduoccoins), coins);
                String title = mainActivity.getResources().getString(khichle[n_string]);

                Log.d("TAG", nhancoins + " ___coins:" + coins);

                dialogWinThuThach.setTextCoins(nhancoins);
                dialogWinThuThach.setTextTitle(title);
                dialogWinThuThach.setImage(img[n_img]);
            }
        });

        try{
            dialogWinThuThach.show(mainActivity.getFragmentManager(), "TAG");
        }catch (Exception e){}

        if (!SavedStore.getFirstRating()) {
            SavedStore.editFirstRating(true);
            try {
                DialogRating dialogRating = new DialogRating();
                dialogRating.show(mainActivity.getFragmentManager(), "TAG");
            }catch (Exception e){}
        }

        if (currentLevel >= 30 && !mActivity.showShareDialog) {
            mActivity.showShareDialog = true;
            try {
                DialogShareFafebook dialogShareFafebook = DialogShareFafebook.newInstance(mActivity);
                dialogShareFafebook.show(mainActivity.getFragmentManager(), "TAG");
            }catch (Exception e){}
        }


        Log.d("TAG", "showAds");
        mActivity.showAdsThuThach();
    }

    private void onClickLuyenTap() {
        try {
            mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getLinkUrl())));
        } catch (android.content.ActivityNotFoundException anfe) {

        }

    }

    private void OnClickNext() {
        int currentLevel = 0;

        currentLevel = SavedStore.getCurrentNumberQuestion();


        coins = 100;
        if (currentLevel > 100)
            coins = 300;
        else if (currentLevel > 500)
            coins = 500;
        else if (currentLevel > 1000)
            coins = 1000;

        String skip = String.format(mResources.getString(R.string.thuthach_boquathuthach), coins);

        Utils.buildDialog(mActivity, "Yes", "No", "Confirm!", skip, new OnClickDialog() {

            @Override
            public void PositiveClick() {
                int curCoins = SavedStore.getCoin();
                if (curCoins >= coins) {
                    SavedStore.editCoin(curCoins - coins);
                    doOnNextThuThach();
                    dialogGameOver.dismiss();
                } else
                    mActivity.showToast(mResources.getString(R.string.not_enough_coin), true);
            }

            @Override
            public void NegativeClick() {
            }
        });

    }

    private void onSaveMeConfirmDialogClick() {
        dialogConfirmSaveMe.removeAllCallBack();

        int curCoins = SavedStore.getCoin();
        if (curCoins >= coinsSaveMe) {
            SavedStore.editCoin(curCoins - coinsSaveMe);
            onSaveMeSuccess();
            dialogConfirmSaveMe.dismiss();
        } else
            mActivity.showToast(mResources.getString(R.string.not_enough_coin), true);

    }

    protected String getLinkUrl() {
        // TODO Auto-generated method stub
        return "https://play.google.com/store/apps/developer?id=TK+Developer";
    }

    protected abstract void resetGame();

    protected void onDimissConfirmDialogClick() {
        numSaveMe = 0;

        try {
            DialogGameOver dialogGameOver = getDialogGameOver();
            dialogGameOver.setStringTextTitle(mResources.getString(R.string.thuthach_find_same_pic_gameover) + "\n "
                    + String.format(mResources.getString(R.string.thuthach_find_same_pic_need), numThuThach));
            dialogGameOver.show(mActivity.getFragmentManager(), "TAG");
        }catch (Exception e){}

    }

    protected void onSaveMeSuccess() {

    }

    protected void doOnNextThuThach() {
        isWin = false;
        SavedStore.editThuThach(0);
        mActivity.popFragment();
        mActivity.pushFragment(new FrgMain(), true);
    }

}
