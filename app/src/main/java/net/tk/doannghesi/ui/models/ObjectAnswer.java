package net.tk.doannghesi.ui.models;

import android.view.View;

public class ObjectAnswer {
	private int id;
	private String text;
	private View child;
	private ObjectHint objectHint;
	public ObjectAnswer() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public ObjectAnswer(int id, String text, View child, ObjectHint objectHint) {
		super();
		this.id = id;
		this.text = text;
		this.child = child;
		this.objectHint = objectHint;
	}


	public ObjectAnswer(int id, String text, ObjectHint objectHint) {
		super();
		this.id = id;
		this.text = text;
		this.objectHint = objectHint;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	
	public View getChild() {
		return child;
	}


	public void setChild(View child) {
		this.child = child;
	}


	public ObjectHint getObjectHint() {
		return objectHint;
	}
	public void setObjectHint(ObjectHint objectHint) {
		this.objectHint = objectHint;
	}
	
}
