package net.tk.doannghesi.ui.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Questions implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -261469141517231010L;

	@SerializedName("img_name")
	@Expose
	private String img_name;

	@SerializedName("answer")
	@Expose
	private String answer;

	@SerializedName("wrong_answer")
	@Expose
	private String wrong_answer;

	@SerializedName("note")
	@Expose
	private String note;

	@SerializedName("name")
	@Expose
	private String name;

	public Questions() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Questions(String img_name, String answer, String wrong_answer, String note, String name) {
		super();
		this.img_name = img_name;
		this.answer = answer;
		this.wrong_answer = wrong_answer;
		this.note = note;
		this.name = name;
	}

	public String getImg_name() {
		return img_name;
	}

	public void setImg_name(String img_name) {
		this.img_name = img_name;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getWrong_answer() {
		return wrong_answer;
	}

	public void setWrong_answer(String wrong_answer) {
		this.wrong_answer = wrong_answer;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
