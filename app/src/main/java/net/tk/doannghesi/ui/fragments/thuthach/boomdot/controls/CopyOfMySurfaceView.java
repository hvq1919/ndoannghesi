package net.tk.doannghesi.ui.fragments.thuthach.boomdot.controls;
//package net.tk.boomdot.controls;
//
//import net.tk.boomdot.Define;
//import net.tk.boomdot.Define.TYPE_COLOR;
//import net.tk.boomdot.objects.RunningDot;
//import net.tk.boomdot.thread.MyThread;
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.graphics.Paint;
//import android.util.AttributeSet;
//import android.util.Log;
//import android.view.MotionEvent;
//import android.view.SurfaceHolder;
//import android.view.SurfaceHolder.Callback;
//import android.view.SurfaceView;
//
//public class CopyOfMySurfaceView extends SurfaceView implements Callback {
//	private static final String TAG = "TAG";
//
//	// paint_level_size = device's width / RATE_PAINT_LEVEL_SIZE
//	private final int RATE_PAINT_LEVEL_SIZE = 8;
//
//	// radius = device's width / RATE_RADIUS
//	private final int RATE_RADIUS = 20;
//
//	// ////////////////////////////////////////////////////////////
//
//	private SurfaceHolder surfaceHolder;
//	private MyThread myThread;
//
//	private int level = 0;
//
//	private int[] background_color;
//
//	private RunningDot mRunningDot;
//
//	// Level Paint
//	private Paint paint_level;
//	private int paint_level_size;
//
//	// BLUE
//	private Paint paint_blue;
//	private float mX_blue;
//	private float mY_blue;
//
//	// BLUE blur
//	private Paint paint_blue_blur;
//
//	// BLUE blur
//	private Paint paint_red_blur;
//
//	// RED
//	private Paint paint_red;
//	private float mX_red;
//	private float mY_red;
//
//	private float radius;
//
//	private boolean gameIsRunning;
//	private boolean shouldRotate;
//	private boolean isGameOverShowing = true;
//
//	public interface IOnGameOverShow {
//		public void onGameOverShow();
//	}
//
//	private IOnGameOverShow mGameOverShow;
//
//	public void setOnGameOverShow(IOnGameOverShow onGameOverShow) {
//		mGameOverShow = onGameOverShow;
//	}
//
//	public CopyOfMySurfaceView(Context context) {
//		super(context);
//		init();
//	}
//
//	public CopyOfMySurfaceView(Context context, AttributeSet attrs) {
//		super(context, attrs);
//		init();
//	}
//
//	public CopyOfMySurfaceView(Context context, AttributeSet attrs, int defStyle) {
//		super(context, attrs, defStyle);
//		init();
//	}
//
//	private void init() {
//		setFocusable(true); // make sure we get key events
//		surfaceHolder = getHolder();
//
//		background_color = Define.BackgroundColors[0];
//
//		initPaint();
//
//		surfaceHolder.addCallback(this);
//
//	}
//
//	/**
//	 * Call after get device height
//	 * 
//	 * @param w
//	 * @param h
//	 */
//	private void initDrawObjects(int w, int h) {
//		initRate(w, h);
//
//		mRunningDot = new RunningDot(w, h);
//		initDot(w, h);
//
//	}
//
//	private void initRate(int w, int h) {
//		paint_level_size = w / RATE_PAINT_LEVEL_SIZE;
//		radius = w / RATE_RADIUS;
//	}
//
//	private void initDot(int w, int h) {
//		mX_blue = mX_red = w / 2;
//		mY_blue = h / 2 - radius;
//		mY_red = h / 2 + radius;
//	}
//
//	private void initPaint() {
//		// Blue paint
//		paint_blue = new Paint();
//		paint_blue.setColor(Define.BLUE_COLOR);
//		paint_blue.setFlags(Paint.ANTI_ALIAS_FLAG);
//
//		// Red paint
//		paint_red = new Paint();
//		paint_red.setColor(Define.RED_COLOR);
//		paint_red.setFlags(Paint.ANTI_ALIAS_FLAG);
//
//		paint_level = new Paint();
//		paint_level.setFlags(Paint.ANTI_ALIAS_FLAG);
//		
//		paint_blue_blur= new Paint();
//		paint_blue_blur.setColor(Define.BLUE_COLOR_BLUR);
//		paint_blue_blur.setFlags(Paint.ANTI_ALIAS_FLAG);
//		
//		paint_red_blur= new Paint();
//		paint_red_blur.setColor(Define.RED_COLOR_BLUR);
//		paint_red_blur.setFlags(Paint.ANTI_ALIAS_FLAG);
//
//	}
//
//	private void restartGame(int w, int h) {
//		initDot(w, h);
//		mRunningDot.initDot();
//		level = 0;
//		radius_blur = 0;
//		gameIsRunning = red_blur = blue_blur = false;
//
//		isGameOverShowing = true;
//		if (mGameOverShow != null)
//			mGameOverShow.onGameOverShow();
//	}
//
//	private int bg_count = 0;
//
//	private void startNewLevel(int w, int h) {
//		// Check 10 level => change background
//		bg_count = level / 10;
//		bg_count = bg_count % Define.BackgroundColors.length;
//		background_color = Define.BackgroundColors[bg_count];
//
//		level += 1;
//		mRunningDot.initDot();
//	}
//
//	/**
//	 * This method set event listener for drawing.
//	 * 
//	 * @param event
//	 *            the instance of MotionEvent
//	 * @return
//	 */
//	@SuppressLint("ClickableViewAccessibility")
//	@Override
//	public boolean onTouchEvent(MotionEvent ev) {
//		switch (ev.getAction()) {
//		case MotionEvent.ACTION_DOWN:
//			touch_start(ev.getX(), ev.getY());
//			break;
//		case MotionEvent.ACTION_MOVE:
//			// touch_move(ev.getX(), ev.getY());
//			break;
//		case MotionEvent.ACTION_UP:
//			// this.onActionUp(event);
//			break;
//		default:
//			break;
//		}
//
//		return true;
//	}
//
//	private void touch_start(float x, float y) {
//		if (isGameOverShowing)
//			return;
//
//		if (gameIsRunning)
//			shouldRotate = true;
//		gameIsRunning = true;
//		degree = 0;
//
//		// BLUE
//		if (mY_blue < getHeight() / 2)
//			startDegreeBlue = 90;
//
//		else
//			startDegreeBlue = 270;
//
//	}
//
//	public void drawSomething(Canvas canvas) {
//		canvas.drawColor(Color.rgb(background_color[0], background_color[1], background_color[2]));
//
//		drawObject(canvas, getWidth(), getHeight());
//	}
//
//	private int startDegreeBlue;
//	double degree = 0;
//
//	private void drawObject(Canvas canvas, int width, int height) {
//		// Draw level
//		drawLevel(canvas, width, height);
//
//		canvas.drawCircle(mX_blue, mY_blue, radius, paint_blue);
//		canvas.drawCircle(mX_red, mY_red, radius, paint_red);
//
//		drawDotScale(canvas, width, height);
//
//		if (shouldRotate) {
//			degree += 18;
//
//			mX_blue = (float) (width / 2 + radius * Math.cos(Math.toRadians(degree + startDegreeBlue)));
//			mY_blue = (float) (height / 2 - radius * Math.sin(Math.toRadians(degree + startDegreeBlue)));
//
//			mX_red = (float) (width / 2 + radius * Math.cos(Math.toRadians(degree + 360 - startDegreeBlue)));
//			mY_red = (float) (height / 2 - radius * Math.sin(Math.toRadians(degree + 360 - startDegreeBlue)));
//
//			canvas.drawCircle(mX_blue, mY_blue, radius, paint_blue);
//			canvas.drawCircle(mX_red, mY_red, radius, paint_red);
//
//			if (degree >= 180)
//				shouldRotate = false;
//		}
//
//		if (gameIsRunning) {
//			mRunningDot.draw(canvas, width, height);
//			checkVaCham(canvas, width, height);
//		}
//
//	}
//
//	private void drawLevel(Canvas canvas, int width, int height) {
//		paint_level.setColor(Color.WHITE);
//		canvas.drawCircle(width / 2, height / 4, paint_level_size, paint_level);
//
//		paint_level.setColor(Color.rgb(background_color[0], background_color[1], background_color[2]));
//		paint_level.setTextAlign(Paint.Align.CENTER);
//		paint_level.setTextSize(paint_level_size);
//
//		int xPos = (canvas.getWidth() / 2);
//		int yPos = (int) ((canvas.getHeight() / 4) - ((paint_level.descent() + paint_level.ascent()) / 2));
//
//		if (level < 10)
//			canvas.drawText("0" + level, xPos, yPos, paint_level);
//		else
//			canvas.drawText("" + level, xPos, yPos, paint_level);
//	}
//
//	private void checkVaCham(Canvas canvas, int width, int height) {
//		if (mRunningDot.getType_color() == TYPE_COLOR.BULE && checkVaCham(mX_blue, mY_blue)) {
//			Log.d("TAG", "Running Blue new Level");
//			startNewLevel(width, height);
//			blue_blur = true;
//		}
//		if (mRunningDot.getType_color() == TYPE_COLOR.RED && checkVaCham(mX_blue, mY_blue)) {
//			Log.d("TAG", "Running Red restartGame");
//			// restartGame(width, height);
//
//			startNewLevel(width, height);
//			red_blur = true;
//		}
//		if (mRunningDot.getType_color() == TYPE_COLOR.BULE && checkVaCham(mX_red, mY_red)) {
//			Log.d("TAG", "Running BLue restartGame");
//			// restartGame(width, height);
//
//			startNewLevel(width, height);
//			blue_blur = true;
//		}
//		if (mRunningDot.getType_color() == TYPE_COLOR.RED && checkVaCham(mX_red, mY_red)) {
//			Log.d("TAG", "Running Red startNewLevel");
//			startNewLevel(width, height);
//			red_blur = true;
//		}
//	}
//
//	private int radius_blur = 0;
//	private boolean blue_blur;
//	private boolean red_blur;
//
//	/**
//	 * To draw scale dot after selecting correct dot
//	 */
//	private void drawDotScale(Canvas canvas, int width, int height) {
//		if (!blue_blur && !red_blur)
//			return;
//		radius_blur += 10;
//		if (radius_blur >= paint_level_size * 4 /*2*/) {
//			radius_blur = 0;
//			blue_blur = red_blur = false;
//		}
//		if (blue_blur)
//			canvas.drawCircle(width / 2, height / 2, radius_blur, paint_blue_blur);
//		else if (red_blur)
//			canvas.drawCircle(width / 2, height / 2, radius_blur, paint_red_blur);
//	}
//
//	private boolean checkVaCham(float mX, float mY) {
//		float total = (mX - mRunningDot.getxPos()) * (mX - mRunningDot.getxPos()) + (mY - mRunningDot.getyPos())
//				* (mY - mRunningDot.getyPos());
//		double d = Math.sqrt(total);
//		if (d <= (radius + mRunningDot.getRadius()))
//			return true;
//		return false;
//	}
//
//	@Override
//	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//		Log.d(TAG, "MySurfaceView surfaceChanged w:" + width + "h:" + height);
//		initDrawObjects(width, height);
//	}
//
//	@Override
//	public void surfaceCreated(SurfaceHolder holder) {
//		Log.d(TAG, "MySurfaceView surfaceCreated");
//
//		myThread = new MyThread(this);
//		if (myThread != null)
//			myThread.surfaceCreated();
//
//	}
//
//	@Override
//	public void surfaceDestroyed(SurfaceHolder holder) {
//		Log.d(TAG, "MySurfaceView surfaceDestroyed");
//
//		if (myThread != null)
//			myThread.surfaceDestroyed();
//
//	}
//
//	public void onResume() {
//		if (myThread != null)
//			myThread.onResume();
//	}
//
//	public void onPause() {
//		if (myThread != null)
//			myThread.onPause();
//	}
//
//	public int[] getBackground_color() {
//		return background_color;
//	}
//
//	public boolean isGameOverShowing() {
//		return isGameOverShowing;
//	}
//
//	public void setGameOverShowing(boolean isGameOverShowing) {
//		this.isGameOverShowing = isGameOverShowing;
//	}
//
//	public int getLevel() {
//		return level;
//	}
//
//	public void setLevel(int level) {
//		this.level = level;
//	}
//
//}
