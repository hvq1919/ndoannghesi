package net.tk.doannghesi.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by admin on 7/27/2017.
 */

public class FrgSuggestionArtist extends BaseFragment implements View.OnClickListener {

    EditText name_value;
    EditText link_fb_value;
    EditText main_info_value;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frg_suggestion_artist, container, false);

        name_value = (EditText) mRootView.findViewById(R.id.name_value);
        link_fb_value = (EditText) mRootView.findViewById(R.id.link_fb_value);
        main_info_value = (EditText) mRootView.findViewById(R.id.main_info_value);

        mRootView.findViewById(R.id.back).setOnClickListener(this);
        mRootView.findViewById(R.id.btn_gopy).setOnClickListener(this);
        return mRootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                mActivity.popFragment();
                break;
            case R.id.btn_gopy:
                if(TextUtils.isEmpty(name_value.getText().toString())){
                    mActivity.showToast(mResources.getString(R.string.nhap_name), true);
                }else{
                    mActivity.mAPIService.suggestionArtist(name_value.getText().toString(),link_fb_value.getText().toString(),main_info_value.getText().toString()).enqueue(new Callback<BaseObject<String>>() {
                        @Override
                        public void onResponse(Call<BaseObject<String>> call, Response<BaseObject<String>> response) {
                            mActivity.dismissLoadingDialog();
                            name_value.setText("");
                            mActivity.showToast(mResources.getString(R.string.nhap_nghe_si_thanhcong), false);
                            try  {
                                InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), 0);
                            } catch (Exception e) {

                            }

                            mActivity.popFragment();
                        }

                        @Override
                        public void onFailure(Call<BaseObject<String>> call, Throwable t) {
                            mActivity.dismissLoadingDialog();
                        }
                    });
                }
                break;
            default:
                break;
        }
    }
}
