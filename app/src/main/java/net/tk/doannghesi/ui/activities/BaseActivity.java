package net.tk.doannghesi.ui.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.tools.Utils;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.ui.databases.MyDatabase;
import net.tk.doannghesi.ui.models.Questions;
import net.tk.doannghesi.ui.threads.CopyDatabase;
import net.tk.doannghesi.ui.threads.CopyDatabase.OnGetListObject;
import net.tk.doannghesi.R;

import java.util.ArrayList;
import java.util.Random;

public abstract class BaseActivity extends Activity {

    private final int TIME_DELAY = 3000;

    private MyDatabase db;

    protected Handler mHandler;
    protected ArrayList<Questions> mQuestions;

    protected FragmentManager mFragmentManager;

    public int[] array_actionbar_color;
    public int[] array_bg_color;
    public int[] array_text_white;
    public int[] array_text_black;

    public int bg_splash;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getArrayResources();
        init();

    }


    private void getArrayResources() {
        array_text_white = this.getResources().getIntArray(R.array.text_white);
        array_text_black = this.getResources().getIntArray(R.array.text_black);
        array_bg_color = this.getResources().getIntArray(R.array.bg_color);
        array_actionbar_color = this.getResources().getIntArray(R.array.actionbar_color);
    }

    private void init() {
        mFragmentManager = getFragmentManager();
        mHandler = new Handler();
        bg_splash = getRandomBgColor();
        initDatabase();
    }

    private void initDatabase() {
        db = new MyDatabase(this);

        if (SavedStore.getListQuestions() != null) {
            mQuestions = SavedStore.getListQuestions();
//			mHandler.postDelayed(new Runnable() {
//
//				@Override
//				public void run() {
//					OnDoneGetDatabase();
//				}
//			}, TIME_DELAY);
        }
        /* To copy databases from assets folder */
        else
            new CopyDatabase(db, new OnGetListObject() {

                @SuppressWarnings("unchecked")
                @Override
                public void getListObject(Object object) {
                    // TODO Goto
                    Logger.d("Oncall back");
                    mQuestions = (ArrayList<Questions>) object;

//					mHandler.postDelayed(new Runnable() {
//
//						@Override
//						public void run() {
//							OnDoneGetDatabase();
//						}
//					}, TIME_DELAY);
                }
            }).execute();

    }

//	protected void OnDoneGetDatabase() {
//
//	}

    /**
     * To get random color from bg list
     *
     * @return
     */
    public int getRandomBgColor() {
        return array_bg_color[new Random().nextInt(array_bg_color.length)];
    }

    /**
     * To get random color from actionbar list
     *
     * @return
     */
    public int getRandomActionBarColor() {
        return array_actionbar_color[new Random().nextInt(array_actionbar_color.length)];
    }

    /**
     * To get random color from text black list
     *
     * @return
     */
    public int getRandomTextBlackColor() {
        return array_text_black[new Random().nextInt(array_text_black.length)];
    }

    /**
     * To get random color from text white list
     *
     * @return
     */
    public int getRandomTextWhiteColor() {
        return array_text_white[new Random().nextInt(array_text_white.length)];
    }

    /**
     * To get current question
     *
     * @param numberQuestion
     * @return
     */
    public Questions getCurrentQuestion(int numberQuestion) {
        return mQuestions.get(numberQuestion);
    }

    /***
     * To get list questions
     *
     * @return
     */
    public ArrayList<Questions> getListQuestion() {
        return mQuestions;
    }

    /**
     * To get Fragment manager
     */
    public FragmentManager getmFragmentManager() {
        return mFragmentManager;
    }

    /**
     * To get current fragment of activity
     *
     * @return current fragment
     */
    public Fragment getCurrentFragment() {
        return mFragmentManager.findFragmentById(R.id.fragment);
    }

    /**
     * To get Fragment Transaction
     *
     * @return Fragment Transaction
     */
    @SuppressLint("CommitTransaction")
    public FragmentTransaction getFragmentTransaction() {
        return mFragmentManager.beginTransaction();
    }

    /**
     * The the default should animation and should add into back stack
     *
     * @param fragment
     */
    public void pushFragment(Fragment fragment) {
        FragmentTransaction ft = getFragmentTransaction();
        ft.addToBackStack(null);
        // setTransition(ft);
        ft.replace(R.id.fragment, fragment);
        ft.commit();
    }

    /**
     * push fragment and add into back stack or not
     *
     * @param fragment
     * @param shouldAdd
     */
    public void pushFragment(Fragment fragment, boolean shouldAdd) {
        FragmentTransaction ft = getFragmentTransaction();
        if (shouldAdd)
            ft.addToBackStack(null);
        // setTransition(ft);
        ft.add(R.id.fragment, fragment);
        ft.commit();
    }

    /**
     * To set push and pop transition for fragment
     *
     * @param ft
     */
    private void setTransition(FragmentTransaction ft) {
        //ft.setCustomAnimations(R.anim.slide_in_right, R.anim.none, R.anim.none, R.anim.slide_out_right);
    }

    /**
     * To pop fragment out of BackStack
     */
    public void popFragment() {
        mFragmentManager.popBackStack();

        /**
         * In normally, after pop fragment from BackStack, the top fragment
         * don't run any override method. The code below allow it run
         * OnStart method to update some things
         */
        if (mFragmentManager != null)
            getCurrentFragment().onStart();
    }

    /**
     * To clear BackStack
     */
    public void clearBackStack() {
        mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onBackPressed() {
        if (mFragmentManager.getBackStackEntryCount() >= 1) {
            super.onBackPressed(); // just pop fragment

            /**
             * In normally, after pop fragment from BackStack, the top fragment
             * don't run any override method. The code below allow it run
             * OnStart method to update some things
             */
            if (mFragmentManager != null)
                getCurrentFragment().onStart();
        } else {
            Utils.buildExitDialog(BaseActivity.this);
        }
    }

}
