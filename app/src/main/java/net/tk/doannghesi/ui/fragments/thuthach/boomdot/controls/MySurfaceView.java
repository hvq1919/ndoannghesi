package net.tk.doannghesi.ui.fragments.thuthach.boomdot.controls;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

import net.tk.doannghesi.ui.activities.MainActivity;
import net.tk.doannghesi.ui.fragments.thuthach.boomdot.Define;
import net.tk.doannghesi.ui.fragments.thuthach.boomdot.Define.TYPE_COLOR;
import net.tk.doannghesi.ui.fragments.thuthach.boomdot.FrgBoomDot;
import net.tk.doannghesi.ui.fragments.thuthach.boomdot.objects.RunningDot;
import net.tk.doannghesi.ui.fragments.thuthach.boomdot.thread.MyThread;
// Draw star and triangle
// http://stackoverflow.com/questions/20544668/how-to-draw-filled-triangle-on-android-canvas
// http://stackoverflow.com/questions/3501126/how-to-draw-a-filled-triangle-in-android-canvas
// http://stackoverflow.com/questions/7007429/android-how-to-draw-triangle-star-square-heart-on-the-canvas

public class MySurfaceView extends SurfaceView implements Callback {
	private static final String TAG = "TAG";

	// paint_level_size = device's width / RATE_PAINT_LEVEL_SIZE
	private final int RATE_PAINT_LEVEL_SIZE = 8;

	// radius = device's width / RATE_RADIUS
	private final int RATE_RADIUS = 20;

	// ////////////////////////////////////////////////////////////

	private SurfaceHolder surfaceHolder;
	private MyThread myThread;

	private int level = 0;

	private int[] background_color;

	private RunningDot mRunningDot;

	// Level Paint
	private Paint paint_level;
	private int paint_level_size;

	// BLUE
	private Paint paint_blue;
	private float mX_blue;
	private float mY_blue;

	// BLUE blur
	private Paint paint_blue_blur;

	// BLUE blur
	private Paint paint_red_blur;

	// RED
	private Paint paint_red;
	private float mX_red;
	private float mY_red;

	private float radius;

	// private boolean gameIsRunning;
	private boolean shouldRotate;
	// private boolean isGameOverShowing = true;

	// //////////////////////////////////////////////////
	// ------------- Text Boom Dots -------------------
	private Paint paint_text_boom_dots;
	private int color_text_boom_dots = Color.parseColor("#2E7D32");

	// //////////////////////////////////////////////////
	// ------------- Text Boom Dots -------------------
	private Paint paint_text_score;
	private int color_text_score = Color.parseColor("#6D4C41");

	private FrgBoomDot mFrgBoomDot;

	private MainActivity mainActivity;
	private Typeface tf_boom_dot;

	public enum GAME_STATUS {
		GAME_START, GAME_NEW, GAME_RUNNING, GAME_OVER
	}

	private GAME_STATUS currentStatus = GAME_STATUS.GAME_START;

	public MySurfaceView(Context context) {
		super(context);
		mainActivity = (MainActivity) context;
		tf_boom_dot = Typeface.createFromAsset(context.getAssets(), "fonts/boomdot.otf");
		init();
	}

	public MySurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		tf_boom_dot = Typeface.createFromAsset(context.getAssets(), "fonts/boomdot.otf");
		mainActivity = (MainActivity) context;
		init();
	}

	public MySurfaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		tf_boom_dot = Typeface.createFromAsset(context.getAssets(), "fonts/boomdot.otf");
		mainActivity = (MainActivity) context;
		init();
	}

	private void init() {
		setFocusable(true); // make sure we get key events
		surfaceHolder = getHolder();

		background_color = Define.BackgroundColors[0];

		initPaint();

		surfaceHolder.addCallback(this);

	}

	/**
	 * Call after get device height
	 * 
	 * @param w
	 * @param h
	 */
	private void initDrawObjects(int w, int h) {
		initRate(w, h);

		mRunningDot = new RunningDot(w, h);
		initDot(w, h);

	}

	private void initRate(int w, int h) {
		paint_level_size = w / RATE_PAINT_LEVEL_SIZE;
		radius = w / RATE_RADIUS;
	}

	private void initDot(int w, int h) {
		mX_blue = mX_red = w / 2;
		mY_blue = h / 2 - radius;
		mY_red = h / 2 + radius;
	}

	private void initPaint() {
		// Blue paint
		paint_blue = new Paint();
		paint_blue.setColor(Define.BLUE_COLOR);
		 paint_blue.setFlags(Paint.ANTI_ALIAS_FLAG);

		// Red paint
		paint_red = new Paint();
		paint_red.setColor(Define.RED_COLOR);
		 paint_red.setFlags(Paint.ANTI_ALIAS_FLAG);

		paint_level = new Paint();
		paint_level.setFlags(Paint.ANTI_ALIAS_FLAG);

		paint_blue_blur = new Paint();
		paint_blue_blur.setColor(Define.BLUE_COLOR_BLUR);
		 //paint_blue_blur.setFlags(Paint.ANTI_ALIAS_FLAG);

		paint_red_blur = new Paint();
		paint_red_blur.setColor(Define.RED_COLOR_BLUR);
		// paint_red_blur.setFlags(Paint.ANTI_ALIAS_FLAG);

		// Text Boom Dots
		paint_text_boom_dots = new Paint();
		// if (new Random().nextInt(100) % 2 == 1)
		// color_text_boom_dots = Define.RED_COLOR;
		// else
		// color_text_boom_dots = Define.BLUE_COLOR;
		paint_text_boom_dots.setColor(color_text_boom_dots);
		paint_text_boom_dots.setFlags(Paint.ANTI_ALIAS_FLAG);
		paint_text_boom_dots.setTypeface(tf_boom_dot);
		paint_text_boom_dots.setTextAlign(Paint.Align.CENTER);

		// Text Score
		paint_text_score = new Paint();
		paint_text_score.setColor(color_text_score);
		paint_text_score.setFlags(Paint.ANTI_ALIAS_FLAG);
		paint_text_score.setTextAlign(Paint.Align.CENTER);
	}

	private void restartGame(int w, int h) {
		currentStatus = GAME_STATUS.GAME_OVER;
		initDot(w, h);
		mRunningDot.initDot();
		radius_blur = 0;
		red_blur = blue_blur = false;

		mFrgBoomDot.gameOver();
	}

	private int bg_count = 0;

	private void startNewLevel(int w, int h) {
		// Check 10 level => change background
		bg_count = level / 10;
		bg_count = bg_count % Define.BackgroundColors.length;
		background_color = Define.BackgroundColors[bg_count];

		level += 1;
		// if (level > best_score)
		// best_score = level;

		mRunningDot.initDot();

	}

	/**
	 * This method set event listener for drawing.
	 *
	 *            the instance of MotionEvent
	 * @return
	 */
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touch_start(ev.getX(), ev.getY());
			break;
		case MotionEvent.ACTION_MOVE:
			// touch_move(ev.getX(), ev.getY());
			break;
		case MotionEvent.ACTION_UP:
			// this.onActionUp(event);
			break;
		default:
			break;
		}

		return true;
	}

	private void touch_start(float x, float y) {
		// if (isGameOverShowing)
		// return;

		if (currentStatus == GAME_STATUS.GAME_RUNNING)
			shouldRotate = true;
		currentStatus = GAME_STATUS.GAME_RUNNING;
		degree = 0;

		// BLUE
		if (mY_blue < getHeight() / 2)
			startDegreeBlue = 90;

		else
			startDegreeBlue = 270;

	}

	public void drawSomething(Canvas canvas) {
		canvas.drawColor(Color.rgb(background_color[0], background_color[1], background_color[2]));

		drawObject(canvas, getWidth(), getHeight());
	}

	private int startDegreeBlue;
	double degree = 0;
	boolean isLeftToRight;

	private void drawObject(Canvas canvas, int width, int height) {
		switch (currentStatus) {
		case GAME_START:
			drawBlueOrRed(canvas, width, height);

			canvas.drawCircle(mX_blue, mY_blue, radius, paint_blue);
			canvas.drawCircle(mX_red, mY_red, radius, paint_red);

			if (isLeftToRight)
				degree += 1;
			else
				degree -= 1;
			if (degree > 360)
				isLeftToRight = false;
			if (degree < 0)
				isLeftToRight = true;

			startDegreeBlue = 90;

			mX_blue = (float) (width / 2 + radius * Math.cos(Math.toRadians(degree + startDegreeBlue)));
			mY_blue = (float) (height / 2 - radius * Math.sin(Math.toRadians(degree + startDegreeBlue)));

			mX_red = (float) (width / 2 + radius * Math.cos(Math.toRadians(degree + 360 - startDegreeBlue)));
			mY_red = (float) (height / 2 - radius * Math.sin(Math.toRadians(degree + 360 - startDegreeBlue)));

			break;

		case GAME_NEW:
			drawTapToJump(canvas, width, height);
			drawLevel(canvas, width, height);

			initDot(width, height);
			degree = 0;
			canvas.drawCircle(mX_blue, mY_blue, radius, paint_blue);
			canvas.drawCircle(mX_red, mY_red, radius, paint_red);

			break;
		case GAME_RUNNING:
			// Draw level
			drawLevel(canvas, width, height);

			canvas.drawCircle(mX_blue, mY_blue, radius, paint_blue);
			canvas.drawCircle(mX_red, mY_red, radius, paint_red);

			drawDotScale(canvas, width, height);

			if (shouldRotate) {
				degree += 18;

				mX_blue = (float) (width / 2 + radius * Math.cos(Math.toRadians(degree + startDegreeBlue)));
				mY_blue = (float) (height / 2 - radius * Math.sin(Math.toRadians(degree + startDegreeBlue)));

				mX_red = (float) (width / 2 + radius * Math.cos(Math.toRadians(degree + 360 - startDegreeBlue)));
				mY_red = (float) (height / 2 - radius * Math.sin(Math.toRadians(degree + 360 - startDegreeBlue)));

				canvas.drawCircle(mX_blue, mY_blue, radius, paint_blue);
				canvas.drawCircle(mX_red, mY_red, radius, paint_red);

				if (degree >= 180)
					shouldRotate = false;
			}

			mRunningDot.draw(canvas, width, height);

			checkVaCham(canvas, width, height);

			break;
		case GAME_OVER:
			drawBlueOrRed(canvas, width, height);
			drawTextScore(canvas, width, height);
			break;

		default:
			break;
		}

	}

	private void drawTextScore(Canvas canvas, int width, int height) {
		paint_text_score.setTextSize(width / 15);
		paint_text_score.setTypeface(tf_boom_dot);

		canvas.drawText("Score: " + level, width / 2, height / 2 - height / 20, paint_text_score);
	}

	private void drawBlueOrRed(Canvas canvas, int width, int height) {
		paint_text_boom_dots.setShadowLayer(width / 50, 0F, 0F, Color.GRAY);
		paint_text_boom_dots.setTextSize(width / 12);
		canvas.drawText("BLUE OR RED ", width / 2, height / 6, paint_text_boom_dots);
	}

	int rate_jump_pos = 0;
	boolean isToTop;

	private void drawTapToJump(Canvas canvas, int width, int height) {
		if (rate_jump_pos > width / 50)
			isToTop = false;
		if (rate_jump_pos < (-1 * width / 50))
			isToTop = true;

		if (isToTop)
			rate_jump_pos += 1;
		else
			rate_jump_pos -= 1;

		paint_text_boom_dots.clearShadowLayer();
		canvas.drawText("TAP TO PLAY", width / 2, height * 3 / 8 + rate_jump_pos, paint_text_boom_dots);
	}

	private void drawLevel(Canvas canvas, int width, int height) {
		paint_level.setColor(Color.WHITE);
		canvas.drawCircle(width / 2, height / 4, paint_level_size, paint_level);

		paint_level.setColor(Color.rgb(background_color[0], background_color[1], background_color[2]));
		paint_level.setTextAlign(Paint.Align.CENTER);
		paint_level.setTextSize(paint_level_size);

		int xPos = (canvas.getWidth() / 2);
		int yPos = (int) ((canvas.getHeight() / 4) - ((paint_level.descent() + paint_level.ascent()) / 2));

		if (level < 10)
			canvas.drawText("0" + level, xPos, yPos, paint_level);
		else
			canvas.drawText("" + level, xPos, yPos, paint_level);
	}

	private void checkVaCham(Canvas canvas, int width, int height) {
		if (mRunningDot.getType_color() == TYPE_COLOR.BULE && checkVaCham(mX_blue, mY_blue)) {
			Log.d("TAG", "Running Blue new Level");
			startNewLevel(width, height);
			blue_blur = true;

			// TODO play sound vacham
			mFrgBoomDot.playSound(mFrgBoomDot.soundBlue);
		}
		if (mRunningDot.getType_color() == TYPE_COLOR.RED && checkVaCham(mX_blue, mY_blue)) {
			Log.d("TAG", "Running Red restartGame");
			restartGame(width, height);

			// startNewLevel(width, height);
			// red_blur = true;
		}
		if (mRunningDot.getType_color() == TYPE_COLOR.BULE && checkVaCham(mX_red, mY_red)) {
			Log.d("TAG", "Running BLue restartGame");
			restartGame(width, height);

			// startNewLevel(width, height);
			// blue_blur = true;
		}
		if (mRunningDot.getType_color() == TYPE_COLOR.RED && checkVaCham(mX_red, mY_red)) {
			Log.d("TAG", "Running Red startNewLevel");
			startNewLevel(width, height);
			red_blur = true;

			// TODO play sound red
			mFrgBoomDot.playSound(mFrgBoomDot.soundRed);
		}
	}

	private int radius_blur = 0;
	private boolean blue_blur;
	private boolean red_blur;

	/**
	 * To draw scale dot after selecting correct dot
	 */
	private void drawDotScale(Canvas canvas, int width, int height) {
		if (!blue_blur && !red_blur)
			return;
		radius_blur += 10;
		if (radius_blur >= paint_level_size * 2) {
			radius_blur = 0;
			blue_blur = red_blur = false;
		}
		if (blue_blur)
			canvas.drawCircle(width / 2, height / 2, radius_blur, paint_blue_blur);
		else if (red_blur)
			canvas.drawCircle(width / 2, height / 2, radius_blur, paint_red_blur);

	}

	private boolean checkVaCham(float mX, float mY) {
		float total = (mX - mRunningDot.getxPos()) * (mX - mRunningDot.getxPos()) + (mY - mRunningDot.getyPos())
				* (mY - mRunningDot.getyPos());
		double d = Math.sqrt(total);
		if (d <= (radius + mRunningDot.getRadius()))
			return true;
		return false;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		Log.d(TAG, "MySurfaceView surfaceChanged w:" + width + "h:" + height);
		initDrawObjects(width, height);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "MySurfaceView surfaceCreated");

		myThread = new MyThread(this);
		if (myThread != null)
			myThread.surfaceCreated();

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "MySurfaceView surfaceDestroyed");

		if (myThread != null)
			myThread.surfaceDestroyed();

	}

	public void onResume() {
		if (myThread != null)
			myThread.onResume();
	}

	public void onPause() {
		if (myThread != null)
			myThread.onPause();
	}

	public int[] getBackground_color() {
		return background_color;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public void setCurrentStatus(GAME_STATUS currentStatus) {
		this.currentStatus = currentStatus;
	}

	public void resetLevelSaveMe() {
		background_color = Define.BackgroundColors[0];
	}
	
	public void resetLevel() {
		level = 0;
		background_color = Define.BackgroundColors[0];
	}

	public Bitmap createBitmap() {
		int width = getWidth();
		int height = getHeight();

		Bitmap myBitmap = Bitmap.createBitmap(width, height, Config.RGB_565);
		Canvas canvas = new Canvas(myBitmap);

		canvas.drawColor(Color.rgb(background_color[0], background_color[1], background_color[2]));

		drawLevel(canvas, width, height);

		canvas.drawCircle(mX_blue, mY_blue, radius, paint_blue);
		canvas.drawCircle(mX_red, mY_red, radius, paint_red);

		return myBitmap;
	}

	public FrgBoomDot getmFrgBoomDot() {
		return mFrgBoomDot;
	}

	public void setmFrgBoomDot(FrgBoomDot mFrgBoomDot) {
		this.mFrgBoomDot = mFrgBoomDot;
	}

}
