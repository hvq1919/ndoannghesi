package net.tk.doannghesi.ui.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.facebook.FacebookHelper;
import net.tk.doannghesi.facebook.FacebookUser;
import net.tk.doannghesi.retrofit.objects.BaseObject;
import net.tk.doannghesi.retrofit.objects.User;
import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.tools.log.Toaster;
import net.tk.doannghesi.ui.adapter.TopPlayerAdapter;
import net.tk.doannghesi.ui.fragments.base.BaseFragment;
import net.tk.doannghesi.ui.models.Player;
import net.tk.doannghesi.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgTopPlayer extends BaseFragment implements OnClickListener {
    private final int NUM_OF_USER = 500;

    private ListView mListView;

    private TopPlayerAdapter mPlayerAdapter;

    private TextView tvLoginFB;
    private Button btnAll,btnTopTuan,btnBanbe;

    private void getListUserFireBase(){
        //Logger.e("=== GetListUserFireBase");
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("users");
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Logger.e("=== There are " + dataSnapshot.getChildrenCount() + " players");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void getListUser() {
        if (SavedStore.getIsFbLogged()) tvLoginFB.setVisibility(View.GONE);
        mActivity.showLoadingDialog();

        mActivity.mAPIService.getListUsers().enqueue(new Callback<BaseObject<List<User>>>() {
            @Override
            public void onResponse(Call<BaseObject<List<User>>> call, Response<BaseObject<List<User>>> response) {
                mActivity.dismissLoadingDialog();
                if (response.body()!= null && response.body().isSuccess() && response.body().getData() != null) {
                    ArrayList<User> users = new ArrayList<User>(response.body().getData());
                    mPlayerAdapter.clear();
                    mPlayerAdapter.addAll(users);
                } else {
                    Logger.d("Error get list users");
                }
            }

            @Override
            public void onFailure(Call<BaseObject<List<User>>> call, Throwable t) {
                mActivity.dismissLoadingDialog();
                Logger.d("Invalid response's format.");
            }
        });
    }

    private void getListUserWeek() {
        if (SavedStore.getIsFbLogged()) tvLoginFB.setVisibility(View.GONE);
        mActivity.showLoadingDialog();

        mActivity.mAPIService.getListUsersWeek().enqueue(new Callback<BaseObject<List<User>>>() {
            @Override
            public void onResponse(Call<BaseObject<List<User>>> call, Response<BaseObject<List<User>>> response) {
                mActivity.dismissLoadingDialog();
                if (response.body() != null && response.body().isSuccess() && response.body().getData() != null) {
                    ArrayList<User> users = new ArrayList<User>(response.body().getData());
                    mPlayerAdapter.clear();
                    mPlayerAdapter.addAll(users);
                } else {
                    Logger.d("Error get list users");
                }
            }

            @Override
            public void onFailure(Call<BaseObject<List<User>>> call, Throwable t) {
                mActivity.dismissLoadingDialog();
                Logger.d("Invalid response's format.");
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.frg_top_player, container, false);
        mListView = (ListView) mRootView.findViewById(R.id.listView);

        tvLoginFB = (TextView) mRootView.findViewById(R.id.tvLoginFB);

        btnAll = (Button) mRootView.findViewById(R.id.btn_all);
        btnTopTuan = (Button) mRootView.findViewById(R.id.btn_top_tuan);
        btnBanbe = (Button) mRootView.findViewById(R.id.btn_friend);

        //TODO set onclick all-toptuan-banbe

        mPlayerAdapter = new TopPlayerAdapter(mActivity, new ArrayList<User>() /*mPlayers*/);
        mListView.setAdapter(mPlayerAdapter);

        //mListView.setOnScrollListener(this);
        btnAll.setOnClickListener(this);
        btnTopTuan.setOnClickListener(this);
        btnBanbe.setOnClickListener(this);
        mRootView.findViewById(R.id.back).setOnClickListener(this);
        mRootView.findViewById(R.id.fb).setOnClickListener(this);

        if (SavedStore.getIsFbLogged())
            mRootView.findViewById(R.id.fb).setVisibility(View.GONE);

        btnTopTuan.setActivated(true);
        getListUserWeek();

        updateUserFb();

        getListUserFireBase();
        return mRootView;
    }


    private void getFriendList() {
        tvLoginFB.setVisibility(View.VISIBLE);
        //tvLoginFB.setText("");
        if (!SavedStore.getIsFbLogged()) {
            tvLoginFB.setText("Login Facebook để xem bạn bè nhé!");
            return;
        }

        String friends = SavedStore.getListFbFriends();

        // TODO update list friends
        try {
            if (mActivity.getFacebookHelper().getAccessToken() != null)
                mActivity.getFacebookHelper().makeUserFriendListsResquest(mActivity.getFacebookHelper().getAccessToken());
        } catch (Exception e) {
            loginFb();
        }

        try {
            String list_friends = "";
            JSONArray jsonArray = new JSONArray(friends);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String id = jsonObject.getString("id");
                if (i == 0) list_friends += id;
                else list_friends += "," + id;
            }
            FacebookUser facebookUser = (FacebookUser) SavedStore.getFbUserObject();
            if (!TextUtils.isEmpty(list_friends) && facebookUser != null)
                list_friends += "," + facebookUser.getId();
            if (!TextUtils.isEmpty(list_friends)) callApiGetListFriends(list_friends);
        } catch (Exception e) {
        }
    }

    private void callApiGetListFriends(String list) {
        mActivity.showLoadingDialog();
        mActivity.mAPIService.getListFriends(list).enqueue(new Callback<BaseObject<List<User>>>() {
            @Override
            public void onResponse(Call<BaseObject<List<User>>> call, Response<BaseObject<List<User>>> response) {
                mActivity.dismissLoadingDialog();
                if (response.body() != null && response.body().isSuccess() && response.body().getData() != null) {
                    ArrayList<User> friends = new ArrayList<User>(response.body().getData());
                    mPlayerAdapter.clear();
                    mPlayerAdapter.addAll(friends);

                    if (friends.size() > 0) tvLoginFB.setVisibility(View.GONE);
                    else tvLoginFB.setText("Rủ thêm bạn bè chơi để xem ai giỏi hơn nhé");
                } else {
                    tvLoginFB.setText("Rủ thêm bạn bè chơi để xem ai giỏi hơn nhé");
                }
            }

            @Override
            public void onFailure(Call<BaseObject<List<User>>> call, Throwable t) {
                mActivity.dismissLoadingDialog();
                Logger.d("Invalid response's format");
            }
        });
    }

    public class CustomComparator implements Comparator<Player> {
        @Override
        public int compare(Player o1, Player o2) {
            return o2.getLevel() - o1.getLevel();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                mActivity.popFragment();
                break;
            case R.id.fb:
                new AlertDialog.Builder(mActivity)
                        .setTitle("Facebook")
                        .setMessage("Đăng nhập Facebook để xem xếp hạng.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                loginFb();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();


                break;

            case R.id.btn_all:
                if(!btnAll.isActivated()){
                    btnTopTuan.setActivated(false);
                    btnAll.setActivated(true);
                    btnBanbe.setActivated(false);
                    mPlayerAdapter.clear();
                    getListUser();
                }
                break;
            case R.id.btn_top_tuan:
                if(!btnTopTuan.isActivated()){
                    btnTopTuan.setActivated(true);
                    btnAll.setActivated(false);
                    btnBanbe.setActivated(false);
                    mPlayerAdapter.clear();
                    getListUserWeek();
                }

                break;
            case R.id.btn_friend:
                if(!btnBanbe.isActivated()){
                    btnTopTuan.setActivated(false);
                    btnAll.setActivated(false);
                    btnBanbe.setActivated(true);
                    mPlayerAdapter.clear();
                    // getFriendList();
                }
                break;
            default:
                break;
        }
    }

    public void updateUserFb(){
        FacebookUser facebookUser = (FacebookUser) SavedStore.getFbUserObject();
        if(facebookUser == null) return;

        String android_id = Settings.Secure.getString(mActivity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        User user = new User();
        user.setDeviceId(android_id);
        user.setFullname(facebookUser.getName());
        user.setFbid(facebookUser.getId());
        user.setAvatarurl(facebookUser.getAvatarURL());
        mActivity.mAPIService.updateUserFb(user).enqueue(new Callback<BaseObject<User>>() {
            @Override
            public void onResponse(Call<BaseObject<User>> call, Response<BaseObject<User>> response) {

            }
            @Override
            public void onFailure(Call<BaseObject<User>> call, Throwable t) {

            }
        });
    }

    private void loginFb() {
        mActivity.getFacebookHelper().doLogin(new FacebookHelper.OnLoginSuccess() {
            @Override
            public void onLoginSuccess(FacebookUser facebookUser) {
                if (facebookUser != null) {
                    SavedStore.saveFbUserObject(facebookUser);

                    updateUserFb();

                    mRootView.findViewById(R.id.fb).setVisibility(View.GONE);
                    SavedStore.editIsFbLogged(true);

                    tvLoginFB.setVisibility(View.GONE);
                    if(btnTopTuan.isActivated()) getListUserWeek();
                    else if (btnAll.isActivated()) getListUser();
                    else{
                        tvLoginFB.setVisibility(View.VISIBLE);
                        // getFriendList();
                    }

                    Toaster.showToast(mActivity, "Success Logged Facebook");
                } else
                    mActivity.showToast("Can't login Facebook", true);
            }

        });
    }


}
