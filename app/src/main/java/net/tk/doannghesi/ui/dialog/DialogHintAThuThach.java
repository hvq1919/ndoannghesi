package net.tk.doannghesi.ui.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.R;

public class DialogHintAThuThach extends BaseFullScreenDialog {

    private TextView textTitle, textContent;
    private Button btnSanSang;


    private String mTextTitle, mTextContent;
    private OnClickListener mOnClickListener;


//	public  DialogHintAThuThach(String tTitle,String tContent,OnClickListener onClick) {
//		mTextTitle = tTitle;
//		mTextContent = tContent;
//		mOnClickListener = onClick;
//	}


    public static DialogHintAThuThach newInstance(String tTitle, String tContent, OnClickListener onClick) {
        DialogHintAThuThach dialogHintAThuThach = new DialogHintAThuThach();

        dialogHintAThuThach.setmTextTitle(tTitle);
        dialogHintAThuThach.setmTextContent(tContent);
        dialogHintAThuThach.setmOnClickListener(onClick);

        return dialogHintAThuThach;
    }

    public void setmTextTitle(String mTextTitle) {
        this.mTextTitle = mTextTitle;
    }

    public void setmTextContent(String mTextContent) {
        this.mTextContent = mTextContent;
    }

    public void setmOnClickListener(OnClickListener mOnClickListener) {
        this.mOnClickListener = mOnClickListener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.dialog_fragment_hint_thu_thach, container, false);

        textTitle = (TextView) rootView.findViewById(R.id.textTitle);
        textContent = (TextView) rootView.findViewById(R.id.textContent);
        btnSanSang = (Button) rootView.findViewById(R.id.btn);


        TextView tvHint = (TextView) rootView.findViewById(R.id.textFirst);
        if (!SavedStore.getFirstHint()) tvHint.setVisibility(View.VISIBLE);

        textContent.setText(mTextContent);
        textTitle.setText(mTextTitle);
        btnSanSang.setOnClickListener(mOnClickListener);

        return rootView;
    }

    public void setOnSanSangClick(OnClickListener onClickListener) {
        btnSanSang.setOnClickListener(onClickListener);
    }

    public void setTextTitle(String title) {
        textTitle.setText(title);
    }

    public void setTextContent(String text) {
        textContent.setText(text);
    }
}
