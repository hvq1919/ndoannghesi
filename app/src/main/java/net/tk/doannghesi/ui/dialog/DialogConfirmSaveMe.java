package net.tk.doannghesi.ui.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import net.tk.doannghesi.R;

public class DialogConfirmSaveMe extends BaseFullScreenDialog {

	public static final String TAG = DialogConfirmSaveMe.class.getSimpleName();

	private TextView tvCount, tvCotinue, tvNo;
	private View relSaveMe;

	private String textContinue;
	
	private int TIME = 3;
	private Handler mHandler = new Handler();

	private Runnable runnableCountDown = new Runnable() {

		@Override
		public void run() {
			TIME -= 1;
			tvCount.setText("" + TIME);
			if (TIME <= 0) {
				mHandler.removeCallbacks(runnableCountDown);
				DialogConfirmSaveMe.this.dismiss();
				if (mOnDimissClick != null)
					mOnDimissClick.onDimissClick();
			} else {
				mHandler.postDelayed(runnableCountDown, 1000);
			}
		}
	};

	public interface IOnDimissClick {
		public void onDimissClick();
	}

	private IOnDimissClick mOnDimissClick;

	private OnClickListener saveMeOnclickListener;

	public static DialogConfirmSaveMe newInstance(OnClickListener saveMeOnclickListener, IOnDimissClick iOnDimissClick) {
		DialogConfirmSaveMe dialogConfirmSaveMe = new DialogConfirmSaveMe();

		dialogConfirmSaveMe.setOnDimissClick(iOnDimissClick);
		dialogConfirmSaveMe.setSaveMeOnclickListener(saveMeOnclickListener);

		return dialogConfirmSaveMe;
	}

//	public DialogConfirmSaveMe(OnClickListener saveMeOnclickListener, IOnDimissClick iOnDimissClick) {
//		super();
//		this.saveMeOnclickListener = saveMeOnclickListener;
//		this.mOnDimissClick = iOnDimissClick;
//	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.confirm_game_over, container, false);

		tvCount = (TextView) rootView.findViewById(R.id.tvCount);
		tvCotinue = (TextView) rootView.findViewById(R.id.tvCotinue);
		tvNo = (TextView) rootView.findViewById(R.id.tvNo);
		relSaveMe = rootView.findViewById(R.id.relSaveMe);

		if(!TextUtils.isEmpty(textContinue)) tvCotinue.setText(textContinue);
		
		relSaveMe.setOnClickListener(saveMeOnclickListener);

		TIME = 3;
		mHandler.postDelayed(runnableCountDown, 1000);

		tvNo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mHandler.removeCallbacksAndMessages(null);
				DialogConfirmSaveMe.this.dismiss();
				if (mOnDimissClick != null)
					mOnDimissClick.onDimissClick();
			}
		});

		return rootView;
	}

	
	
	
	public void setTextContinue(String textContinue) {
		this.textContinue = textContinue;
	}


	public void saveMeClicked() {
		mHandler.removeCallbacksAndMessages(null);
		DialogConfirmSaveMe.this.dismiss();
	}
	public void removeAllCallBack(){
		mHandler.removeCallbacksAndMessages(null);
	}

	public void setSaveMeOnclickListener(OnClickListener saveMeOnclickListener) {
		this.saveMeOnclickListener = saveMeOnclickListener;
	}

	public void setOnDimissClick(IOnDimissClick onDimissClick) {
		this.mOnDimissClick = onDimissClick;
	}
}
