package net.tk.doannghesi.ui.threads;

import android.os.AsyncTask;

import net.tk.doannghesi.tools.log.Logger;
import net.tk.doannghesi.ui.databases.MyDatabase;

/**
 * To copy Sqlite databases from assets folder
 * 
 * @author My PC
 * 
 */
public class CopyDatabase extends AsyncTask<Void, Void, Void> {

	public interface OnGetListObject {
		public void getListObject(Object object);
	}

	private MyDatabase db;
	private OnGetListObject mOnGetListObject;
	private Object object;

	public CopyDatabase(MyDatabase database, OnGetListObject onGetListObject) {
		db = database;
		mOnGetListObject = onGetListObject;
	}

	@Override
	protected Void doInBackground(Void... params) {
		object = db.getQuestion();
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		Logger.d("onPost");
		if(mOnGetListObject!= null) mOnGetListObject.getListObject(object);
	}

}
