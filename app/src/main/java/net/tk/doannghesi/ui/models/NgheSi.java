package net.tk.doannghesi.ui.models;

import java.io.Serializable;


public class NgheSi implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6807389315042823469L;
    private String ImgName;
    private int Like;
    private String Name;
    private String ObjectId;


    public NgheSi() {
        super();
    }

    public String getImgName() {
        return ImgName;
    }

    public void setImgName(String imgName) {
        ImgName = imgName;
    }

    public int getLike() {
        return Like;
    }

    public void setLike(int like) {
        Like = like;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getObjectId() {
        return ObjectId;
    }

    public void setObjectId(String objectId) {
        ObjectId = objectId;
    }


}
