package net.tk.doannghesi.ui.models;

/**
 * Created by quanhv on 2/14/2016.
 */
public class ArtistKey{
    private Artist artist;
    private String key;


    public ArtistKey(Artist artist, String key) {
        this.artist = artist;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }


}
