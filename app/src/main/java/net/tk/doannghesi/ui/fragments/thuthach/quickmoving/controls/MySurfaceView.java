package net.tk.doannghesi.ui.fragments.thuthach.quickmoving.controls;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.Define;
import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.FrgQuickMoving;
import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.objects.Blue;
import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.objects.Parent;
import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.objects.Red;
import net.tk.doannghesi.ui.fragments.thuthach.quickmoving.thread.MyThread;

import java.util.ArrayList;

public class MySurfaceView extends SurfaceView implements Callback {
	private static final String TAG = "TAG";

	private static final float TOUCH_TOLERANCE = 2;

	private SurfaceHolder surfaceHolder;
	private MyThread myThread;

	private ArrayList<Blue> blues;
	private ArrayList<Red> reds;

	// Setting for Main dot
	private Paint paint;
	private int color = Color.parseColor("#FFEB3B");// Yellow
	private float radius = 10;
	private float mainDotX;
	private float mainDotY;
	private float speedMain = 1.0f;

	private int bg_color;

	// Setting for draw text;
	private Paint paintText;
	private int text_color = Color.parseColor("#636303");

	private float mX;
	private float mY;

	private int level = 1;

	private int width, height;

	// ////////////////////////////////////////
	// --------Game Status ------------
	public enum GAME_STATUS {
		GAME_NEW, GAME_RUNNIG, GAME_COFIRM, GAME_OVER
	};

	public interface IGameStatus {

		public void onGameOver();

	}

	private IGameStatus mGameStatus;

	public void setIGameStatus(IGameStatus gameStatus) {
		mGameStatus = gameStatus;
	}

	private GAME_STATUS currentStatus = GAME_STATUS.GAME_NEW;

	private Typeface tf_dot_tappy;

	private FrgQuickMoving mainFragment;

	public MySurfaceView(Context context) {
		super(context);
		tf_dot_tappy = Typeface.createFromAsset(context.getAssets(), "fonts/dottappy.otf");
		init();
	}

	public MySurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		tf_dot_tappy = Typeface.createFromAsset(context.getAssets(), "fonts/dottappy.otf");
		init();
	}

	public MySurfaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		tf_dot_tappy = Typeface.createFromAsset(context.getAssets(), "dottappy.otf");
		init();
	}

	private void init() {
		Log.d(TAG, "MySurfaceView init");

		setFocusable(true); // make sure we get key events
		surfaceHolder = getHolder();

		initPaint();

		surfaceHolder.addCallback(this);

	}

	private void initPaint() {
		paint = new Paint();
		paint.setColor(color);
		paint.setFlags(Paint.ANTI_ALIAS_FLAG);

		paintText = new Paint();
		paintText.setColor(text_color);
		paintText.setFlags(Paint.ANTI_ALIAS_FLAG);
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	private void initVatCan(int w, int h) {
		// vatcan_speed = SurfaceView's height / RATE_SPEED
		radius = h / Define.RATE_HEIGHT_MAIN;
	}

	private void initDrawObjects(int w, int h) {
		blues = new ArrayList<Blue>();
		reds = new ArrayList<Red>();

		initVatCan(w, h);

		Log.d("TAG", "w;" + w + "H:" + h);
		for (int i = 0; i < 5; i++) {
			// Blue blue = new Blue();
			// blue.initParent(w, h);
			// blue.initSpeed(level);
			//
			// blues.add(blue);

			Red red = new Red();
			red.initParent(w, h);
			red.initSpeed(3 / 2);

			reds.add(red);
		}

		addBlues(w, h);
	}

	public void restartGameSaveMe() {
		if (width == 0 || height == 0 || blues == null || reds == null)
			return;
		blues.clear();
		reds.clear();
		initDrawObjects(width, height);

	}

	public void restartGame() {
		if (width == 0 || height == 0 || blues == null || reds == null)
			return;
		blues.clear();
		reds.clear();
		level = 1;
		initDrawObjects(width, height);

	}

	private void startNewLevel(int w, int h) {
		// TODO sound new level
		mainFragment.playSound(mainFragment.soundLevelUp);
		
		level += 1;

		// if (level < 4) {
		// Red red = new Red();
		// red.initParent(w, h);
		// red.initSpeed(level / 2);
		//
		// reds.add(red);
		// }

		addBlues(w, h);

	}

	private void addBlues(int w, int h) {
		for (int i = 0; i < 3; i++) {
			Blue blue = new Blue();
			blue.initParent(w, h);
			blue.initSpeed(level / 2);

			blues.add(blue);
		}
	}

	/**
	 * This method set event listener for drawing.
	 *            the instance of MotionEvent
	 * @return
	 */
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		switch (ev.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touch_start(ev.getX(), ev.getY());
			break;
		case MotionEvent.ACTION_MOVE:
			touch_move(ev.getX(), ev.getY());
			break;
		case MotionEvent.ACTION_UP:
			// this.onActionUp(event);
			break;
		default:
			break;
		}

		// Re draw
		// this.invalidate();

		return true;
	}

	private void touch_move(float x, float y) {
		float dx = Math.abs(x - mX);
		float dy = Math.abs(y - mY);
		if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
			mainDotX += speedMain * (x - mX);
			if (mainDotX <= radius)
				mainDotX = radius;
			else if (mainDotX >= getWidth() - radius)
				mainDotX = getWidth() - radius;

			mainDotY += speedMain * (y - mY);
			if (mainDotY <= radius)
				mainDotY = radius;
			else if (mainDotY >= getHeight() - radius)
				mainDotY = getHeight() - radius;

			mX = x;
			mY = y;
		}
	}

	private void touch_start(float x, float y) {
		mX = x;
		mY = y;
	}

	public int getDuration() {
		return myThread.getSleep();
	}

	public void setDuration(int time) {
		myThread.setSleep(time);
	}

	public void drawSomething(Canvas canvas) {

		canvas.drawColor(Color.BLACK);
		drawObject(canvas, getWidth(), getHeight());
	}

	private void drawObject(Canvas canvas, int width, int height) {
		switch (currentStatus) {
		case GAME_NEW:
			drawQuickMoving(canvas, width, height);
			break;
		case GAME_RUNNIG:
			drawLevel(canvas, width, height);
			canvas.drawCircle(mainDotX, mainDotY, radius, paint);
			for (Blue blue : blues)
				blue.draw(canvas, width, height);

			for (Red red : reds)
				red.draw(canvas, width, height);

			for (Blue blue : blues)
				if (checkVaCham(blue)) {
					blues.remove(blue);
					if (blues.size() == 0)
						startNewLevel(width, height);
					else {
						// TODO playsound touch blue dot
						// mainFragment.playSound(mainFragment.soundTouchBlueDots);
						mainFragment.playSound(mainFragment.soundTouchBlueDots);
					}
					break;
				}
			for (Red red : reds)
				if (checkVaCham(red)) {
					currentStatus = GAME_STATUS.GAME_OVER;
					if (mGameStatus != null)
						mGameStatus.onGameOver();
					break;
				}

			break;

		case GAME_OVER:
			canvas.drawText("Score : " + level, width / 2, height / 2 - radius * 2, paintText);
			drawQuickMoving(canvas, width, height);
			break;
		default:
			break;
		}

	}

	private void drawQuickMoving(Canvas canvas, int width, int height) {

		paintText.setColor(text_color);

		paintText.setTextSize(3 * radius);
		paintText.setTextAlign(Paint.Align.CENTER);
		paintText.setTypeface(tf_dot_tappy);
		paintText.setShadowLayer(radius / 4, radius / 8, radius / 8, Color.GRAY);
		canvas.drawText("QUICK MOVING", width / 2, height / 6, paintText);

		paintText.clearShadowLayer();
		paintText.setTextSize(radius * 2);

		// canvas.drawText("Best Score : " + best_score, width / 2, height / 2,
		// paintText);
		// canvas.drawText("Games Played : " + gamePlayed, width / 2, height / 2
		// + radius * 2, paintText);

	}

	private void drawLevel(Canvas canvas, int width, int height) {
		paintText.setColor(text_color);
		canvas.drawCircle(width / 2, width / 5, width / 10, paintText);

		paintText.setColor(Color.BLACK);
		paintText.setTextAlign(Paint.Align.CENTER);
		paintText.setTextSize(width / 10);

		int xPos = (width / 2);
		int yPos = (int) ((width / 5) - ((paintText.descent() + paintText.ascent()) / 2));
		//canvas.drawText("0" + level, xPos, yPos, paintText);

		if (level < 10)
			canvas.drawText("0" + level, xPos, yPos, paintText);
		else
			canvas.drawText("" + level, xPos, yPos, paintText);
	}

	private boolean checkVaCham(Parent parent) {
		float total = (mainDotX - parent.getxPos()) * (mainDotX - parent.getxPos()) + (mainDotY - parent.getyPos())
				* (mainDotY - parent.getyPos());
		double d = Math.sqrt(total);
		if (d <= (radius + parent.getRadius()))
			return true;
		return false;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		width = w;
		height = h;

		mainDotX = width / 2;
		mainDotY = height / 2;

		initDrawObjects(width, height);
	}

	public GAME_STATUS getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(GAME_STATUS currentStatus) {
		this.currentStatus = currentStatus;
	}

	public void setMainFragment(FrgQuickMoving mainFragment) {
		this.mainFragment = mainFragment;
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "MySurfaceView surfaceCreated");

		myThread = new MyThread(this);
		if (myThread != null)
			myThread.surfaceCreated();

	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.d(TAG, "MySurfaceView surfaceDestroyed");

		if (myThread != null)
			myThread.surfaceDestroyed();

	}

	public void onResume() {
		if (myThread != null)
			myThread.onResume();
	}

	public void onPause() {
		if (myThread != null)
			myThread.onPause();
	}

}
