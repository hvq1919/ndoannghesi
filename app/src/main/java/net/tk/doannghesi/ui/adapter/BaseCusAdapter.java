package net.tk.doannghesi.ui.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Collection;

public abstract class BaseCusAdapter<T> extends ArrayAdapter<T> {
	private ArrayList<T> mDatas;

	public BaseCusAdapter(Context context, ArrayList<T> lists) {
		super(context, 0, lists);
		mDatas = lists;
	}

	public ArrayList<T> getDatas() {
		return mDatas;
	}

	public void addAllNewList(ArrayList<T> arrayList) {
		clear();
		mDatas.addAll(arrayList);
		notifyDataSetChanged();
	}

	@Override
	public void remove(T object) {
		mDatas.remove(object);
		notifyDataSetChanged();
	}

	@Override
	public void add(T object) {
		mDatas.add(object);
		notifyDataSetChanged();
	}

	@Override
	public void addAll(Collection<? extends T> collection) {
		mDatas.addAll(collection);
		notifyDataSetChanged();
	}

	@Override
	public void clear() {
		mDatas.clear();
		notifyDataSetChanged();
	}

}
