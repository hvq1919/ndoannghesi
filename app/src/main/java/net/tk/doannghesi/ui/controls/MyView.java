package net.tk.doannghesi.ui.controls;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import net.tk.doannghesi.ui.models.PathInfo;
import net.tk.doannghesi.ui.models.PathInfo.TYPE;

import java.util.ArrayList;

public class MyView extends View {

	private static final float TOUCH_TOLERANCE = 4;
	private static final int SPEED = 10;
	private float mX, mY;
	private int COLOR_OVERLAY = Color.GRAY;

	private int StrokeWidth = 10;
	private float MaskFilterRadius = 2;
	/* Using to save in SharedPreferences */
	private ArrayList<PathInfo> mPaths;

	private Bitmap mBitmap;
	private Canvas mCanvas;
	private Path mPath;
	private Paint mBitmapPaint;
	private Paint mPaint;
	private BlurMaskFilter mBlur;

	private boolean canScratch = true;

	private OnGetPercent mOnGetPercent;

	public interface OnGetPercent {
		public void onGetPercent(float percent);
	}

	public MyView(Context c) {
		super(c);

		init();

	}

	public MyView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	public MyView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		mPaths = new ArrayList<PathInfo>();
		mPath = new Path();
		mBitmapPaint = new Paint(Paint.DITHER_FLAG);

		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(StrokeWidth);

		mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
		mBlur = new BlurMaskFilter(MaskFilterRadius, BlurMaskFilter.Blur.NORMAL);
		mPaint.setMaskFilter(mBlur);

		// mPaint.setShadowLayer(2, 2, 2, Color.RED);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		Log.d("TAG", "onSizeChanged:" + w / 10);

		StrokeWidth = w / 90;
		if (mPaint != null)
			mPaint.setStrokeWidth(StrokeWidth);

		// Bitmap bm =
		// BitmapFactory.decodeResource(MyApplication.getInstance().getApplicationContext().getResources(),
		// R.drawable.ic_launcher);

		mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		mCanvas = new Canvas(mBitmap);
		mCanvas.drawColor(COLOR_OVERLAY);

		Log.d("TAG", "onSizeChanged w:" + w + " h:" + h);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		Log.d("TAG", "onDraw");
		// canvas.drawColor(0xFFAAAAAA);
		mCanvas.drawPath(mPath, mPaint);
		canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
		// super.onDraw(canvas);
	}

	private void touch_start(float x, float y) {
		Log.d("TAG", "touch_start");
		mPath.reset();
		mPath.moveTo(x, y);

		mPaths.add(new PathInfo(x, y, 0, 0, TYPE.START));

		mX = x;
		mY = y;
	}

	private void touch_move(float x, float y) {
		Log.d("TAG", "touch_move");
		float dx = Math.abs(x - mX);
		float dy = Math.abs(y - mY);
		if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
			mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);

			mPaths.add(new PathInfo(mX, mY, (x + mX) / 2, (y + mY) / 2, TYPE.QUADTO));

			mX = x;
			mY = y;
		}
	}

	private void touch_up() {
		Log.d("TAG", "touch_up");
		mPath.lineTo(mX, mY);

		mPaths.add(new PathInfo(mX, mY, 0, 0, TYPE.END));

		// commit the path to our offscreen
		mCanvas.drawPath(mPath, mPaint);
		// kill this so we don't double draw
		mPath.reset();
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!canScratch)
			return false;

		float x = event.getX();
		float y = event.getY();

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			touch_start(x, y);
			invalidate();
			break;
		case MotionEvent.ACTION_MOVE:
			touch_move(x, y);
			invalidate();

			if (mOnGetPercent != null)
				mOnGetPercent.onGetPercent(getScratchedRatio(SPEED));
			break;
		case MotionEvent.ACTION_UP:
			touch_up();
			invalidate();
			break;
		}
		return true;
	}

	public float getScratchedRatio(int speed) {
		final int width = mBitmap.getWidth();
		final int height = mBitmap.getHeight();

		int count = 0;
		for (int i = 0; i < width; i += speed) {
			for (int j = 0; j < height; j += speed) {
				if (0 == Color.alpha(mBitmap.getPixel(i, j))) {
					count++;
				}
			}
		}
		float completed = (float) count / ((width / speed) * (height / speed)) * 100;

		return completed;
	}

	// /////////////////////////////////////////////////////////
	// /////// -- Public Method -- /////////
	// /////////////////////////////////////////////////////////

	/**
	 * To set percent callback
	 * 
	 * @param onGetPercent
	 */
	public void setOnGetPercent(OnGetPercent onGetPercent) {
		mOnGetPercent = onGetPercent;
	}

	public ArrayList<PathInfo> getPaths() {
		return mPaths;
	}

	public void setmPaths(ArrayList<PathInfo> paths) {
		this.mPaths = paths;
	}

	public void drawPaths(ArrayList<PathInfo> paths) {
		Log.d("TAG", "drawPaths");
		this.mPaths = paths;
		for (PathInfo pathInfo : paths) {
			switch (pathInfo.getmType()) {
			case START:
				mPath.reset();
				mPath.moveTo(pathInfo.getX1(), pathInfo.getY1());
				break;
			case QUADTO:
				mPath.quadTo(pathInfo.getX1(), pathInfo.getY1(), pathInfo.getX2(), pathInfo.getY2());
				break;
			case END:
				mPath.lineTo(pathInfo.getX1(), pathInfo.getY1());
				break;
			default:
				break;
			}
			mCanvas.drawPath(mPath, mPaint);
		}

	}

	public boolean isCanScratch() {
		return canScratch;
	}

	public void setCanScratch(boolean canScratch) {
		this.canScratch = canScratch;
	}

	public void validate() {
		invalidate();
		if (mOnGetPercent != null)
			mOnGetPercent.onGetPercent(getScratchedRatio(SPEED));
	}

	public void clearAllBitmap() {
		mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
		mPath.reset();
		mPaths.clear();
	}

	public void resetMyView() {
		mCanvas.drawColor(COLOR_OVERLAY);
		invalidate();
	}

	public void caoLai() {
		mCanvas.drawColor(COLOR_OVERLAY);
		mPath.reset();
		mPaths.clear();
		invalidate();
	}
}