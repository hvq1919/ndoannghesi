package net.tk.doannghesi.ui.models;

import java.io.Serializable;

public class PathInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4758468452108965681L;

	public static enum TYPE {
		START, QUADTO, END
	}

	private float x1, y1;
	private float x2, y2;

	private TYPE mType;

	public PathInfo(float x1, float y1, float x2, float y2, TYPE mType) {
		super();
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.mType = mType;
	}

	public PathInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public float getX1() {
		return x1;
	}

	public void setX1(float x1) {
		this.x1 = x1;
	}

	public float getY1() {
		return y1;
	}

	public void setY1(float y1) {
		this.y1 = y1;
	}

	public float getX2() {
		return x2;
	}

	public void setX2(float x2) {
		this.x2 = x2;
	}

	public float getY2() {
		return y2;
	}

	public void setY2(float y2) {
		this.y2 = y2;
	}

	public TYPE getmType() {
		return mType;
	}

	public void setmType(TYPE mType) {
		this.mType = mType;
	}

}
