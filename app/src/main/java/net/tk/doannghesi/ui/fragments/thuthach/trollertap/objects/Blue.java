package net.tk.doannghesi.ui.fragments.thuthach.trollertap.objects;

import android.graphics.Canvas;
import android.graphics.Color;

public class Blue extends Parent {
	private int color = Color.parseColor("#2196F3");// Blue
	private int radius = 10;

	public Blue() {
		super();
		init();
	}

	public Blue(float de) {
		super(de);
		init();
	}

	public Blue(int xPos, int yPos) {
		super(xPos, yPos);
		init();
	}

	public Blue(int xPos) {
		super(xPos);
		init();
	}

	private void init() {
		setRadius(radius*density);
		setColor(color);
	}

	@Override
	public void draw(Canvas canvas, int w, int h, boolean pauseGame) {
		super.draw(canvas, w, h, pauseGame);
	}

	// -----------------------------------------------------------//
	// ---------------------- Setter && Getter ------------------ //
	// -----------------------------------------------------------//

}
