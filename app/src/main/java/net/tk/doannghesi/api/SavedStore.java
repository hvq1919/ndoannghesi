package net.tk.doannghesi.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import android.util.Log;

import net.tk.doannghesi.Define;
import net.tk.doannghesi.MyApplication;
import net.tk.doannghesi.retrofit.objects.Ads;
import net.tk.doannghesi.retrofit.objects.Qcs;
import net.tk.doannghesi.tools.utils.ObjectSerializer;
import net.tk.doannghesi.ui.fragments.thuthach.trollertap.objects.CurrentMainDot;
import net.tk.doannghesi.ui.models.PathInfo;
import net.tk.doannghesi.ui.models.Questions;
import net.tk.doannghesi.ui.models.UserDetails;
import net.tk.doannghesi.R;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

@SuppressWarnings("unchecked")
public class SavedStore {
    private static final String PREFS_KEY = "account-session";
    private static final String PREFS_USER_DETAILS = "prefsUserDetails";
    private static final String PREFS_TOKEN = "prefsOauthToken";
    private static final String PREFS_REMEMBER = "prefsRememberMe";
    private static final String PREFS_CURRENT_NUMBER_QUESTION = "prefsCurrentNumberQuestion";

    private static final String PREFS_FIRST_GET_TIME = "PREFS_FIRST_GET_TIME";
    private static final String PREFS_FIRST_SPIN_NOW = "PREFS_FIRST_SPIN_NOW";

    private static final String PREFS_TIME_RUNNABLE_ACTIONBAR = "PREFS_TIME_RUNNABLE_ACTIONBAR";
    private static final String PREFS_TIME_RUNNABLE_BACKGROUND = "PREFS_TIME_RUNNABLE_BACKGROUND";

    private static final String PREFS_TimeRate = "prefsTimeRate";
    private static final String PREFS_COIN = "prefsCoin";
    private static final String PREFS_KC = "PREFS_KC";
    private static final String PREFS_SPIN = "prefsSpin";
    private static final String PREFS_NOTED = "prefsNoted";
    private static final String PREFS_TIME_SPIN_COUNTDOWN = "prefsTimeSpinCountDown";
    private static final String PREFS_TI_LE_CAO = "PREFS_Tlc";
    private static final String PREFS_IS_VIP = "PREFS_ivpi";
    private static final String PREFS_first_nghesi = "PREFS_first_nghesi";
    private static final String PREFS_list_GiftCode = "PREFS_ofdkfksef45d";
    private static final String PREFS_IsBinhChon = "PREFS_IsBinhChon";
    private static final String PREFS_SDT = "PREFS_SDT";
    private static final String PREFS_ShowAdMob = "PREFS_ShowAdMob";
    /* Sound */
    private static final String PREFS_MUSIC = "prefsMusic";
    private static final String PREFS_SOUND = "prefsSound";

    private static final String prefsFirstHintGame = "prefsFirstHintGame";
    private static final String prefsFirstRating = "prefsFirstRating";

    /* Using for facebook */
    private static final String PREFS_FB_USER_OBJECT = "fb_user_object";
    private static final String PREFS_Achievements = "PREFS_Achievements";
    private static final String PREFS_PATHS = "PREFS_PATHS";

    private static final String PREFS_LIST_QUESTION = "PREFS_LIST_QUESTION";

    private static final String START_QUESTION = "START_QUESTION";

    private static final String LOGGED_FB = "LOGGED_FB";

    private static final String PREFS_MAIN_DOT = "PREFS_MADOQW";
    private static final String PREFS_DAY = "PREFS_DAY";

    private static final String PREFS_THUTHACH = "PREFS_THUTHACH";
    private static final String PREFS_NoAdsSKU = "NoAdsSKU";
    private static final String PREFS_NoAdsFireBase = "NoAdsFireBase";
    private static final String PREFS_Hienthi = "Hienthi";
    private static final String PREFS_ONLY_FB_USER = "PREFS_ONLY_FB_USER";
    private static final String PREFS_LIST_FB_FRIEND = "PREFS_LIST_FB_FRIEND";
    private static final String PREFS_Ads_OBJECT = "PREFS_Ads_OBJECT";
    private static final String PREFS_Ads_OBJECT2 = "PREFS_Ads_OBJECT2";
    private static final String PREFS_NEW_QCS = "PREFS_NEW_QCS";
    private static final String PREFS_QCS_VER = "PREFS_QCS_VER";
    private static final String PREFS_QCS_CLICK = "PREFS_QCS_CLICK";
    private static final String PREFS_QCS_CLICK2 = "PREFS_QCS_CLICK2";
    private static final String PREFS_QCS_INSTALL = "PREFS_QCS_INSTALL";
    private static final String PREFS_TIME_FIRST = "TIME_FIRST";
    private static final String PREFS_VERSION_CODE = "PREFS_VERSION_CODE";

    private static final String PREFS_ONLINE_IS_SHOW = "PREFS_ONLINE_IS_SHOW";
    private static final String PREFS_ONLINE_CURRENT_QUESTION = "PREFS_ONLINE_CURRENT_QUESTION";
    private static final String PREFS_ONLINE_TOTAL_QUESTION = "PREFS_ONLINE_TOTAL_QUESTION";

    public static int getTotalOnlineQuestion() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String number = decryptString(mShared.getString(PREFS_ONLINE_TOTAL_QUESTION, ""));
        try {
            return Integer.parseInt(number);
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean editTotalOnlineQuestion(int number) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_ONLINE_TOTAL_QUESTION, encryptString(number + ""));
        return editor.commit();
    }

    public static int getCurrentOnlineQuestion() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String number = decryptString(mShared.getString(PREFS_ONLINE_CURRENT_QUESTION, ""));
        try {
            return Integer.parseInt(number);
        } catch (Exception e) {
            return 1;
        }
    }

    public static boolean editCurrentOnlineQuestion(int number) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_ONLINE_CURRENT_QUESTION, encryptString(number + ""));
        return editor.commit();
    }

    /* Get clicked on image note status */
    public static boolean getIsOnlineQuestion() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_ONLINE_IS_SHOW, false);
    }

    public static boolean editIsOnlineQuestion(boolean isOnline) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_ONLINE_IS_SHOW, isOnline);
        return editor.commit();
    }

    private static String encryptString(String s) {
        Context context = MyApplication.getInstance().getApplicationContext();
        String skey = context.getResources().getString(R.string.app_id);
        try {
            DESKeySpec keySpec = new DESKeySpec(skey.getBytes());
            SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
            SecretKey key = factory.generateSecret(keySpec);

            DesEncrypter encrypter = new DesEncrypter(key);
            String encrypted = encrypter.encrypt(s);

            Log.d("TAG", "encryption: " + encrypted);

            return encrypted;
        } catch (Exception e) {
            return "";
        }

    }

    private static String decryptString(String s) {
        Context context = MyApplication.getInstance().getApplicationContext();
        String skey = context.getResources().getString(R.string.app_id);
        try {
            DESKeySpec keySpec = new DESKeySpec(skey.getBytes());
            SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
            SecretKey key = factory.generateSecret(keySpec);

            DesEncrypter encrypter = new DesEncrypter(key);
            String decrypted = encrypter.decrypt(s);
            Log.d("TAG", "decrypted: " + decrypted);
            return decrypted;
        } catch (Exception e) {
            return "";
        }

    }

    public static long getTimeFirst() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getLong(PREFS_TIME_FIRST, System.currentTimeMillis());
    }

    public static boolean editTimeFirst(long time) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putLong(PREFS_TIME_FIRST, time);
        return editor.commit();
    }

    public static long getTimeRate() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getLong(PREFS_TimeRate, 0);
    }

    public static boolean editTimeRate(long sdt) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putLong(PREFS_TimeRate, sdt);
        return editor.commit();
    }

    public static String getSDT() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getString(PREFS_SDT, "");
    }

    public static boolean editSDT(String sdt) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_SDT, sdt);
        return editor.commit();
    }

    public static String getListFbFriends() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getString(PREFS_LIST_FB_FRIEND, "");
    }

    public static boolean editListFbFriends(String listFriend) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_LIST_FB_FRIEND, listFriend);
        return editor.commit();
    }

    public static int getStart_Question() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String number = decryptString(mShared.getString(START_QUESTION, ""));
        try {
            return Integer.parseInt(number);
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean editStart_Question(int number) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(START_QUESTION, encryptString(number + ""));
        return editor.commit();
    }

    public static int getThuThach() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String number = decryptString(mShared.getString(PREFS_THUTHACH, ""));
        try {
            return Integer.parseInt(number);
        } catch (Exception e) {
            return 0;
        }
    }

    public static boolean editThuThach(int thuthach) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_THUTHACH, encryptString(thuthach + ""));
        return editor.commit();
    }

    /* Get clicked on image note status */
    public static boolean getNewQcs() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_NEW_QCS, true);
    }

    public static boolean editNewQcs(boolean wasNoted) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_NEW_QCS, wasNoted);
        return editor.commit();
    }


    /* Get clicked on image note status */
    public static boolean getShowAdMob() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_ShowAdMob, true);
    }

    public static boolean editShowAdMob(boolean wasNoted) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_ShowAdMob, wasNoted);
        return editor.commit();
    }

    /* Get clicked on image note status */
    // true : will not show ads
    public static boolean getNoadsFirebase() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_NoAdsFireBase, false);
    }

    public static boolean editNoadsFirebase(boolean noads) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_NoAdsFireBase, noads);
        return editor.commit();
    }

    /* Get clicked on image note status */
    // true : will not show ads
    public static int getVersionCode() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getInt(PREFS_VERSION_CODE, 0);
    }

    public static boolean editVersionCode(int hienthi) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putInt(PREFS_VERSION_CODE, hienthi);
        return editor.commit();
    }

    /* Get clicked on image note status */
    // true : will not show ads
    public static int getHienThi() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getInt(PREFS_Hienthi, 10);
    }

    public static boolean editHienThi(int hienthi) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putInt(PREFS_Hienthi, hienthi);
        return editor.commit();
    }

    public static int getQcs_Ver() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getInt(PREFS_QCS_VER, 0);
    }

    public static boolean editQcs_Ver(int hienthi) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putInt(PREFS_QCS_VER, hienthi);
        return editor.commit();
    }


    /* Get clicked on image note status */
    // true : will not show ads
    public static ArrayList<String> getQcsInstall() {
        try {
            ArrayList<String> qcsInstall = (ArrayList<String>) getObject(PREFS_QCS_INSTALL);
            return qcsInstall == null ? new ArrayList<String>() : qcsInstall;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public static boolean editQcsInstall(ArrayList<String> install) {
        try {
            return saveObject(PREFS_QCS_INSTALL, install);
        } catch (Exception e) {
            return false;
        }
    }

    /* Get clicked on image note status */
    // true : will not show ads
//    public static Qcs getQcsClick() {
//        try {
//            return (Qcs) getObject(PREFS_QCS_CLICK);
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    public static boolean editQcsClick(Qcs qcs) {
//        try {
//            return saveObject(PREFS_QCS_CLICK, qcs);
//        } catch (Exception e) {
//            return false;
//        }
//    }
//
//    public static void clearQcsClick() {
//        clear(PREFS_QCS_CLICK);
//    }

    public static Qcs getQcsClick() {
        try {
            return (Qcs) getObject(PREFS_QCS_CLICK2);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean editQcsClick(Qcs qcs) {
        try {
            return saveObject(PREFS_QCS_CLICK2, qcs);
        } catch (Exception e) {
            return false;
        }
    }

    public static void clearQcsClick() {
        clear(PREFS_QCS_CLICK2);
    }

    /* Get clicked on image note status */
    // true : will not show ads
//    public static AdsObject getAdsObject() {
//        try {
//            return (AdsObject) getObject(PREFS_Ads_OBJECT);
//        } catch (Exception e) {
//            return null;
//        }
//    }
//
//    public static boolean editAdsObject(AdsObject adsObject) {
//        try {
//            return saveObject(PREFS_Ads_OBJECT, adsObject);
//        } catch (Exception e) {
//            return false;
//        }
//    }

    public static Ads getAdsObject() {
        try {
            return (Ads) getObject(PREFS_Ads_OBJECT2);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean editAdsObject(Ads adsObject) {
        try {
            return saveObject(PREFS_Ads_OBJECT2, adsObject);
        } catch (Exception e) {
            return false;
        }
    }

    /* Get clicked on image note status */
    // true : will not show ads
    public static boolean getOnlyFbUser() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_ONLY_FB_USER, true);
    }

    public static boolean editOnlyFbUser(boolean noads) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_ONLY_FB_USER, noads);
        return editor.commit();
    }


    public static long getDaySpin() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String number = decryptString(mShared.getString(PREFS_DAY, ""));
        try {
            return Long.parseLong(number);
        } catch (Exception e) {
            return -1;
        }
    }

    public static boolean editDaySpin(long day) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_DAY, encryptString(day + ""));
        return editor.commit();
    }

//	public static int getDaySpin() {
//		Context context = MyApplication.getInstance().getApplicationContext();
//		SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
//		String number = decryptString(mShared.getString(PREFS_DAY, ""));
//		try {
//			return Integer.parseInt(number);
//		} catch (Exception e) {
//			return -1;
//		}
//	}
//
//	public static boolean editDaySpin(int day) {
//		Context context = MyApplication.getInstance().getApplicationContext();
//		Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
//		editor.putString(PREFS_DAY, encryptString(day + ""));
//		return editor.commit();
//	}

    /**
     * @description: get access token of user.
     * @param: No parameter
     * @exception: ClassCastException exception
     * @return: access token value.
     */
    public static String getToken() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getString(PREFS_TOKEN, "");
    }

    /**
     * @description: edit access token of user.
     * @param: String sAccessToken parameter
     * @exception: ClassCastException exception
     * @return: true if success otherwise return false value.
     */
    public static boolean editAccessToken(String sAccessToken) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_TOKEN, sAccessToken);
        return editor.commit();
    }

    /* Get clicked on image note status */
    public static boolean getNoted() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_NOTED, false);
    }

    public static boolean editNoted(boolean wasNoted) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_NOTED, wasNoted);
        return editor.commit();
    }

    /* Get clicked on image note status */
    public static boolean getIsFbLogged() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(LOGGED_FB, false);
    }

    public static boolean editIsFbLogged(boolean wasNoted) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(LOGGED_FB, wasNoted);
        return editor.commit();
    }

    /* Get clicked on image note status */
    public static boolean getIsBinhChon() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_IsBinhChon, false);
    }

    public static boolean editIsBinhChon(boolean wasNoted) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_IsBinhChon, wasNoted);
        return editor.commit();
    }

    /* Get clicked on image note status */
    public static boolean getFirstNgheSi() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_first_nghesi, false);
    }

    public static boolean editFirstNgheSi(boolean wasNoted) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_first_nghesi, wasNoted);
        return editor.commit();
    }

    /* Get clicked on image note status */
    public static boolean getFirstTime() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_FIRST_GET_TIME, false);
    }

    public static boolean editFirstTime(boolean wasNoted) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_FIRST_GET_TIME, wasNoted);
        return editor.commit();
    }

    /* Get clicked on image note status */
    public static boolean getSpinNow() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String spin = decryptString(mShared.getString(PREFS_FIRST_SPIN_NOW, ""));
        try {
            return Boolean.parseBoolean(spin);
        } catch (Exception e) {
            return false;
        }
    }

    /* Get clicked on image note status */
    // true : will not show ads
    public static boolean getNoadsSKU() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_NoAdsSKU, false);
    }

    public static boolean editNoadsSKU(boolean noads) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_NoAdsSKU, noads);
        return editor.commit();
    }

    public static boolean editSpinNow(boolean wasNoted) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();

        editor.putString(PREFS_FIRST_SPIN_NOW, encryptString(wasNoted + ""));

        return editor.commit();
    }

    /* Get clicked on image note status */
    public static boolean getIsVip() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String spin = decryptString(mShared.getString(PREFS_IS_VIP, ""));
        try {
            return Boolean.parseBoolean(spin);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean editIsVip(boolean isVip) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();

        editor.putString(PREFS_IS_VIP, encryptString(isVip + ""));

        return editor.commit();
    }

    public static boolean getFirstHint() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(prefsFirstHintGame, false);
    }

    public static boolean editFirstHint(boolean hint) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(prefsFirstHintGame, hint);
        return editor.commit();
    }

    public static boolean getFirstRating() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(prefsFirstRating, false);
    }

    public static boolean editFirstRating(boolean hint) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(prefsFirstRating, hint);
        return editor.commit();
    }

    public static boolean getMusic() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_MUSIC, true);
    }

    public static boolean editMusic(boolean wasNoted) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_MUSIC, wasNoted);
        return editor.commit();
    }

    public static boolean getSound() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_SOUND, true);
    }

    public static boolean editSound(boolean wasNoted) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_SOUND, wasNoted);
        return editor.commit();
    }

    /**
     * To get current number question
     */
    public static int getCurrentNumberQuestion() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);

        try {
            return Integer.parseInt(decryptString(mShared.getString(PREFS_CURRENT_NUMBER_QUESTION, "0")));
        } catch (Exception e) {
            return 0;
        }

    }

    /**
     * To set current number question
     *
     * @param number
     * @return
     */
    public static boolean editCurrentNumberQuestion(String number) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_CURRENT_NUMBER_QUESTION, encryptString(number));
        return editor.commit();
    }

    public static int getSpin() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);

        String preSpin = mShared.getString(PREFS_SPIN, "");
        if(TextUtils.isEmpty(preSpin)) return 9; // 10 spins for user first log into app
        String spin = decryptString(preSpin);
        try {
            return Integer.parseInt(spin);
        } catch (Exception e) {
            return -1 /*0*/;
        }
    }

    public static boolean editSpin(int number) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_SPIN, encryptString(number + ""));
        return editor.commit();
    }

    public static int getTiLeCao() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String spin = decryptString(mShared.getString(PREFS_TI_LE_CAO, ""));
        try {
            return Integer.parseInt(spin);
        } catch (Exception e) {
            return 10;
        }
    }

    public static boolean editTiLeCao(int tile) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(PREFS_TI_LE_CAO, encryptString(tile + ""));
        return editor.commit();
    }

    public static int getTimeSpinCountDown() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String time = decryptString(mShared.getString(PREFS_TIME_SPIN_COUNTDOWN, ""));
        try {
            return Integer.parseInt(time);
        } catch (Exception e) {
            return Define.TIME_COUNTDOWN_DEFAULT;
        }
    }

    public static boolean editTimeSpinCountDown(int number) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        // editor.putInt(PREFS_TIME_SPIN_COUNTDOWN, number);

        editor.putString(PREFS_TIME_SPIN_COUNTDOWN, encryptString(number + ""));

        return editor.commit();
    }

    /**
     * To get current coin
     */
    public static int getCoin() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String coin = decryptString(mShared.getString(PREFS_COIN, ""));
        try {
            return Integer.parseInt(coin);
        } catch (Exception e) {
            return 49;
        }

    }

    /**
     * To set coin
     *
     * @param number
     * @return
     */
    public static boolean editCoin(int number) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        // editor.putInt(PREFS_COIN, number);
        editor.putString(PREFS_COIN, encryptString(number + ""));
        return editor.commit();
    }

    /**
     * To get current coin
     */
    public static int getKC() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        String coin = decryptString(mShared.getString(PREFS_KC, ""));
        try {
            return Integer.parseInt(coin);
        } catch (Exception e) {
            return -1;
        }

    }

    /**
     * To set coin
     *
     * @param number
     * @return
     */
    public static boolean editKC(int number) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        // editor.putInt(PREFS_KC, number);
        editor.putString(PREFS_KC, encryptString(number + ""));
        return editor.commit();
    }

    public static int getTimeRunnalbeActionbar() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getInt(PREFS_TIME_RUNNABLE_ACTIONBAR, 20000);
    }

    public static boolean editTimeRunnalbeActionbar(int time) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putInt(PREFS_TIME_RUNNABLE_ACTIONBAR, time);
        return editor.commit();
    }

    public static int getTimeRunnalbeBackground() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getInt(PREFS_TIME_RUNNABLE_BACKGROUND, 10000);
    }

    public static boolean editTimeRunnalbeBackground(int time) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putInt(PREFS_TIME_RUNNABLE_BACKGROUND, time);
        return editor.commit();
    }

    /**
     * @description: get remember me flag.
     * @param: No parameter
     * @exception: ClassCastException exception
     * @return: flag remember me value.
     */
    public static boolean getRememberMe() {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences mShared = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return mShared.getBoolean(PREFS_REMEMBER, false);
    }

    /**
     * @description: edit remember me.
     * @param: IsRememberMe(boolean) parameter
     * @exception: Any exception
     * @return: true if success otherwise return false.
     */
    public static boolean editRememberMe(boolean rememberMe) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(PREFS_REMEMBER, rememberMe);
        return editor.commit();
    }

    /**
     * @description: get user name value.
     * @param: No parameter
     * @exception: ClassCastException exception
     * @return: user name value.
     */
    public static UserDetails getUserDetails() {
        try {
            return (UserDetails) getObject(PREFS_USER_DETAILS);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @description: to saving user details object
     * @param: No parameter
     * @exception: ClassCastException exception
     * @return: user name value.
     */
    public static boolean saveUserDetails(UserDetails userDetails) {
        try {
            return saveObject(PREFS_USER_DETAILS, userDetails);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean saveAchievements(Serializable dataObj) {
        try {
            return saveObject(PREFS_Achievements, dataObj);
        } catch (IOException e) {
            return false;
        }
    }

    public static Object getAchievements() {
        try {
            return getObject(PREFS_Achievements);
        } catch (IOException e) {
            return null;
        }
    } // -------- #End FaceBook ------------//

    public static ArrayList<PathInfo> getPaths() {
        try {
            return (ArrayList<PathInfo>) getObject(PREFS_PATHS);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static void editPaths(ArrayList<PathInfo> pathInfos) {
        try {
            saveObject(PREFS_PATHS, pathInfos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getListGiftCode() {
        try {
            return (ArrayList<String>) getObject(PREFS_list_GiftCode);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static void editListGiftCode(ArrayList<String> questions) {
        try {
            saveObject(PREFS_list_GiftCode, questions);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Questions> getListQuestions() {
        try {
            return (ArrayList<Questions>) getObject(PREFS_LIST_QUESTION);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static void editListQuestions(ArrayList<Questions> questions) {
        try {
            saveObject(PREFS_LIST_QUESTION, questions);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @description: save Object with object key.
     * @param: String objKey, Serializable dataObj parameter
     * @exception: Any exception
     * @return: true if success otherwise return false.
     * <p/>
     * NOTES: All elements of object must be implements Serializable
     * (ex: LatLng, Location,...) without simple type(ex: int, string,
     * double,...)
     */
    public static boolean saveObject(String objKey, Serializable dataObj) throws IOException {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(objKey, ObjectSerializer.serialize(dataObj));
        return editor.commit();
    }

    /**
     * @description: get Object with object key.
     * @param: String objKey parameter
     * @exception: ClassCastException exception
     * @return: Object data.
     */
    public static Object getObject(String objKey) throws IOException {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences savedSession = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        Object dataObj = null;
        try {
            dataObj = (Object) ObjectSerializer.deserialize(savedSession.getString(objKey, null));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return dataObj;
    }

    public static boolean saveMainDot(CurrentMainDot currentMainDot) {
        try {
            return saveObject(PREFS_MAIN_DOT, currentMainDot);
        } catch (IOException e) {
            return false;
        }
    }

    public static Object getMainDot() {
        try {
            return getObject(PREFS_MAIN_DOT);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * saveString
     *
     * @param key
     * @param val
     */
    public static boolean saveString(String key, String val) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(key, val);
        return editor.commit();
    }

    /**
     * getString
     *
     * @param key
     * @param defValue
     * @return
     */
    public static String getString(String key, String defValue) {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences savedSession = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return savedSession.getString(key, defValue);
    }

    /**
     * saveString
     *
     * @param key
     * @param val
     */
    public static boolean saveBoolean(String key, boolean val) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(key, val);
        return editor.commit();
    }

    /**
     * getString
     *
     * @param key
     * @param defValue
     * @return
     */
    public static boolean getBoolean(String key, boolean defValue) {
        Context context = MyApplication.getInstance().getApplicationContext();
        SharedPreferences savedSession = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
        return savedSession.getBoolean(key, defValue);
    }

    /**
     * @description: clear Object with object key.
     * @param: String objKey parameter
     * @exception: Any exception
     * @return: No return value.
     */
    public static void clear(String objKey) {
        Context context = MyApplication.getInstance().getApplicationContext();
        Editor editor = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.remove(objKey);
        editor.commit();
    }

    // --------------- FaceBook SharedPreferences ---------- //

    /**
     * To save fb user object
     *
     * @param dataObj
     * @return
     */
    public static boolean saveFbUserObject(Serializable dataObj) {
        try {
            return saveObject(PREFS_FB_USER_OBJECT, dataObj);
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * TO get fb user object
     *
     * @return
     */
    public static Object getFbUserObject() {
        try {
            return getObject(PREFS_FB_USER_OBJECT);
        } catch (IOException e) {
            return null;
        }
    } // -------- #End FaceBook ------------//

}
