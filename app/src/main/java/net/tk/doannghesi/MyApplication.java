package net.tk.doannghesi;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.util.Base64;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import net.tk.doannghesi.api.SavedStore;
import net.tk.doannghesi.tools.log.Logger;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;

public class MyApplication extends Application {

	private static MyApplication _mInstance;

	public static MyApplication getInstance() {
		return _mInstance;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		_mInstance = this;


		// Init Parse
		//Parse.initialize(this, "UbiASaKJguJ14Dgo2ny9yw2PHzmwu54SEV7bKn3A", "ub8PWTuXM5NJ1N15LJIODC5mDMRT54zRcc7rN7QU");

		initImageLoader(_mInstance);
		getKeyHash();
		//Logger.d("AndroidID:" + Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID));


		if (!SavedStore.getFirstTime()) {
			SavedStore.editFirstTime(true);
			SavedStore.editTimeFirst(System.currentTimeMillis());

			Calendar c = Calendar.getInstance();
			int h = c.get(Calendar.HOUR);
			int m = c.get(Calendar.MINUTE);

			// at 7h00
			int t_h = 6 - h;
			int t_m = 60 - m;

			if (h >= 7)
				t_h = 18 - h;

			int ms = (t_h * 3600 + t_m * 60) * 1000;
			SavedStore.editTimeSpinCountDown(ms);

			Logger.d("h:" + h + " -Min:" + m);
		}
	}

	private void getKeyHash() {
		try {
			Logger.e("package name: " + getPackageName());
			PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Logger.e("KeyHash:" + Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}
	}

	private void initImageLoader(Context context) {
		// Initialize Universal Image Loader
		// Create global configuration and initialize ImageLoader with this
		// configuration
		DisplayImageOptions options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.ic_avatar_default) // resource or drawable
		 .showImageForEmptyUri(R.drawable.ic_avatar_default) // resource or drawable
		 .showImageOnFail(R.drawable.ic_avatar_default) // resource or drawable
		// .resetViewBeforeLoading(true) // default
		// .delayBeforeLoading(1000)
				.cacheInMemory(true) // default
				.cacheOnDisc(true) // default
				// .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2) //
				// default
				// .bitmapConfig(Bitmap.Config.ARGB_8888) // default
				// .decodingOptions(...)
				.displayer(new SimpleBitmapDisplayer()) // default
														// SimpleBitmapDisplayer,RoundedBitmapDisplayer(10),FadeInBitmapDisplayer
				// .handler(new Handler()) // default
				.build();

		File cacheDir = StorageUtils.getCacheDirectory(context);
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.memoryCacheExtraOptions(480, 800)
				// default = device screen dimensions
				//.discCacheExtraOptions(480, 800, CompressFormat.PNG, 100, null)
				.threadPoolSize(10)
				// default
				.threadPriority(Thread.NORM_PRIORITY - 1)
				// default
				.denyCacheImageMultipleSizesInMemory()
				//.discCache(new UnlimitedDiscCache(cacheDir))
				// default
				.discCacheFileNameGenerator(new Md5FileNameGenerator()).tasksProcessingOrder(QueueProcessingType.LIFO)
				.memoryCache(new LruMemoryCache(2 * 1024 * 1024)).memoryCacheSize(2 * 1024 * 1024)
				.memoryCacheSizePercentage(13)
				// default
				.discCacheSize(50 * 1024 * 1024).discCacheFileCount(100)
				.imageDownloader(new BaseImageDownloader(context)).defaultDisplayImageOptions(options) // default
				// .writeDebugLogs() // Remove for release app
				.build();

		// Initialize ImageLoader with configuration.
		ImageLoader.getInstance().init(config);
	}

}
