package net.tk.doannghesi;

public class Define {

    //public static final int START_QUESTION = 0;
    public static final int TILE_CAO = 10;
    public static final int TILE_CAO_CHEAT = 2;

    /* The coin lost in each opening one word*/
    public static final int COIN_NOTE = 5;
    public static final int COIN_CAOLAI = 5;
    public static final int COIN_HUONGDAN = 60;
    public static final int COIN_NEXT_QUESTION = 100;

    public static final int TIME_COUNTDOWN_DEFAULT = 43200000;// ms = 12hs

    public static final int RC_SIGN_IN = 9001;

    public static final String app_id = "8838007150108660";
    //public static final String app_id ="8838007150108661";// "1738007150108276";

    public static final String NgheSi = "NgheSi";
    public static final String NgheSiPlayer = "NgheSiPlayer";


    public static final int THUTHACH_SAMEPIC = 1;
    public static final int THUTHACH_MEMORIES = 2;
    public static final int THUTHACH_BOOMDOT = 3;
    public static final int THUTHACH_DIFFRENCECOLOR = 4;
    public static final int THUTHACH_QUICKMOVING = 5;
    public static final int THUTHACH_TROLLERTAP = 6;

    public static final String ROOT_URL = "https://guess-artist.firebaseio.com/";

    public enum TYPE {
        ARTIST, ANIMALS, LIFE
    }


    public static Define.TYPE mCurType = Define.TYPE.ARTIST;// TODO edit later

}
